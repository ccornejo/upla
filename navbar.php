<!-- Menu lateral-->
<div class="panel-group" id="panel-1">
	
	<!-- Info-->
	<div class="panel panel-info">
		<div class="panel-heading">
			 <a class="panel-title" href="/proyecto_upla/info.php">Sobre el Catálogo</a>
		</div>
	</div>
	<!-- /Info-->



	<!-- Menú publicaciones-->
	<div class="panel panel-pub">
		<div class="panel-heading">
			 <a id="publicaciones" class="panel-title" data-toggle="collapse" data-parent="#panel-1" href="#panel-element-1">Producción de Conocimiento</a>
		</div>
		<div id="panel-element-1" class="panel-collapse collapse">
				<div id="tri-nav-pub"class="tri-nav"></div>
				<div class="panel panel-pub">
					<div class="panel-heading sub-panel">
						<a class="panel-title" href="/proyecto_upla/upla/visualizacion/1/"> Buscador </a>
					</div>
					<div class="panel-heading sub-panel">
						<a class="panel-title" href="/proyecto_upla/upla/visualizacion/5/"> Producción Histórica </a>
					</div>

					<div class="panel-heading sub-panel">
						 <a class="panel-title" href="/proyecto_upla/upla_alpha/search.php">Red de Coautoría</a>
					</div>
					<!--
					<div id="sub-panel-element-12" class="panel-collapse collapse">		
						<div id="tri-nav-sub"class="tri-nav"></div>
						<div class="panel-body">
							<a class="panel-link-pub" href="/proyecto_upla/upla_alpha/search.php"> Red de Coautoría </a>
						</div>
						<div class="panel-body">
							Chat
						</div>
						<div class="panel-body">
							e-mail
						</div>
					</div>
					-->
				</div>
				<!--
				<div class="panel panel-pub">
					<div class="panel-heading sub-panel">
						 <a class="panel-title" href="">Indicadores</a>
					</div>
				</div>
				-->
		</div>
	</div> <!-- /menu publicaciones -->

	<!-- Menú proyectos-->
	<div class="panel panel-proy">
		<div class="panel-heading">
			 <a class="panel-title" data-toggle="collapse" data-parent="#panel-1" href="#panel-element-3">Gestión del conocimiento y la innovación</a>
		</div>
		<div id="panel-element-3" class="panel-collapse collapse">
			<div id="tri-nav-proy"class="tri-nav"></div>
			<div class="panel panel-proy">
				<div class="panel-heading sub-panel">
					<!--<a class="panel-title" data-toggle="collapse" data-parent="#panel-3.1" href="#sub-panel-element-33">Buscador</a>-->
					<a class="panel-title" href="/proyecto_upla/upla/visualizacion/1/index3.php">Buscador</a>
				</div>
				<div class="panel-heading sub-panel">
					<!--<a class="panel-title" data-toggle="collapse" data-parent="#panel-3.1" href="#sub-panel-element-33">Producción</a>-->
					<a class="panel-title" href="/proyecto_upla/upla/visualizacion/5/index3.php">Producción</a>
				</div>
				<!--
				<div id="sub-panel-element-33" class="panel-collapse collapse">
					<div id="tri-nav-sub"class="tri-nav"></div>
					<div class="panel-body">
						<a class="panel-link-proy" href="">Buscador</a>
					</div>
					<div class="panel-body">
						<a class="panel-link-proy" href="/proyecto_upla/upla/visualizacion/5/index3.php">Producción</a> 
					</div>
				</div>-->
			</div>	
		</div> 
	</div> <!-- /menu proyectos -->
	<!-- Menú congresos-->	
	<div class="panel panel-con">
		<div class="panel-heading">
			 <a class="panel-title" data-toggle="collapse" data-parent="#panel-1" href="#panel-element-4" >Difusión del conocimiento</a>
		</div>
		<div id="panel-element-4" class="panel-collapse collapse">
			<div id="tri-nav-con"class="tri-nav"></div>
			<div class="panel panel-con ">
				<div class="panel-heading sub-panel">
					<!--<a class="panel-title" data-toggle="collapse" data-parent="#panel-2.1" href="#sub-panel-element-44">Facultades</a>-->
					<a class="panel-title" href="/proyecto_upla/upla/visualizacion/1/index2.php">Buscador</a>
				</div>
				<div class="panel-heading sub-panel">
					<a class="panel-title" href="/proyecto_upla/upla/visualizacion/5/index2.php">Difusión histórica </a>
				</div>
				<!--<div id="sub-panel-element-44" class="panel-collapse collapse">
					<div id="tri-nav-sub"class="tri-nav"></div>
					<div class="panel-body">
						<a class="panel-link-con" href="/proyecto_upla/upla/visualizacion/1/index2.php">Buscador</a>
					</div>
					<div class="panel-body">
						<a class="panel-link-con" href="/proyecto_upla/upla/visualizacion/5/index2.php"> Producción</a>
					</div>
				</div>-->
			</div>	
		</div>
	</div> <!-- /menu congresos -->
	<!-- Consulta-->
	<div class="panel panel-consulta">
		<div class="panel-heading">
			 <a class="panel-title" href="/proyecto_upla/upla2/datos/consulta.php">Consulta</a>
		</div>
	</div>

	<!-- Menú Personal-->
	<div class="panel panel-personal">
		<div class="panel-heading">
			 <a class="panel-title" href="/proyecto_upla/upla/visualizacion/6/">Personal Upla</a>
		</div>
	</div>
	<!-- /Menú Personal-->



</div>
