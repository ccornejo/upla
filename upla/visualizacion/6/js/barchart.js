function barchart(input,type){
	$("#legend").empty()
	var margin = {top: 20, right: 100, bottom: 30, left: 40},
		width = 960 - margin.left - margin.right,
		height = 500 - margin.top - margin.bottom;

	var x = d3.scale.ordinal()
		.rangeRoundBands([0, width], .1);

	var y = d3.scale.linear()
		.rangeRound([height, 0]);

	var color = d3.scale.ordinal()
		.range(["#E41A1C","#377EB8", "#4DAF4A", "#984EA3", "#FF7F00", "#A65628", "#999999"]);

	var xAxis = d3.svg.axis()
		.scale(x)
		.orient("bottom");

	var yAxis = d3.svg.axis()
		.scale(y)
		.orient("left")
		.ticks(10);
		//~ .tickFormat(d3.format(".0%"));

	var svg = d3.select("#barchart").append("svg")
		.attr("width", width + margin.left + margin.right)
		.attr("height", 225+height + margin.top + margin.bottom)
	  .append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
	  data = d3.tsv.parse(input)
	

	  color.domain(d3.keys(data[0]).filter(function(key) { return key !== "Faculty"; }));

	  data.forEach(function(d) {
		var y0 = 0;
		d.ages = color.domain().map(function(name) { return {name: name, y0: y0, y1: y0 += +d[name]}; });
		if (type == 0)
			d.ages.forEach(function(d) { d.y0 /= y0; d.y1 /= y0; });
	  });
	  //~ if (type == 0)
		//~ data.sort(function(a, b) { return b.ages[1].y1 - a.ages[1].y1; });
	  //~ if (type == 1)
		//~ data.sort(function(a, b) { return b.ages[1].y1 - a.ages[1].y1; });

	  x.domain(data.map(function(d) { return d.Faculty; }));
	  if (type == 1)
		y.domain([0, d3.max(data, function(d) { return d.ages[1].y1; })]);
	  svg.append("g")
		  .attr("class", "x axis")
		  .attr("transform", "translate(0," + height + ")")
		  .call(xAxis)
		  .selectAll("text")  
            .style("text-anchor", "end")
            .attr("dx", "-.8em")
            .attr("dy", ".15em")
            .attr("transform", "rotate(-75)" );

	  svg.append("g")
		  .attr("class", "y axis")
		  .call(yAxis);

	  var state = svg.selectAll(".state")
		  .data(data)
		.enter().append("g")
		  .attr("class", "state")
		  .attr("transform", function(d) { return "translate(" + x(d.Faculty) + ",0)"; });

	  state.selectAll("rect")
		  .data(function(d) { return d.ages; })
		.enter().append("rect")
		  .attr("width", x.rangeBand())
		  .attr("y", function(d) { return y(d.y1); })
		  .attr("height", function(d) { return y(d.y0) - y(d.y1); })
		  .style("fill", function(d) { 
			  return color(d.name); 
		   });
	  
	 
	  
	  var legend = svg.select(".state:last-child").selectAll(".legend")
		  .data(function(d) { return d.ages; })
		.enter().append("g")
		  .attr("class", function(d){
				$("#legend").append("<tr><td><div style='cursor:pointer;width:10px;height:10px;background-color:"+color(d.name)+";display:inline-block;margin-left:3px;margin-right:10px;'></div></td><td>"+d.name+"</td></tr>")
				
				return "legend"
		   });

	  //~ legend.append("line")
		  //~ .attr("x2", 10);

	  //~ legend.append("text")
		  //~ .attr("x", 13)
		  //~ .attr("dy", ".35em")
		  //~ .text(function(d) { console.log(d.name,color(d.name));return d.name; });

	
}
