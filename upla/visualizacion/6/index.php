<html>
<head>
	<title>Personal</title>
	<link rel="shortcut icon" href="http://www.upla.cl/inclusion/wp-content/uploads/2013/05/favicon.ico" type="image/x-icon">
	<script src="/proyecto_upla/upla/visualizacion/5/js/jquery.min.js"></script>
	<link rel="stylesheet" href="/proyecto_upla/upla/visualizacion/1/css/jquery-ui.css" />
	<script src="/proyecto_upla/upla/visualizacion/6/js/bootstrap.min.js"></script>	
	<link href="/proyecto_upla/upla/visualizacion/6/css/bootstrap.min.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
	<script src="//d3js.org/d3.v3.min.js"></script>
	<script src="js/barchart.js"></script>
	<link rel="stylesheet" type="text/css" href="/proyecto_upla/css/catalogo.css"/>

</head>
<body>

	<!-- titulo -->
	<?php include '../../../header.php'; ?>

	<!-- Content -->
	<div class="container-fluid">
	<div class="row">
	
		<div class="col-md-2">
			<?php include '../../../navbar.php'; ?>			
		</div> <!-- /col-md-4 -->		
	
		<!-- Contenido --> 
		<div class="col-md-10">
			<!-- barra ubicacion breadcrumb -->
			<?php include '../../../breadcrumb.php'; ?>

			<!-- Modal -->
			<button type="button" class="btn btn-primary btn-lg glyphicon glyphicon-info-sign" data-toggle="modal" data-target="#myModal"></button>
			<div id="myModal" class="modal fade" role="dialog">
			  <div class="modal-dialog">
			    <!-- Modal content-->
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
			     		<h4 class="modal-title">
			     			<!--Título--> 
			     			Personal
			     			<!--Título--> 
			     		</h4>
			      </div>
			      <div class="modal-body">
			        <p>
			        <!--Texto--> 
			        	La herramienta muestra la cantidad o proporción (se puede escoger la opción) de distintos atributos de personas vinculadas a las Facultades y otros departamentos de la Universidad de Playa Ancha. Los atributos que pueden ser visualizados son género, tipo de contrato y grado académico.
			        <!--Texto--> 
			        </p>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			      </div>
			    </div>
			  </div>
			</div>
			<!-- /Modal -->

			<br>
			<br>
			<div id="data" style="visibility:hidden;display:none;"><? echo json_encode($DATA); ?></div>
			<input type="radio" name="g1" value="0" checked> Porcentual<br>
			<input type="radio" name="g1" value="1"> Absolutos<br>
			<select id="faculty">
				<option value=1>Género</option>
				<option value=2>Tipo de Contrato</option>
				<option value=3>Grado Académico</option>
			</select>
			<div id="legend"></div>
			<div id="barchart"></div>

		</div> <!-- /col-md-10 -->		

	</div><!-- /row -->
	</div><!-- /conteriner -->	
	
	<br>
	<br>
	<!-- footer -->
	<?php include '../../../footer.php'; ?>
	  

	<script>
	
	$(document).ready(function(){

		$('#tri-inicio').css('border-left-color','#004462');
	 	$('#barra-inicio').css('background-color','#004462');
	 	$('#barra-inicio').css('color','#fff');
	 	$('#tri-inicio').css('background-color','#002332');
	 	$('#tri1').css('border-left-color','#002332');
	 	$('#barra1').html('Personal')
	 	$('#barra1').css('background-color','#002332');
	 	$('#barra1').css('color','#fff');
	 	$('#tri1').css('background-color','#fff');

	 	$('.tri#tri2').css('display','none');
	 	$('#barra2').css('display','none')
	 	$('.tri#tri3').css('display','none');
	 	$('#barra3').css('display','none')



		$.post("webservice.php",{type:1},function(data){barchart(data,0)})
		$("#faculty").on("change",function(){
			$("#barchart").empty()
			$.post("webservice.php",{type:$(this).val()},function(data){barchart(data,$('input[type=radio]:checked').val())})
		})
		$("input[type=radio]").on("change",function(){
			$("#barchart").empty()
			$.post("webservice.php",{type:$("#faculty").val()},function(data){barchart(data,$('input[type=radio]:checked').val())})
		})
	});
	
	</script>
	
</body>
</html>
