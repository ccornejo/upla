<!DOCTYPE html>
<head>
	<meta charset="utf-8">

	<link href="/proyecto_upla/upla/visualizacion/5/nv/src/nv.d3.css" rel="stylesheet" type="text/css">
	<title>Productividad en el Tiempo</title>
	<link rel="shortcut icon" href="http://www.upla.cl/inclusion/wp-content/uploads/2013/05/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="/proyecto_upla/upla/visualizacion/5/css/style.css"/>
	<link rel="stylesheet" type="text/css" href="/proyecto_upla/upla/visualizacion/css/style.css"/>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="../../../css/catalogo.css" rel="stylesheet">

</head>
<body class='with-3d-shadow with-transitions'>

		<!-- titulo -->
		<?php include '../../../header.php';?>
		
		<!-- Content -->
		<div class="container-fluid">
		<div class="row">
		
			<div class="col-md-2">
				<?php include '../../../navbar.php';?>			
			</div> <!-- /col-md-4 -->
		
			<!-- Contenido --> 
			<div class="col-md-10">
				<!-- barra ubicacion breadcrumb -->
				<?php include '../../../breadcrumb.php'; ?>			

				<!-- Modal -->
				<button type="button" class="btn btn-primary btn-lg glyphicon glyphicon-info-sign" data-toggle="modal" data-target="#myModal"></button>
				<div id="myModal" class="modal fade" role="dialog">
				  <div class="modal-dialog">
				    <!-- Modal content-->
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal">&times;</button>
				     		<h4 class="modal-title">
				     			<!--Título--> 
				     			Producción Histórica
				     			<!--Título--> 
				     		</h4>
				      </div>
				      <div class="modal-body">
				        <p>
				        <!--Texto--> 
				        	La herramienta muestra la producción (número de publicaciones o su aporte porcentual) por Facultad o Centro de la Universidad de Playa Ancha, desde el año 2010 al 2015. Interactúa con este gráfico, que además es exportable,  para ver el aporte de cada Facultad o Centro pinchando el área de color en el gráfico (para aislar la Facultad o Centro) o pinchando en el nombre de la Facultad o Centro en el listado de nombres (para “apagarlo” en el gráfico).
				        <!--Texto--> 
				        </p>
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				      </div>
				    </div>
				  </div>
				</div>
				<!-- /Modal -->

				<br>
				<br>
				<br>
				<div id="contenido">
					<div id="main">		
						  <div id="opt">
								<input class="nv-legend-text" type="radio" name="opt" value="1" checked>Publicaciones 
					<!--
								<input class="nv-legend-text" type="radio" name="opt" value="2">Congresos 
								<input class="nv-legend-text" type="radio" name="opt" value="3">Patentes  
					-->
						  </div>
						  <label for="opt">Tipo de Datos: &nbsp;</label><br>
						  <div id="graph">
							<svg id="chart1"></svg>
						  </div>
						  <span id="network_extra">
							<span id="tosvg" title="Descarga la red en formato SVG"><a href="javascript:(function () { var e = document.createElement('script'); if (window.location.protocol === 'https:') { e.setAttribute('src', 'https://rawgit.com/NYTimes/svg-crowbar/gh-pages/svg-crowbar.js'); } else { e.setAttribute('src', 'http://nytimes.github.com/svg-crowbar/svg-crowbar.js'); } e.setAttribute('class', 'svg-crowbar'); document.body.appendChild(e); })();"><img src="css/icons/download.png"></a></span>
							</span>
					  </div>
					 </div>
					</div>
											
	
		</div><!-- /row -->
		</div><!-- /conteriner -->	
			
		<!-- footer -->
		<?php include '../../../footer.php'; ?>


<script src="/proyecto_upla/upla/visualizacion/5/js/jquery.min.js"></script>
<script src="/proyecto_upla/upla/visualizacion/5/nv/lib/d3.v3.js"></script>
<script src="/proyecto_upla/upla/visualizacion/5/nv/nv.d3.js"></script>
<script src="/proyecto_upla/upla/visualizacion/5/nv/src/utils.js"></script>
<script src="/proyecto_upla/upla/visualizacion/5/nv/src/models/axis.js"></script>
<script src="/proyecto_upla/upla/visualizacion/5/nv/src/tooltip.js"></script>
<script src="/proyecto_upla/upla/visualizacion/5/nv/src/interactiveLayer.js"></script>
<script src="/proyecto_upla/upla/visualizacion/5/nv/src/models/legend.js"></script>
<script src="/proyecto_upla/upla/visualizacion/5/nv/src/models/axis.js"></script>
<script src="/proyecto_upla/upla/visualizacion/5/nv/src/models/scatter.js"></script>
<script src="/proyecto_upla/upla/visualizacion/5/nv/src/models/stackedArea.js"></script>
<script src="/proyecto_upla/upla/visualizacion/5/nv/src/models/stackedAreaChart.js"></script>
<script src="/proyecto_upla/upla/visualizacion/js/svg.js"></script>
<script src="../../../js/bootstrap.min.js"></script>


<script>
$(document).ready(function(){
	$('#panel-element-1').addClass("in");
  $('#sub-panel-element-11').addClass("in");
 
 	$('#tri-inicio').css('border-left-color','#2D2D2D');
 	$('#barra-inicio').css('background-color','#2D2D2D');
 	$('#barra-inicio').css('color','#fff');
 	$('#tri-inicio').css('background-color','#004462');
  $('#barra1').html('Producción de Conocimiento');
 	$('#tri1').css('border-left-color','#004462');
 	$('#barra1').css('background-color','#004462');
 	$('#barra1').css('color','#fff');
 	$('#tri1').css('background-color','#668EA0');
 	$('#tri2').css('border-left-color','#668EA0');
  $('#barra2').html('Producción Histórica');
 	$('#barra2').css('background-color','#668EA0');
 	$('#barra2').css('color','#fff');
 	//$('#tri2').css('background-color','#DDDDDD');
	//$('#tri3').css('border-left-color','#DDDDDD');
  //$('#barra3').html('Producción Histórica');
  //$('#barra3').css('background-color','#DDDDDD');
  //$('#barra3').css('color','#818181');
  $('#tri3').css('visibility','hidden');

	$('#tosvg a').click(function(){
		//~ $('#tosvg a').attr("download",$("#key").text()+".svg")
		//~ .attr("href",getSVG());
	});
})	
function foo(opt) {
    var jqXHR = $.ajax({
        url : "/proyecto_upla/upla/visualizacion/5/webservice.php",
        async: false
    });
    //~ console.log(jqXHR.responseText)
	
    return jqXHR.responseText;
}

function stacked(opt){
	
	var histcatexplong = JSON.parse(foo(opt));
	console.log(histcatexplong)
	var colors = d3.scale.category20();
	keyColor = function(d, i) {return colors(d.key)};

	var chart;
	nv.addGraph(function() {
	  chart = nv.models.stackedAreaChart()
				   // .width(600).height(500)
					.useInteractiveGuideline(true)
					.x(function(d) { return d[0] })
					.y(function(d) { return d[1] })
					.color(keyColor)
					.transitionDuration(300);
					//.clipEdge(true);
	//~ console.log(nv)
	// chart.stacked.scatter.clipVoronoi(false);

	  chart.xAxis
		  .tickFormat(function(d) { if (d/d.toFixed(0) == 1) return d3.time.format("%Y")(new Date(d,0)) });

	  chart.yAxis
		  .tickFormat(d3.format(',.1f'));

	  d3.select('#chart1')
		.datum(histcatexplong)
		.transition().duration(1000)
		.call(chart)
		// .transition().duration(0)
		.each('start', function() {
			setTimeout(function() {
				d3.selectAll('#chart1 *').each(function() {
				  //~ console.log('start',this.__transition__, this)
				  // while(this.__transition__)
				  if(this.__transition__)
					this.__transition__.duration = 1;
				})
			  }, 0)
		  })
		// .each('end', function() {
		//         d3.selectAll('#chart1 *').each(function() {
		//           console.log('end', this.__transition__, this)
		//           // while(this.__transition__)
		//           if(this.__transition__)
		//             this.__transition__.duration = 1;
		//         })});

	  nv.utils.windowResize(chart.update);

	  // chart.dispatch.on('stateChange', function(e) { nv.log('New State:', JSON.stringify(e)); });

	  return chart;
	});
}


$(document).ready(function(){

	stacked(1)
	
	
	$("#opt input[type=radio]").click(function(){
		if($('input:radio[type=radio]:checked').val() == "1"){
			$("#chart1").remove()
			$("#graph").append('<svg id="chart1"></svg>')
			stacked(1)
		}
		if($('input:radio[type=radio]:checked').val() == "2"){
			$("#chart1").remove()
			$("#graph").append('<svg id="chart1"></svg>')
			stacked(2)
		}
		if($('input:radio[type=radio]:checked').val() == "3"){
			$("#chart1").remove()
			$("#graph").append('<svg id="chart1"></svg>')
			stacked(3)
		}
	});
})



</script>
</body>
</html>
