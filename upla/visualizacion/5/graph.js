function onSuccess(data){
    $("#notification").html(data);
}

function graph(jvalue,orden,univ,flag,flag2,pag){
	jvalueG = jvalue
	if(orden == "Citas"){ 
		$("#citas").css("color","red").css("font-weight","bold")
		$("#publicaciones").css("color","grey").css("font-weight","normal")
	}else{
		$("#publicaciones").css("color","red").css("font-weight","bold")
		$("#citas").css("color","grey").css("font-weight","normal")
	}
	
	function truncate(str, maxLength, suffix) {
		if(str.length > maxLength) {
			str = str.substring(0, maxLength + 1); 
			str = str.substring(0, Math.min(str.length, str.lastIndexOf(" ")));
			str = str + suffix;
		}
		return str;
	}

	var margin = {top: 20, right: 700, bottom: 0, left: 20},
		width = 230,
		height = 650;
	year = new Date().getFullYear()
	var start_year = year-7,
		end_year = year-1;

	//var c = d3.scale.category10();
        
        var c = d3.scale.linear().domain([1,20]).range(['red', 'blue']);

	var x = d3.scale.linear()
		.range([0, width]);

	var xAxis = d3.svg.axis()
		.scale(x)
		.orient("top");

	var formatYears = d3.format("0000");
	xAxis.ticks(7).tickFormat(formatYears);
	$("svg").remove();
	var svg = d3.select("#graph").append("svg")
		.attr("width", width + margin.left + margin.right)
		.attr("height", height + margin.top + margin.bottom)
		.style("margin-left", margin.left + "px")
		.append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
	
	d3.json(jvalue, function(data) {
		//~ console.log(data)
		data_aux = data
		
		if (orden == "Citas") data = data.sort(function(a,b){return b.totalcitas - a.totalcitas;});
		if (orden == "Publicaciones") data = data.sort(function(a,b){return b.totalpaper - a.totalpaper;});
		csv = JSON2CSV(data,flag,flag2,univ)
		//~ console.log(csv)
		if (flag == 1 && univ != "Todas"){
			data_aux2 = objArray3[univ]
			data_aux = []
			for(i in data_aux2)
				data_aux.push(data_aux2[i])
				
			//~ console.log(data_aux)
		}
		if (flag2 == 1){
			univ = $("#combobox3").val()
			a = $(".token-input-list li p").text()
			institution_val = $('.custom-combobox-input').focus().val();
			if (institution_val == '')
				institution_val = "Todas"
			if(univ == "Todas")
				$("#searchRank").text("Lugar "+ search(objArray1,a) + " de " + Object.keys(objArray1).length + " en la disciplina " + $("#combobox").find('option:selected').text() + ", subdisciplina " + $("#combobox2").find('option:selected').text() + " en " + institution_val)
			else
				$("#searchRank").text("Lugar "+ search(objArray3[univ],a) + " de " + Object.keys(objArray3[univ]).length + " en la disciplina " + $("#combobox").find('option:selected').text() + ", subdisciplina " + $("#combobox2").find('option:selected').text() + " en " + institution_val )
		}	
		var citas_max = d3.max(data, function(d) { return +d.totalcitas;});
		var paper_max = d3.max(data, function(d) { return +d.totalpaper;});
		x.domain([start_year, end_year+1]);
		
		var xScale = d3.scale.linear()
			.domain([start_year, end_year+1])
			.range([0, width]);
		
		svg.append("g")
			.attr("class", "x axis")
			.attr("transform", "translate(0," + 0 + ")")
			.call(xAxis);
		svg.append("g")
			.attr("class", "x axis")
			.attr("transform", "translate(620," + 0 + ")")
			.call(xAxis);
		len = 30
		
		a = $(".token-input-list li p").text()
		if (flag2 == 1)
			aux_j = search(data_aux,a)
		k = 0
		l = 0
		//~ if ($("#pag").text() == "")
			//~ pag = 1
		//~ else
			//~ pag = parseInt($("#pag").text())
		j = len*(pag-1)
		maxp = Math.floor(data_aux.length/30)

		html = ""
		p = pag - 5
		if (p < 1)
			p = 1
		pi = 0
		while (true){
			if (pi == 0 && p > 1)
				html += "<a href='javascript:graph(jvalueG,&#39"+orden+"&#39,&#39"+univ+"&#39,"+flag+","+flag2+","+p+");'><< <a>"
			//~ console.log(jvalue)
			if (p == pag)
				html += "<a href='javascript:graph(jvalueG,&#39"+orden+"&#39,&#39"+univ+"&#39,"+flag+","+flag2+","+p+");' style='font-weight:bold;'>"+p+" <a>"
			else
				html += "<a href='javascript:graph(jvalueG,&#39"+orden+"&#39,&#39"+univ+"&#39,"+flag+","+flag2+","+p+");'>"+p+" <a>"
			if (pi == 11 && p < maxp)
				html += "<a href='javascript:graph(jvalueG,&#39"+orden+"&#39,&#39"+univ+"&#39,"+flag+","+flag2+","+p+");'>>><a>"
			p++
			pi++
			if (pi == 12 || pi >= maxp-1)
				break
		}
		//~ console.log(data_aux)
		$("#paginator").html(html)	
		while (true && flag2 == 0){
			//~ console.log(data_aux[j])
			var g1 = svg.append("g").attr("class","journal").attr("id",data_aux[j]['id']);
			var g = g1.append("g").attr("class","citation");
			var g2 = g1.append("g").attr("class","papers");
			var circles = g.selectAll("circle")
				.data(data_aux[j]['citation'])
				.enter()
				.append("circle");				

			var text = g.selectAll("text")
				.data(data_aux[j]['citation'])
				.enter()
				.append("text");
	
			var circles2 = g2.selectAll("circle")
				.data(data_aux[j]['papers'])
				.enter()
				.append("circle");				

			var text2 = g2.selectAll("text")
				.data(data_aux[j]['papers'])
				.enter()
				.append("text");
			
			var rScale = d3.scale.linear()
				.domain([0, citas_max])
				.range([1, 9]);
			var rScale2 = d3.scale.linear()
				.domain([0, paper_max])
				.range([1, 9]);
				

			circles
				.attr("cx", function(d, i) { return xScale(d[0]); })
				.attr("cy", k*20+20)
				.attr("r", function(d) { return rScale(d[1]); })
				.attr("id","graph_info")
				.on("click",function(d){
					aid = $(this).parent().parent().attr('id')
					py = d[0]
					$.ajax({
					  url : 'webservice.php?id='+aid+'&year='+py,
					  success : function(result){  
						onSuccess(result)
						$( "#notification" ).dialog({
						  minWidth: 1000,
						  closeText: "",
						  title: "Publicaciones Registradas"
						  
						}).prev(".ui-dialog-titlebar").css("background","#e8641c").css("color","white");
					  }
					 })
				})
				.style("fill", function(d) { return c(j); });
			
			text
				.attr("y", k*20+25)
				.attr("x",function(d, i) { return xScale(d[0])-5; })
				.attr("class","value")
				.attr("id","graph_info")
				.attr("font-size","13px !important")
				.on("click",function(d){
					aid = $(this).parent().parent().attr('id')
					py = d[0]
					$.ajax({
					  url : 'webservice.php?id='+aid+'&year='+py,
					  success : function(result){  
						onSuccess(result)
						$( "#notification" ).dialog({
						  minWidth: 1000,
						  closeText: "",
						  title: "Publicaciones Registradas"
						  
						}).prev(".ui-dialog-titlebar").css("background","#e8641c").css("color","white");

					  }
					 })
				})
				.text(function(d){ return d[1]; })
				.style("fill", function(d) { return c(j); })
				.style("display","none");
			circles2
				.attr("cx", function(d, i) { return 2.5*250+xScale(d[0]); })
				.attr("cy", k*20+20)
				.attr("id","graph_info")
				.attr("r", function(d) { return rScale2(d[1]); })
				.on("click",function(d){
					aid = $(this).parent().parent().attr('id')
					py = d[0]
					$.ajax({
					  url : 'webservice.php?id='+aid+'&year='+py,
					  success : function(result){  
						onSuccess(result)
						$( "#notification" ).dialog({
						  minWidth: 1000,
						  closeText: "",
						  title: "Publicaciones Registradas"
						  
						}).prev(".ui-dialog-titlebar").css("background","#e8641c").css("color","white");
					  }
					 })
				})
				.style("fill", function(d) { return c(j); });
			
			text2
				.attr("y", k*20+25)
				.attr("x",function(d, i) { return 2.5*250+xScale(d[0])-5; })
				.attr("class","value")
				.attr("id","graph_info")
				.attr("font-size","13px !important")
				.on("click",function(d){
					aid = $(this).parent().parent().attr('id')
					py = d[0]
					$.ajax({
					  url : 'webservice.php?id='+aid+'&year='+py,
					  success : function(result){  
						onSuccess(result)
						$( "#notification" ).dialog({
						  minWidth: 1000,
						  closeText: "",
						  title: "Publicaciones Registradas"
						  
						}).prev(".ui-dialog-titlebar").css("background","#e8641c").css("color","white");
						
					  }
					 })
				})
				.text(function(d){ return d[1]; })
				.style("fill", function(d) { return c(j); })
				.style("display","none");

			g1.append("text")
				.attr("y", k*20+25)
				.attr("x",width+30)
				.attr("class","label")				
				
				.text(truncate(j+1+'. '+data_aux[j]['name'],56,"..."))
				.style("fill", function(d) { return c(j); })
				.style("width", "500px !important")
				.attr("font-size","13px !important")
				.style("text-align", "center")
				.on("click", function(d){
					window.open(('http://www.portaldelinvestigador.cl/'))
				})
				.on("mouseover", mouseover)
				.on("mouseout", mouseout);
			k++
			j++
			if (k == len || j == data_aux.length - 1)
				break
				
		}
		for(j in data_aux){
		
			if(flag2 == 1){
				if (l > aux_j - 5 && l < aux_j + 3 ){
					
					if (univ != "Todas"){
						//~ console.log("ss")
						var g1 = svg.append("g").attr("class","journal").attr("id",data_aux[j]['id']);
						var g = g1.append("g").attr("class","citation");
						var g2 = g1.append("g").attr("class","papers");
						
						var circles = g.selectAll("circle")
							.data(data_aux[j]['citation'])
							.enter()
							.append("circle");				

						var text = g.selectAll("text")
							.data(data_aux[j]['citation'])
							.enter()
							.append("text");
				
						var circles2 = g2.selectAll("circle")
							.data(data_aux[j]['papers'])
							.enter()
							.append("circle");				

						var text2 = g2.selectAll("text")
							.data(data_aux[j]['papers'])
							.enter()
							.append("text");
						
						var rScale = d3.scale.linear()
							.domain([0, citas_max])
							.range([1, 9]);
						var rScale2 = d3.scale.linear()
							.domain([0, paper_max])
							.range([1, 9]);
							

						circles
							.attr("cx", function(d, i) { return xScale(d[0]); })
							.attr("cy", k*20+20)
							.attr("r", function(d) { return rScale(d[1]); })
							.attr("id","graph_info")
							.on("click",function(d){
								aid = $(this).parent().parent().attr('id')
								py = d[0]
								$.ajax({
								  url : 'webservice.php?id='+aid+'&year='+py,
								  success : function(result){  
									onSuccess(result)
									$( "#notification" ).dialog({
									  minWidth: 1000,
									  closeText: "",
									  title: "Publicaciones Registradas"
									  
									}).prev(".ui-dialog-titlebar").css("background","#e8641c").css("color","white");

								  }
								 })
							})
							.style("fill", function(d) { return c(j); });
						
						text
							.attr("y", k*20+25)
							.attr("x",function(d, i) { return xScale(d[0])-5; })
							.attr("class","value")
							.attr("id","graph_info")
							.text(function(d){ return d[1]; })
							.style("fill", function(d) { return c(j); })
							.on("click",function(d){
								aid = $(this).parent().parent().attr('id')
								py = d[0]
								$.ajax({
								  url : 'webservice.php?id='+aid+'&year='+py,
								  success : function(result){  
									onSuccess(result)
									$( "#notification" ).dialog({
									  minWidth: 1000,
									  closeText: "",
									  title: "Publicaciones Registradas"
									  
									}).prev(".ui-dialog-titlebar").css("background","#e8641c").css("color","white");

								  }
								 })
							})
							.attr("font-size","13px !important")
							.style("display","none");
						circles2
							.attr("cx", function(d, i) { return 2.5*250+xScale(d[0]); })
							.attr("cy", k*20+20)
							.attr("r", function(d) { return rScale2(d[1]); })
							.attr("id","graph_info")
							.on("click",function(d){
								aid = $(this).parent().parent().attr('id')
								py = d[0]
								$.ajax({
								  url : 'webservice.php?id='+aid+'&year='+py,
								  success : function(result){  
									onSuccess(result)
									$( "#notification" ).dialog({
									  minWidth: 1000,
									  closeText: "",
									  title: "Publicaciones Registradas"
									  
									}).prev(".ui-dialog-titlebar").css("background","#e8641c").css("color","white");

								  }
								 })
							})
							.style("fill", function(d) { return c(j); });
						
						text2
							.attr("y", k*20+25)
							.attr("x",function(d, i) { return 2.5*250+xScale(d[0])-5; })
							.attr("class","value")
							.attr("id","graph_info")
							.text(function(d){ return d[1]; })
							.on("click",function(d){
								aid = $(this).parent().parent().attr('id')
								py = d[0]
								$.ajax({
								  url : 'webservice.php?id='+aid+'&year='+py,
								  success : function(result){  
									onSuccess(result)
									$( "#notification" ).dialog({
									  minWidth: 1000,
									  closeText: "",
									  title: "Publicaciones Registradas"
									  
									}).prev(".ui-dialog-titlebar").css("background","#e8641c").css("color","white");

								  }
								 })
							})
							.attr("font-size","13px !important")
							.style("fill", function(d) { return c(j); })
							.style("display","none");

						g1.append("text")
							.attr("y", k*20+25)
							.attr("x",width+30)
							.attr("class","label")				
							.text(truncate(j+1+'. '+data_aux[j]['name'],56,"..."))
							.style("fill", function(d) { if (l+1 != aux_j) return c(j); else return "orange"; })
							.attr("font-size","13px !important")
							.style("width", "400px !important")
							
							.style("text-align", "center")
							.on("click", function(d){
								window.open(('http://www.portaldelinvestigador.cl/'))
							})
							.on("mouseover", mouseover)
							.on("mouseout", mouseout);
					}else{
						
						var g1 = svg.append("g").attr("class","journal").attr("id",data_aux[j]['id']);
						var g = g1.append("g").attr("class","citation");
						var g2 = g1.append("g").attr("class","papers");
						
						var circles = g.selectAll("circle")
							.data(data_aux[j]['citation'])
							.enter()
							.append("circle");				

						var text = g.selectAll("text")
							.data(data_aux[j]['citation'])
							.enter()
							.append("text");
				
						var circles2 = g2.selectAll("circle")
							.data(data_aux[j]['papers'])
							.enter()
							.append("circle");				

						var text2 = g2.selectAll("text")
							.data(data_aux[j]['papers'])
							.enter()
							.append("text");
						
						var rScale = d3.scale.linear()
							.domain([0, citas_max])
							.range([1, 9]);
						var rScale2 = d3.scale.linear()
							.domain([0, paper_max])
							.range([1, 9]);
							

						circles
							.attr("cx", function(d, i) { return xScale(d[0]); })
							.attr("cy", k*20+20)
							.attr("r", function(d) { return rScale(d[1]); })
							.attr("id","graph_info")
							.on("click",function(d){
								aid = $(this).parent().parent().attr('id')
								py = d[0]
								$.ajax({
								  url : 'webservice.php?id='+aid+'&year='+py,
								  success : function(result){  
									onSuccess(result)
									$( "#notification" ).dialog({
									  minWidth: 1000,
									  closeText: "",
									  title: "Publicaciones Registradas"
									  
									}).prev(".ui-dialog-titlebar").css("background","#e8641c").css("color","white");
	
								  }
								 })
							})
							.style("fill", function(d) { return c(j); });
						
						text
							.attr("y", k*20+25)
							.attr("x",function(d, i) { return xScale(d[0])-5; })
							.attr("class","value")
							.attr("id","graph_info")
							.text(function(d){ return d[1]; })
							.style("fill", function(d) { return c(j); })
							.on("click",function(d){
								aid = $(this).parent().parent().attr('id')
								py = d[0]
								$.ajax({
								  url : 'webservice.php?id='+aid+'&year='+py,
								  success : function(result){  
									onSuccess(result)
									$( "#notification" ).dialog({
									  minWidth: 1000,
									  closeText: "",
									  title: "Publicaciones Registradas"
									  
									}).prev(".ui-dialog-titlebar").css("background","#e8641c").css("color","white");

								  }
								 })
							})
							.attr("font-size","13px !important")
							.style("display","none");
						circles2
							.attr("cx", function(d, i) { return 2.5*250+xScale(d[0]); })
							.attr("cy", k*20+20)
							.attr("id","graph_info")
							.attr("r", function(d) { return rScale2(d[1]); })
							.on("click",function(d){
								aid = $(this).parent().parent().attr('id')
								py = d[0]
								$.ajax({
								  url : 'webservice.php?id='+aid+'&year='+py,
								  success : function(result){  
									onSuccess(result)
									$( "#notification" ).dialog({
									  minWidth: 1000,
									  closeText: "",
									  title: "Publicaciones Registradas"
									  
									}).prev(".ui-dialog-titlebar").css("background","#e8641c").css("color","white");

								  }
								 })
							})
							.style("fill", function(d) { return c(j); });
						
						text2
							.attr("y", k*20+25)
							.attr("x",function(d, i) { return 2.5*250+xScale(d[0])-5; })
							.attr("class","value")
							.attr("id","graph_info")
							.text(function(d){ return d[1]; })
							.style("fill", function(d) { return c(j); })
							.on("click",function(d){
								aid = $(this).parent().parent().attr('id')
								py = d[0]
								$.ajax({
								  url : 'webservice.php?id='+aid+'&year='+py,
								  success : function(result){  
									onSuccess(result)
									$( "#notification" ).dialog({
									  minWidth: 1000,
									  closeText: "",
									  title: "Publicaciones Registradas"
									  
									}).prev(".ui-dialog-titlebar").css("background","#e8641c").css("color","white");
								  }
								 })
							})
							.attr("font-size","13px !important")
							.style("display","none");

						g1.append("text")
							.attr("y", k*20+25)
							.attr("x",width+30)
							.attr("class","label")				
							.text(truncate(j+1+'. '+data_aux[j]['name'],56,"..."))
							.style("fill", function(d) { if (l+1 != aux_j) return c(j); else return "orange"; })
							.style("width", "400px !important")
							.attr("font-size","13px !important")
							.style("text-align", "center")
							.on("click", function(d){
								window.open(('http://www.portaldelinvestigador.cl'))
						})
							.on("mouseover", mouseover)
							.on("mouseout", mouseout);
						
						
					}
					k++
				}
				l++
			}
			
			
		}
		//~ if($('#searcher p').text() != '' && flag2 == 1){
			 //~ a = $(".token-input-list li p").text()
			//~ $("#searchRank").text("Lugar "+ jsonArray[a] + " de " + Object.keys(jsonArray).length)
		//~ }
		$.each($('.x.axis g text'),function(i,d){
			if (i == 7 || i == 15){
				$(d).text('TOTAL')
				$(d).css('fill','#e8641c')
				$(d).css('font-weight','bold')
			}
		})
		show()
		function mouseover(p) {
			if (opcion == "Completa"){
				var g = d3.select(this).node().parentNode;
				d3.select(g).selectAll("circle").style("display","none");
				d3.select(g).selectAll("text.value").style("display","block");
				d3.select(g).selectAll("text.value2").style("display","block");
			}
		}

		function mouseout(p) {
			if (opcion == "Completa"){
				var g = d3.select(this).node().parentNode;
				d3.select(g).selectAll("circle").style("display","block");
				d3.select(g).selectAll("text.value").style("display","none");
				d3.select(g).selectAll("text.value2").style("display","none");
			}
		}
	});
	
	return csv
}
