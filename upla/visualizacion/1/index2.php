<!DOCTYPE html>
<head>
	<title>Navegador de Publicaciones</title>
	<link rel="shortcut icon" href="http://www.upla.cl/inclusion/wp-content/uploads/2013/05/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" href="/proyecto_upla/upla/visualizacion/1/css/jquery-ui.css" />
	<link href="/proyecto_upla/upla/visualizacion/1/css/bootstrap.min.css" rel="stylesheet">
	<!-- No está
	<link href="css/tooltip.css" rel="stylesheet">
	-->

	<link rel="stylesheet" type="text/css" href="/proyecto_upla/upla/visualizacion/css/style.css"/>
	<link rel="stylesheet" type="text/css" href="/proyecto_upla/upla/visualizacion/1/css/style.css"/>
	<link rel="stylesheet" type="text/css" href="/proyecto_upla/css/catalogo.css"/>

</head>
<body>

	<!-- titulo -->
	<?php include '../../../header.php'; ?>
	
	<!-- Content -->
	<div class="container-fluid">
	<div class="row">
	
		<div class="col-md-2">
			<?php include '../../../navbar.php'; ?>			
		</div> <!-- /col-md-4 -->		
	
		<!-- Contenido --> 
		<div class="col-md-10">
			<!-- barra ubicacion breadcrumb -->
			<?php include '../../../breadcrumb.php'; ?>			

			<!-- Modal -->
			<button type="button" class="btn btn-primary btn-lg glyphicon glyphicon-info-sign" data-toggle="modal" data-target="#myModal"></button>
			<div id="myModal" class="modal fade" role="dialog">
			  <div class="modal-dialog">
			    <!-- Modal content-->
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
			     		<h4 class="modal-title">
			     			<!--Título--> 
			     			Buscador
			     			<!--Título--> 
			     		</h4>
			      </div>
			      <div class="modal-body">
			        <p>
			        <!--Texto--> 
			        	La herramienta muestra un navegador interactivo y exportable de Congresos y otras actividades del mismo tipo realizados por los Investigadores de la Universidad de Playa Ancha. Este navegador está ordenado por Facultades o Centros. Selecciona un año para navegar, cierra o abre cada una de las Facultades o Centros con un click para ver a sus investigadores y su participación en estas instancias. 
			        <!--Texto--> 
			        </p>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			      </div>
			    </div>
			  </div>
			</div>
			<!-- /Modal -->

			<br/>
			<div id="contenido">
				<div id="main">
				  <div id="container">
					<div class="btn-toolbar" role="toolbar">
					  <div class="btn-group">
<!--
							<button type="button" class="btn btn-default year" id="2010">2010</button>
-->
							<button type="button" class="btn btn-default year" id="2011">2011</button>
							<button type="button" class="btn btn-default year" id="2012">2012</button>
							<button type="button" class="btn btn-default year" id="2013">2013</button>
							<button type="button" class="btn btn-default year" id="2014">2014</button>
							<button type="button" class="btn btn-default year" id="2015">2015</button>
						  </div>
						</div>
					  </div>
					  <span id="network_extra">
						<span id="tosvg" title="Descarga la red en formato SVG"><a href="javascript:(function () { var e = document.createElement('script'); if (window.location.protocol === 'https:') { e.setAttribute('src', 'https://rawgit.com/NYTimes/svg-crowbar/gh-pages/svg-crowbar.js'); } else { e.setAttribute('src', 'http://nytimes.github.com/svg-crowbar/svg-crowbar.js'); } e.setAttribute('class', 'svg-crowbar'); document.body.appendChild(e); })();"><img src="css/icons/download.png"></a></span>
					  </span>
	  				</div>
				</div>
			</div>
					
	
		</div><!-- /row -->
		</div><!-- /conteriner -->	

	<!-- footer -->
	<?php include '../../../footer.php'; ?>
	  
	<script src="/proyecto_upla/upla/visualizacion/1/js/jquery.min.js"></script>
	<script src="/proyecto_upla/upla/visualizacion/js/svg.js"></script>
	<script src="/proyecto_upla/upla/visualizacion/1/js/jquery-ui.min.js"></script>
	<script src="/proyecto_upla/upla/visualizacion/1/js/d3.v3.min.js"></script>
	<script src="/proyecto_upla/js/bootstrap.min.js"></script>	
	
  <script>
  String.prototype.capitalize = function(){
       return this.replace( /(^|\s)([a-z])/g , function(m,p1,p2){ return p1+p2.toUpperCase(); } );
      };
  function truncate(str, maxLength, suffix) {
		if(str.length > maxLength) {
			str = str.substring(0, maxLength + 1); 
			str = str.substring(0, Math.min(str.length, str.lastIndexOf(" ")));
			str = str + suffix;
		}
		return str;
	}
  
  var DATA;
  var margin = {top: 30, right: 20, bottom: 30, left: 20},
	  width = 960 - margin.left - margin.right,
	  barHeight = 20,
	  barWidth = width * .8;

  var i = 0,
	  duration = 400,
	  root;

  var tree = d3.layout.tree()
	  .nodeSize([0, 20]);

  var diagonal = d3.svg.diagonal()
	  .projection(function(d) { return [d.y, d.x]; });

  var svg = d3.select("#container").append("svg")
	  .attr("width", width + margin.left + margin.right)
	.append("g")
	  .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  load_year("2014");

  $(".year").click(function(){
	load_year($(this).attr("id"));
  });

  function load_year(year){
	d3.json("/proyecto_upla/upla/data/" + year + "c.json", function(error, flare) {
	
	  flare.x0 = 0;
	  flare.y0 = 0;
	  DATA = update(root = flare);
	  DATA = DATA.children;
	  DATA.forEach(function(d){click(d)})
	});
  }

 function update(source) {

	// Compute the flattened node list. TODO use d3.layout.hierarchy.
	var nodes = tree.nodes(root);

	var height = Math.max(500, nodes.length * barHeight + margin.top + margin.bottom);

	d3.select("svg").transition()
		.duration(duration)
		.attr("height", height);

	d3.select(self.frameElement).transition()
		.duration(duration)
		.style("height", height + "px");

	// Compute the "layout".
	nodes.forEach(function(n, i) {
	  n.x = i * barHeight;
	});

	// Update the nodes	
	var node = svg.selectAll("g.node")
		.data(nodes, function(d) { return d.id || (d.id = ++i); });

	var nodeEnter = node.enter().append("g")
		.attr("class", "node")
		.attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
		.style("opacity", 1e-6);

	// Enter any new nodes at the parent's previous position.
	nodeEnter.append("rect")
		.attr("y", -barHeight / 2)
		.attr("class",function(d){return d.name})
		.attr("height", barHeight)
		.attr("width", barWidth)
		.style("fill", color)
		.on("click", click);

	nodeEnter.append("text")
		.attr("dy", 3.5)
		.attr("dx", 5.5)
		.text(function(d) { return truncate(String(d.name).toLowerCase().capitalize(),150,"...");});

	// Transition nodes to their new position.
	nodeEnter.transition()
		.duration(duration)
		.attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; })
		.style("opacity", 1);

	node.transition()
		.duration(duration)
		.attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; })
		.style("opacity", 1)
	  .select("rect")
		.style("fill", color);

	// Transition exiting nodes to the parent's new position.
	node.exit().transition()
		.duration(duration)
		.attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
		.style("opacity", 1e-6)
		.remove();

	// Update the links
	var link = svg.selectAll("path.link")
		.data(tree.links(nodes), function(d) { return d.target.id; });

	// Enter any new links at the parent's previous position.
	link.enter().insert("path", "g")
		.attr("class", "link")
		.attr("d", function(d) {
		  var o = {x: source.x0, y: source.y0};
		  return diagonal({source: o, target: o});
		})
	  .transition()
		.duration(duration)
		.attr("d", diagonal);

	// Transition links to their new position.
	link.transition()
		.duration(duration)
		.attr("d", diagonal);

	// Transition exiting nodes to the parent's new position.
	link.exit().transition()
		.duration(duration)
		.attr("d", function(d) {
		  var o = {x: source.x, y: source.y};
		  return diagonal({source: o, target: o});
		})
		.remove();

	// Stash the old positions for transition.
	nodes.forEach(function(d) {
	  d.x0 = d.x;
	  d.y0 = d.y;
	});
	
	return source
  }

  // Toggle children on click.
  function click(d) {

	if(d.depth == -1){
	  window.open("/proyecto_upla/upla_alpha/search.php?id=" + d.user_id);
	}else{
	  if (d.children) {
		d._children = d.children;
		d.children = null;
	  } else {
		d.children = d._children;
		d._children = null;
	  }
	  update(d);
	}
  }
  
  function color(d) {
	Q = ["Q1","Q2","Q3","Q4","CUARTIL NO DEFINIDO"]
	if (Q.indexOf(d.name) <= -1){
		try{
			if (d.parent.name == "Q1")
				return "blue"
			if (d.parent.name == "Q2")
				return "green"
			if (d.parent.name == "Q3")
				return "orange"
			if (d.parent.name == "Q4")
				return "red"
			if (d.parent.name == "CUARTIL NO DEFINIDO")
				return "purple"
		}catch(e){
			var a = 1
		}
	}
	if (Q.indexOf(d.name) > -1){
		if (d.name == "Q1")
			return "blue"
		if (d.name == "Q2")
			return "green"
		if (d.name == "Q3")
			return "orange"
		if (d.name == "Q4")
			return "red"
		if (d.name == "CUARTIL NO DEFINIDO")
			return "purple"
	}
	if(d.depth == 4)
		return "#FFF";
	if(d.depth == 1)
		return "#BBB";
	if(d.depth == 0)
		return "#666";
	if (d.depth == 2)
		return "rgb(0, 68, 98)";
	  
  }
  

  
$(document).ready(function(){
	$('#panel-element-4').addClass("in");
 	$('#sub-panel-element-44').addClass("in");
 	$('#tri-inicio').css('border-left-color','#004462');
 	$('#barra-inicio').css('background-color','#004462');
 	$('#barra-inicio').css('color','#fff');
 	$('#tri-inicio').css('background-color','#2D2D2D');
  $('#barra1').html('Difusión del conocimiento');
 	$('#tri1').css('border-left-color','#2D2D2D');
 	$('#barra1').css('background-color','#2D2D2D');
 	$('#barra1').css('color','#fff');
 	$('#tri1').css('background-color','#818181');
 	$('#tri2').css('border-left-color','#818181');
  $('#barra2').html('Buscador');
  $('#barra2').css('background-color','#818181');
  $('#barra2').css('color','#fff');
 	$('#tri2').css('background-color','#818181');
	$('#tri3').css('border-left-color','#818181');
  $('#barra3').hide();
 	//$('#barra3').css('background-color','#818181');
 	//$('#barra3').css('color','#fff');
})		


  </script>
</body>
</html>
