function createSlider() {
    $( "#slider" ).slider({
      value:2012,
      min: 2011,
      max: 2012,
      step: 1,
      slide: function( event, ui ) {
        $( "#date-range" ).text( ui.value);
      }
    });
    $( "#date-range" ).text($( "#slider" ).slider( "value"));
  }
function upper (text) {
	var aux = text.split(" ")
	text = ""
	aux.forEach(function(char){
		char = char.toLowerCase();
		if (char.length > 3 || char == 'q1' || char == 'q2' || char == 'q3' || char == 'q4') text = text+ " " + char.charAt(0).toUpperCase() + char.slice(1);
		else text = text + " " + char
	});
    return text;
}
function sun_load(year){
  activation = sequences("/proyecto_upla/upla/data/sun" + year + ".csv",1,1);
  createSlider();
  $("#ano").html(year);
}
$(".year").click(function(){
    sun_load($(this).attr("id"));
  });
$(document).ready(function(){
	sun_load(2014);

});
