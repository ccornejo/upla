<?php
//Start session
//~ session_start();
 
//Check whether the session variable SESS_MEMBER_ID is present or not
//~ if(!isset($_SESSION['sess_user_id']) || (trim($_SESSION['sess_user_id']) == '')) {
	//~ header("location: /upla/upla/login.html");
	//~ exit();
//~ }
?>
<!DOCTYPE html>
<head>
  <meta charset="utf-8">
  <title>Redes de Colaboración</title>
  <link rel="shortcut icon" href="http://www.upla.cl/inclusion/wp-content/uploads/2013/05/favicon.ico" type="image/x-icon">
  <link rel="stylesheet" href="/proyecto_upla/upla/visualizacion/3/css/style.css">
  <link rel="stylesheet" href="/proyecto_upla/upla/visualizacion/3/css/jquery.switchButton.css" />
  <link href="/proyecto_upla/upla/visualizacion/3/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="/proyecto_upla/upla/visualizacion/css/style.css"/>
  <link rel="stylesheet" href="/proyecto_upla/upla/visualizacion/3/css/jquery-ui.css" />
  <link rel="stylesheet" href="/proyecto_upla/upla/visualizacion/3/css/token-input.css" />
  <link rel="stylesheet" type="text/css" href="/proyecto_upla/css/catalogo.css"/>

</head>

<body>
	<!-- titulo -->
	<?php include '../../../header.php'; ?>

	<!-- Content -->
	<div class="container-fluid">
	<div class="row">
	
		<div class="col-md-2">
			<?php include '../../../navbar.php'; ?>			
		</div> <!-- /col-md-4 -->		
	
		<!-- Contenido --> 
		<div class="col-md-10">

			<!-- barra ubicacion -->
			<div id="barra">
				<div id="barra1">Congresos</div>
				<div class="tri" id="tri1"></div>
				<div id="barra2">Investigación</div>
				<div class="tri" id="tri2"></div>
				<div id="barra3">Redes Personas</div>
				<div class="tri" id="tri3"></div>
			</div>
			<br/>
			<div id="main">
				<div id="filter">
					<p>&#9660 FILTROS &#9660</p>
					<div class="select"><span id="select">Seleccionar Todo</span> <span id="unselect">Deseleccionar Todo</span></div>
					<div id="year"></div> 
					<div id="quartile"></div>
				</div>
				<div id="node_search">
					<input type="text" id="search-network" name="search" placeholder="Buscar nueva entidad"><img src="/proyecto_upla/upla/visualizacion/3/css/images/node_search.png"/>
				</div>
				<div id="options">
					<a class="icon" href="javascript:void(0)" onclick="resetNetwork();"><img id="reset" src="/proyecto_upla/upla/visualizacion/3/css/images/refresh.png" title="Restablecer Tamaño" /></a>
					<a class="icon" href="javascript:void(0)" onclick="zooming(1);"><img id="zoomout" src="/proyecto_upla/upla/visualizacion/3/css/images/zoomout.png" title="Alejar" /></a>
					<a class="icon" href="javascript:void(0)" onclick="zooming(0);"><img id="zoomin" src="/proyecto_upla/upla/visualizacion/3/css/images/zoomin.png" title="Acercar" /></a>
				</div>
				<div id="legend">
					<label></label><input type="checkbox" id="deploy" checked/>
		<!--
					<label></label><input type="checkbox" id="hull" checked />
		-->
				</div>
				<div id="help">
					<div id="style-4" class="scrollbar">
						<h3>SOBRE LA VISUALIZACIÓN</h3>
						<p>La visualización que ves corresponde a una red  de investigadores de la UTFSM vinculados por co-autoría en artículos científicos. El conjunto de investigadores externos a la Universidad están "colapsados" en un único nodo llamado RED EXTERNA USM. <br> El tamaño de los nodos refleja el número de colaboraciones, mientras que su color indica el departamento al que pertenece. Pincha en un nodo para ver sus colaboraciones históricas (resaltadas en color rojo).<br> El grosor de los enlaces indica la frecuencia de colaboración. <br> <br> Seleccionando la opción DEPARTAMENTO (<img src="/proyecto_upla/upla/visualizacion/3/css/icons/layer2.png" width="16px">) en la esquina superior derecha de la visualización, podrás visualizar la colaboración entre departamentos. Con un doble click sobre estos podrás ver a las personas que lo componen.</p>
						
						<p>La visualización puede ser filtrada por años y por calidad de las revistas (cuartil Q desde 2007). Explora estas opciones en los FILTROS.</p>
					</div>
				</div>
				<div id="helpicon"></div>
				
				<div id="vis"></div>
				<span id="legend_bottom">
					<div class="legend size" id="size_l"><img src="/proyecto_upla/upla/visualizacion/3/css/icons/size.png"/> Número de colaboraciones</div>
					<span class="legend" id="person"><img src="/proyecto_upla/upla/visualizacion/3/css/icons/autor.png"/> Autor</span>
					<span class="legend" id="organization"><img src="/proyecto_upla/upla/visualizacion/3/css/icons/org.png"/> Departamento</span>
				</span>
				
				<span id="network_extra">
					<span id="tosvg" title="Descarga la red en formato SVG"><a href="javascript:(function () { var e = document.createElement('script'); if (window.location.protocol === 'https:') { e.setAttribute('src', 'https://rawgit.com/NYTimes/svg-crowbar/gh-pages/svg-crowbar.js'); } else { e.setAttribute('src', 'http://nytimes.github.com/svg-crowbar/svg-crowbar.js'); } e.setAttribute('class', 'svg-crowbar'); document.body.appendChild(e); })();"><img src="/proyecto_upla/upla/visualizacion/3/css/icons/download.png"></a></span>
				</span>
		    </div>
		</div>
					
	
		</div><!-- /row -->
		</div><!-- /conteriner -->	

	<!-- footer -->
	<?php include '../../../footer.php'; ?>

	<script src="/proyecto_upla/upla/visualizacion/3/js/jquery.min.js"></script>
	<script src="/proyecto_upla/upla/visualizacion/3/js/d3.v3.min.js"></script>
	<script src="/proyecto_upla/upla/visualizacion/3/js/jquery-ui.min.js" type="text/javascript"></script>
	<script src="/proyecto_upla/upla/visualizacion/3/js/jquery.switchButton.js" type="text/javascript"></script>
	<script src="/proyecto_upla/upla/visualizacion/3/js/network.js"></script>
	<script src="/proyecto_upla/js/bootstrap.min.js"></script>	
	<script type="text/javascript" src="/proyecto_upla/upla/visualizacion/3/js/jquery.tokeninput.js"></script>
	<script>
		var flag =0
		var lastlink = "";
		var lasttypes = "";
		var lasttypet = "";
		var lastnode = "";
		var lastcolor = "";
		var lastsearch = "";
		function zooming(type){
			var newzoom = zoom.scale();
			var newtranslate = zoom.translate();
			if (type == 0) newzoom += 0.05;
			if (type == 1) newzoom -= 0.05;
			zoom.scale(newzoom);
			zoom.translate(newtranslate);
			vis.attr("transform","translate("+newtranslate+") scale("+newzoom+")");
		}
		function resetNetwork(){
			vis.attr("transform","scale(1)");
			zoom.scale(1).translate([0,0]);
		}
		
		function color(){
			q = []
			q[3] = $("#Q4").attr("val")
			q[2] = $("#Q3").attr("val")
			q[1] = $("#Q2").attr("val")
			q[0] = $("#Q1").attr("val")
			$("line").each(function(){
				if ($(this).attr("relation") != undefined){
					$(this).attr("color","#333")
					for (var i = 3; i >= 0; i--){
						aux = q[i].split(",")
						relation = ($(this).attr("relation")).split(",")
						for (var k=0; k<relation.length; k++){ 
							if (aux.indexOf(relation[k]) != -1){
								if ( i == 3)
									$(this).attr("color","#112233")
								if ( i == 2)
									$(this).attr("color","#223344")
								if ( i == 1)
									$(this).attr("color","#334455")
								if ( i == 0)
									$(this).attr("color","#445566")
							}
						}
					}	 
				}
			})
		}	
		
		function hide(){
			$("line").css("visibility","hidden")
			$("line[leaf=false]").css("visibility","visible")
			$("text[class='node leaf']").css("visibility","hidden")
			$("path[class='node leaf']").css("visibility","hidden")
			aux_q = ""
			//~ $("input[class='opt_q']").each(function(i,val){
				//~ if ( val.checked == true){
					//~ q_aux = $("#"+val.id).attr("val")
					//~ aux_q = aux_q + q_aux + ","
				//~ }
			//~ })
			//~ aux_q = aux_q + ","
			//~ aux_q = aux_q.replace(",,","")
			//~ aux_q = aux_q.split(",")
			
			
			
			$("input[class='opt']").each(function(i,val){
				if ( val.checked == true){
					console.log(val)
					q_aux = $("#"+val.id).attr("val")
					edge = q_aux.split(",")
					$("line").each(function(){
						if ($(this).attr("relation") != undefined){
							relation = ($(this).attr("relation")).split(",")
							r_aux = $(this).attr("relation")
							source = ""
							target = ""
							if ($(this).attr("source") != undefined)
								source = ($(this).attr("source")).toLowerCase()
							if ($(this).attr("target") != undefined)
								target= ($(this).attr("target")).toLowerCase()
							
							for (var k=0; k<relation.length; k++){
								if (edge.indexOf(relation[k]) != -1){
									//~ console.log(r_aux)
									$('line[relation="'+r_aux+'"]').css("visibility","visible")
									$('text[name="'+source+'"]').css("visibility","visible")
									$('text[name="'+target+'"]').css("visibility","visible")
									$('path[name="'+source+'"]').css("visibility","visible")
									$('path[name="'+target+'"]').css("visibility","visible")
									
								}
							}
						}
					})
				}
			})
		}	
		

		function nodecenter(searchRegEx){
			searchRegEx = searchRegEx.toLowerCase()
			x = ($("#vis").width())/2;
			y = ($("#vis").height())/2;
			cx = (x*0.7 - $("g[name='"+searchRegEx+"']").attr("cx"));
			cy = (y*0.7 - $("g[name='"+searchRegEx+"']").attr("cy"));
			lastcolor =  $("g[name='"+searchRegEx+"'] path").css("fill")
			$("g[name='"+searchRegEx+"'] path").css("fill","red")
			$("g[name='"+searchRegEx+"'] text").css("fill","blue")
			if (lastnode != ""){
				$("g[name='"+lastnode+"'] path").css("fill",lastcolor)
				$("g[name='"+lastnode+"'] text").css("fill","black")
			}
			lastnode = searchRegEx;
			console.log(cx)
			if (isNaN(cx)){
				resetNetwork()
			}else{
				zoom.translate([cx,cy]);
				zoom.scale(1.2);
				vis.attr("transform","translate("+[cx,cy]+") scale("+zoom.scale()+")");
			}
		}

		
		$(document).ready(function(){

			$('#panel-element-2').addClass("in");
		  $('#sub-panel-element-2').addClass("in");

    	$('#tri1').css('border-left-color','#2D2D2D');
    	$('#barra1').css('background-color','#2D2D2D');
    	$('#barra1').css('color','#fff');
    	$('#tri1').css('background-color','#818181');

    	$('#tri2').css('border-left-color','#818181');
    	$('#barra2').css('background-color','#818181');
    	$('#barra2').css('color','#fff');
      $('#tri2').css('background-color','#DDDDDD');

			$('#tri3').css('border-left-color','#DDDDDD');
    	$('#barra3').css('background-color','#DDDDDD');
    	$('#barra3').css('color','#818181');





			iconflag = 1;
			
			$("#help").hide()
			
			$("#helpicon").click(function(){
				if (iconflag == 1){
					$("#help").show()
					$("#help").height($("#vis").height())
					$("#help").width("300px")
					$("#helpicon").css("left","310px")
					$(".scrollbar").css("max-height",$("#vis").height()-20)
					iconflag = 0;
				}else{
					$("#help").hide()
					$("#helpicon").css("left","0")
					iconflag = 1;
				}
				
			})
			
			$(window).resize(function(){
				//~ $("#vis").css("height",$(window).height()-360)
				$("svg").css("height",$(window).height()-360)
				$("#help").height($("#vis").height())
				$(".scrollbar").css("max-height",$("#vis").height()-20)
				}
			
			)
			$(".select").hide()
			$("#year").hide()
			$("#quartile").hide()
			
			$('#node_search').bind("DOMSubtreeModified",function(){
				
				if ($( "#node_search .token-input-token p" ).text() != "" && $( "#node_search .token-input-token p" ).text() != lastsearch){
					//~ console.log($(".token-input-list").siblings("#search-network").val())
					id = $( "#node_search .token-input-token p" ).text()
					lastsearch = $( "#node_search .token-input-token p" ).text()
					console.log(id)
					expand[groups[id]] = true;
					console.log(expand)
					init();
					uniqueItems();
					nodecenter(lastsearch)
					type = $("g[name='"+lastsearch+"'] path").attr("type")
					window.location.href='#'+(id);
				}
			});
					
			$('#filter p').click(function(){
				if (flag==0){
					$("#year").show()
					$("#quartile").show();
					$(".select").show()
					$("#filter p").text("▲ FILTROS ▲")
					flag = 1
				}else{
					$(".select").hide()
					$("#year").hide()
					$("#quartile").hide();
					$("#filter p").text("▼ FILTROS ▼")
					flag = 0
				}
			});
			
			$('#tosvg a').click(function(){
				//~ $('#tosvg a').attr("download",$("#key").text()+".svg")
							//~ .attr("href",getSVG());
			});
			
			$("#hull").switchButton({
										on_label: "<img width='20px' src='/proyecto_upla/upla/visualizacion/3/css/icons/nolayer.png' />",
										off_label: "<img width='20px' src='/proyecto_upla/upla/visualizacion/3/css/icons/layer.png' />"
									});
			$("#deploy").switchButton({
									on_label: "<img width='20px' src='/proyecto_upla/upla/visualizacion/3/css/icons/person.png' />",
									off_label: "<img width='20px' src='/proyecto_upla/upla/visualizacion/3/css/icons/institution.png' />"
								});
									
			$("#hull").on("change",function(){
				if ($("#hull").prop("checked") == true){
					$("path[class=hull]").hide()
				}else{
					$("path[class=hull]").show()
				}
			});
			$("#deploy").on("change",function(){
				if ($("#deploy").prop("checked") == true){
					$("g.nodo[name='grouped']").each(function(d){
						expand[$(this).attr("group")] = true
					})
					hide()
				}else{
					for(var i in expand){
						expand[i] = false
					}
				}
				expand[1000000] = false;
				init();
				hide()
				
			})
			
			$.ajax({
				url : "/proyecto_upla/upla/visualizacion/3/functions/year.php",
				success : function(ret){
					d = ret.split(";")
					$('#year').append('<div><b>Año</b></div>')
					for (var i=0; i<d.length; i++) {
						q = d[i].split(":")
						if ( q != ""){
							
							if (q[0] == "2014")
								$('#year').append('<input class="opt" id="y'+q[0]+'" val='+q[1]+' type="checkbox" checked /> ' + q[0] + ' ');
							else
								$('#year').append('<input class="opt" id="y'+q[0]+'" val='+q[1]+' type="checkbox" /> ' + q[0] + ' ');
						}
					}
					hide()

				}
			})
			//~ $.ajax({
				//~ url : "functions/quartile.php",
				//~ success : function(ret){
					//~ d = ret.split(";")
					//~ $('#quartile').append('<div><b>Cuartil</b></div>')
					//~ for (var i=0; i<d.length; i++) {
						//~ q = d[i].split(":")
						//~ if ( q != "")
							//~ $('#quartile').append('<input class="opt_q" id="'+q[0]+'" type="checkbox" val="'+q[1]+'" checked /> ' + q[0] + ' ');
					//~ }
					//~ hide()
				//~ }
			//~ })
			
			$('#year').click(function(){
				hide()
			})
			$('#quartile').click(function(){
				hide()
			})
			
			$("#select").click(function () {
				$('input[type=checkbox]').prop('checked',"true");
				hide()
			});
			$("#unselect").click(function () {
				$('input[type=checkbox]').prop('checked',"");
				hide()
			});
					
			
			groups = {}
			groups = $.getJSON("/proyecto_upla/upla/visualizacion/3/data/data.json",function(d){
				for(i=0;i<d.nodes.length;i++){
					groups[d.nodes[i].short_name] = d.nodes[i].group
				}
				$("#node_search").tokenInput(d.nodes, {
					hintText: "Ingrese su búsqueda",
					noResultsText: "No hay resultados",
					searchingText: "Buscando...",
					resultsLimit: 10,
					minChars: 1,
					propertyToSearch: "short_name",
					tokenLimit: 1,
					tokenValue: "id",
					preventDuplicates: true,
					onAdd: function(){
						id = $( ".token-input-token p" ).text()
						lastsearch = $( ".token-input-token p" ).text()
						expand[groups[id]] = true;
						init();
						uniqueItems();
						nodecenter(lastsearch)
						type = $("g[name='"+lastsearch+"'] path").attr("type")
						
		
					},
					onDelete: function(){
						
						
					}
				});
				$('input[type=text]').attr('placeholder','Buscar investigador en la red');
				$("#token-input-node_search").css("min-width","300px")
				return groups
			});
			
			
		});
	</script>
</body>
</html>
