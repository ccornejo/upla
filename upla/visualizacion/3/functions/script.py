#!/home/redciencia_complejos/bin/python2.6
# -*- coding: utf-8 -*- 
import os
os.environ['PYTHON_EGG_CACHE'] = '/var/www/.python-eggs/'
import MySQLdb as mysql
import networkx as nx
import sys

def complete(var1,var2,arr):
	for i in var1:
		arr = arr + str(i) + "," + str(var2[i])+ ","
	arr = arr + "?"
	return arr
mysql_input = str(sys.argv[1])

mysql_input = mysql_input.split(";")
db = mysql.connect(mysql_input[0],mysql_input[1],mysql_input[2],mysql_input[3])
c = db.cursor()

consulta = "SELECT REPLACE(source,'-',''), REPLACE(target,'-',''), count(*) as cont FROM network group by source, target"
	
c.execute(consulta)
resultado = c.fetchall()
G = nx.Graph()
for registro in resultado:
	source = registro[0]
	target = registro[1]
	G.add_edge(source,target)

bet = nx.betweenness_centrality(G)
prk = nx.pagerank(G,max_iter=1000)


arr = ""
arr = complete(G.nodes(),bet,arr)
arr = complete(G.degree(),G.degree(),arr)
arr = complete(G.nodes(),prk,arr)


print arr
