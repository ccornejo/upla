<?php
	//Start session
	session_start();
	 
	//Check whether the session variable SESS_MEMBER_ID is present or not
	if(!isset($_SESSION['sess_user_id']) || (trim($_SESSION['sess_user_id']) == '')) {
		header("location: ../login.html");
		exit();
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Ingreso de Datos de Proyectos para Personas Asociadas</title>
  <link rel="shortcut icon" href="http://www.upla.cl/inclusion/wp-content/uploads/2013/05/favicon.ico" type="image/x-icon">
	<link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/datepicker.css" rel="stylesheet">
    <link href="css/token-input.css" type="text/css" rel="stylesheet">
    <link href="css/token-input-facebook.css" type="text/css" rel="stylesheet">
    <link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap-tokenfield.min.css" type="text/css" rel="stylesheet">
    <link href="css/style.css" type="text/css" rel="stylesheet">
    <link href="css/jquery.editable-select.min.css" rel="stylesheet">
    <script src="js/ie-emulation-modes-warning.js"></script>
    <script src="js/ie10-viewport-bug-workaround.js"></script>
	<style type="text/css">
		.table, #isb, #edi, #eds, #importar{display: none;}
	</style>	
		
		
		
</head>
<body>
	<div class="content">
		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<a id="img-usm" class="navbar-brand" href="/proyecto_upla/">
            <img src="/proyecto_upla/img/logo_upla.png" class="img-responsive" width="120">
					</a>
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<!--<li><a href="/proyecto_upla/upla"><span class="glyphicon glyphicon-home"></span></a></li>-->
						<li><a href="persona.php">Persona</a></li>
						<li class="active"><a href="proyecto.php">Proyecto</a></li>
						<li><a href="publicacion.php">Publicación</a></li>
						<li><a href="congreso.php">Congreso</a></li>
            <li><a href="/proyecto_upla/">Volver</a></li>
						<!--<li><a href="patente.php">Patente</a></li>
						<li><a href="revista.php">Revista</a></li>-->
					</ul>
					<div id="logout"><a href="/proyecto_upla/upla/logout.php">Cerrar Sesion</a></div>
					<!--<div id="logout"><a href="http://fractalnet-works.ddns.net/proyecto_upla/upla/logout.php">Cerrar Sesion</a></div>-->
				</div>
			</div>
		</div>
		<div id="title">
			<div>
				<h1>Proyectos</h1>
				<p class="lead">Módulo para el ingreso y edición de datos de proyectos asociados a investigadores</p>
			</div>
		</div>
		<div class="field">
			<form role="form">
				<div class="row" id="row-participante">
					<h2>Proyectos registrados en el sistema</h2>
					<font> Conoce los proyectos en los cuales participa un académico, ingresando su nombre o RUN.</font>
					<div class="col-xs-12">
						<div class="form-group" id="divautor">
							<label for="participante">Participante</label>
								<input class="form-control" id="participante" placeholder="Ej.: 12675974-k">
								<button type="button" class="btn btn-default" id="agrega_participante">
									<span class="glyphicon glyphicon-eye-open"></span><font> Ver proyectos ya registrados</font>
								</button>
							<div id="data_result"></div>
						</div>
						<div class="form-group">
							<table class="table" id="tabla_participante">
								<thead>
									<tr>
										<td value="run">RUN</td>
										<td></td>
										<td></td>
									</tr>
								</thead>
								<tbody id="display">
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="row" id="formulario">
					<h2>Ingresa un Nuevo Proyecto</h2>
					<div id="internal">
						<div class="form-group group">
							<label for="id">ID/Número</label>
							<input type="text" class="form-control short" id="id" placeholder="Ej.: 11121292">
						</div>
						
						<div class="form-group group">
							<label for="title2">Título</label>
							<input type="text" class="form-control medium" id="title2" placeholder="Ej.: Automatización de Procesos">
						</div>
						
						<div class="form-group medium">
							<label for="participantes">Participante(s)</label>
							<input class="tokenfield medium" id="participantes" placeholder="Ej.: Patricio Vargas, Eduardo Soto">
<!--
              <font color="red">*Es recomendable que TODO participante perteneciente a la UPLA sea escogido de la lista desplegable. Si la presona no se encuentra, es necesario registrarla en el ambiente persona.</font>
-->
						</div>
						
						<div class="form-group group">
							<label for="resource">Tipo Proyecto</label>
							<select class="form-control short" id="resource" placeholder="Selecciona tipo">
								<option value="CONICYT">CONICYT</option>
								<option value="CORFO">CORFO</option>
								<option value="EXPLORA">EXPLORA</option>
								<option value="FAI">FAI</option>
								<option value="FIC">FIC</option>
								<option value="FONDECYT (Beca Postdoctorado)">FONDECYT (Beca Postdoctorado)</option>
								<option value="FONDECYT (Iniciación)">FONDECYT (Iniciación)</option>
								<option value="FONDECYT (Regular)">FONDECYT (Regular)</option>
								<option value="Fondo Basal por desempeño">Fondo Basal por desempeño</option>
								<option value="FPA">FPA</option>
								<option value="MEC">MEC</option>
								<option value="Ministerio del Medio Ambiente">Ministerio del Medio Ambiente</option>
								<option value="PMI">PMI</option>
								<option value="Otro">Otro</option>
							</select>
						</div>
					  
							<div class="form-group group">
								<label for="year">Año de Adjudicación</label>
								<input type="text" class="form-control short" id="year" placeholder="Ej.: 2000">
							</div>
					</div>
					<div class="button">
						<button type="button" class="btn btn-danger" id="eliminar">ELIMINAR</button>
						<button type="button" class="btn btn-default" id="guardar">GUARDAR</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.tokeninput.js"></script>
	<script src="js/jquery-ui.min.js"  type="text/javascript"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-tokenfield.min.js" type="text/javascript"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/jquery.editable-select.min.js"></script>	
	<script type="text/javascript">
		$(document).on('ready', function() {
			$('#resource').editableSelect();
			$("#participante").tokenInput("webservice/get_rut2.php",{
				preventDuplicates:true,
				hintText: "Ingrese su búsqueda",
				noResultsText: "No hay resultados",
				resultsLimit: 10,
				tokenLimit: 1,
				
				});
			$("#participantes").tokenInput("webservice/get_rut.php",{
				preventDuplicates:true,
				hintText: "Ingrese su búsqueda",
				noResultsText: "No hay resultados",
				resultsLimit: 10,
				theme:"facebook",
				
				});
				
			$("#agrega_participante").on("click",function(){
				ids = $("#participante").tokenInput("get");
				participantes = ""
				for (i in ids){
					participantes = ids[i]["id"]
				}
				
				$.post("webservice/get_projects.php",{id:participantes},
					function(result){
						$("#data_result").empty();
						$("#data_result").append("<ul>")
						result = JSON.parse(result)
						for (i in result){
							$("#data_result").append("<li>"+result[i].title+" ("+result[i].year+")</li>")
						}
						$("#data_result").append("</ul>")
					
					})
			})
			$("#guardar").on("click",function(){
				ids = $("#participantes").tokenInput("get");
				participantes = ""
				for (i in ids){
					participantes += ids[i]["id"]+","
				}
				participantes += ','
				participantes = participantes.replace(',,','')
				$.post("webservice/project.php",
					{id:$("#id").val(),
					 title:$("#title2").val(),
					 resource:$("#resource").val(),
					 year:$("#year").val(),
					 participantes:participantes
					},function(result){
						alert("Proyecto guardado")
						location.reload();
				})
			})
			$("#eliminar").on("click",function(){
				var action = confirm('¿Está seguro que desea eliminar este proyecto?');
				if (action == true) {
					$.post("webservice/project_remove.php",{id:$("#id").val()},
					function(result){
						alert("Proyecto eliminado correctamente")
						location.reload();
					}
				)}
			})
		})
      
    </script>
</body>
</html>
