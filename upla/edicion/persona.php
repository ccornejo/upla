<?php
	//Start session
	session_start();
	 
	//Check whether the session variable SESS_MEMBER_ID is present or not
	if(!isset($_SESSION['sess_user_id']) || (trim($_SESSION['sess_user_id']) == '')) {
		header("location: ../login.html");
		exit();
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Ingreso de Datos de Académicos, Investigadores y otros</title>
  <link rel="shortcut icon" href="http://www.upla.cl/inclusion/wp-content/uploads/2013/05/favicon.ico" type="image/x-icon">
	<link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/datepicker.css" rel="stylesheet">
  <link href="css/token-input.css" type="text/css" rel="stylesheet">
  <link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css">
  <link href="css/bootstrap-tokenfield.min.css" type="text/css" rel="stylesheet">
  <link href="css/jquery.editable-select.min.css" rel="stylesheet">
  <link href="css/style.css" type="text/css" rel="stylesheet">
  <script src="js/ie-emulation-modes-warning.js"></script>
  <script src="js/ie10-viewport-bug-workaround.js"></script>
	<style type="text/css">
		.grado, #grado1, #tip, #ing, #ret, #dep, #importar, #eliminar,#modificar{display: none;}
	</style>
		
		
		
</head>
<body>
	<div class="content">
		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<a id="img-usm" class="navbar-brand" href="/proyecto_upla/">
			            <img src="/proyecto_upla/img/logo_upla.png" class="img-responsive" width="120">
					</a>
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<!--<li><a href="/proyecto_upla/upla"><span class="glyphicon glyphicon-home"></span></a></li>-->
						<li class="active"><a href="persona.php">Persona</a></li>
						<li><a href="proyecto.php">Proyecto</a></li>
						<li><a href="publicacion.php">Publicación</a></li>
						<li><a href="congreso.php">Congreso</a></li>
						<li><a href="/proyecto_upla/">Volver</a></li>
						<!--<li><a href="patente.php">Patente</a></li>
						<li><a href="revista.php">Revista</a></li>-->
					</ul>
					<div id="logout"><a href="/proyecto_upla/upla/logout.php">Cerrar Sesion</a></div>
					<!--<div id="logout"><a href="http://fractalnet-works.ddns.net/proyecto_upla/upla/logout.php">Cerrar Sesion</a></div>-->
				</div>
			</div>
		</div>
		<div id="title">
			<div>
				<h1>Personas</h1>
				<p class="lead">Módulo para el ingreso y edición de datos de académicos, investigadores y otros</p>
			</div>
		</div>
		<div class="field">
			<!-- DATOS PERSONALES -->
			<div class="row">
				<h2>Datos Personales</h2>
				
				<div class="person">
					<div>
						<font>Para cargar datos ya registrados o editar estos, ingresa su <b>RUN</b> y luego presione enter.</font>
						<input type="text" class="form-control" id="run_editar" placeholder="Ej.: 12587162-k">
						*Si la persona no está en la lista, ingresa sus datos manualmente.
					</div>
				</div>

				<div class="col-lg-3">
					<label>RUN</label>
					<input type="text" class="form-control" id="rut" placeholder="Ej.: 12587162-k">
				</div>
				<div class="col-lg-3">
					<label>Nombres</label>
					<input type="text" class="form-control" id="name" placeholder="Ej.: Juan Carlos">
				</div>
				<div class="col-lg-3">
					<label>Apellido Paterno</label>
					<input type="text" class="form-control" id="last_name_p" placeholder="Ej.: Salazar">
				</div>
				<div class="col-lg-3">
					<label>Apellido Materno</label>
					<input type="text" class="form-control" id="last_name_m" placeholder="Ej.: Rodríguez">
				</div>
				<div class="col-lg-3">
					<label>Sexo</label><br>
					<input type="radio" id="genre_m" name="genre" value='M'>M 
					<input type="radio" id="genre_f" name="genre" value='F'>F
				</div>
				<div class="col-lg-3">
					<label>E-Mail institucional</label>
					<input type="text" class="form-control" id="email_i" placeholder="Ej.: usuario@upla.cl">
				</div>
				<div class="col-lg-3">
					<label>Facultad Asociada</label>
					<select id="faculty" class="form-control" placeholder="Ej.: FACULTAD DE ARTES, ACADEMICOS">
						<option>CENTRO DE ESTUDIOS AVANZADOS (VIPI)</option>
						<option>CONTRALORIA INTERNA</option>
						<option>CONV.DESEMPEÑO: CULTURA DESEM. UPA 1398</option>
						<option>DIREC.DE ESTUD.E INNOVAC.CURRIC.Y DESARR</option>
						<option>DIREC.GENERAL DESARROLLO ESTUDIANTIL</option>
						<option>DIRECCION DE ADMINISTRACION DE RR.HH.</option>
						<option>DIRECCION DE CARRERAS VESPERTINAS</option>
						<option>DIRECCION DE EDUCACION VIRTUAL EJECUC.</option>
						<option>DIRECCION GENERAL DE PREGRADO GENERAL</option>
						<option>DIRECCION GENERAL GESTION DE LA CALIDAD</option>
						<option>DIRECCION GENERAL GRADOS PROM. ACADEMICA</option>
						<option>DIRECCION GRAL.VINCULACION CON EL MEDIO</option>
						<option>FAC. EDUC. FISICA, ACADEMICOS</option>
						<option>FAC.CS. NATURALES Y EXACTAS, ACADEMICOS</option>
						<option>FACULTAD CS. NATURALES Y EXACTAS EJECUC</option>
						<option>FACULTAD CS. SOCIALES, ACADEMICOS</option>
						<option>FACULTAD DE ARTES EJECUCION</option>
						<option>FACULTAD DE ARTES, ACADEMICOS</option>
						<option>FACULTAD DE CIENCIAS SOCIALES DECANATO</option>
						<option>FACULTAD DE CS. ACT.FISICA Y DEL DEPORTE</option>
						<option>FACULTAD DE CS. DE LA SALUD, ACADEMICOS</option>
						<option>FACULTAD DE EDUCACION DECANATO EJECUCION</option>
						<option>FACULTAD DE EDUCACION, ACADEMICOS</option>
						<option>FACULTAD DE HUMANIDADES EJECUCION</option>
						<option>FACULTAD DE HUMANIDADES, ACADEMICOS</option>
						<option>FACULTAD DE INGENIERIA, ACADEMICOS</option>
						<option>FONDO DE LAS ARTES Y ASUNTOS PATRIMON.</option>
						<option>PRACTICA PROFESIONAL</option>
						<option>PROY.INST.UCI 9001 NIVELAC.DE COMP.</option>
						<option>PROYECTO OBRAS DE AMPLIACION SEDE SAN FE</option>
						<option>RECTORIA EJECUCION</option>
						<option>SELLO EDITORIAL PUNTANGELES</option>
						<option>TITULOS Y GRADOS</option>
						<option>VICERRECT INVESTIGAC., POSTGRADO E INNOV</option>
						<option>VICERRECTORIA CAMPUS SAN FELIPE EJECUC.</option>
						<option>VICERRECTORIA DE DESARROLLO EJECUCIÓN</option>
					</select>
				</div>
				<div class="col-lg-3">
					<label>Cargo</label>
					<select type="text" class="form-control" id="position" placeholder="Ej.: ACADEM-CONTRATA">
						<option>ACADEM JORNADA COMPLETA</option>
						<option>ACADEM MEDIA JORNADA   +  ACADEM MEDIA JORNADA</option>
						<option>ACADEM-CONTRATA</option>
						<option>ACADEMICO MEDIA JORNADA</option>
						<option>ACADEMICO MEDIA JORNADA + HORAS</option>
						<option>ACADEMICO PLANTA</option>
						<option>ACADM MEDIA JORNADA  +   PROFESOR  06 HORAS</option>
						<option>ADMIN-CONTRATA</option>
						<option>PROFESIONAL </option>
						<option>PROFESOR HORA</option>
					</select>
				</div>
			</div>
			<!-- FORMACIÓN ACADÉMICA -->
			<div class="row">
				<h2>Formación Académica</h2>
				<table class="person table">
					<thead>
						<tr>
							<th>Profesión</th>
							<th>Grado</th>
						</tr>
					</thead>
					<tbody id="tabla_grado">
						<tr>
							<td><input type="text" class="form-control titulo" id="profession" placeholder="Ej.: Ingeniero Civil / Dr. PhD. Magíster"></td>
							<td>
								<input type="checkbox" id="doctor_grade" value='Doctor'> Doctor 
								<input type="checkbox" id="magister_grade" value='Magíster'> Magister
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="button">
				<button type="button" class="btn btn-danger" id="eliminar">ELIMINAR</button>
				<button type="button" class="btn btn-warning" id="modificar">MODIFICAR</button>
				<button type="button" class="btn btn-default" id="guardar">GUARDAR</button>
				
			</div>
	</div>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.tokeninput.js"></script>
	<script src="js/jquery-ui.min.js"  type="text/javascript"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-tokenfield.min.js" type="text/javascript"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/jquery.editable-select.min.js"></script>	
	<script type="text/javascript">
		function clean(){
			$("#rut").val("")
			$("#name").val("")
			$("#last_name_p").val("")
			$("#last_name_m").val("")
			$("#email_i").val("")
			$("#faculty").val("")
			$("#position").val("")
			$("#profession").val("")
			$("input[type=radio][value=M]").attr("checked",false)
			$("input[type=radio][value=F]").attr("checked",false)
			document.getElementById("doctor_grade").checked = false
			document.getElementById("magister_grade").checked = false
			document.getElementById('genre_f').checked = false
			document.getElementById('genre_m').checked = false
			$("#eliminar").css("visibility","hidden").css("display","none")
			$("#modificar").css("visibility","hidden").css("display","none")
			
		}
		
		
		$(document).on('ready', function() {
			$('#faculty').editableSelect();
			$('#position').editableSelect();
			
			$("#guardar").on("click",function(){
				$.post("webservice/person.php",
						{rut:$("#rut").val(),
						name:$("#name").val(),
						last_name_p:$("#last_name_p").val(),
						last_name_m:$("#last_name_m").val(),
						email_i:$("#email_i").val(),
						faculty:$("#faculty").val(),
						position:$("#position").val(),
						profession:$("#profession").val(),
						genre:$("input[type=radio]:checked").val(),
						doctor_grade:$("input[type=checkbox][id=doctor_grade]:checked").val(),
						magister_grade:$("input[type=checkbox][id=magister_grade]:checked").val()
							
							},
						function (result){
							clean()
							alert("Usuario agregado correctamente")
						}
					);
			})
			
			$("#modificar").on("click",function(){
				console.log($("input[type=checkbox][id=doctor_grade]:checked").val())
				$.post("webservice/person_modify.php",
						{rut:$("#rut").val(),
						name:$("#name").val(),
						last_name_p:$("#last_name_p").val(),
						last_name_m:$("#last_name_m").val(),
						email_i:$("#email_i").val(),
						faculty:$("#faculty").val(),
						position:$("#position").val(),
						profession:$("#profession").val(),
						genre:$("input[type=radio]:checked").val(),
						doctor_grade:$("input[type=checkbox][id=doctor_grade]:checked").val(),
						magister_grade:$("input[type=checkbox][id=magister_grade]:checked").val()
							
							},
						function (result){
							clean()
							alert("Usuario modificado correctamente")
						}
					);
			})
			
			$("#eliminar").on("click",function(){
				var action = confirm('¿Está seguro que desea eliminar este usuario?');
				if (action == true) {
					$.post("webservice/person_remove.php",{rut:$("#run_editar").val()},
					function(result){
						clean()
						alert("Usuario eliminado correctamente")
					}
				)}
			})
			
			$("#run_editar").keypress(function (e) {
				if (e.which == 13) {
					$.post("webservice/person_get.php",{rut:$("#run_editar").val()},
					function(result){
						if (result == 'null'){
							alert("No existe el usuario buscado.")
						}else{
							clean()
							result = JSON.parse(result)
							$("#rut").val(result["rut"])
							$("#name").val(result["name"])
							$("#last_name_p").val(result["last_name_p"])
							$("#last_name_m").val(result["last_name_m"])
							$("#email_i").val(result["email_i"])
							$("#faculty").val(result["faculty"])
							$("#position").val(result["position"])
							$("#profession").val(result["profession"])
							if (result["genre"] == 'M')
								document.getElementById('genre_m').checked = true
							if (result["genre"] == 'F')
								document.getElementById('genre_f').checked = true
							//~ $("input[type=radio][value="+result["genre"]+"]").attr("checked",true)
							if (result["doctor_grade"] != "")
								document.getElementById("doctor_grade").checked = true
							if (result["magister_grade"] != "")
								document.getElementById("magister_grade").checked = true
							$("#eliminar").css("visibility","visible").css("display","inline-block")
							$("#modificar").css("visibility","visible").css("display","inline-block")
						}
					})
				}
			});
			
		});

    </script>
</body>
</html>
