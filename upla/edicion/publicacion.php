<?php
	//Start session
	session_start();
	 
	//Check whether the session variable SESS_MEMBER_ID is present or not
	if(!isset($_SESSION['sess_user_id']) || (trim($_SESSION['sess_user_id']) == '')) {
		header("location: ../login.html");
		exit();
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Ingreso de Datos de Publicaciones para Personas Asociadas</title>
  <link rel="shortcut icon" href="http://www.upla.cl/inclusion/wp-content/uploads/2013/05/favicon.ico" type="image/x-icon">
	<link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/datepicker.css" rel="stylesheet">
    <link href="css/token-input.css" type="text/css" rel="stylesheet">
    <link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css">
    <link href="css/token-input-facebook.css" type="text/css" rel="stylesheet">
    <link href="css/bootstrap-tokenfield.min.css" type="text/css" rel="stylesheet">
    <link href="css/style.css" type="text/css" rel="stylesheet">
    <script src="js/ie-emulation-modes-warning.js"></script>
    <script src="js/ie10-viewport-bug-workaround.js"></script>
	<style type="text/css">
		.table, #isb, #edi, #eds, #importar, #eliminar{display: none;}
	</style>
		
</head>
<body>
	<div class="content">
		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<a id="img-usm" class="navbar-brand" href="/proyecto_upla/">
            <img src="/proyecto_upla/img/logo_upla.png" class="img-responsive" width="120">
					</a>
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<!--<li><a href="/proyecto_upla/upla"><span class="glyphicon glyphicon-home"></span></a></li>-->
						<li><a href="persona.php">Persona</a></li>
						<li><a href="proyecto.php">Proyecto</a></li>
						<li class="active"><a href="publicacion.php">Publicación</a></li>
						<li><a href="congreso.php">Congreso</a></li>
						<li><a href="/proyecto_upla/">Volver</a></li>
						<!--<li><a href="revista.php">Revista</a></li>-->
					</ul>
					<div id="logout"><a href="/proyecto_upla/upla/logout.php">Cerrar Sesion</a></div>
					<!--<div id="logout"><a href="http://fractalnet-works.ddns.net/proyecto_upla/upla/logout.php">Cerrar Sesion</a></div>-->
				</div>
			</div>
		</div>
		<div id="title">
			<div>
				<h1>Publicaciones</h1>
				<p class="lead">Módulo para el ingreso y edición de datos de publicaciones asociadas a investigadores</p>
			</div>
		</div>
		<div class="field">
			<form role="form">
				<div class="row" id="row-participante">
					<h2>Publicaciones registradas en el sistema</h2>
					<font> Conoce las publicaciones registradas de un autor, ingresando su nombre o RUN.</font>
					<div class="col-xs-12">
						<div class="form-group" id="divautor">
							<label for="autor">Autor Registrado</label>
							<input class="form-control" id="autor" placeholder="Ej.: Patricio Vargas">
							<button type="button" class="btn btn-default" id="agrega_autor">
								<span class="glyphicon glyphicon-eye-open"></span> <font> Ver publicaciones ya registradas</font>
							</button>
							<div id="data_result"></div>
						</div>
						<div class="form-group">
							<table class="table" id="tabla_autor_pub">
								<thead>
									<tr>
										<td value="run">RUN</td>
										<td value="ref">Otras publicaciones ya registradas</td>
										<td></td>
									</tr>
								</thead>
								<tbody id="display">
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="row" id="formulario">
					<h2>Carga nuevas Publicaciones</h2>
					<div id="internal">
						<div class="form-group medium">
							<label for="autores">Autor(es) UPLA</label>
							<input class="tokenfield" id="autores" placeholder="Ej.: Patricio Vargas, Eduardo Soto">
<!--
              <font color="red">*Ingrese la lista de autores UPLA en orden de importancia, el primero es el AUTOR PRINCIPAL.<br>*Es recomendable que TODO participante perteneciente a la UPLA sea escogido de la lista desplegable. Si la presona no se encuentra, es necesario registrarla en el ambiente persona.</font>
-->
						</div>
					 
						<div class="form-group group">			  
							<label for="tipo_publicacion">Tipo de publicación</label>
								<select class="form-control short" id="tipo_publicacion" placeholder="Selecciona tipo">
									<option value="Articulo">Artículo</option>
									<option value="Libro">Libro</option>
									<option value="Capitulo">Capítulo de libro</option>
								</select>
						</div>
						
						<div class="form-group group">
							<label for="titulo">Título</label>
							<input type="text" class="form-control medium" id="titulo" placeholder="Ej.: Investigación Aplicada">
						</div>
						
						<div class="form-group group">
							<label for="ano">Año publicación</label>
							<input type="text" class="form-control short" id="ano" placeholder="Ej.: 2010">
						</div>

						<div class="form-group group short" id="rev_prueba">
							<label id="nombre_label" for="revista">Nombre Revista</label>
							<input type="text" class="form-control medium" id="revista" placeholder="Ej.: International Journal of...">
						</div>

						<div class="form-group group" id="vol"> 
							<label for="volumen">Volumen(Número)</label>
							<input type="text" class="form-control tiny" id="volumen" placeholder="Ej.: 2(10)">
						</div>
						<div class="form-group group">
							<label for="paginas">Páginas</label>
							<input type="text" class="form-control tiny" id="paginas" placeholder="Ej.: 47-56">
						</div>
						<div class="form-group group" id="do">
							<label for="doi">DOI</label>
							<input type="text" class="form-control short" id="doi" placeholder="Ej.:">
						</div>
						<div class="form-group group" id="isbnc">
							<label for="isbn">ISBN</label>
							<input type="text" class="form-control short" id="isbn" placeholder="Ej.:">
						</div>
						<div class="form-group group" id="auth">
							<label for="authors">Listado de autores</label>
							<input type="text" class="form-control medium" id="authors" placeholder="Ej.:Perez, J; Fuentes, M;">
						</div>
						<div class="form-group medium" id="kw">
							<label for="keyword">Palabras Clave</label>  (opcional)
							<textarea class="form-control" rows="3" id="keyword" placeholder="Ej.: model, networks"></textarea>
						</div>
						
						<div class="button">
<!--
							<button type="button" class="btn btn-danger" id="eliminar">ELIMINAR</button>
-->
							<button type="button" class="btn btn-default" id="guardar">GUARDAR</button>
						</div>
					</div>
				</div>          
			</form>	
		</div>	
	</div>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.tokeninput.js"></script>
	<script src="js/jquery-ui.min.js"  type="text/javascript"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-tokenfield.min.js" type="text/javascript"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script type="text/javascript">
		$(document).on('ready', function() {
			//~ $('#resource').editableSelect();
			$("#autor").tokenInput("webservice/get_rut2.php",{
				preventDuplicates:true,
				hintText: "Ingrese su búsqueda",
				noResultsText: "No hay resultados",
				resultsLimit: 10,
				tokenLimit: 1,
				
				});
			$("#autores").tokenInput("webservice/get_rut.php",{
				preventDuplicates:true,
				hintText: "Ingrese su búsqueda",
				noResultsText: "No hay resultados",
				resultsLimit: 10,
				theme:"facebook",
				
				});
			
			$("#agrega_autor").on("click",function(){
				ids = $("#autor").tokenInput("get");
				participantes = ""
				for (i in ids){
					participantes = ids[i]["id"]
				}
				
				$.post("webservice/get_publications.php",{id:participantes},
					function(result){
						$("#data_result").empty();
						$("#data_result").append("<ul>")
						result = JSON.parse(result)
						for (i in result){
							$("#data_result").append("<li>"+result[i].title+" ("+result[i].year+")</li>")
						}
						$("#data_result").append("</ul>")
					
					})
			})
			
			
			$("#guardar").on("click",function(){
				ids = $("#autores").tokenInput("get");
				participantes = ""
				for (i in ids){
					participantes += ids[i]["id"]+","
				}
				participantes += ','
				participantes = participantes.replace(',,','')
				$.post("webservice/publication.php",
					{title:$("#titulo").val(),
					 type:$("#tipo_publicacion").val(),
					 year:$("#ano").val(),
					 journal:$("#revista").val(),
					 volume:$("#volumen").val(),
					 page:$("#paginas").val(),
					 doi:$("#doi").val(),
					 isbn:$("#isbn").val(),
					 keywords:$("#keyword").val(),
					 coauthor:$("#authors").val(),
					 participantes:participantes
					},function(result){
						console.log(result)
						alert("Publicación guardada")
						location.reload();
				})
			})
			$("#eliminar").on("click",function(){
				var action = confirm('¿Está seguro que desea eliminar este proyecto?');
				if (action == true) {
					$.post("webservice/project_remove.php",{id:$("#id").val()},
					function(result){
						alert("Proyecto eliminado correctamente")
						location.reload();
					}
				)}
			})
		})
      
    </script>
</body>
</html>
