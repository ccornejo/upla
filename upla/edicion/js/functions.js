// RUTA WEBSERVICES
var urlws = 'webservice/webservice.php';

// ENABLED / DISABLED
function estadoinput (id, estado, value){
  if (value) $('#' + id).val(value);
  $('#' + id).prop('disabled', estado);
}

// LLENA LAS TABLAS HTML
function llenatablatokeninput(arr, tabla, id){
  var id = $("#"+tabla).val();
  if($("#tabla_" + tabla).css("display") == 'none') $("#tabla_" + tabla).css("display", "block");
  var string = $("#tabla_" + tabla + " tbody").html();
  var flag = $("#display tr").size();
  if (flag % 2 == 0)
	string += '<tr class="par">';
  else
    string += '<tr>';
  for(i=0; i<arr.length; i++){
    string += '<td value="' + $("#" + arr[i]).val() + '">' + $("#" + arr[i]).parent().children('ul').children('.token-input-token').children('p').html() + '</td>';
    if($("#" + arr[i]).prop("tagName") == "INPUT"){
      $("#" + arr[i]).html('');
      $("#" + arr[i]).val('');
    }
  }
  if(tabla == 'participante' || tabla == 'autor'){
    string += '<td id="r'+id+'"></td>';
    desplegar_proyectos(id,tabla);
  } 
  string += '<td><span style="color:#B10000;cursor:pointer;" class="glyphicon glyphicon-remove"  onclick="borralinea($(this))"></span></td></tr>';
  $("#tabla_" + tabla + " tbody").html(string);
}
function llenatablatokeninputcheck(arr, tabla){
  var id = $("#autor").val();
  if($("#tabla_" + tabla).css("display") == 'none') $("#tabla_" + tabla).css("display", "block");
  var string = $("#tabla_" + tabla + " tbody").html();
  var flag = $("#display tr").size();
  if (flag % 2 == 0)
	string += '<tr class="par">';
  else
    string += '<tr>';
  for(i=0; i<arr.length; i++){
    var val = $("#" + arr[i]).parent().children('ul').children('.token-input-token').children('p').html();
	string += '<td value="' + $("#" + arr[i]).val() + '">' + val + '</td>';
    if(tabla == 'autor') string += '<td><input type="radio" name="principal" onclick="estadoinput(\'co_a\', true, \'' + val + '\')"></td>';
  }
  if(tabla == 'participante' || tabla == 'autor_pub'){
    string += '<td id="r'+id+'"></td>';
    desplegar_proyectos(id,"autor");
  } 
  string += '<td><span style="color:#B10000;cursor:pointer;" class="glyphicon glyphicon-remove"  onclick="borralinea($(this))"></span></td></tr>';
  $("#tabla_" + tabla + " tbody").html(string);
}

// DESPLEGAR PROYECTOS
function desplegar_proyectos(id,type){
  if (type == "participante"){
	  $.ajax({
		url : "webservice/projects.php",
		type : 'GET',
		data : {"id":id},
		contentType : 'application/json utf-8',
		dataType : 'html',
		success : function(data){        
		  $("#r"+id).html(data);
		},
	  });
  }
  if (type == "autor"){
	  $.ajax({
		url : "webservice/publications.php",
		type : 'GET',
		data : {"id":id},
		contentType : 'application/json utf-8',
		dataType : 'html',
		success : function(data){      
		  $("#r"+id).html(data);
		},
	  });
  }
}
// CREA UN SPAN, A PARTIR DE UN ARRAY, CON LOS CONTENIDOS SEPARDOS POR COMA
function llenaspan(contenedor, contenido){
  var cont = $(contenedor).html();
  if ($(contenedor).html().length == 0) cont = $(contenido).val();
  else cont += ',' + $(contenido).val();
  $(contenedor).html(cont);
}
// RECIBE UNA TABLA HTML RETORNA UN JSON
function tablajson(tabla){
  var arr = new Array();
  for(i=0; i<tabla.children('tbody').children('tr').length; i++){
    var obj = new Object();
    for(j=0; j<tabla.children('tbody').children('tr').eq(i).children('td').length-1; j++){
      obj[tabla.children('thead').children('tr').eq(0).children('td').eq(j).attr('value')] = tabla.children('tbody').children('tr').eq(i).children('td').eq(j).attr("value");
    }
    arr.push(obj);
  }
  return (arr);
}
// RECIBE UNA TABLA HTML RETORNA UN JSON
function tablavaluetojson(tabla){
  var arr = new Array();
  for(i=0; i<tabla.children('tbody').children('tr').length; i++){
    var obj = new Object();
    for(j=0; j<tabla.children('tbody').children('tr').eq(i).children('td').length-1; j++){
      obj[tabla.children('thead').children('tr').eq(0).children('td').eq(j).attr('value')] = tabla.children('tbody').children('tr').eq(i).children('td').eq(j).attr('value');
    }
    arr.push(obj);
  }
  return (arr);
}
// RECIBE UNA TABLA HTML (DE 1 COLUMNA) Y RETORNA UN ARRAY 
function tablatoarray(tabla){
  var arr = new Array();
  for(i=0; i<tabla.children('tbody').children('tr').length; i++){
    arr.push(tabla.children('tbody').children('tr').eq(i).children('td').eq(0).html());
  }
  return (arr);
}
// RECIBE UNA TABLA HTML (DE 1 COLUMNA) Y RETORNA UN ARRAY 
function tablavaluetoarray(tabla){
  var arr = new Array();
  for(i=0; i<tabla.children('tbody').children('tr').length; i++){
    arr.push(tabla.children('tbody').children('tr').eq(i).children('td').eq(0).attr("value"));
  }
  return (arr);
}
// RECIBE UNA TABLA HTML (DE 1 VALUE Y 1 SELECT COLUMNAS) Y RETORNA UNA MATRIZ 
function tablavaluechecktomatrix(tabla){
  var mat = new Array();
  for(i=0; i<tabla.children('tbody').children('tr').length; i++){
    var arr = new Array();
    arr.push(tabla.children('tbody').children('tr').eq(i).children('td').eq(0).attr("value"));
    arr.push(tabla.children('tbody').children('tr').eq(i).children('td').eq(1).children("input").prop("checked") ? 1 : 0);
    mat.push(arr);
  }
  return (mat);
}
// RECIBE UNA TABLA HTML Y RETORNA UN STRING CON LOS VALORES, SEPARADOS POR COMA
function tablatostring(tabla){
  var str = "";
  for(i=0; i<tabla.children('tbody').children('tr').length; i++){
    str += tabla.children('tbody').children('tr').eq(i).children('td').eq(0).html();
    if(i != tabla.children('tbody').children('tr').length - 1) str += ',';
  }
  return (str);
}
// RECIBE UN JSON [{CAMPO1: , CAMPO...},{},{}..] Y RETORNA UNA MATRIZ [[CAMPO1, CAMPO2,..],[]..]
function jsontomatrix(jsondatos){
  var matriz = new Array();
  for(i=0; i<jsondatos.length; i++){
    var a = jsondatos[i];
    var fila = new Array();
    for (j in a){
      fila.push(a[j]);
    }
    matriz.push(fila);
  }
  return matriz;
}
// CONVIERTE UN ARRAY EN UNA MATRIZ CON LOS MISMOS VALORES
function arraytomatrix(arr){ 
  var matriz = new Array();
  if (arr){
    for(i=0; i<arr.length; i++){
      var fila = new Array();
      fila.push(arr[i]);
      matriz.push(fila);
    }
  }
  return matriz;
}
// RECIBE UN BOOLEANO Y RETORNA UN ENTERO
function booltoint(arg){
  if(arg == true) return 1;
  else return 0;
}
// GUARDA PARTICIPANTE (PROYECTO)
function guardaparticipante(arr, id, tabla){
  var pop = arr.pop();
  var metodo = 'add_data_to_table';
  var arg1 = ['person_id', 'project_id'];
  var arg2 = [pop, id];
  var arg = [tabla, arg1, arg2];
  invocar_webservice(metodo, arg, function(data){
    if(arr.length > 0) guardaparticipante(arr, id, tabla);
  });
}
// GUARDA UNO O MAS REGISTROS EN UNA TABLA
function guardatabla(columns, values, tabla, ids, callback){
  var retorno = new Object();
  if (values.length > 0){
    var pop = values.pop();
    var metodo = 'add_data_to_table';
    var arg1 = columns;
    var arg2 = pop;
    var arg = [tabla, arg1, arg2];
    invocar_webservice(metodo, arg, function(data){
      if(data.status != 1){
        retorno["status"] = -1;
        retorno["result"] = data;
        callback(retorno);
      }
      else{
        ids.push(data.id);
        if (values.length > 0) {
          guardatabla(columns, values, tabla, ids, callback);
        }
        else {
          retorno["status"] = 1;
          retorno["id"] = ids;
          retorno["result"] = data;
          callback(retorno);
        }
      }
    });
  }
  else {
    retorno["status"] = 0;
    retorno["result"] = 'NO HAY DATOS';
    callback(retorno);
  }
}
// GUARDA UNO O MAS REGISTROS EN UNA TABLA
function guardasubtabla(id, columns, values, tabla, callback){
  var retorno = new Object();
  if (values.length > 0){
    var pop = values.pop();
    pop.push(id);
    var metodo = 'add_data_to_table';
    var arg1 = columns;
    var arg2 = pop;
    var arg = [tabla, arg1, arg2];
    invocar_webservice(metodo, arg, function(data){
      if(data.status != 1){
        retorno["status"] = -1;
        retorno["result"] = data;
        callback(retorno);
      }
      else{
        if (values.length > 0){
          guardasubtabla(id, columns, values, tabla, callback);
        }
        else {
          retorno["status"] = 1;
          retorno["result"] = data;
          callback(retorno);
        }
      }
    });
  }
  else {
    retorno["status"] = 0;
    retorno["result"] = 'NO HAY DATOS';
    callback(retorno);
  }
}
// LEER REGISTRO DE UNA TABLA A PARTIR DE UN ARRAY DE ID'S
function leertabla(arr, tabla, out, callback){
  var retorno = new Object();
  if (arr.length > 0){
    var pop = arr.pop();
    var metodo = 'read_table';
    var arg = [tabla, pop];
    invocar_webservice(metodo, arg, function(data){
      if(data.status != 1){
        retorno["status"] = -1;
        retorno["result"] = data;
        return(retorno);
      }
      else{
        out.push(data.result);
        if (arr.length > 0){
          leertabla(arr, tabla, out, callback);
        }
        else {
          retorno["status"] = 1;
          retorno["result"] = out;
          callback(retorno);
        }
      }
    });
  }
  else {
    retorno["status"] = 0;
    retorno["message"] = 'NO HAY DATOS';
    retorno["result"] = [];
    callback(retorno);
  }
}
// ELIMINAR RELACIONES ENTRE TABLAS (PERSONAS)
function eliminarrelaciones(id, arr, callback){
  var retorno = new Object();
  if(arr.length>0){
    var pop = arr.pop();
    var metodo = 'delete_table';
    var columns = new Array('person_id');
    var values = new Array(id.toString());
    var arg = [pop, columns, values];
    invocar_webservice(metodo, arg, function(data){
      if(data.status != 1){
        retorno["status"] = -1;
        retorno["result"] = data;
        return(retorno);
      }
      else{
        if(arr.length>0){
          eliminarrelaciones(id, arr, callback)
        }
        else {
          retorno["status"] = 1;
          retorno["result"] = id;
          callback(retorno);
        };
      }
    });
  }
  else {
    retorno["status"] = 0;
    retorno["result"] = 'NO HAY DATOS';
    callback(retorno);
  }
}
// ELIMINAR UNA RELACIÓN
function eliminarrelacion(id, person, tabla, callback){
  var retorno = new Object();
  var metodo = 'delete_table';
  var columns = new Array('person_id', tabla + '_id');
  var values = new Array(person, id);
  var arg = ['person_has_' + tabla, columns, values];
  invocar_webservice(metodo, arg, function(data){
    if(data.status != 1){
      retorno["status"] = -1;
      retorno["result"] = data;
      callback(retorno);
    }
    else{
      retorno["status"] = 1;
      retorno["result"] = "Eliminado con éxito";
      callback(retorno);
    }
  });
}
// ELIMINAR UN CONJUNTO DE ELEMENTOS (POR ID) DE UNA TABLA
function eliminardetabla(tabla, arr, callback){
  var retorno = new Object();
  if(arr.length>0){
    var pop = arr.pop();
    var metodo = 'delete_table';
    var columns = new Array('id');
    var values = new Array(pop.toString());
    var arg = [tabla, columns, values];
    invocar_webservice(metodo, arg, function(data){
      if(data.status != 1){
        retorno["status"] = -1;
        retorno["result"] = data;
        return(retorno);
      }
      else{
        if(arr.length>0){
          eliminardetabla(tabla, arr, callback);
        }
        else {
          retorno["status"] = 1;
          retorno["result"] = "FILA ELIMINADA";
          callback(retorno);
        };
      }
    });
  }
  else {
    retorno["status"] = 0;
    retorno["result"] = 'NO HAY DATOS';
    callback(retorno);
  }
}
// ELIMINAR VARIOS ELEMENTOS DE VARIAS TABLAS (PERSONAS)
function eliminartablas (arr, mat, callback){
  var retorno = new Object();
  if(arr.length>0){
    var tabla = arr.pop();
    var ids = mat.pop();
    eliminardetabla(tabla, ids, function(data){
      if(data.status == -1){
        retorno["status"] = -1;
        retorno["result"] = data;
        callback(retorno);
      }
      else{
        if(arr.length>0){
          eliminartablas(arr, mat, callback);
        }
        else {
          retorno["status"] = 1;
          retorno["result"] = "TABLAS ELIMINADAS";
          callback(retorno);
        };
      }
    });
  }
  else {
    retorno["status"] = 0;
    retorno["result"] = 'NO HAY DATOS';
    callback(retorno);
  }
}
// INVOCA WEBSERVICES
function invocar_webservice(metodo, arg, callback){
  var data = {method : metodo, arguments: arg};        
  $.ajax({
    url : urlws,
    type : 'POST',
    data : JSON.stringify(data),
    contentType : 'application/json utf-8',
    dataType : 'json',
    success : function(result){     
      callback(result);
    },
    error: function (xhr, ajaxOptions, thrownError) {
      callback('error: ' + thrownError);
    }               
  });
}
// AGREGA UNA REVISTA SI ES QUE NO EXISTE Y DEVUELVE EL ID, ADEMÁS RECARGA DEL JSON DE REVISTAS
function id_revista(id, callback){
  if($("#" + id).tokenfield('getTokens').length > 0){
   // 100662 
    var aux = $("#" + id).tokenfield('getTokens');
    var val = aux[0]["value"];
    var nom = aux[0]["label"];
    if (val == nom){
      var metodo = 'add_data_to_table';
      var tabla = 'journal';
      var arg1 = ["name"];
      var arg2 = [val];
      var arg = [tabla, arg1, arg2];
      invocar_webservice(metodo, arg, function(data){
        if(data.status == -1){
          callback(-1);
        }
        else {
          var idout = data["id"];
          callback(idout);
        }
      });
    }
    else{
      callback(val);
    } 
  }
  else {
    callback('100662')
  }
}
// AGREGAUNIVERSIDAD SI ES QUE NO EXISTE Y RETORNA EL ID
function id_u(id, callback){
  var aux = $("#" + id).tokenfield('getTokens');
  var val = aux[0]["value"];
  var nom = aux[0]["label"];
  if (val == nom){
    var metodo = 'add_data_to_table';
    var tabla = 'institution';
    var arg1 = ["name"];
    var arg2 = [val];
    var arg = [tabla, arg1, arg2];
    invocar_webservice(metodo, arg, function(data){
      if(data.status == -1){
        callback(-1);
      }
      else {
        var idout = data["id"];
        callback(idout);
      }
    });
  }
  else{
    callback(val);
  } 
}
// AGREGA PERSONAS SI ES QUE NO EXISTEN Y DEVUELVE EL ID, ADEMÁS RECARGA DEL JSON DE PERSONAS
function id_person(jsonin, jsonout, callback){
  var per = jsonin.pop();
  var ca = jsonin.length == 0 ? 1 : 0;
  var val = per.value;
  var nom = per.label;
  var arr = Array();
  if (val == nom){
    var metodo = 'add_data_to_table';
    var tabla = 'person';
    var arg1 = ["name"];
    var arg2 = [val];
    var arg = [tabla, arg1, arg2];
    invocar_webservice(metodo, arg, function(data){
      if(data.status == -1){
        callback(-1);
      }
      else {
        arr.push(data["id"]);
        arr.push(ca);
        jsonout.push(arr);
        if(jsonin.length > 0){
          id_person(jsonin, jsonout, callback);
        }
        else{
          callback(jsonout);
        }
      }
    });
  }
  else{
    arr.push(val);
    arr.push(ca);
    jsonout.push(arr);
    if(jsonin.length > 0){
      id_person(jsonin, jsonout, callback);
    }
    else{
      callback(jsonout);
    }
  } 
}
function token_input_to_array(jsond){
  var out = new Array();
  for(i=0; i < jsond.length; i++){
    out.push(jsond[i].name);
  }
  return out;
}
// BORRA UNA LINEA DE UNA TABLA HTML
function borralinea(esto){
  if(esto.parent().parent().parent().children("tr").length > 0){
    esto.parent().parent().remove();
  }
}
// BUSCA UNA UNIVERSIDAD EN UN JSON Y RETORNA EL OBJETO CORRESPONDIENTE AL ID
function busca_u(jsonb, id){
  if(jsonb){
    for(b=0; b<jsonb.length; b++){
      if(jsonb[b]){
        if(jsonb[b].value == id){
          return jsonb[b];
        }  
      }
    }
  }
  return null;
}
// ACTIVA YEARPICKER'S
function ano_datepicker(){
  $(".year_picker").prop("readonly", true);
  $(".year_picker").css("cursor", "pointer");
  $(".year_picker").datepicker({
    format: " yyyy",
    viewMode: "years", 
    autoclose: true,
    minViewMode: "years"
  });
}
// ACTIVA TOKENFIELD DE UNIVERSIDADES
function universidad(){
  $.getJSON( "/proyecto_upla/upla/data/ues_val_label.json", function( data ) {
    jsonu = data;
    $('.universidad').tokenfield({
      limit: 1,
      createTokensOnBlur: true,
      autocomplete: {
        source: jsonu,
        delay: 100,
        minLength: 1
      },
      showAutocompleteOnFocus: true
    });
  });
}
// ACTIVA TOKENFIELD DE ESTUDIANTES
function estudiante(){
  $('.estudiante').tokenfield({
    createTokensOnBlur: true,
    autocomplete: {
      delay: 100,
      source: [],
      minLength: 1
    },
    showAutocompleteOnFocus: true
  });
}
// MUESTRA TÍTULO O GRADO SEGÚN CORRESPONDA EN CADA FILA (FORMACIÓN ACADÉMICA)
function tipo_grado(){
  $(".es_titulo").click(function(){
    $(this).parent().parent().parent().parent().find(".titulo").css("display", "block");
    $(this).parent().parent().parent().parent().find(".grado").css("display", "none");
  });
  $(".es_grado").click(function(){
    $(this).parent().parent().parent().parent().find(".titulo").css("display", "none");
    $(this).parent().parent().parent().parent().find(".grado").css("display", "block");
  });
}
function tipo_grado_edit(){
  if($(".es_titulo").prop("checked")){
    $(this).parent().parent().parent().parent().find(".titulo").css("display", "block");
    $(this).parent().parent().parent().parent().find(".grado").css("display", "none");
  }
  else{
    $(this).parent().parent().parent().parent().find(".titulo").css("display", "none");
    $(this).parent().parent().parent().parent().find(".grado").css("display", "block");    
  }
}
// AGREGA UNA FILA A UNA TABLA DE INGRESO DE DATOS HTML
function construye_tr(clases, ph){
  var tr = "<tr>";
  for(i=0; i<clases.length; i++){
    tr += "<td>";
    if(clases[i] == "tipo"){
      var lg = $("#tabla_grado").children("tr").length;
      tr += "<div class='radio tipo'><label><input type='radio' class='es_titulo' name='tipo" + lg + "' value='0' checked = 'true'>Título</label><br><label><input type='radio' class='es_grado' name='tipo" + lg + "' value='1'>Grado</label></div>"
    }
    else if(clases[i] == "titulo_grado"){
      tr += "<input class='form-control titulo' >";
      tr += "<select class='form-control grado'><option value='magister'>Magister</option><option value='doctorado'>Doctorado</option></select>";
    }
    else if(clases[i] == "max"){
      tr += "<input type='radio' name='max' class='max'>";
    }
    else if(clases[i] == "quita"){
      tr += "<button type='button' class='btn btn-default " + clases[i] + "' onclick='borralinea($(this))'><span class='glyphicon glyphicon-remove'></span></button>";
    }
    else if(clases[i] == "ano"){
      tr += '<input type="text" class="form-control ' + clases[i] + ' year_picker" >';
    }
    else if(clases[i] == "universidad"){
      tr += '<input type="text" class="tokenfield ' + clases[i] + '">';
    }
    else if(clases[i] == "participacion"){
      var lp = $("#tabla_programa").children("tr").length;
      tr += '<div class="radio-inline">';
      tr += '<input type="radio" name="participacion' + lp + '" class="claustro" value="claustro" checked>Claustro';
      tr += '</div>';
      tr += '<div class="radio-inline">';
      tr += '<input type="radio" name="participacion' + lp + '" class="colaborador" value="colaborador">Colaborador';
      tr += '</div>';
      tr += '<div class="radio-inline">';
      tr += '<input type="radio" name="participacion' + lp + '" class="visita" value="visita">Visitante';
      tr += '</div>';
    }
    else if(clases[i] == "programa"){
      tr += '<select class="form-control ' + clases[i] + '">';
      tr += '<option value="1">Doctorado en Biotecnología</option>';
      tr += '<option value="2">Doctorado en Ciencias, mención Física</option>';
      tr += '<option value="3">Doctorado en Ciencias, mención Química</option>';
      tr += '<option value="4">Doctorado en Matemática</option>';
      tr += '<option value="5">Doctorado en Ingeniería Electrónica</option>';
      tr += '<option value="6">Doctorado en Ingeniería Informática</option>';
      tr += '<option value="7">Doctorado en Ingeniería Química</option>';
      tr += '<option value="8">Magíster en Ciencias de la Ingeniería Civil</option>';
      tr += '<option value="9">Magíster en Ciencias de la Ingeniería Eléctrica</option>';
      tr += '<option value="10">Magíster en Ciencias de la Ingeniería  Electrónica</option>';
      tr += '<option value="11">Magíster en Ciencias de la Ingeniería Informática</option>';
      tr += '<option value="12">Magíster en Ciencias de la Ingeniería Mecánica</option>';
      tr += '<option value="13">Magíster en Ciencias de la Ingeniería Química</option>';
      tr += '<option value="14">Magíster en Ciencias de la Ingeniería Industrial</option>';
      tr += '<option value="15">Magíster en Ciencias de la Ingeniería Telemática</option>';
      tr += '<option value="16">Magíster en Ciencias de la Ingeniería Metalúrgica</option>';
      tr += '<option value="17">Magíster en Ciencias, mención Física</option>';
      tr += '<option value="18">Magíster en Ciencias, mención Matemática</option>';
      tr += '<option value="19">Magíster en Ciencias, mención Química</option>';
      tr += '<option value="20">Magíster en Redes y Telecomunicaciones</option>';
      tr += '<option value="21">Magíster en Economía Energética</option>';
      tr += '<option value="22">MBA-Magíster en Gestión Empresarial </option>';
      tr += '<option value="23">Magíster en Ingeniería Aeronáutica</option>';
      tr += '<option value="24">Magíster en Innovación Tecnológica y Emprendimiento</option>';
      tr += '<option value="25">Magíster en Gestión de Activos y Mantenimiento</option>';
      tr += '<option value="26">Magíster en Tecnologías de la Información</option>';
      tr += '<option value="27">Magíster en Gestión y Tecnología Agronómica</option>';
      tr += '</select>';
    }
    else if(clases[i] == "tipo_tesis"){
      tr += '<select class="form-control ' + clases[i] + '">';
      tr += '<option value="pregrado">Pregrado</option>';
      tr += '<option value="magister">Magíster</option>';
      tr += '<option value="doctorado">Doctorado</option>';
      tr += '</select>';
    }
    else if(clases[i] == "participacion_tesis"){
      tr += '<select class="form-control participacion_tesis">';
      tr += '<option value="Director">Director</option>';
      tr += '<option value="Co-Director">Co-Director</option>';
      tr += '</select>';
    }
    else if(clases[i] == "estado"){
      tr += '<select class="form-control ' + clases[i] + '" >';
      tr += '<option value="Solicitada">Solicitada</option>';
      tr += '<option value="Registrada">Registrada</option>';
      tr += '</select>';
    }
    else if(clases[i] == "quartile"){
      tr += '<select class="form-control quartile">';
      tr += '<option value="Q1">Q1</option>';
      tr += '<option value="Q2">Q2</option>';
      tr += '<option value="Q3">Q3</option>';
      tr += '<option value="Q4">Q4</option>';
      tr += '</select>';
    }
    else{
      tr += '<input class="form-control ' + clases[i] + '" >';
    }
    tr+= "</td>";
  }
  tr += "</tr>";
  return tr;
}
// AGREGA UNA FILA A UNA TABLA DE INGRESO DE DATOS HTML
function json_to_tr(tabla, clases, valores){
  for(i=0; i<clases.length; i++){
    var tog = 0;
    if(clases[i] == "tipo"){
      tog = valores[i];
      if(valores[i] == 1){
        $("#" + tabla).children("tr").last().find("." + clases[i]).find(".es_grado").prop("checked", true);
      }
      else{
        $("#" + tabla).children("tr").last().find("." + clases[i]).find(".es_titulo").prop("checked", true);
      }
      tipo_grado();
    }
    else if(clases[i] == "titulo_grado"){
      if(tog == 1){
        $("#" + tabla).children("tr").last().find(".grado").val(valores[i]);
      }
      else{
        $("#" + tabla).children("tr").last().find(".titulo").val(valores[i]);
      }
    }
    else if(clases[i] == "max"){
      if(valores[i] == 1) $("#" + tabla).children("tr").last().find("." + clases[i]).prop("checked", true);
    }
    else if(clases[i] == "universidad"){
      var id = valores[i];
      $("#" + tabla).children("tr").last().find(".universidad").tokenfield({
        limit: 1,
        createTokensOnBlur: true,
        autocomplete: {
          source: jsonu,
          delay: 100,
          minLength: 1
        },
        showAutocompleteOnFocus: true
      });
      var inst = busca_u(jsonu, id);
      if(inst) $("#" + tabla).children("tr").last().find(".universidad").tokenfield("setTokens", [inst]);  
      $("#" + tabla).children("tr").last().find(".token").attr("title", inst.label);
    }
    else if(clases[i] == "participacion"){
      if(valores[i] == "claustro") $("#" + tabla).children("tr").last().find(".claustro").prop("checked", true);
      else if(valores[i] == "colaborador") $("#" + tabla).children("tr").last().find(".colaborador").prop("checked", true);
      else if(valores[i] == "visita") $("#" + tabla).children("tr").last().find(".visita").prop("checked", true);
    }
    else{
      $("#" + tabla).children("tr").last().find("." + clases[i]).val(valores[i]);
    }
  }
}
// LEE UNA TABLA DE DATOS HTML Y RETORNA LOS DATOS EN FORMATO JSON
function tr_to_json(tabla, clases){
  var jsonout = new Array();

  for(i=0; i < $("#" + tabla).children("tr").length; i++){
    var jsonin = new Object();
    for(j=0; j < clases.length; j++){
      if(clases[j] == "tipo"){
        if($("#" + tabla).children("tr").eq(i).find(".es_grado").prop("checked")){
          jsonin[clases[j]] = 1;
        }
        else{
          jsonin[clases[j]] = 0;
        }
      }
      else if(clases[j] == "titulo_grado"){
        if($("#" + tabla).children("tr").eq(i).find(".es_grado").prop("checked")){
          jsonin[clases[j]] = $("#" + tabla).children("tr").eq(i).find(".grado").val();
        }
        else{
          jsonin[clases[j]] = $("#" + tabla).children("tr").eq(i).find(".titulo").val();
        }
      }
      else if(clases[j] == "max"){  
        if($("#" + tabla).children("tr").eq(i).find(".max").prop("checked") == true){
          jsonin[clases[j]] = 1;
        }
        else{
          jsonin[clases[j]] = 0;
        }
      }
      else if(clases[j] == "participacion"){
        if($("#" + tabla).children("tr").eq(i).find(".claustro").prop("checked")){
          jsonin[clases[j]] = "claustro";
        }
        else if($("#" + tabla).children("tr").eq(i).find(".colaborador").prop("checked")){
          jsonin[clases[j]] = "colaborador";
        }
        else if($("#" + tabla).children("tr").eq(i).find(".visita").prop("checked")){
          jsonin[clases[j]] = "visita";
        } 
      }
      else{
        jsonin[clases[j]] = $("#" + tabla).children("tr").eq(i).find("." + clases[j]).eq(0).val().trim();
      }
    }
    jsonout.push(jsonin);
  }
  return jsonout
}

