﻿<?php
	//Start session
	session_start();
	 
	//Check whether the session variable SESS_MEMBER_ID is present or not
	if(!isset($_SESSION['sess_user_id']) || (trim($_SESSION['sess_user_id']) == '')) {
		header("location: ../login.html");
		exit();
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Ingreso de Datos de Congresos</title>
  <link rel="shortcut icon" href="http://www.upla.cl/inclusion/wp-content/uploads/2013/05/favicon.ico" type="image/x-icon">
	<link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/datepicker.css" rel="stylesheet">
    <link href="css/token-input.css" type="text/css" rel="stylesheet">
    <link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap-tokenfield.min.css" type="text/css" rel="stylesheet">
    <link href="css/style.css" type="text/css" rel="stylesheet">
    <script src="js/ie-emulation-modes-warning.js"></script>
    <link href="css/token-input-facebook.css" type="text/css" rel="stylesheet">
    <script src="js/ie10-viewport-bug-workaround.js"></script>
	<style type="text/css">
		.table, #isb, #edi, #eds, #importar, #eliminar{display: none;}
    .group{
      vertical-align: top;
    }
	</style>	
		
		
		
</head>
<body>
	<div class="content">
		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<a id="img-usm" class="navbar-brand" href="/proyecto_upla/">
            <img src="/proyecto_upla/img/logo_upla.png" class="img-responsive" width="120">
					</a>
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<!--<li><a href="/proyecto_upla/upla"><span class="glyphicon glyphicon-home"></span></a></li>-->
						<li><a href="persona.php">Persona</a></li>
						<li><a href="proyecto.php">Proyecto</a></li>
						<li><a href="publicacion.php">Publicación</a></li>
						<li class="active"><a href="congreso.php">Congreso</a></li>
            <li><a href="/proyecto_upla/">Volver</a></li>
						<!--<li><a href="patente.php">Patente</a></li>
						<li><a href="revista.php">Revista</a></li>-->
					</ul>
					<div id="logout"><a href="/proyecto_upla/upla/logout.php">Cerrar Sesion</a></div>
					<!--<div id="logout"><a href="http://fractanet-works.ddns.net/proyecto_upla/upla/logout.php">Cerrar Sesion</a></div>-->
				</div>
			</div>
		</div>
		<div id="title">
			<div>
				<h1>Congresos</h1>
				<p class="lead">Módulo para el ingreso de datos sobre Congresos</p> 
			</div>
		</div>
		<div class="field">
			
			<form role="form">
				<div class="row">
					<h2>Datos Congreso</h2>
					<!-- <div class="person">
						<div>
							<font>Para cargar datos ya registrados de una revista y editarlos, ingresa su <b>NOMBRE</b></font>
							<input type="text" class="form-control" id="id" placeholder="Ej.: NatGeo">
							*Si la revista no está en la lista, ingresa sus datos manualmente.
						</div>
					</div> -->

					<div class="form-group group">
						<label for="titulo">Título de la Ponencia</label>
						<input type="text" class="form-control short-medium" id="titulo" placeholder="Introduce título de la ponencia">
					</div>
					<div class="form-group group">
						<label for="congreso">Nombre del Congreso</label>
						<input type="text" class="form-control short-medium" id="congreso" placeholder="Introduce nombre del congreso">
					</div>
					<div></div>
					<div class="form-group group">
						<label for="ano">Año </label>
						<input type="text" class="form-control short" id="ano">
					</div>
					<div class="form-group group medium">
						<label for="investigador">Investigador(es) Asociados UPLA</label>
						<input type="text" class="tokenfield medium" id="investigador">
<!--
            <font color="red">*Es recomendable que TODO participante perteneciente a la UPLA sea escogido de la lista desplegable. Si la presona no se encuentra, es necesario registrarla en el ambiente persona.</font>
-->
					</div>
					<div></div>
					<div class="button">
						<button type="button" class="btn btn-default" id="guardar">GUARDAR</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.tokeninput.js"></script>
	<script src="js/jquery-ui.min.js"  type="text/javascript"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-tokenfield.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/functions.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script type="text/javascript">
		$(document).on('ready', function() {
			//~ $('#resource').editableSelect();
			$("#investigador").tokenInput("webservice/get_rut.php",{
				preventDuplicates:true,
				hintText: "Ingrese su búsqueda",
				noResultsText: "No hay resultados",
				resultsLimit: 10,
				theme:"facebook",
				
				});
			$("#guardar").on("click",function(){
				ids = $("#investigador").tokenInput("get");
				participantes = ""
				for (i in ids){
					participantes += ids[i]["id"]+","
				}
				participantes += ','
				participantes = participantes.replace(',,','')
				$.post("webservice/congress.php",
					{type:$("#titulo").val(),
					 title:$("#congreso").val(),
					 year:$("#ano").val(),
					 participantes:participantes
					},function(result){
						alert("Proyecto guardado")
						location.reload();
				})
			})
			$("#eliminar").on("click",function(){
				var action = confirm('¿Está seguro que desea eliminar este proyecto?');
				if (action == true) {
					$.post("webservice/project_remove.php",{id:$("#id").val()},
					function(result){
						alert("Proyecto eliminado correctamente")
						location.reload();
					}
				)}
			})
		})
      
    </script>
</body>
</html>
