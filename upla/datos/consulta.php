﻿<?php
//Start session
session_start();
 
//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['sess_user_id']) || (trim($_SESSION['sess_user_id']) == '')) {
	header("location: http://fractalnet-works/proyecto_upla/upla/login.html");
	exit();
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Interfaz de Consultas</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/datepicker.css" rel="stylesheet">
    <link href="css/token-input.css" type="text/css" rel="stylesheet">
    <link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap-tokenfield.min.css" type="text/css" rel="stylesheet">
    <link href="css/style.css" type="text/css" rel="stylesheet">
    <script src="js/ie-emulation-modes-warning.js"></script>
    <script src="js/ie10-viewport-bug-workaround.js"></script>
	<style type="text/css">
		#exportar, #quart, #exp{display: none;}
	</style>
</head>
<body>
	<div class="content">
		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="container">
				<div class="navbar-header">
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a href="/proyecto_upla/upla"><span class="glyphicon glyphicon-home"></span></a></li>
						<!-- <li><a href="ficha.html">Ficha Docente</a></li> -->
						<!-- <li><a href="tabla.html">Tabla Resumen</a></li> -->
						<li class="active"><a href="consulta.php">Interfaz de Consulta</a></li>
					</ul>
					<div id="logout"><a href="http://fractalnet-works.ddns.net/proyecto_upla/upla/logout.php">Cerrar Sesion</a></div>
				</div>
			</div>
		</div>
		<div id="title" class="q">
			<div>
				<h1>Interfaz de Consulta</h1>
				<p class="lead">En este módulo puedes relizar consultas a la BD.</p>
			</div>
		</div>
		<div class="field">
			<div class="form-group short">
				<label for="tabla">Tabla de Consulta</label>
				<select class="form-control" id="tabla" placeholder="Selecciona">
<!--
					<option value="congress">Congreso</option>
					<option value="project">Proyecto</option>
-->
					<option value="publication">Publicación</option>
				</select>					
			</div>
			<div id="filter"><h3>FILTRAR POR:</h3></div>
			<div class="form-group short-medium group">
				<label for="departamento">Departamento</label>
				<select class="form-control" id="departamento">
					<option value="'Alumno','Alumno Postgrado','Arte','CEA','Ciencias','Contraloría Interna','Cs. Actividad Física','Cs. Salud','Cs. Sociales','Ed. Física','Educación','Humanidades','Ingeniería','Red Externa','San Felipe'">Todos</option>
					<option value="'Alumno'">Alumno</option>
					<option value="'Alumno Postgrado'">Alumno Postgrado</option>
					<option value="'Arte'">Arte</option>
					<option value="'CEA'">CEA</option>
					<option value="'Ciencias'">Ciencias</option>
					<option value="'Contraloría Interna'">Contraloría Interna</option>
					<option value="'Cs. Actividad Física'">Cs. Actividad Física</option>
					<option value="'Cs. Salud'">Cs. Salud</option>
					<option value="'Cs. Sociales'">Cs. Sociales</option>
					<option value="'Ed. Física'">Ed. Física</option>
					<option value="'Educación'">Educación</option>
					<option value="'Humanidades'">Humanidades</option>
					<option value="'Ingeniería'">Ingeniería</option>
					<option value="'Red Externa'">Red Externa</option>
					<option value="'San Felipe'">San Felipe</option>
				</select>				
			</div>
			<div class="form-group short group">
				<div class="form-group short group"><label for="ano">Año </label><input type="text" class="form-control" id="ano"></div>
			</div>
<!--
			<div class="form-group short group">
				<div class="form-group short group" id="quart">
					<label for="cuartil">Cuartil</label>
					<select class="form-control" id="cuartil">
						<option value="Q0">Todos</option>
						<option value="Q1">Q1</option>
						<option value="Q2">Q2</option>
						<option value="Q3">Q3</option>
						<option value="Q4">Q4</option>
					</select>
				</div>				
			</div>
-->

			<div class="form-group">
				<button type="button" class="btn btn-default word-export" id="buscar">
					<span >BUSCAR</span>
				</button>
				<a id="dlink"  style="display:none;"></a>
			</div>
			
			
			<div id="exp">
				<table class="table" id="tabla_exp">
					<thead id="head">
						<tr><th>Publicación</th><th>Tipo</th><th>Año</th><th>Revista</th><th>Indexación</th><th>Autor Principal</th><th>Otros Autores</th></tr>
					</thead>
					<tbody id="body">

					</tbody>
				</table>
			</div>
			<div class="form-group">
				<button type="button" class="btn btn-default word-export" id="exportar">
					<span >EXPORTAR</span>
				</button>
				<a id="dlink"  style="display:none;"></a>
			</div>
		
	</div>
	<script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.tokeninput.js" type="text/javascript"></script>
    <script src="js/FileSaver.js"></script>
    <script src="js/jquery.wordexport.js"></script>
	<script src="js/jquery-ui.min.js"  type="text/javascript"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			//~ $("#title").css("background-image","url('./img/query.png')")
			$("#ano").css("cursor","pointer");
			$("#ano").prop("readonly", true);
			$("#ano").datepicker({
			  format: " yyyy",
			  viewMode: "years", 
			  minViewMode: "years",
			  autoclose: true
			});

			$("#buscar").click(function(){
				tabla = $("#tabla").val();
				departamento = $("#departamento").val();
				ano = $("#ano").val().trim();
				cuartil = $("#cuartil").val();
				$.ajax({	
					url : "webservice/consulta.php?t="+tabla+"&d="+departamento+"&a="+ano,
					success : function(ret){
						var a = eval(ret);
						var b = '';
						for(i=0; i<a.length; i++){
							b += '<tr>';
							for(j=0; j<a[i].length; j++){
								b += '<td>' + a[i][j].replace(/,/g,", ") + '</td>'
							}
							b += '</tr>';
						}
						$("#body").html(b);
						$("#exp").css("display","block");
						$("#exportar").css("display","block");
					}
				});
			});
		});
		
		$("#tabla").on("change", function(v){
			if($(this).val() == "publication"){
				$("#quart").css("display","block");
				$("#head").html("<tr><th>Publicación</th><th>Tipo</th><th>Año</th><th>Revista</th><th>Indexación</th><th>Autor Principal</th><th>Otros Autores</th></tr>");
				$("#body").empty();
				$("#exp").css("display","none");
				$("#exportar").css("display","none");
			}
			else if($(this).val() == "congress"){
				$("#quart").css("display","none");
				$("#head").html("<tr><th>Título</th><th>Congreso</th><th>Año</th><th>Participantes</th></tr>");
				$("#body").empty();
				$("#exp").css("display","none");
				$("#exportar").css("display","none");
			} 
			else if($(this).val() == "project"){
				$("#quart").css("display","none");
				$("#head").html("<tr><th>Título</th><th>Tipo</th><th>Subtipo</th><th>Año</th><th>Participantes</th></tr>");
				$("#body").empty();
				$("#exp").css("display","none");
				$("#exportar").css("display","none");
			} 
		});
	   
		$("#exportar").click(function(event) {
			tableToExcel('tabla_exp', 'tabla resumen', 'tabla_consulta.xls')
		}); 

		var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    	})()
    </script>
</body>
</html>
