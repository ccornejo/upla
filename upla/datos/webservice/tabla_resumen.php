<?
header('Content-Type: text/html; charset=UTF-8');
$_mysql = "localhost;root;complexity*-*;upla";
	$mysql_input = explode(";",$_mysql);
	$con = mysqli_connect($mysql_input[0],$mysql_input[1],$mysql_input[2],$mysql_input[3]);
	mysqli_set_charset($con, "utf8");
	$id = $_GET["id"];
	$prog = $_GET["p"];
	$AUT = array();
	$ids = array();
	$query = 'SELECT p.id,p.name,p.last_name, phpg.year FROM person as p JOIN person_has_program as phpg ON phpg.person_id = p.id AND phpg.program_id = '.$prog.' WHERE p.id IN ('.$id.') GROUP BY p.id';
	$res = mysqli_query($con,$query);
	
	while($row = mysqli_fetch_array($res)){
		$name = $row["name"]." ".$row["last_name"];
		if (array_key_exists($row["id"],$ids) == false){
			$pers = array("id" => $row["id"],"name" => $name,"date" => $row["year"],"isi" => 0,"nisi" => 0,"fondef" => 0,"fondecyt" => 0,"fondecyt_r" => 0,"other" => 0);
			array_push($ids, $row["id"]);
			$AUT[$row["id"]] = $pers;
		}
	}
	
	$query = 'SELECT p.id, count(*) as count FROM person as p JOIN person_has_publication as phpub ON p.id = phpub.person_id JOIN publication as pub ON pub.id = phpub.publication_id LEFT JOIN journal j on j.id = pub.journal_id WHERE p.id IN ('.$id.') AND (j.type = 1 OR j.type = 3) GROUP BY p.id';
	$res = mysqli_query($con,$query);
	
	while($row = mysqli_fetch_array($res)){
		if (array_key_exists($row["id"],$AUT) == true)
			$AUT[$row["id"]]["isi"] = $row["count"];
	}

	$query = 'SELECT p.id, count(*) as count FROM person as p JOIN person_has_publication as phpub ON p.id = phpub.person_id JOIN publication as pub ON pub.id = phpub.publication_id LEFT JOIN journal j on j.id = pub.journal_id WHERE p.id IN ('.$id.') AND (j.type = 0 OR j.type IS NULL) GROUP BY p.id';
	$res = mysqli_query($con,$query);
	
	while($row = mysqli_fetch_array($res)){
		if (array_key_exists($row["id"],$AUT) == true)
			$AUT[$row["id"]]["nisi"] = $row["count"];
	}

	$query = 'SELECT p.id, count(*) as count FROM person as p, project as pr, person_has_project as phpr WHERE p.id = phpr.person_id AND p.id IN ('.$id.') AND pr.id = phpr.project_id AND pr.type = "Fondecyt" GROUP BY p.id';
	$res = mysqli_query($con,$query);
	
	while($row = mysqli_fetch_array($res)){
		if (array_key_exists($row["id"],$AUT) == true)
			$AUT[$row["id"]]["fondecyt"] = $row["count"];
	}
	
	$query = 'SELECT p.id, count(*) as count FROM person as p, project as pr, person_has_project as phpr WHERE p.id = phpr.person_id AND p.id IN ('.$id.') AND pr.id = phpr.project_id AND pr.type = "Fondef" GROUP BY p.id';
	$res = mysqli_query($con,$query);
	
	while($row = mysqli_fetch_array($res)){
		if (array_key_exists($row["id"],$AUT) == true)
			$AUT[$row["id"]]["fondef"] = $row["count"];
	}
	$query = 'SELECT p.id, count(*) as count FROM person as p, project as pr, person_has_project as phpr WHERE p.id = phpr.person_id AND p.id IN ('.$id.') AND pr.id = phpr.project_id AND pr.type = "Fondecyt" AND  pr.participation = "responsable" GROUP BY p.id';
	$res = mysqli_query($con,$query);
	
	while($row = mysqli_fetch_array($res)){
		if (array_key_exists($row["id"],$AUT) == true)
			$AUT[$row["id"]]["fondecyt_r"] = $row["count"];
	}
	$query = 'SELECT p.id, count(*) as count FROM person as p, project as pr, person_has_project as phpr WHERE p.id = phpr.person_id AND p.id IN ('.$id.') AND pr.id = phpr.project_id AND pr.type NOT IN ("Fondef","Fondecyt") GROUP BY p.id';
	$res = mysqli_query($con,$query);
	
	while($row = mysqli_fetch_array($res)){
		if (array_key_exists($row["id"],$AUT) == true)
			$AUT[$row["id"]]["other"] = $row["count"];
	}
	
	$retorno = utf8_encode((string)json_encode($AUT));

	echo $retorno;

?>
