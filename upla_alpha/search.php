<?
	$id = $_GET["id"];
	$user = $_GET["user"];
	$comu = $_GET["comu"];
?>
<!doctype html>
<html>
	<head>
		<meta charset="utf-8">

		<title>MAPA DE RELACIONES</title>

		<!--<link rel="stylesheet" href="/proyecto_upla/css/bootstrap.min.css">-->
		<link rel="stylesheet" href="css/test.css">
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="css/jquery-ui.css" />
		<link rel="stylesheet" href="css/token-input.css" />
		<link rel="stylesheet" type="text/css" href="css/tooltipster.css" />
		<link rel="stylesheet" href="/proyecto_upla/css/catalogo.css">
		<link rel="stylesheet" href="css/upla.css">		
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui.js"></script>
		<script type="text/javascript" src="js/jquery.tokeninput.js"></script>
		<script type="text/javascript" src="js/jquery.tooltipster.min.js"></script>
		<script type="text/javascript" src="js/d3.v3.min.js"></script>
		<script type="text/javascript" src="js/functions.js"></script>
		
		<link rel="shortcut icon" href="http://www.upla.cl/inclusion/wp-content/uploads/2013/05/favicon.ico" type="image/x-icon">

	</head>
	<body>
		<!-- titulo -->
		<?php include '../header.php';?>



		<!-- barra ubicacion -->
		<?php include '../breadcrumb.php'; ?>			

		<div class="main">
			<div id="network">
				<div id="node_search">
					<img src="css/images/node_search.png"/><input type="text" id="search-network" name="search" placeholder="Buscar nueva entidad">
				</div>			
				<div id="controls">
					<a class="icon" href="javascript:void(0)" onclick="resetNetwork();"><img src="css/images/refresh.png"/></a>
					<a class="icon" href="javascript:void(0)" onclick="zooming(0);"><img src="css/images/zoomin.png"/></a>
					<a class="icon" href="javascript:void(0)" onclick="zooming(1);"><img src="css/images/zoomout.png"/></a>
				</div>
			</div>

			<div class="options">
				<div class="opt_button" id="help_button"><img src="css/images/help.png"/></div>
				<div class="opt_button" id="resume_button"><img src="css/images/resume.png"/></div>
				<div class="otp_button" id="close"><a href="javascript:void(0)"><img src="css/images/close.png"/></a></div>
			</div>
			
			<div class="info">
				<div class="superior_menu">
					<div class="opt_menu" id="resume_button2">TU BÚSQUEDA</div>
					<div class="opt_menu" id="filter_button">FILTROS</div>
				</div>
				<div class="scrollbar" id="style-4">
					<div id="bio">
						<div id="helper"><a href="javascript:window_hash('#h1');"><img src="css/images/help2.png"/></a></div>
						<div class="title" id="name"></div>
						<div class="title2" id="comu_name"></div>
						<div class="title2" id="email"></div>
						
						
						<div class="info2" id="country"></div>
						<div class="info2" id="genre"></div>
						<div class="info2" id="position"></div>
						<div class="info2" id="faculty"></div>
						<div class="info2" id="profession"></div>
						<div class="info2" id="doctor_grade"></div>
						<div class="info2" id="magister_grade"></div>
						<div style="display:inline-block;margin-bottom:150px;" id ="chart">
							<div style="margin-left:20px;" id="piechart"></div>
							<div style="margin-left:220px;margin-top:-170px;" id="legend2"></div>
						</div>
						
						<div id="node_bio">
							<div class="title2">Publicaciones</div>
							<div id="pubs"></div>

						</div>
						<div id="rank">
							<div class="title2">Vinculaciones Directas</div>
							<div id="rel"></div>
						</div>					
					</div>


					<div id="filters">
						<div id="helper"><a href="javascript:window_hash('#h5');"><img src="css/images/help2.png"/></a></div>
						<div id="node_filter">							
							<div class="title">Nodos</div>
							<div class="content">
								<input type="checkbox" name="node_filter" value=1 /> Alumno<br>
								<input type="checkbox" name="node_filter" value=2 /> Alumno Postgrado<br>
								<input type="checkbox" name="node_filter" value=3 /> Arte<br>
								<input type="checkbox" name="node_filter" value=4 /> CEA<br>
								<input type="checkbox" name="node_filter" value=5 /> Ciencias<br>
								<input type="checkbox" name="node_filter" value=6 /> Contraloría Interna<br>
								<input type="checkbox" name="node_filter" value=8 /> Cs. Actividad Física<br>
								<input type="checkbox" name="node_filter" value=9 /> Cs. Salud<br>
								<input type="checkbox" name="node_filter" value=10 /> Cs. Sociales<br>
								<input type="checkbox" name="node_filter" value=11 /> Ed. Física<br>
								<input type="checkbox" name="node_filter" value=12 /> Educación<br>
								<input type="checkbox" name="node_filter" value=13 /> Humanidades<br>
								<input type="checkbox" name="node_filter" value=14 /> Ingeniería<br>
								<input type="checkbox" name="node_filter" value=16 /> San Felipe<br>
								<input type="checkbox" name="node_filter" value=15 /> Red Externa<br>
								<input type="checkbox" name="node_filter" value=28 /> Red Externa Alumno<br>
								<input type="checkbox" name="node_filter" value=27 /> Red Externa Alumno Postgrado<br>
								<input type="checkbox" name="node_filter" value=17 /> Red Externa Arte<br>
								<input type="checkbox" name="node_filter" value=18 /> Red Externa CEA<br>
								<input type="checkbox" name="node_filter" value=19 /> Red Externa Ciencias<br>
								<input type="checkbox" name="node_filter" value=20 /> Red Externa Contraloría Interna<br>
								<input type="checkbox" name="node_filter" value=21 /> Red Externa Cs. Actividad Física<br>
								<input type="checkbox" name="node_filter" value=22 /> Red Externa Cs. Salud<br>
								<input type="checkbox" name="node_filter" value=23 /> Red Externa Cs. Sociales<br>
								<input type="checkbox" name="node_filter" value=24 /> Red Externa Ed. Física<br>
								<input type="checkbox" name="node_filter" value=25 /> Red Externa Educación<br>
								<input type="checkbox" name="node_filter" value=26 /> Red Externa Humanidades<br>
								<input type="checkbox" name="node_filter" value=27 /> Red Externa San Felipe<br>
								<div class="select"><a id="selectAll2" href="javascript:void(0)">SELECCIONAR TODO</a> | <a id="unselectAll2" href="javascript:void(0)">DESELECCIONAR TODO</a></div>
							</div>
						</div>
						<div id="filter">
							<div class="title">Enlaces</div>
							<div class="content">
								<input type="checkbox" name="filter" value=2010 /> 2010<br>
								<input type="checkbox" name="filter" value=2011 /> 2011<br>
								<input type="checkbox" name="filter" value=2012 /> 2012<br>
								<input type="checkbox" name="filter" value=2013 /> 2013<br>
								<input type="checkbox" name="filter" value=2014 /> 2014<br>
								<input type="checkbox" name="filter" value='aceptada' /> Aceptada<br>
								<input type="checkbox" name="filter" value='enviada' /> Enviada<br>
								<div class="select"><a id="selectAll" href="javascript:void(0)">SELECCIONAR TODO</a> | <a id="unselectAll" href="javascript:void(0)">DESELECCIONAR TODO</a></div>
							</div>
						</div>
					</div>
					<div id="help">					
						<div id="h1" class="title">¿Cómo funciona?</div>
						<div class="content">
							<!--
							Esta red corresponde al mapa de relaciones de co-autoría de artículos científicos entre investigadores de la UPLA e investigadores externos. Al abrir la red por primera vez ésta muestra grupos de investigadores agrupados en un nodo comunidad que representa la facultad o centro al que pertenecen. Para desplegar su contenido y ver las relaciones en su interior, es necesario hacer doble click en ese nodo (lo mismo para cerrarla). Pincha en cada nodo-investigador para ver datos suyos (e.j. cargo en la Universidad,  e-mail de contacto, etc.) así como sus colaboradores directos de la Universidad y externos.
							<h3>Lectura</h3>
							En esta visualización de relaciones entre entidades encontrarás los siguientes iconos:
							<ul>
								<li><img src="css/images/person2.png"/> Personas</li>
								<li><img src="css/images/org2.png"/> Organizaciones</li>
								<li><img src="css/images/emp2.png"/> Empresas</li>
								<li><img src="css/images/com2.png"/> Comunidades</li>
							</ul>
							-->
							El tamaño de los nodos equivale a la recurrencia de interacción de los investigadores entre si.
							<img id="node_description_img" src="css/images/node_description.png"/><br>
						 </div>
						
						<div id="h2" class="title">Tu Búsqueda</div>
						<div class="content">Muestra información específica del investigador seleccionado (click nodo). Puedes buscar un investigador dentro de la red usando el buscador ubicado en la parte superior izquierda de la página.</div>
						  
						
						
					</div>
				</div>		
			</div>
			
<!--
			<div id="networks">Powered by <a href="http://www.net-works.cl" target="_blank">Net-Works</a></div>
-->
			<div id="legend">
<!--
				<div class="legend size" id="size_l"><img src="css/images/size.png"/> Nivel de conectividad</div>
-->
				<span class="legend" id="person"><img src="css/images/person.png"/> Investigador</span>
				<span class="legend" id="organization"><img src="css/images/org.png"/> Centro o Facultad</span>				
			</div>
		</div>
		<div id='user_id' style="display:none:visibility:hidden"><? echo $id; ?></div>
		<div id='comu_name' style="display:none:visibility:hidden"><? echo $comu; ?></div>
		<div id='user_name' style="display:none:visibility:hidden"><? echo $user; ?></div>
		<script type="text/javascript" src="js/piechart.js"></script>
		<script type="text/javascript" src="js/network.js"></script>
	</body>
</html>
