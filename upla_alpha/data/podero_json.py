# -*- coding: utf-8 -*- 

import json
import io
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

topo_json=open('poderopedia_E.json')

data = json.load(topo_json)

for i in range(len(data['nodes'])):
	
	if "'id': u'P" in str(data['nodes'][i]):
		data['nodes'][i]['id_P'] = data['nodes'][i]['id']
	if  "'id': u'O" in str(data['nodes'][i]):
		data['nodes'][i]['id_O'] = data['nodes'][i]['id']
	data['nodes'][i]['id'] = i
	if data['nodes'][i]['shortBio'] == None:
		data['nodes'][i]['shortBio'] = ""
	data['nodes'][i]['shortBio'] = data['nodes'][i]['shortBio'].replace("\r\n","")
	data['nodes'][i]['shortBio'] = data['nodes'][i]['shortBio'].replace("\r","")
	data['nodes'][i]['shortBio'] = data['nodes'][i]['shortBio'].replace("\n","")
	data['nodes'][i]['shortBio'] = data['nodes'][i]['shortBio'].replace("\\","")
	data['nodes'][i]['shortBio'] = data['nodes'][i]['shortBio'].replace('"',"'")
with io.open('poderopedia_D.json', 'w', encoding='utf-8') as f:
	f.write(unicode(json.dumps(data, ensure_ascii=False)))
