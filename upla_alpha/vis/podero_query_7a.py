# -*- coding: utf-8 -*- 

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import simps, trapz
from scipy.stats import kurtosis
import networkx as nx
import json
import codecs
import sys
import re
import md5
import codecs
from partition import modularity,best_partition,partition_at_level,generate_dendogram

def net_first(query):
	for i in range(len(data['nodes'])):
		dato=str(data['nodes'][i]['alias'])
		matchObj = re.search( r"%s" % query, dato, re.M|re.I)
		if matchObj:
			ide=int(data['nodes'][i]['id'])
			#print str(data['nodes'][i]['id']),str(data['nodes'][i]['alias'])
			g.add_node(data['nodes'][i]['id'],deep='0')
			ides.append(ide)			

def net_deep(tipo,gr):
	tipo = str(tipo)	
	for j in ides:		
		for i in range(len(data['links'])):
			if int(j) == int(data['links'][i]['source']) or int(j) == (data['links'][i]['target']):
				
				source=int(data['links'][i]['source'])
				target=int(data['links'][i]['target'])
				
				if source not in nonodos2 and target not in nonodos2:
					if source not in ides_d:
						ides_d.append(source)
					if target not in ides_d:
						ides_d.append(target)
					if (int(tipo) > 3):
						if source not in g.nodes():
							notread.append(source)
						if target not in g.nodes():
							notread.append(target)
					if source not in g.nodes() and source not in ides:
						g.add_node(source,deep=tipo)
						gr += 1
					if target not in g.nodes() and target not in ides:
						g.add_node(target,deep=tipo)
						gr += 1
					g.add_edge(source,target)

	### OBTENGO DISTRIBUCION DE GRADO Y KURTOSIS 
	k_degrees = g.degree() # dictionary node:degree
	k_values = sorted(set(k_degrees.values()))
	k_hist = [k_degrees.values().count(x) for x in k_values]
	
	np_k_hist = np.array(k_hist)
	kurt_k=kurtosis(np_k_hist)

	kurt.append(kurt_k)
	clus.append(nx.average_clustering(g))
	dens.append(nx.density(g))

	for y in ides_d:
		ides.append(y)
	grow.append(gr)
	return gr

def make_edges():
	k = 0
	for j in ides:
		for i in range(len(data['links'])):			
			if j == data['links'][i]['source'] or j == data['links'][i]['target']:
				if data['links'][i]['source'] in ides and data['links'][i]['target']in ides:				
					g.add_edge(int(data['links'][i]['source']),int(data['links'][i]['target']))
					k += 1
	return k

def replace_links(i,datos,rep_string,familia):
	try:
		if data['links'][i][rep_string]:
			if rep_string == 'Familia':
				familia += 1
			datos=datos.replace("{",'{"relation": "'+rep_string+'", "relation_detail": "'+str(data['links'][i][rep_string])+'",')
	except:
		pass
	return [datos,familia]

def replace_edges(i,datos,familia):
	edges = ['Conocidos','Amigos',u'Compañeros de Estudios',u'Cónyuge o Pareja',u'Partidos Políticos','Think Tanks o centros de estudios','Comercial','Dependencia','Donaciones','Propiedad','Subvenciones','Familia','Estudios',u'Cargos Públicos','Cargos en ONG',u'Participación en Empresas','Cargos en Empresas','Grupos religiosos',u'Grupos de apoyo a campaña política',u'Partidos Políticos']
	for edge in edges:
		[datos,familia] = replace_links(i,datos,edge,familia)
	return [datos,familia]


def deep_score(g):
	deeps=nx.get_node_attributes(g,'deep')
	return deeps

def layer():
	for n in g.nodes():
		for i in range(len(capa)):
			if int(deeps[n]) == i+1:
				capa[i].append(n)
	org=0
	peop=0
	for c in range(len(capa)):
		print len(capa[c])
		for n in capa[c]:
			for i in range(len(data['nodes'])):
				if n==data['nodes'][i]['id']:
					if "id_O" in data['nodes'][i]:
						org=org+1
					if "id_P" in data['nodes'][i]:
						peop=peop+1
		grow_o.append(org)
		grow_p.append(peop)

def systemload():		
	reload(sys)
	m = md5.new()
	sys.setdefaultencoding('utf-8')
	query=str(sys.argv[1])
	m.update(query)
	nonodos=str(sys.argv[2])
	nonodos=nonodos.split(",")
	nonodos2=[]
	for n in nonodos:
		n=n.replace("'","")
		n=int(n)
		nonodos2.append(n)
	return [query,nonodos,nonodos2,m]

def area_grow(dx_var):
	y = np.array(grow)

	# Compute the area using the composite trapezoidal rule.
	#area = trapz(y, dx=dx_var)
	area = trapz(y)
	print "area grow=", area

	y = np.array(grow_o)

	# Compute the area using the composite trapezoidal rule.
	#area = trapz(y, dx=dx_var)
	area = trapz(y)
	print "area grow_o=", area

	y = np.array(grow_p)

	# Compute the area using the composite trapezoidal rule.
	#area = trapz(y, dx=dx_var)
	area = trapz(y)
	print "area grow_p=", area

def fileopen(m):
	todo=open("../json/"+str(m.hexdigest())+".json","w")
	#f=open("../temp/queryB.json","w")
	#htmlp=open("../temp/rankp.json","w")
	#htmlo=open("../temp/ranko.json","w")
	#datox=open("data_"+str(query)+".csv","w")
	topo_json=open('../data/poderopedia_D.json')
	data = json.load(topo_json)
	return [todo,topo_json,data]

def replace_char(datos):
	datos=datos.replace("None",'""')
	datos=datos.replace("False",'0')
	datos=datos.replace("True",'1')
	return datos
	
def poder_dict(key):
	if key == 'Cargos en Empresas' or key == 'Comercial' or key == 'Propiedad' or key == u'Participación en Empresas':
		if not j in Poder_eco:
			Poder_eco[j] = 1
		else:
			Poder_eco[j] += 1
	if key == u'Cargos Públicos' or key == u'Partidos Políticos':
		if not j in Poder_poli:
			Poder_poli[j] = 1
		else:
			Poder_poli[j] += 1
	if key == 'Grupos religiosos':
		if not j in Poder_reli:
			Poder_reli[j] = 1
		else:
			Poder_reli[j] += 1
	if key == 'Familia':
		if not j in Poder_fami:
			Poder_fami[j] = 1
		else:
			Poder_fami[j] += 1
	

def htmlo_write(query,text):
	#htmlo.write("[")
	#htmlo.write(text)
	#htmlo.write("]")
	
	todo.write('"ranko":[')
	todo.write(text)
	todo.write("],")
	
def htmlp_write(query,text,familia,g):	
	
	#htmlp.write("[")
	#htmlp.write(text)
	#htmlp.write("]")
	
	todo.write('"rankp":[')
	todo.write(text)
	todo.write("]")

def resumen(query,g,familia,grow,fam_pow,eco_pow,pol_pow,rel_pow,clus_pow,sb):	
	entidad=str(query)
	num_enti=len(g.nodes())
	num_rel=len(g.edges())
	texto=' {"entidad":"'+str(entidad)+'","shortBio":"'+str(sb)+'","fam_pow":'+str(round((float(float(fam_pow)/float(grow[0])))*100,1))+',"eco_pow":'+str(round((float(float(eco_pow)/float(grow[0])))*100,1))+',"pol_pow":'+str(round((float(float(pol_pow)/float(grow[0])))*100,1))+',"rel_pow":'+str(round((float(float(rel_pow)/float(grow[0])))*100,1))+',"cohesion":'+str(round((clus_pow)*100,1))+''
	#,""+str(entidad)+"",',"num_enti":',str(num_enti)+"",',"num_rel":',str(num_rel),"}"
	print "texto: ",texto
	todo.write(',"resumen":[')
	todo.write(texto)
	todo.write("}]}")


def statistics(g,query):
	totalnodos = len(g.nodes())
	print "totalnodos ",totalnodos
	grow_norm = [(float(float(x)/float(totalnodos))*100) for x in grow]
	print grow_norm
	plt.title(""+str(query)+"")
	plt.plot(grow_norm,'b-',marker='o')
	plt.ylabel("% nodos")
	plt.xlabel("capas")
	plt.savefig("data_"+str(query)+"_norm.png")
	print "Organizaciones: ",grow_o
	print "Personas: ",grow_p
	print "Clustering: ",clus
	print "Densidad: ",dens
	print "Kurtosis: ",kurt
	datox.write("capa,grow,growO,growP,clus\n")
	for d in range(len(grow)):
		datox.write(str(d+1)+","+str(grow[d])+","+str(grow_o[d])+","+str(grow_p[d])+","+str(clus[d])+"\n")
	

if __name__ == '__main__':
	
	########INICIALIZAR VARIABLES########
	g = nx.Graph()
	grow=[]
	notread = []
	ides=[]
	ides_d=[]
	datos = ""
	clus=[]
	kurt=[]
	dens=[]
	gr=0
	
	#####################################
	
	[query,nonodos,nonodos2,m] = systemload()
	[todo,topo_json,data] = fileopen(m)
	##f\.write('{ \n  "links": [ \n')
	todo.write('{ \n  "links": [ \n')
	
	net_first(query)
	
	######PROFUNDIDAD########
	for deep in range(4):
		gr = net_deep(deep+1,gr)
		if deep == 2:
			dendo = generate_dendogram(g)
			level = len(dendo) - 1
	print grow
	
	ides = set(ides)
	notread = set(notread)
			
	##### CALCULO DE PROFUNDIDAD
	deeps = deep_score(g)
	
	##### COMUNIDADES
	Poder_eco = {}
	Poder_poli = {}
	Poder_reli = {}
	Poder_fami = {}
	
	ides2 = []
	ides3 = []
	familia=0
	fam_pow=0
	eco_pow=0
	pol_pow=0
	rel_pow=0
	clus_pow=0
	for j in ides:
		for i in range(len(data['links'])):
			aux = ""
			if j == data['links'][i]['source'] or j == data['links'][i]['target']:
				if data['links'][i]['source'] in ides and data['links'][i]['target'] in ides: 
					ids=int(data['links'][i]['source'])
					idt=int(data['links'][i]['target'])
					ides2.append(ids)
					ides2.append(idt)
					if (ids not in notread) and (idt not in notread):
						ides3.append(ids)
						ides3.append(idt)		
						aux += '{'
						for key in data['links'][i]:
							if key in ["source","target","isPast","weight"]:
								aux += '"'+key+'":'+''+str(data['links'][i][key])+','
							else:
								aux += '"'+key+'":'+'"'+unicode(data['links'][i][key])+'",'
							## GENERO DICCIONARIOS PODER
							poder_dict(key)
						aux += '}'
						aux = aux.replace(',}','}')
						[aux,familia] = replace_edges(i,aux,familia)					
						datos += aux
						datos += ",\n"
						
	datos += "\n"
	datos = datos.replace(",\n\n","\n  ],\n")
	datos = replace_char(datos)
	##f\.write(datos)
	todo.write(datos)
	datos = ""
	############## HAGO NODOS ###############

	bb=nx.betweenness_centrality(g)
	try:
		pr=nx.pagerank(g,alpha=0.85)
	except:
		pr=nx.betweenness_centrality(g)
	nx.set_node_attributes(g,'bet',bb)
	nx.set_node_attributes(g,'page',pr)
		
	ides2 = set(ides2)
	ides3 = set(ides3)
	#f.write('"nodes": [ \n')
	todo.write('"nodes": [ \n')
	#### GENERO DICCIONARIO PARA NODOS
	nodes = {}
	for i in range(len(data['nodes'])):
		nodes[data['nodes'][i]['id']] = i
		if data['nodes'][i]['alias'] == query:
			query_id = data['nodes'][i]['id']
	#### GENERO DICCIONARIO PARA DENDOGRAMA
	dendogram = {}
	for id_nodo,comunidad in partition_at_level(dendo, level).iteritems():
		dendogram[id_nodo] = {}
		dendogram[id_nodo]["comu"] = comunidad
		dendogram[id_nodo]["degree"] = g.degree(id_nodo)
	query_comu = dendogram[query_id]["comu"]
	com = {}
	for i in dendogram:
		if dendogram[i]["comu"] in com:
			if dendogram[i]["degree"] > com[dendogram[i]["comu"]]["degree"]:
				com[dendogram[i]["comu"]]["degree"] = dendogram[i]["degree"]
				com[dendogram[i]["comu"]]["nombre"] = i
		if not dendogram[i]["comu"] in com:
			com[dendogram[i]["comu"]] = {}
			com[dendogram[i]["comu"]]["degree"] = dendogram[i]["degree"]
			com[dendogram[i]["comu"]]["nombre"] = i
	com[query_comu]["nombre"] = query_id
	
	###### GENERO NODOS

	htmlo_text = ""
	htmlp_text = ""
	nodos = ""
	id_q=0
	sb=""
	for j in ides2:
		i = nodes[j]		
		aux = '{'
		for key in data['nodes'][i]:
			if key in ["type","degree","bet","page","group","id"]:
				aux += '"'+key+'":'+''+str(data['nodes'][i][key])+','
			if key in ["alias","id_P","shortBio","countryOfResidence","tipo"]:
				aux += '"'+key+'":'+'"'+unicode(data['nodes'][i][key])+'",'
		aux += '}'
		aux = aux.replace(',}','}')
		datos = aux
		try:
			comu = dendogram[j]["comu"]
		except:
			comu = 100
		try:
			pe = Poder_eco[j]
		except:
			pe = 0
		try:
			pp = Poder_poli[j]
		except:
			pp = 0
		try:
			pr = Poder_reli[j]
		except:
			pr = 0
		try:
			pf = Poder_fami[j]
			if str(query)==str(data['nodes'][i]["alias"]):
				#print "ayyyyayayay"
				id_q=data['nodes'][i]["id"]
				sb=str(data['nodes'][i]["shortBio"])
		except:
			pf = 0
		datos=datos.replace("None",'""')
		datos=datos.replace('"alias"','"name"')
		deg_j=g.degree(j)
		pr_j=g.node[j]['page']
		bet_j=g.node[j]['bet']
		deep_j=deeps[j]
		importancia_j=float(float(pr_j)/(float(deep_j)+1))
			
		#dat_rank="{"+'"Nombre":"'+data['nodes'][i]['alias']+'", "Comunidad":'+str(comu)+', "Profundidad":'+str(deep_j)+''+',"Conectividad":'+str(deg_j)+',"PageRank":'+str(pr_j)+''+',"Betweenness":'+str(bet_j)+',"Importancia":'+str(importancia_j)+',"Política":'+str(pp)+',"Económica":'+str(pe)+',"Religiosa":'+str(pr)+',"Familia":'+str(pf)+',"Importancia2":'+str(float(importancia_j*(pp+pe+pr+pf)))+"}"
		
		dat_rank="{"+'"Nombre":"'+data['nodes'][i]['alias']+'", "ShortBio":"'+data['nodes'][i]['shortBio']+'", "group":"'+str(comu)+'", "Importancia":'+str(float(importancia_j*(pp+pe+pr+pf)))+"}"
		
		
		#html2_text += dat_rank
		try:
			comu_name = data['nodes'][com[comu]["nombre"]]["alias"]
		except:
			comu_name = "None"
		if 'id_P' in data['nodes'][i]:

			datos=datos.replace("{",'{"type":0, "degree":'+str(g.degree(j))+', "comu_name": "'+str(comu_name)+'", "bet":'+str(g.node[j]['bet'])+', "page":'+str(g.node[j]['page'])+', "group":'+str(comu)+', "poder_eco":'+str(pe)+', "poder_poli":'+str(pp)+', "poder_reli":'+str(pr)+', "poder_fami":'+str(pf)+', "Importancia2":'+str(float(importancia_j*(pp+pe+pr+pf)))+', ')
		if 'id_O' in data['nodes'][i]:
			datos=datos.replace("{",'{"type":1, "degree":'+str(g.degree(j))+', "comu_name": "'+str(comu_name)+'", "bet":'+str(g.node[j]['bet'])+', "page":'+str(g.node[j]['page'])+', "group":'+str(comu)+', "poder_eco":'+str(pe)+', "poder_poli":'+str(pp)+', "poder_reli":'+str(pr)+', "poder_fami":'+str(pf)+', "Importancia2":'+str(float(importancia_j*(pp+pe+pr+pf)))+', ')

		if "id_P" in data['nodes'][i]:
			htmlp_text += dat_rank
			htmlp_text += ",\n"
		
		if "id_O" in data['nodes'][i]:
			htmlo_text += dat_rank
			htmlo_text += ",\n"
		if j in ides3:
			nodos += datos
			nodos +=",\n"
		#html2_text += ",\n"	

	try:
		fam_pow=Poder_fami[id_q]
	except:
		fam_pow
	try:
		eco_pow=Poder_eco[id_q]
	except:
		eco_pow
	try:
		pol_pow=Poder_poli[id_q]
	except:
		pol_pow
	try:
		rel_pow=Poder_reli[id_q]
	except:
		rel_pow

	try:
		clus_pow=nx.clustering(g,id_q)
	except:
		clus_pow=0
	print "clus_pow",clus_pow

	htmlp_text += "\n"
	htmlo_text += "\n"
	nodos += "\n"
	htmlp_text = htmlp_text.replace(",\n\n","")
	htmlo_text = htmlo_text.replace(",\n\n","")
	nodos = nodos.replace(",\n\n","\n  ]\n }")
	nodos_todos = nodos.replace("\n  ]\n }","\n  ],\n ")
	
	
	##### ESCRIBO ARCHIVOS 
	##f.write(nodos)
	todo.write(nodos_todos)
	
	htmlo_write(query,htmlo_text)
	htmlp_write(query,htmlp_text,familia,g)
	resumen(query,g,familia,grow,fam_pow,eco_pow,pol_pow,rel_pow,clus_pow,sb)
	
	
	#### GENERO ESTADISTICAS
	capa=[[],[],[],[],[],[]]
	grow_o=[]
	grow_p=[]
	layer()
	#statistics(g,query)
	#area_grow(5)
	print str(m.hexdigest())
