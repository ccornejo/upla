var entidad;
var lastlink = "";
var lasttypes = "";
var lasttypet = "";
var lastnode = "";
var lastcolor = "";
var lastsearch = "";

function toTitleCase(str)
{
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}
function bio_start(){
	console.log(11)
	$("#bio #name").text($("#user_name").text())
	$("#bio #comu_name").text($("#comu_name").text())
	$.ajax({
		type: "GET",
		url: "webservice.php?id="+$("#user_id").text(),
		success: function(data){
			$("#pubs").empty()
			$("#pubs").append(toTitleCase(data))
		}
	});
	$.ajax({
		type: "GET",
		url: "webservice2.php?id="+$("#user_id").text(),
		success: function(data){
			$("#rel").empty()
			$("#rel").append(toTitleCase(data))
			$("#rel li").on("mouseover",function(){
				id_pub = $(this).attr('name')
				$("#pubs li[name="+id_pub+"]").css("color","#CCC")
				$('line').each(function(d){
					edge = $(this).attr('edge').split(",")
					s =$(this).attr('source')
					t = $(this).attr('target')
					if($.inArray(id_pub,edge) != -1){
						$(this).css("stroke","red")
						$("#"+s).css("fill","red")
						$("#"+t).css("fill","red")
					}
				})
			})
			$("#rel li").on("mouseout",function(){
				//~ id_pub = $(this).attr('name')
				$("#pubs li").css("color","#FFF")
				$('line').css("stroke","#333")
				$("path").each(function(){$(this).css("fill",$(this).attr("color"))})
				
			}) 
			
		}
	});
	
	$.post( "webservice3.php", {id:$("#user_id").text()},function(data) {
	  json = $.parseJSON(data)
	  if (json.last_name_m == null)
		json.last_name_m = ""
	  $("#bio #name").text(toTitleCase(json.name+" "+json.last_name_p+" "+json.last_name_m))
	  $("#email").empty()
	  $("#email").append("<a href=mailto:"+String(json["email_i"]).toLowerCase()+">"+String(json["email_i"]).toLowerCase()+"</a>")
	  $("#country").empty()
	  $("#country").append("<b>Nacionalidad:</b> "+toTitleCase(json["country"]))
	  $("#genre").empty()
	  genero = json["genre"]
	  if (json["genre"] == "F")
		genero = "Femenino"
	  if (json["genre"] == "M")
		genero = "Masculino"
	  $("#genre").append("<b>Sexo:</b> "+genero)
	  $("#position").empty()
	  $("#position").append("<b>Cargo:</b> "+toTitleCase((json["position"])))
	  $("#faculty").empty()
	  $("#faculty").append("<b>Facultad:</b> "+toTitleCase(json["faculty"]))
	  profession = json["profession"]
	  if (json["profession"] == null)
		profession = "Desconocido"
	  $("#profession").empty()
	  $("#profession").append("<b>Profesión:</b> "+toTitleCase(profession))
	  doctor_grade = json["doctor_grade"]
	  if (json["doctor_grade"] == "Doctor")
		doctor_grade = "Si"
	  else
	    doctor_grade = "No"
	  
	  $("#doctor_grade").empty()
	  $("#doctor_grade").append("<b>Grado de Doctor:</b> "+doctor_grade)
	  magister_grade = json["magister_grade"]
	  if (json["magister_grade"] == "Doctor")
		magister_grade = "Si"
	  else
	    magister_grade = "No"
	  
	  $("#magister_grade").empty()
	  $("#magister_grade").append("<b>Grado de Magister:</b> "+magister_grade)
	  })
	
	$.post( "webservice4.php", {id:$("#user_id").text()},function(data) {				
		piechart($.parseJSON(data))				
		
	})
}

function window_hash(id){
	help_display()
	window.location.href = id
}

function display(opt1,opt2){
	if (opt2 == "#bio")
		opt3 = "#resume_button2"
	if (opt2 == "#filters")
		opt3 = "#filter_button"
	if (opt2 == "#ranking")
		opt3 = "#ranking_button"
	if (opt2 == "#paths")
		opt3 = "#path_button"
	$(".options").css("right","400px")
	//$("#close").css("right","400px")
	$(".opt_menu").css("border-bottom","none")
	$("svg").width($( window ).width())
	$(".info").show()
	$(".social").show()
	$("#close").show()
	$("#bio").hide()
	$("#ranking").hide()
	$("#paths").hide()
	$("#help").hide()
	$("#filters").hide()
	$(opt2).show()
	$(".superior_menu").show()
	$(opt3).css("border-bottom","2px solid #FFF")
	$(".scrollbar").height($( window ).height()-150)
	
}

function help_display(){
	
	$(".options").css("right","400px")
	$(".superior_menu").hide()
	$(".scrollbar").height($( window ).height()-150)
	//$("#close").css("right","400px")
	$("svg").width($( window ).width())
	$(".info").show()
	$("#help").show()
	$("#close").show()
	$("#ranking").hide()
	$("#paths").hide()
	$("#bio").hide()
	$("#filters").hide()
	$(".social").show()
	$("#help_button").css("background-color","#004462")
	$("#resume_button").css("background-color","#668EA0")
}

function close(opt){
	flag = 0
	if($("#resume_button").css("background-color") == "rgb(235, 96, 39)"){
		opt1 = "#resume_button"
		if ($("#resume_button2").css("border-bottom-style") == "solid")
			opt2 = "#bio"
		if ($("#filter_button").css("border-bottom-style") == "solid")
			opt2 = "#filters"
		if ($("#path_button").css("border-bottom-style") == "solid")
			opt2 = "#paths"
		if ($("#ranking_button").css("border-bottom-style") == "solid")
			opt2 = "#ranking"
	}else{
		opt1 = "#help_button"
		flag = 1
	}
	if ($(".options").css("right") == "400px" && $("#close img").attr("src") == "css/images/close.png"){
		if (opt != 0)
			$("#close img").attr("src","css/images/close2.png")
		$(".info").hide()
		$(".social").hide()
		$("svg").width($( window ).width())
		$(".options").css("right","0")
		$("#close").css("right","0")		
	}else if ($(".options").css("right") == "0px" && $("#close img").attr("src") == "css/images/close2.png"){
		$("#close img").attr("src","css/images/close.png")
		if (flag == 0)
			display(opt1,opt2)
		else
			help_display()
	}
	if ($("#controls").css("right") == "50px"){
		$("#controls").css("right","450px")
	}else if(opt == 1)
		$("#controls").css("right","50px")
}

function tooltip(d){
	var content;
	if (d.name != undefined){
		content = '<div class="tooltip">'
		content += '<div class="title">'+(d.name)+'</div>';
		if(d.shortBio == "")
			d.shortBio = "Sin Información"
		if (d.depiction)
			content += '<div class="content"><span class="picture"><img id="image" align="left" src="'+d.depiction+'" /></span>'+d.shortBio+'</div>'
		else
			content += '<div class="content"><span class="picture"><img id="image" align="left" src="http://www.poderopedia.org/poderopedia/static/tmp/imagen-face.gif" /></span>'+d.shortBio+'</div>'
		content += '<div id="perfil" class="buttons"> <a href="'+d.url+'" target="_blank">VER PERFIL</a></div></div>'
	}
	
	//~ content += '<br><a href="'+d.url+'" target="_blank">'+d.url+'</a>' ;
	return content;
  }

function resetNetwork(){
	vis.attr("transform","scale(1)");
	zoom.scale(1).translate([0,0]);
}

function zooming(type){
	var newzoom = zoom.scale();
	var newtranslate = zoom.translate();
	if (type == 0) newzoom += 0.05;
	if (type == 1) newzoom -= 0.05;
	zoom.scale(newzoom);
	zoom.translate(newtranslate);
	vis.attr("transform","translate("+newtranslate+") scale("+newzoom+")");
}

function sortOnKeys(dict) {

    var sorted = [];
    for(var key in dict) {
        sorted[sorted.length] = key;
    }
    sorted.sort();

    var tempDict = {};
    for(var i = 0; i < sorted.length; i++) {
        tempDict[sorted[i]] = dict[sorted[i]];
    }

    return tempDict;
}

function replace_id(val){
	id = val.replace(/ /g,"_")
	id = id.replace(/\./g,"")
	id = id.replace(/\(/g,"")
	id = id.replace(/\)/g,"")
	id = id.replace(/\'/g,"")
	id = id.replace(/\&/g,"")
	return id
}

function bio_click(id){
	flag = $("#"+id+" .short_bio").attr("flag")
	if (flag == '0'){
		$("#"+id+" .short_bio").css("display","block")
		$("#"+id+" .short_bio").attr("flag","1")
	}else{
		$("#"+id+" .short_bio").css("display","none")
		$("#"+id+" .short_bio").attr("flag","0")
	}
}

function entity_click(entity){
	$(entity+" .info").css("display","block")
	flag = $(entity).attr("flag")
	if (flag == '0'){
		$(entity+" .info").css("display","block")
		$(entity).attr("flag","1")
	}else{
		$(entity+" .info").css("display","none")
		$(entity).attr("flag","0")
		$(entity+" .short_bio").css("display","none")
		$(entity+" .short_bio").attr("flag","0")
	}
}

function updateSearch(searchTerms,searchTerms2){
	$("line").css("visibility","hidden");
	$("path").css("visibility","visible");
	$("text").css("visibility","visible");
	
	$("input[name='filter']").each(function(i){
		$('line[leaf=0]').css("visibility","visible");
		if($("input[value='"+searchTerms[i].value+"']").is(":checked") == true){
			id_line = 'y'+searchTerms[i].value
			console.log(id_line)
			$('line['+id_line+'=1]').css("visibility","visible");
		}
	})
	
	$("input[name='node_filter']").each(function(i){
		if($("input[value='"+searchTerms2[i].value+"']").is(":checked") == false){
			$('line[t_source='+searchTerms2[i].value+']').css("visibility","hidden");
			$('line[t_target='+searchTerms2[i].value+']').css("visibility","hidden");
			$("path[group="+searchTerms2[i].value+"]").css("visibility","hidden");
			$("text[group="+searchTerms2[i].value+"]").css("visibility","hidden");
		}
	})
	
}

function nodecenter(searchRegEx){
	x = ($("#network").width())/2;
	y = ($("#network").height())/2;
	cx = (x*0.7 - $("g[name='"+searchRegEx+"']").attr("cx"));
	cy = (y*0.7 - $("g[name='"+searchRegEx+"']").attr("cy"));
	lastcolor =  $("g[name='"+searchRegEx+"'] path").css("fill")
	$("g[name='"+searchRegEx+"'] path").css("fill","red")
	$("g[name='"+searchRegEx+"'] text").css("fill","blue")
	if (lastnode != ""){
		$("g[name='"+lastnode+"'] path").css("fill",lastcolor)
		$("g[name='"+lastnode+"'] text").css("fill","black")
	}
	lastnode = searchRegEx;
	console.log(cx)
	if (isNaN(cx)){
		resetNetwork()
	}else{
		zoom.translate([cx,cy]);
		zoom.scale(1.2);
		vis.attr("transform","translate("+[cx,cy]+") scale("+zoom.scale()+")");
	}
}


function uniqueItems(){
	var unique = [];
	$("g .nodo text").each(function(i,e){
		var thisNodo = $.trim($(e).text());
		if(unique.indexOf(thisNodo) == -1)	unique.push(thisNodo);
		else 	$(e).remove();
	});
	$("g .nodo title").each(function(i,e){
		var thisNodo = $.trim($(e).text());
		if(unique.indexOf(thisNodo) == -1)	unique.push(thisNodo);
		else 	$(e).remove();
	});
	$("g .nodo path").each(function(i,e){
		var thisNodo = $.trim($(e).text());
		if(unique.indexOf(thisNodo) == -1)	unique.push(thisNodo);
		else 	$(e).remove();
	});
}

$(document).ready(function(){

 	$('#tri-inicio').css('border-left-color','#2D2D2D');
 	$('#barra-inicio').css('background-color','#2D2D2D');
 	$('#barra-inicio').css('color','#fff');
 	$('#tri-inicio').css('background-color','#004462');
  $('#barra1').html('Publicaciones');
 	$('#tri1').css('border-left-color','#004462');
 	$('#barra1').css('background-color','#004462');
 	$('#barra1').css('color','#fff');
 	$('#tri1').css('background-color','#668EA0');
 	$('#tri2').css('border-left-color','#668EA0');
  $('#barra2').html('Redes de Coautoría');
 	$('#barra2').css('background-color','#668EA0');
 	$('#barra2').css('color','#fff');
 	$('#tri2').css('background-color','#fff');
	//$('#tri3').css('border-left-color','#DDDDDD');
  //$('#barra3').html('Coautoría');
  //$('#barra3').css('background-color','#DDDDDD');
  //$('#barra3').css('color','#818181');

	//$('.tri#tri2').css('display','none');
	//$('#barra2').css('display','none')
	$('.tri#tri3').css('display','none');
	//$('#barra3').css('display','none')
		 	
	$("#resume_button").css("background-color","#668EA0")
	$("#network_gen").width($( window ).width() -320)
	$("#resume_button2").css("border-bottom","2px solid #FFF")
	$(".scrollbar").height($( window ).height()-150)
	$("#ranking").hide()
	$("#paths").hide()
	$("#help").hide()
	$("#filters").hide()
	display("#resume_button","#bio")
	groups = {}
	//~ groups = $.getJSON("./json/"+key+".json",function(d){
	groups = $.getJSON("./data/poderopedia_D.json",function(d){
		//~ console.log(d.nodes.group)
		for(i=0;i<d.nodes.length;i++){
			groups[d.nodes[i].name] = d.nodes[i].group
		}
		
		$("#search-network").tokenInput(d.nodes, {
			hintText: "Ingrese su búsqueda",
			noResultsText: "No hay resultados",
			searchingText: "Buscando...",
			resultsLimit: 10,
			minChars: 2,
			propertyToSearch: "name",
			tokenLimit: 1,
			tokenValue: "id",
			preventDuplicates: true
			
		});
		$('#node_search input[type=text]').attr('placeholder','Buscar autor en la red');
		$("#path_search .token-input-list").css("margin","auto")
		return groups
	})
	
	$("#close").click(function () {
		close(1)
	})
	$("#resume_button").click(function () {
		close(0);
		display("#resume_button2","#bio")
		$("#resume_button").css("background-color","#668EA0")
		$("#help_button").css("background-color","#004462")
		
	})
	$("#resume_button2").click(function () {
		display("#resume_button2","#bio")
	})
	$("#ranking_button").click(function () {
		display("#ranking_button","#ranking")
	})
	$("#path_button").click(function () {
		display("#path_button","#paths")
	})
	$("#filter_button").click(function () {
		display("#filter_button","#filters")
	})
	$("#help_button").click(function () {
		close(0)
		help_display();
	})
	
	$("#selectAll").click(function () {
		$('input[name=filter]').prop('checked',"true");
		updateSearch($("input[name='filter']"),$("input[name='node_filter']"));
	});
	$("#unselectAll").click(function () {
		$('input[name=filter]').prop('checked',"");
		updateSearch($("input[name='filter']"),$("input[name='node_filter']"));
	});
	$("#selectAll2").click(function () {
		$('input[name=node_filter]').prop('checked',"true");
		updateSearch($("input[name='filter']"),$("input[name='node_filter']"));
	});
	$("#unselectAll2").click(function () {
		$('input[name=node_filter]').prop('checked',"");
		updateSearch($("input[name='filter']"),$("input[name='node_filter']"));
	});
	$('#node_search').bind("DOMSubtreeModified",function(){
		if ($( "#node_search .token-input-token p" ).text() != "" && $( "#node_search .token-input-token p" ).text() != lastsearch){
			//~ console.log($(".token-input-list").siblings("#search-network").val())
			id = $( "#node_search .token-input-token p" ).text()
			lastsearch = $( "#node_search .token-input-token p" ).text()
			console.log(id)
			expand[groups[id]] = true;
			console.log(expand)
			init();
			uniqueItems();
			nodecenter(lastsearch)
			type = $("g[name='"+lastsearch+"'] path").attr("type")
			window.location.href='#'+(id);
		}
	});
	
	$("#filter").change(function() {
		updateSearch($("input[name='filter']"),$("input[name='node_filter']"));
	});
	$("#node_filter .content").change(function() {
		updateSearch($("input[name='filter']"),$("input[name='node_filter']"));
	});
	
	$("#network_gen_button").click(function () {
		if($("#search .token-input-list").siblings("input[type=text]").val() != "")
			window.location = 'vis/query2.php?q1='+$(".token-input-list").siblings("#search-input").val();
	});
	
	$("#path_search .buttons").click(function () {
			var q1 = $(".token-input-list").siblings("#poder-input1").val()
			var q2 = $(".token-input-list").siblings("#poder-input2").val()
			console.log(q1)
			if (q1 != "" && q2 != ""){
				var parametros = {
						"q1" : q1,
						"q2" : q2,
						"key" : key
					};
				$.ajax({
					type: "GET",
					url: "./vis/query.php",
					data: parametros,
					success: function(data){
						$("#path").empty()
						$("#path").append(data)
						$("#path").append('<span id="end"></span>')
						$("#path").css("display","block")
						window.location.href = "#end";
						//~ $("#path").append("<div id='close'>Cerrar</div>")
					}
				});
			}
	});
	
	$.getJSON("./data/poderopedia_D.json",function(d){
		$("#poder-input1").tokenInput(d.nodes, {
			hintText: "Ingrese su búsqueda",
			noResultsText: "No hay resultados",
			searchingText: "Buscando...",
			resultsLimit: 10,
			minChars: 3,
			propertyToSearch: "alias",
			tokenLimit: 1,
			tokenValue: "id",
			preventDuplicates: true
			
		});
		$("#poder-input2").tokenInput(d.nodes, {
			hintText: "Ingrese su búsqueda",
			noResultsText: "No hay resultados",
			searchingText: "Buscando...",
			resultsLimit: 10,
			minChars: 3,
			propertyToSearch: "alias",
			tokenLimit: 1,
			tokenValue: "id",
			preventDuplicates: true
			
		});
		
		$("#search-input").tokenInput(d.nodes,{
			hintText: "Ingrese su búsqueda",
			noResultsText: "No hay resultados",
			searchingText: "Buscando...",
			resultsLimit: 10,
			minChars: 3,
			propertyToSearch: "alias",
			tokenLimit: 1,
			tokenValue: "alias",
			preventDuplicates: true
			
		});
		$('#network_gen input[type=text]').attr('placeholder','Generar red de otra entidad ');
	})
	
	//~ $.getJSON( "./json/"+key+".json", function( data ) {
	$.getJSON( "./data/poderopedia_D.json", function( data ) {
		$.each( data, function( key, val ) {
			options = new Array()
			if (key =="links"){
				$.each( val, function( key2, val2 ) {
					if (val2.relation != undefined){
						options[val2.relation] = val2.relation
					}
			
				})
				options = sortOnKeys(options)
				for (relation in options){
					$('<input type="checkbox" name="filter" value="'+relation+'" class="checkbox"> '+relation+'<br>').appendTo("#filter .content")
				}
				$('input:checkbox').prop('checked',"true");
			}
			if (key =="rankp"){
				i = 0
				val = val.sort(function(a, b){
						return b.Importancia - a.Importancia;
					  });
				$.each( val, function( key2, val2 ) {
					i++
					if(i == 11) return false;
					id = replace_id(val2.Nombre);
					//~ $("#person_rank").append("<div class='entity_field' id='"+id+"' name='"+val2.Nombre+"' group='"+val2.group+"'></div>");
					$("#person_rank .content").append("<div id='title_rank'>"+i+". "+val2.Nombre+"</div>")
					//~ if(val2.ShortBio == "") val2.ShortBio = "Sin Información";
					//~ $("#"+id).append("<div class='short_bio' flag='0'>"+val2.ShortBio+"</div>")
				});
			}
			if (key =="ranko"){
				i = 0
				val = val.sort(function(a, b){
						return b.Importancia - a.Importancia;
					  });
				$.each( val, function( key2, val2 ) {
					i++
					if(i == 11) return false;
					id = replace_id(val2.Nombre);
					//~ $("#person_rank").append("<div class='entity_field' id='"+id+"' name='"+val2.Nombre+"' group='"+val2.group+"'></div>");
					$("#org_rank .content").append("<div id='title_rank'>"+i+". "+val2.Nombre+"</div>")
					//~ if(val2.ShortBio == "") val2.ShortBio = "Sin Información";
					//~ $("#"+id).append("<div class='short_bio' flag='0'>"+val2.ShortBio+"</div>")
				});
			}
			if (key =="ranke"){
				i = 0
				val = val.sort(function(a, b){
						return b.Importancia - a.Importancia;
					  });
				$.each( val, function( key2, val2 ) {
					i++
					if(i == 11) return false;
					id = replace_id(val2.Nombre);
					//~ $("#person_rank").append("<div class='entity_field' id='"+id+"' name='"+val2.Nombre+"' group='"+val2.group+"'></div>");
					$("#emp_rank .content").append("<div id='title_rank'>"+i+". "+val2.Nombre+"</div>")
					//~ if(val2.ShortBio == "") val2.ShortBio = "Sin Información";
					//~ $("#"+id).append("<div class='short_bio' flag='0'>"+val2.ShortBio+"</div>")
				});
			}
			if (key =="resumen"){
				val = val.sort(function(a, b){
						return b.Importancia - a.Importancia;
					  });
				$.each( val, function( key2, val2 ) {
					entidad = val2.entidad
					principal = entidad
					document.title = 'PODEROPEDIA: MAPA DE RELACIONES DE '+entidad.toUpperCase();
					//~ $(".twitter-share-button").attr("data-text","PODEROPEDIA: MAPA DE RELACIONES DE "+principal.toUpperCase())
					(function(d, s, id) {
						  var js, fjs = d.getElementsByTagName(s)[0];
						  if (d.getElementById(id)) return;
						  js = d.createElement(s); js.id = id;
						  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.0";
						  fjs.parentNode.insertBefore(js, fjs);
						}(document, 'script', 'facebook-jssdk'));
					!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
					if(val2.shortBio == "")	val2.shortBio = "Sin Información"
					$("path.tooltip.node.leaf[name='"+principal+"']").attr("class","tooltip node leaf principal")
					$("text.tooltip.node.leaf[name='"+principal+"']").attr("class","tooltip node leaf principal")
					$("#name").append(val2.entidad);
					$("#description").append(val2.shortBio);
					$("#perfil").append("<a href='"+val2.url+"' target='_blank'>VER PERFIL</a>")
					if(val2.fam_pow != 0){
						$("#rank .content").append("<div class='row'><div class='cell' id='pow_link'>VÍNCULOS FAMILIARES</div><div class='cell' id='dots'>:</div><div class='cell' id='value'>"+val2.fam_pow+"%</div>");
					}
					if(val2.eco_pow != 0){
						$("#rank .content").append("<div class='row'><div class='cell' id='pow_link'>VÍNCULOS ECONÓMICOS</div><div class='cell' id='dots'>:</div><div class='cell' id='value'>"+val2.eco_pow+"%</div>");
					}
					if(val2.pol_pow != 0){
						$("#rank .content").append("<div class='row'><div class='cell' id='pow_link'>VÍNCULOS POLÍTICOS</div><div class='cell' id='dots'>:</div><div class='cell' id='value'>"+val2.pol_pow+"%</div>");
					}
					if(val2.rel_pow != 0){
						$("#rank .content").append("<div class='row'><div class='cell' id='pow_link'>VÍNCULOS RELIGIOSOS</div><div class='cell' id='dots'>:</div><div class='cell' id='value'>"+val2.rel_pow+"%</div>");
					}
					if(val2.cohesion == 0){
						$("#rank .content").append("<div class='row'><div class='cell' id='pow_link'>COHESIÓN</div><div class='cell' id='dots'>:</div><div class='cell' id='value'>Sin Cohesión</div>");
					}
					if(val2.cohesion != 0){
						$("#rank .content").append("<div class='row'><div class='cell' id='pow_link'>COHESIÓN</div><div class='cell' id='dots'>:</div><div class='cell' id='value'>"+val2.cohesion+"%</div>");
					}
					$(".info .picture").append("<img src='"+val2.image+"' />");
					
				});
			}			
		});
	});
	

	
	$( window ).resize(function() {
		$("#network_gen").width($( window ).width() -300)
		$(".scrollbar").height($( window ).height()-150)
		$("svg").width($( window ).width())
		$("svg").height($( window ).height() -20)
	});
	
	if($("#user_id").text() != "")
		bio_start()
	//~ $("#path").on("click","#close",function(){
		//~ $("#path").empty()
		//~ $("#path").css("display","none")
//~ 
	//~ });
	
	//~ $("#person").on("click","#person_header",function(){
		//~ entity_click("#person");
		//~ 
	//~ });
	$("#organization").on("click","#organization_header",function(){
		entity_click("#organization");
	});
	$("#person_header").on("mouseover",function(){
		$(".net_bio #selected_name").text("AYUDA")
		$(".net_bio #selected_bio").text("Las Personas están ordenadas en un ranking de acuerdo a su posición estratégica en la red respecto a la entidad para quien fue generada la red. Pinche cada recuadro del nombre para obtener información adicional y genere su red correspondiente en la opción generar Red.")
	});
	$("#organization_header").on("mouseover",function(){
		$(".net_bio #selected_name").text("AYUDA")
		$(".net_bio #selected_bio").text("Las Organizaciones y Empresas están ordenadas en un ranking de acuerdo a su posición estratégica en la red respecto a la entidad para quien fue generada la red. Pinche cada recuadro del nombre para obtener información adicional y genere su red corresponidiente en la opción generar Red.")
	});
	$("#path_search").on("mouseover",function(){
		$(".net_bio #selected_name").text("AYUDA")
		$(".net_bio #selected_bio").text("Ingrese el nombre de dos entidades (Personas u Organizaciones/Empresas) para buscar cómo y a través de qué entidades están vinculadas en esta red.")
	});
	$("#network").on("mouseout",function(){
		$(".net_bio #selected_name").text("AYUDA")
		$(".net_bio #selected_bio").text("Esta red corresponde al mapa de relaciones entre entidades vinculadas directa e indirectamente (profundidad 3) a la entidad buscada. La red muestra comunidades de entidades agrupadas en un nodo. Para desplegar las comunidades, y ver las relaciones en su interior, es necesario hacer click en cada una de éstas (lo mismo para cerrarlas). Por defecto, la red aparece con la comunidad en la que está presente la entidad buscada, como desplegada. Posicionate en cada nodo para resaltar sus contactos directos y obtener información adicional. El tamaño de los nodos refleja su nivel de conectividad y su forma si es que son Personas (círculos) o Empresas/Organizaciones (rombos).")
	});
	$(".scrollbar").on("mouseover",".net",function(){
		console.log("net")
		$(".net_bio #selected_name").text("AYUDA")
		$(".net_bio #selected_bio").text("Presione acá para generar la red de correspondiente a la entidad seleccionada.")
	});
	
	$(".scrollbar").on("mouseover",".google",function(){
		$(".net_bio #selected_name").text("AYUDA")
		$(".net_bio #selected_bio").text("Presione acá para buscar en la web a la entidad seleccionada.")
	});
	
	$(".scrollbar").on("mouseover","#info_title",function(){
		$(".net_bio #selected_name").text("AYUDA")
		$(".net_bio #selected_bio").text("Al hacer click en esta entidad, desplegará su biografía caso de estar disponible. Además, centrará a la entidad dentro de la visualización.")
	});
	
	$("#search").on("mouseover",function(){
		$(".net_bio #selected_name").text("AYUDA")
		$(".net_bio #selected_bio").text("Escriba en el recuadro a la entidad que desea visualizar, si al buscar no sucede nada, significa que esta entidad no se encuentra en la red. Adicionalmente, puede hacer click en buscar para generar la red de la entidad digitada.")
	});
	
	$("#combobox").on("mouseover",function(){
		$(".net_bio #selected_name").text("AYUDA")
		$(".net_bio #selected_bio").text("Elija una opción para visualizar exclusivamente los nodos y enlaces correspondiente a la selección. Para volver a mostrar todos los nodos y enlaces, debe seleccionar 'Todos'.")
	});
	
	$("#person").on("click",".entity_field",function(){
		id = $(this).attr("id")
		bio_click(id);
		expand[$(this).attr("group")] = true;
		init();
		uniqueItems();
		nodecenter($(this).attr("name"))
		
	});
	$("#organization").on("click",".entity_field",function(){
		id = $(this).attr("id")
		bio_click(id);
		expand[$(this).attr("group")] = true;
		init();
		uniqueItems();
		nodecenter($(this).attr("name"))
	});
	
})
