function piechartColor(id){
	if(id == "Proyectos"){
		return "green"
	}
	if(id == "Congresos"){
		return "blue"
	}
	if(id == "Publicaciones AI"){
		return "#666"
	}
	if(id == "Publicaciones BI"){
		return "#CCC"
	}
}
function piechart(data){

	$("#piechart").empty()
	$("#legend").empty()
	var width = 200,
		height = 200,
		radius = Math.min(width, height) / 2;
	data = d3.entries(data)
	data2 = data
	texto = ""
	for(i=0;i<data.length;i++){
		if (i == data.length - 1) char = ""
		else char = ", "
		texto = texto+data[i].key + char
	}
	var color = d3.scale.ordinal()
		.range(["#1066ac", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);

	var arc = d3.svg.arc()
		.outerRadius(radius - 10)
		.innerRadius(0);

	var pie = d3.layout.pie()
		.sort(null)
		.value(function(d) { return d.value; });

	var svg = d3.select("#piechart").append("svg")
		.attr("width", width)
		.attr("height", height)
	  .append("g")
		.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

		console.log(data)
	  var g = svg.selectAll(".arc")
		  .data(pie(data))
		.enter().append("g")
		  .attr("class", "arc");

	  g.append("path")
		  .attr("d", arc)
		  .attr("cursor","pointer")
		  .on("mouseover",function(d){
			  d3.select(this).style("fill","red");})
		  .on("mouseout",function(d){

			  d3.select(this).style("fill",function(d){return piechartColor(d.data.key)});			 

			  })
		  .on("click",function(d){

				})
				
		  .style("fill", function(d) {return piechartColor(d.data.key); });
	  /*g.append("text")
		  .attr("transform", function(d) { return "translate(" + arc.centroid(d) + ")"; })
		  .attr("dy", ".25em")
		  .style("text-anchor", "middle")
		  .text(function(d) { return d.data.key; });
		*/  
	  g.append("svg:title")
		  .text(function(d){return (d.data.key+": "+d.data.value + " ("+((((d.endAngle - d.startAngle) / (2 * Math.PI)) * 100).toFixed(1))+"%)");});
	   //~ $("#instrumento2").text(capitalize(texto));
	  data_aux = {}
	  for (i in data){
		data_aux[data[i]["key"]] = data[i]["value"]
	  }
	  $("#legend2").html('<div style="display:inline-block;background-color:#555;width:12px;height:12px;"></div> Publicaciones Alto Impacto ('+data_aux['Publicaciones AI']+')<br><div style="display:inline-block;background-color:#ccc;width:12px;height:12px;"></div> Publicaciones Bajo Impacto ('+data_aux['Publicaciones BI']+')<br><div style="display:inline-block;background-color:green;width:12px;height:12px;"></div> Proyectos ('+data_aux['Proyectos']+')<br><div style="display:inline-block;background-color:blue;width:12px;height:12px;"></div> Congresos ('+data_aux['Congresos']+')')
	  

}
