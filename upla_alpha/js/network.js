var expand;

var linkedByIndex = {};
function neighboring(a, b) {
	if (linkedByIndex[a.id + "," + b.id]) return linkedByIndex[a.id + "," + b.id];
	if (linkedByIndex[b.id + "," + a.id]) return linkedByIndex[b.id + "," + a.id]; 
}

var width = $( window ).width()+15,
	height = $( window ).height() -20,
    dr = 4,      // default point radius
    off = 15,    // cluster hull offset
    expand = {}, // expanded clusters
    data, net, force, hullg, hull, linkg, link, nodeg, node;

var curve = d3.svg.line()
    .interpolate("cardinal-closed")
    .tension(.85);

var fill = d3.scale.category20();

function noop() { return false; }

function nodeid(n) {
  return n.size ? "_g_"+n.group : n.name;
}

function linkid(l) {
  var u = nodeid(l.source),
      v = nodeid(l.target);
  return u<v ? u+"|"+v : v+"|"+u;
}

function getGroup(n) { return n.group; }

// constructs the network to visualize
function network(data, prev, index, expand) {
	expand = expand || {};
	var gm = {},    // group map
		nm = {},    // node map
		lm = {},    // link map
		gn = {},    // previous group nodes
		gc = {},    // previous group centroids
		nodes = [], // output nodes
		links = []; // output links

  // process previous nodes for reuse or centroid calculation
	if (prev) {
		prev.nodes.forEach(function(n) {
			var i = index(n), o;
			if (n.size > 0) {
				gn[i] = n;
				n.size = 0;
			}else {
				o = gc[i] || (gc[i] = {x:0,y:0,count:0});
				o.x += n.x;
				o.y += n.y;
				o.count += 1;
			}
		});
	}

  // determine nodes
	for (var k=0; k<data.nodes.length; ++k) {
		var n = data.nodes[k],
			i = index(n),
			l = gm[i] || (gm[i]=gn[i]) || (gm[i]={group:i, size:0, nodes:[]});

    if (expand[i]) {
		// the node should be directly visible
		nm[n.name] = nodes.length;
		nodes.push(n);
		if (gn[i]) {
			// place new nodes at cluster location (plus jitter)
			n.x = gn[i].x + Math.random();
			n.y = gn[i].y + Math.random();
		}
	}else{
		// the node is part of a collapsed cluster
		if (l.size == 0) {
			// if new cluster, add to set and position at centroid of leaf nodes
			nm[i] = nodes.length;
			nodes.push(l);
			if (gc[i]) {
				l.x = gc[i].x / gc[i].count;
				l.y = gc[i].y / gc[i].count;
			}
		}
      l.nodes.push(n);
    }
		// always count group size as we also use it to tweak the force graph strengths/distances
		l.size += 1;
		n.group_data = l;
	}

	for (i in gm) { gm[i].link_count = 0; }

	// determine links
	for (k=0; k<data.links.length; ++k) {
		var e = data.links[k],
			u = index(e.source),
			v = index(e.target);
			z = e.Year;
			//~ console.log(e.source.id,e.target.id)
			linkedByIndex[e.source.id + "," + e.target.id] = 1;
		if (u != v) {
			gm[u].link_count++;
			gm[v].link_count++;
		}
		u = expand[u] ? nm[e.source.name] : nm[u];
		v = expand[v] ? nm[e.target.name] : nm[v];
		var i = (u<v ? u+"|"+v : v+"|"+u),
			l = lm[i] || (lm[i] = {source:u, target:v, size:0, relation:z,edge:e.edge});
		l.size += 1;
	}
	for (i in lm) { links.push(lm[i]); }

	return {nodes: nodes, links: links};
}

function convexHulls(nodes, index, offset) {
	var hulls = {};

	// create point sets
	for (var k=0; k<nodes.length; ++k) {
		var n = nodes[k];
		if (n.size) continue;
		var i = index(n),
			l = hulls[i] || (hulls[i] = []);
		l.push([n.x-offset, n.y-offset]);
		l.push([n.x-offset, n.y+offset]);
		l.push([n.x+offset, n.y-offset]);
		l.push([n.x+offset, n.y+offset]);
	}

	// create convex hulls
	var hullset = [];
	for (i in hulls) {
		hullset.push({group: i, path: d3.geom.hull(hulls[i])});
	}

	return hullset;
}

function drawCluster(d) {
	return curve(d.path); // 0.8
}
function redraw() {
	vis.attr("transform","translate(" + d3.event.translate + ")" + " scale(" + d3.event.scale + ")");
}


// --------------------------------------------------------
var group;
var entity;
var zoom = d3.behavior.zoom();
var body = d3.select("#network");
var vis = body.append("svg:svg")
   .attr("width", width)
   .attr("height", height)
   .attr("pointer-events", "all")
   .append('svg:g')
   .call(zoom.scaleExtent([0.1, 5]).scale(1).on("zoom", redraw)).on("dblclick.zoom", null)
   .append('svg:g');
vis.append('svg:rect')
    .attr('width', 1000000)
    .attr('height', 1000000)
    .attr('x', -10000)
    .attr('y', -10000)
    .attr('fill', '#F1F1F1');
//~ d3.json("./json/"+key+".json", function(json) {
d3.json("./data/poderopedia_D.json", function(json) {
	data = json;
	for (var i=0; i<data.links.length; ++i) {
		o = data.links[i];
		var auxsource=null;
		var auxtarget=null;
		//~ entity = data.resumen[0].entidad;
		entity = 'Todo'
		for (var j=0; j<data.nodes.length; ++j) {
			if (data.nodes[j].comu_name == entity)
				group = data.nodes[j].group
			if (data.nodes[j].id==o.source)
				o.source = data.nodes[j];
    
			if (data.nodes[j].id==o.target)
				o.target = data.nodes[j];
		}
	}	
	hullg = vis.append("g");
	linkg = vis.append("g");
	nodeg = vis.append("g");
	init();

	vis.attr("opacity", 1e-6)
		.transition()
		.duration(1000)
		.attr("opacity", 1);
	expand[4] = true;
	init();
	//~ uniqueItems();
});
var shape;
function init() {
	if (force) force.stop();

	net = network(data, net, getGroup, expand);

	force = d3.layout.force()
		.nodes(net.nodes)
		.links(net.links)
		.size([width, height])
		.linkDistance(200)
		.linkStrength(function(l, i) {
			return 1;
		})
		.gravity(0.1)   // gravity+charge tweaked to ensure good 'grouped' view (e.g. green group not smack between blue&orange, ...
		.charge(-1000)    // ... charge is important to turn single-linked groups to the outside
		.friction(0.5)   // friction adjusted to get dampened display: less bouncy bouncy ball [Swedish Chef, anyone?]
		.start();

	hullg.selectAll("path.hull").remove();
	hull = hullg.selectAll("path.hull")
		.data(convexHulls(net.nodes, getGroup, off))
		.enter().append("path")
		.attr("class", "hull")
		.attr("d", drawCluster)
		.style("fill", function(d) {return fill(d.group); })
		.on("dblclick", function(d) {		
			expand[d.group] = false; init();
			//~ uniqueItems();
		});

	link = linkg.selectAll("line.link").data(net.links, linkid);
	link.exit().remove();
	link.enter().append("line")
		.attr("class", "link")
		.attr("edge",function(d){return d.edge})
		.attr("y2010", function(d){rel = d.relation.split(",");if ($.inArray('2010',rel) > -1) return 1; else return 0;})
		.attr("y2011", function(d){rel = d.relation.split(",");if ($.inArray('2011',rel) > -1) return 1; else return 0;})
		.attr("y2012", function(d){rel = d.relation.split(",");if ($.inArray('2012',rel) > -1) return 1; else return 0;})
		.attr("y2013", function(d){rel = d.relation.split(",");if ($.inArray('2013',rel) > -1) return 1; else return 0;})
		.attr("y2014", function(d){rel = d.relation.split(",");if ($.inArray('2014',rel) > -1) return 1; else return 0;})
		.attr("yaceptada", function(d){rel = d.relation.split(",");if ($.inArray('aceptada',rel) > -1) return 1; else return 0;})
		.attr("yenviada", function(d){rel = d.relation.split(",");if ($.inArray('enviada',rel) > -1) return 1; else return 0;})
		.attr("leaf", function(d){
			if (d.source.comu_name != undefined && d.target.comu_name != undefined)
				return "true";
			if (d.target.comu_name != undefined)
				return "true";
			if (d.source.comu_name == undefined && d.target.comu_name == undefined){
				//~ console.log(d)
				return "false";
			}
		})
		.attr("source",function(d){
			return d.source.id
		})
		.attr("target",function(d){
			return d.target.id
		})
		.attr("t_target",function(d){
			if (d.target.type != undefined)
				return d.target.group
			else
				return d.target.nodes[0].group
		})
		.attr("t_source",function(d){
			if (d.source.type != undefined)
				return d.source.group
			else
				return d.source.nodes[0].group
			
		})
		.attr("leaf",function(d){
			if (d.source.type == undefined && d.target.type == undefined)
				return 0
		})
		.attr("x1", function(d) { return d.source.x; })
		.attr("y1", function(d) { return d.source.y; })
		.attr("x2", function(d) { return d.target.x; })
		.attr("y2", function(d) { return d.target.y; })
		.style("stroke-width", function(d) { return d.size/5 || 1; });
	node = nodeg.selectAll("g.nodo").data(net.nodes, nodeid);
	node.exit().remove();
  
  
    
	node.enter().append("g")
		.attr("class","nodo")
		.attr("name",function(d){return d.name;});
	
	node.append("path")
		.attr("class", function(d) {return "tooltip node" + (d.size?"":" leaf"); })
		.attr("id", function(d) {return d.id})
		.attr("title",function(d) { return tooltip(d); })
		.attr("name",function(d){
			if (d.name)	
				return d.name		
			if (d.nodes[0].comu_name)
				return d.nodes[0].comu_name	
		})
		.attr("group",function(d){return d.group})
		.attr("color",function(d){return fill(d.group)})
		.attr("type",function(d){
			if(d.type != undefined) 
				return d.type;
			if(d.type == undefined)
				return "3";
		})
		.attr("d", function(d) {
			//~ r = d.size ? (d.size/8)+5 : (Math.log(d.degree)*10)+5
			if (d.degree == undefined)
				r = 5
			else
				r = 5 + Math.sqrt(d.degree)
			//~ return "m -"+r+",0 a "+r+","+r+" 0 1, 0 "+r*2+",0 a "+r+","+r+" 0 1, 0 -"+r*2+",0" ;
			//~ if (d.type == "0")
				//~ return "m -"+r+",0 a "+r+","+r+" 0 1, 0 "+r*2+",0 a "+r+","+r+" 0 1, 0 -"+r*2+",0" ;
			//~ if (d.type == "1")
				//~ return "M"+r+",0 L"+r+",0 0,"+r+" -"+r+",0 0,-"+r+" Z";
			//~ if (d.type == "2")
				//~ return "M"+r+",0 L"+r+",0 0,"+r/2+" -"+r+",0 0,-"+r+" Z";
			if (d.type == undefined)
				return "M"+r+",0 L"+r+",0 0,"+r+" -"+r+",0 0,-"+r+" Z";
			else
				return "m -"+r+",0 a "+r+","+r+" 0 1, 0 "+r*2+",0 a "+r+","+r+" 0 1, 0 -"+r*2+",0" ;
		})
		.style("fill", function(d) {
			  return fill(d.group);
		})
		.style("stroke-width", function(d) {
			if(d.type == undefined)
				return 1;
		})
		.on("mouseover",function(d){
			link.style('stroke', function(l) {
				if (d === l.source || d === l.target)	return "red";
				else 	return "#333";
			});
			node.select("text.leaf").style("fill",function(o){
				return neighboring(d, o) ? "blue" : "black";
			})
			node.select("path.leaf").style("fill",function(o){
				return neighboring(d, o) ? "red" : fill(o.group);;
			})
			d3.select(this).style("fill", "red")
			if (d.name){
				$("text[name='"+d.name+"']").css("fill","blue")
				$(".net_bio #selected_name").text((d.name).toUpperCase())
			}
			else
				$("text[name='"+d.nodes[0].comu_name+"']").css("fill","blue")
			
			
			if(d.shortBio == "")
				shortBio = "Sin Información";
			else
				shortBio = d.shortBio;
			$(".net_bio #selected_bio").text(shortBio)
		})
		.on("mouseout",function(d){
			link.style('stroke',"#333");
			node.select("path.leaf").style("fill",function(o){
				
				return fill(o.group)
			})
			node.select("text.leaf").style("fill",function(o){
				return "black"
			})
			d3.select(this).style("fill", fill(d.group))
			if (d.name)
				$("text[name='"+d.name+"']").css("fill","black")
			else
				$("text[name='"+d.nodes[0].comu_name+"']").css("fill","black")
			$(".net_bio #selected_name").text("")
			$(".net_bio #selected_bio").text("")
		})
		.on("dblclick", function(d) {			
			expand[d.group] = !expand[d.group];
			init();
			//~ uniqueItems()
			updateSearch($("input[name='filter']"),$("input[name='node_filter']"));
		})
		.on("click",function(d){

			$("#bio #name").text(d.name)
			
			  
		})
		
		.text(function(d){
			if (d.name != undefined)
				return d.id;
			else
				return d.group;
		});
	
	node.append("text")
		.attr("group",function(d){return d.group})
		.attr("id", function(d) {return d.id})
		.attr("class", function(d) {return "tooltip node" + (d.size?"":" leaf"); })
		.attr("title",function(d) { return tooltip(d); })
		.attr("name",function(d){

			if (d.name)
				return d.name		
			if (d.nodes[0].comu_name)
				return d.nodes[0].comu_name		
		})
		.attr("type",function(d){
			if(d.type != undefined) 
				return d.type;
			if(d.type == undefined)
				return "3";
		})
		.on("dblclick", function(d) {			
			expand[d.group] = !expand[d.group];
			init();
			//~ uniqueItems()
			updateSearch($("input[name='filter']"),$("input[name='node_filter']"));
		})
		.on("mouseover",function(d){
			link.style('stroke', function(l) {
				if (d === l.source || d === l.target)	return "red";
				else	return "#333";
			});
			node.select("path.leaf").style("fill",function(o){
				return neighboring(d, o) ? "red" : fill(o.group);;
			})
			node.select("text.leaf").style("fill",function(o){
				return neighboring(d, o) ? "blue" : "black";
			})
			d3.select(this).style("fill", "blue")
			if (d.name)
				$("path[name='"+d.name+"']").css("fill","red")
			else
				$("path[name='"+d.nodes[0].comu_name+"']").css("fill","red")
			//~ $(".net_bio #selected_name").text((d.name).toUpperCase())
			if(d.shortBio == "")
				shortBio = "Sin Información";
			else
				shortBio = d.shortBio;
			$(".net_bio #selected_bio").text(shortBio)
		})
		.on("mouseout",function(d){
			link.style('stroke',"#333");
			node.select("text.leaf").style("fill",function(o){
				return "black"
			})
			node.select("path.leaf").style("fill",function(o){
				return fill(o.group)
			})
			d3.select(this).style("fill", "black")
			if (d.name)
				$("path[name='"+d.name+"']").css("fill",fill(d.group))
			else
				$("path[name='"+d.nodes[0].comu_name+"']").css("fill",fill(d.group))
			$(".net_bio #selected_name").text("")
			$(".net_bio #selected_bio").text("")
		})
		.on("click",function(d){
			$("#bio #name").text(d.name)
			$("#bio #comu_name").text(d.comu_name)
			$.ajax({
				type: "GET",
				url: "webservice.php?id="+d.id,
				success: function(data){
					$("#pubs").empty()
					$("#pubs").append(toTitleCase(data))
				}
			});
			$.ajax({
				type: "GET",
				url: "webservice2.php?id="+d.id,
				success: function(data){
					$("#rel").empty()
					$("#rel").append(toTitleCase(data))
					$("#rel li").on("mouseover",function(){
						id_pub = $(this).attr('name')
						$("#pubs li[name="+id_pub+"]").css("color","#CCC")
						$('line').each(function(d){
							edge = $(this).attr('edge').split(",")
							s =$(this).attr('source')
							t = $(this).attr('target')
							if($.inArray(id_pub,edge) != -1){
								$(this).css("stroke","red")
								$("#"+s).css("fill","red")
								$("#"+t).css("fill","red")
							}
						})
					})
					$("#rel li").on("mouseout",function(){
						//~ id_pub = $(this).attr('name')
						$("#pubs li").css("color","#FFF")
						$('line').css("stroke","#333")
						$("path").each(function(){$(this).css("fill",$(this).attr("color"))})
						
					}) 
					
				}
			});
			
			$.post( "webservice3.php", {id:d.id},function(data) {
			  json = $.parseJSON(data)
			  if (json.last_name_m == null)
				json.last_name_m = ""
			  $("#bio #name").text(toTitleCase(json.name+" "+json.last_name_p+" "+json.last_name_m))
			  $("#email").empty()
			  $("#email").append("<a href=mailto:"+String(json["email_i"]).toLowerCase()+">"+String(json["email_i"]).toLowerCase()+"</a>")
			  $("#country").empty()
			  $("#country").append("<b>Nacionalidad:</b> "+toTitleCase(json["country"]))
			  $("#genre").empty()
			  genero = json["genre"]
			  if (json["genre"] == "F")
				genero = "Femenino"
			  if (json["genre"] == "M")
				genero = "Masculino"
			  $("#genre").append("<b>Sexo:</b> "+genero)
			  $("#position").empty()
			  $("#position").append("<b>Cargo:</b> "+toTitleCase((json["position"])))
			  $("#faculty").empty()
			  $("#faculty").append("<b>Facultad:</b> "+toTitleCase(json["faculty"]))
			  profession = json["profession"]
			  if (json["profession"] == null)
				profession = "Desconocido"
			  $("#profession").empty()
			  $("#profession").append("<b>Profesión:</b> "+toTitleCase(profession))
			  doctor_grade = json["doctor_grade"]
			  if (json["doctor_grade"] == "Doctor")
				doctor_grade = "Si"
			  else
			    doctor_grade = "No"
			  
			  $("#doctor_grade").empty()
			  $("#doctor_grade").append("<b>Grado de Doctor:</b> "+doctor_grade)
			  magister_grade = json["magister_grade"]
			  if (json["magister_grade"] == "Doctor")
				magister_grade = "Si"
			  else
			    magister_grade = "No"
			  
			  $("#magister_grade").empty()
			  $("#magister_grade").append("<b>Grado de Magister:</b> "+magister_grade)
			  })
			
			$.post( "webservice4.php", {id:d.id},function(data) {				
				piechart($.parseJSON(data))				
				
			})

		})
		.attr("text-anchor", "middle") 
		.attr("fill","#000")
		.style("pointer-events", "none")
		.attr("style", "font-size: 10px")
		.attr("font-weight", "100")
		.text( function(d) {
			if (d.name)	return d.name		
			if (d.nodes[0].comu_name)	return d.nodes[0].comu_name			
		});
		
	//~ node.append("title")
		//~ .attr("title",function(d) { return tooltip_link(d); });
		//~ .text(function(d){return d.shortBio});
		
	force.on("tick", function() {
		if (!hull.empty()) {
		hull.data(convexHulls(net.nodes, getGroup, off))
			.attr("d", drawCluster);
		}

		link.attr("x1", function(d) { return d.source.x; })
			.attr("y1", function(d) { return d.source.y; })
			.attr("x2", function(d) { return d.target.x; })
			.attr("y2", function(d) { return d.target.y; });

		node.attr("cx", function(d) { return d.x; })
			.attr("cy", function(d) { return d.y; })
			.attr("transform", function(d) { 
				if (d.type == 4){
					//~ return "translate(" + (d.x -Math.log(d.degree)*10/2) + "," + (d.y-Math.log(d.degree)*10/2) + ")";
					return "translate(" + (d.x) + "," + (d.y) + ")";
				}else
					return "translate(" + d.x + "," + d.y + ")";
				
				});
	});
	
	//~ $('.tooltip').tooltipster({arrow: false, interactive: true,speed: 0, trigger: 'click',position: 'right',});
	//~ 
	//~ try{
		//~ $("path.tooltip.node.leaf[name='"+principal+"']").attr("class","tooltip node leaf principal")
		//~ $("text.tooltip.node.leaf[name='"+principal+"']").attr("class","tooltip node leaf principal")
	//~ }catch(e){
	//~ }
}
