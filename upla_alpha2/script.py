# -*- coding: utf-8 -*- 
# -*- encoding: utf-8 -*- 
import MySQLdb as mysql
import networkx as nx
from networkx.readwrite import json_graph
import json

G = nx.Graph()

DB_HOST = 'localhost' 
DB_USER = 'root' 
DB_PASS = 'complexity*-*' 
DB_NAME = 'tri3' 
datos = [DB_HOST, DB_USER, DB_PASS, DB_NAME]
con = mysql.connect(*datos)
c = con.cursor()
c.execute('SET SESSION group_concat_max_len = 1000000;')
c.execute('TRUNCATE network')
c.execute('TRUNCATE user')
c.execute('TRUNCATE user_has_patent')

query = "SELECT patent_number,inventor,ano_publicacion FROM inventiva"
c.execute(query)
resultado = c.fetchall()
NET = {}
DB = {}
Y = {}
for registro in resultado:
	if registro[1] != None:
		aux = registro[1].split("; ")
		for i in range(len(aux)):
			j = i +1
			if aux[i] not in DB:
				if aux[i] != "" and aux[i] not in DB:
					DB[aux[i]] = len(DB)
					query = "INSERT INTO user VALUES ("+str(DB[aux[i]])+",'"+aux[i]+"')"
					c.execute(query)
					con.commit()
					
					G.add_node(DB[aux[i]],name=aux[i].decode('iso-8859-1').encode('utf8'),id_P = int(DB[aux[i]]),comu_name="TODO",tipo="CEA",type=1,group=4)
			if aux[i] != "":
				if not DB[aux[i]] in Y:
					Y[DB[aux[i]]] = str(registro[2])
				if str(registro[2]) not in Y[DB[aux[i]]]:
					Y[DB[aux[i]]] = Y[DB[aux[i]]]+","+str(registro[2])
				query = "INSERT INTO user_has_patent VALUES ("+str(DB[aux[i]])+",'"+registro[0]+"')"
				c.execute(query)
				con.commit()
			while True:
				if j >= len(aux):
					break			
				if aux[j] != "" and aux[i] != "":
					
					flag = 0
					if aux[i] in NET:
						if aux[j] not in NET[aux[i]]:
							NET[aux[i]][aux[j]] = {}
							NET[aux[i]][aux[j]]['edge'] = ""
							NET[aux[i]][aux[j]]['year'] = ""
						if str(registro[2]) not in NET[aux[i]][aux[j]]['year']:
							NET[aux[i]][aux[j]]['year'] = NET[aux[i]][aux[j]]['year']+str(registro[2])+","
						NET[aux[i]][aux[j]]['edge'] = NET[aux[i]][aux[j]]['edge']+str(registro[0])+","
						flag = 1
					if aux[j] in NET and flag == 0:
						if aux[i] not in NET[aux[j]]:
							NET[aux[j]][aux[i]] = {}
							NET[aux[j]][aux[i]]['edge'] = ""
							NET[aux[j]][aux[i]]['year'] = ""
						if str(registro[2]) not in NET[aux[j]][aux[i]]['year']:
							NET[aux[j]][aux[i]]['year'] = NET[aux[j]][aux[i]]['year']+str(registro[2])+","
						NET[aux[j]][aux[i]]['edge'] = NET[aux[j]][aux[i]]['edge']+str(registro[0])+","
						flag = 1
					
					if flag == 0:
						NET[aux[i]] = {}
						NET[aux[i]][aux[j]] = {}
						NET[aux[i]][aux[j]]['edge'] = ""
						NET[aux[i]][aux[j]]['year'] = ""
						if str(registro[2]) not in NET[aux[i]][aux[j]]['year']:
							NET[aux[i]][aux[j]]['year'] = NET[aux[i]][aux[j]]['year']+str(registro[2])+","
						NET[aux[i]][aux[j]]['edge'] = NET[aux[i]][aux[j]]['edge']+str(registro[0])+","
					
				j += 1

#~ print NET
for s in NET:
	for t in NET[s]:
		edges = NET[s][t]['edge'][:-1].split(",")
		for e in edges:
			query = "INSERT INTO network VALUES("+str(DB[s])+","+str(DB[t])+",'"+e+"')"
			c.execute(query)
		con.commit()
		G.add_edge(int(DB[s]),int(DB[t]),linka=int(DB[s]),linkb=int(DB[t]),weight=(len(NET[s][t]['edge'].split(","))-1),Year=NET[s][t]['year'][:-1], edge=NET[s][t]['edge'][:-1])

DEG = {}
for i in DB:
	DEG[DB[i]] = G.degree(DB[i])
nx.set_node_attributes(G,'degree',DEG)
nx.set_node_attributes(G,'Year',Y)

salida = open("data/poderopedia_D.json","w")
output = json_graph.node_link_data(G)
output = json.dumps(output)
output = output.replace('"source"','"s"').replace('"target"','"t"').replace('"linka"','"source"').replace('"linkb"','"target"')
salida.write(output)
