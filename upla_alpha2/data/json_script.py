import io,json
with open('force.json') as data_file:    
	data = json.load(data_file)

for tipo in data:
	if tipo == "nodes":
		for i in range(len(data[tipo])):
			id = data[tipo][i]["id"]
			if "P" in id:
				data[tipo][i]["id_P"] = id
			if "O" in id:
				data[tipo][i]["id_O"] = id
			data[tipo][i]["id"] = i
			if data[tipo][i]["shortBio"]:
				data[tipo][i]["shortBio"] = data[tipo][i]["shortBio"].replace("\r\n"," ")
				data[tipo][i]["shortBio"] = data[tipo][i]["shortBio"].replace("\n"," ")
				data[tipo][i]["shortBio"] = data[tipo][i]["shortBio"].replace("\r"," ")
				data[tipo][i]["shortBio"] = data[tipo][i]["shortBio"].replace('\"',"'")
				data[tipo][i]["shortBio"] = data[tipo][i]["shortBio"].replace('\f',"")
				data[tipo][i]["shortBio"] = data[tipo][i]["shortBio"].replace('\\',"")
				data[tipo][i]["shortBio"] = data[tipo][i]["shortBio"].replace('\u0004',"")
				data[tipo][i]["shortBio"] = data[tipo][i]["shortBio"].replace('\u0005',"")
				data[tipo][i]["shortBio"] = data[tipo][i]["shortBio"].replace('\u0007',"")

	if tipo == "links":
		for i in range(len(data[tipo])):
			if "Cargos en Empresas" in data[tipo][i]:
				data[tipo][i]["Cargos en Empresas"] = data[tipo][i]["Cargos en Empresas"].replace('\"',"'")
with io.open('poderopedia_D.json', 'w', encoding='utf-8') as f:
	f.write(unicode(json.dumps(data, ensure_ascii=False)))
