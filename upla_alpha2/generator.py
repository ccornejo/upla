# -*- coding: utf-8 -*-
# -*- decoding: utf-8 -*-
import MySQLdb as mysql
import networkx as nx
from networkx.readwrite import json_graph
import json

DICT = {"Alumno":1,"Alumno Postgrado":2,"Arte":3,"CEA":4,"Ciencias":5,"Contraloría Interna":6,"Cs. Actividad F\xc3\xadsica":8,"Cs. Salud":9,"Cs. Sociales":10,"Ed. F\xc3\xadsica":11,"Educaci\xc3\xb3n":12,"Humanidades":13,"Ingeniería":14,"Red Externa":15,"San Felipe":16,"Red Externa Ingeniería":16,"Red Externa Arte":17,"Red Externa CEA":18,"Red Externa Ciencias":19,"Red Externa Contraloría Interna":20,"Red Externa Cs. Actividad F\xc3\xadsica":21,"Red Externa Cs. Salud":22,"Red Externa Cs. Sociales":23,"Red Externa Ed. F\xc3\xadsica":24,"Red Externa Educaci\xc3\xb3n":25,"Red Externa Humanidades":26,"Red Externa San Felipe":26,"Red Externa Alumno Postgrado":27,"Red Externa Alumno":28,"Otros":29,"No definido":30}
G = nx.Graph()
db = mysql.connect('localhost','educaupl_network','complexity*-*','educaupl_upla')
c = db.cursor()

c.execute("TRUNCATE network")
PHP = {}
query = "SELECT person_id,publication_id FROM person_has_publication;"
c.execute(query)
resultado = c.fetchall()
for registro in resultado:
	if registro[1] not in PHP:
		PHP[registro[1]] = []
	if registro[0] not in PHP[registro[1]]:
		PHP[registro[1]].append(registro[0])
	
for p in PHP:
	for i in range(len(PHP[p])):
		j = i + 1
		while True:
			if j == len(PHP[p]):
				break
			c.execute("INSERT INTO network VALUES("+str(PHP[p][i])+","+str(PHP[p][j])+","+str(p)+");")
			j += 1
			db.commit()
NORM = {}
query = "SELECT user_id, norm_id FROM normalization;"
c.execute(query)
resultado = c.fetchall()
for registro in resultado:
	NORM[registro[0]] = registro[1]

EXT = {}
query = "SELECT n.source,GROUP_CONCAT(DISTINCT f.name) FROM network n, faculty f, user p WHERE n.source >= 1000 AND n.target < 1000 AND n.target = p.id AND p.id = f.user_id GROUP BY n.source;"
c.execute(query)
resultado = c.fetchall()
for registro in resultado:
	id = registro[0]
	if id in NORM:
		id = NORM[id]
	if id >= 1000:
		if id not in EXT:
			EXT[id] = []
		fac = registro[1].split(",")
		for f in fac:
			if f not in EXT[id]:
				EXT[id].append(f)
query = "SELECT n.target,GROUP_CONCAT(DISTINCT f.name) FROM network n, faculty f, user p WHERE n.target >= 1000 AND n.source < 1000 AND n.source = p.id AND p.id = f.user_id GROUP BY n.target;"
c.execute(query)
resultado = c.fetchall()
for registro in resultado:
	id = registro[0]
	if id in NORM:
		id = NORM[id]
	if id >= 1000:
		if id not in EXT:
			EXT[id] = []
		fac = registro[1].split(",")
		for f in fac:
			if f not in EXT[id]:
				EXT[id].append(f)	

	
NORM2 = {}
query = "SELECT u.id, f.name FROM user u, faculty f WHERE f.user_id = u.id GROUP BY id;"
c.execute(query)
resultado = c.fetchall()
for registro in resultado:
	fac = registro[1]
	if registro[0] in EXT:
		if len(EXT[registro[0]]) == 1:
			fac = "Red Externa "+EXT[registro[0]][0]
	NORM2[registro[0]] = fac

query = "SELECT u.id,CONCAT(u.name,' ',u.last_name), f.name, count(*) FROM user u, faculty f, network n WHERE f.user_id = u.id and (u.id =n.source or u.id = n.target) GROUP BY id;"
c.execute(query)
resultado = c.fetchall()
for registro in resultado:
	
	if registro[0] in NORM:
		user_id = NORM[registro[0]]
	else:
		user_id = registro[0]
	
	group_id = DICT[NORM2[user_id].decode('iso-8859-1').encode('utf8')]
	group = NORM2[user_id].decode('iso-8859-1').encode('utf8')
	G.add_node(int(user_id),type=1,group=group_id,countryOfResidence='Chile',tipo=registro[2].decode('iso-8859-1').encode('utf8'),degree=int(registro[3]),id_P=int(registro[0]),url='',shortBio='',name=registro[1].decode('iso-8859-1').encode('utf8'),comu_name=group)

query = "SELECT n.source, n.target, count(*), group_concat(DISTINCT p.year) as cont,group_concat(DISTINCT n.relation) FROM network n,publication p WHERE p.id = n.relation and p.year != 'rechazada' group by n.source, n.target"
c.execute(query)
resultado = c.fetchall()
for registro in resultado:
	if registro[0] in NORM:
		source = NORM[registro[0]]
	else:
		source = registro[0]
	if registro[1] in NORM:
		target = NORM[registro[1]]
	else:
		target = registro[1]

	G.add_edge(int(source),int(target),linka=int(source),linkb=int(target),weight=int(registro[2]),Familia='',Year=registro[3], edge=registro[4])

salida = open("data/poderopedia_D.json","w")
output = json_graph.node_link_data(G)
print output
output = json.dumps(output)
output = output.replace('"source"','"s"').replace('"target"','"t"').replace('"linka"','"source"').replace('"linkb"','"target"')
print output
salida.write(output)


