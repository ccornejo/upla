<html>
	<head>
		<title>Catálogo del Conocimiento</title>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<script src="js/jquery-1.11.2.min.js"></script>
		<script src="js/bootstrap.min.js"></script>	
		<link href="css/bootstrap.min.css" rel="stylesheet">
 		<link type="text/css" rel="stylesheet" href="css/catalogo.css">	
		<link rel="shortcut icon" href="http://www.upla.cl/inclusion/wp-content/uploads/2013/05/favicon.ico" type="image/x-icon">
	</head>
	
	<style>
	</style>	

	<body>
		<!-- titulo -->
		<?php include 'header.php';?>
		
		<!-- Content -->
		<div class="container-fluid">
		<div class="row">
		
			<div class="col-md-2">
				<?php include 'navbar.php';?>			
			</div> <!-- /col-md-2 -->
		
			<div class="col-md-10">
				<?php include 'breadcrumb.php'; ?>	
				<br/>
				<br/>
				<div id="info-text">
						El <b>Catálogo de Conocimientos en Investigación, Innovación y Difusión</b> permite
					vincular las áreas producción científica, grupos de investigación del entorno científico 					
					con la investigación aplicada transferible al proyecto TRI3. Por otro lado, permite 					
					conocer la relación entre estos grupos y de éstos con otras entidades externas del 				
					medio regional, y extraer los principales datos e indicadores asociados con la 					
					generación y difusión del conocimiento, además de  conocer qué activos intangibles 					
					podrían ser vinculados inter regionalmente a proyectos asociativos o áreas de interés 					
					regional en ámbitos del conocimiento priorizados por el GORE y la economía regional.
					<br/>	
					<br/>				
						El desarrollo del Catálogo ha sido financiado con aportes de fondos externos 					
					provenientes de proyectos regionales del Fondo de Innovación para la Competitividad 					
					FIC (Tricubo / Innovanza).
				</div>
			</div>

		</div><!-- /row -->
		</div><!-- /conteriner -->	
			
		<br/>
		<br/>
		<br/>
		<!-- footer -->
		<?php include 'footer.php'; ?>
	</body>

<script>
$(document).ready(function(){
	$('#tri-inicio').css('border-left-color','#2D2D2D');
 	$('#barra-inicio').css('background-color','#2D2D2D');
 	$('#barra-inicio').css('color','#fff');
 	$('#tri-inicio').css('background-color','#002333');
  	$('#barra1').html('Sobre el Catálogo');
 	$('#tri1').css('border-left-color','#002333');
 	$('#barra1').css('background-color','#002333');
 	$('#barra1').css('color','#fff');
 	
 	$('#tri1').css('background-color','transparent');
 	$('#tri2').css('visibility','hidden');
  	
  	$('#barra2').css('visibility','hidden');

  	$('#tri3').css('visibility','hidden');
})		
</script>		

