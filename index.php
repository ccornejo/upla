<html>
	<head>
		<title>Catálogo del Conocimiento</title>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<script src="js/jquery-1.11.2.min.js"></script>
		<script src="js/bootstrap.min.js"></script>	
		<link href="css/bootstrap.min.css" rel="stylesheet">
 		<link type="text/css" rel="stylesheet" href="css/catalogo.css">	
		<link rel="shortcut icon" href="http://www.upla.cl/inclusion/wp-content/uploads/2013/05/favicon.ico" type="image/x-icon">
	</head>
	
	<style>
	</style>	

	<body>
		<!-- titulo -->
		<?php include 'header.php';?>
		
		<!-- Content -->
		<div class="container-fluid">
		<div class="row">
		
			<div class="col-md-2">
				<?php include 'navbar.php';?>			
			</div> <!-- /col-md-2 -->
		
			<!-- Contenido --> 
			<div class="col-md-10">
				<img src="img/catalogo.png" class="img-index responsive">
				<div id="catalogo">
					<span style="color:#002332">Catálogo de Conocimiento</span>
					<br/>
					<span style="color:#004462">en Investigación,</span> 
					<br/>
					<span style="color:#0088B4">Innovación y Difusión</span>
					<br/> 
					<p><small class="smaller">participación en publicaciones,<br/> proyectos, congresos</small></p>
				</div>
			</div><!-- /col-md-10 -->
						
	
		</div><!-- /row -->
		</div><!-- /conteriner -->	
			
		<!-- footer -->
		<?php include 'footer.php'; ?>
	</body>

<script>
//	$( document ).ready(function() {
//
//	});
</script>		

