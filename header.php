<header>
<div class="container">
	<div class="row">
		<div class="col-md-4">
			<a href="http://www.upla.cl/">
				<img src="/proyecto_upla/img/logo_upla.png" class="img-responsive" id="logo_upla"></img>
			</a>
		</div>
		<div class="col-md-6">
			<a href="/proyecto_upla/" id="titulo">
				<h1>
					Catálogo del Conocimiento
				</h1>
				 <h4 style="color:#004462;font-weigh:bold !important;" >PROYECTO FIC TRICUBO e INNOVANZA - Núcleo de Innovación</h4>
			</a>
		</div>
		<div class="col-md-2">
			<br>
			<a href="/proyecto_upla/upla/edicion/persona.php">
				<button class="btn btn-info">Carga y Edición de datos</button>			
			</a>
		</div>
	</div>
</div>
</header>
<div id="line"></div>
