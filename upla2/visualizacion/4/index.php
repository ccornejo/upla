<?php
//Start session
session_start();
 
//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['sess_user_id']) || (trim($_SESSION['sess_user_id']) == '')) {
	header("location: http://190.162.227.177/usm2/login.html");
	exit();
}
?>
<!DOCTYPE html>
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
    <title>Grafo de Colaboración</title>
    <link rel="shortcut icon" href="http://usm.cl/assets/img/icon/favicon.ico">
    <link type="text/css" rel="stylesheet" href="css/style.css"/>
    <link rel="stylesheet" type="text/css" href="../css/style.css"/>
    <link rel="stylesheet" href="css/jquery-ui.css" />
	  <link href="css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
      .text_tooltip {
        display: inline;
        position: relative;
      }
      .text_tooltip:hover:after {
        bottom: 26px;
        content: attr(title); /* este es el texto que será mostrado */
        left: 20%;
        position: absolute;
        z-index: 98;
        /* el formato gráfico */
        background: rgba(255,255,255, 0.2); /* el color de fondo */
        border-radius: 5px;
        color: #FFF; /* el color del texto */
        font-family: Georgia;
        font-size: 12px;
        padding: 5px 15px;
        text-align: center;
        text-shadow: 1px 1px 1px #000;
        width: 150px;
      }
      .text_tooltip:hover:before {
        bottom: 20px;
        content: "";
        left: 50%;
        position: absolute;
        z-index: 99;
        /* el triángulo inferior */
        border: solid;
        border-color: rgba(255,255,255, 0.2) transparent;
        border-width: 6px 6px 0 6px;
      }
    </style>
  </head>
  <body>
	  <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<a id="img-usm" class="navbar-brand" href="#">
					<img src="/usm2/img/header-usm.png" alt="UTFSM" class="img-thumbnail" width="300">
				</a>
			</div>
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="/usm2"><span class="glyphicon glyphicon-home"></span></a></li>
					<li><a href="../1"><img alt="Navegador de Publicaciones" class="svg" src="../img/1.png" width="20px"></a></li>
					<li><a href="../2"><img alt="Calidad de Publicaciones" class="svg" src="../img/2.png" width="20px"></a></li>
					<li><a href="../3"><img alt="Redes de Colaboración" class="svg" src="../img/3.png" width="20px"></a></li>
					<li class="active"><a href="../4"><img alt="Grafo de Colaboración" class="svg" src="../img/4b.png" width="20px"></a></li>
					<li><a href="../5"><img alt="Productividad en el Tiempo" class="svg" src="../img/5.png" width="20px"></a></li>
				</ul>
				<div id="logout"><a href="http://190.162.227.177/usm2/logout.php">Cerrar Sesion</a></div>
			</div>
		</div>
	</div>
	<div id="title">
		<div>
			<h1>Grafo de Colaboración</h1>
			<p class="lead">Esta visualización muestra la colaboración en artículos científicos de los investigadores de la UTFSM registrados en el sistema.
Cada grupo de investigadores corresponde a un departamento, de esa forma podrás visualizar la colaboración entre estos.
Interactúa con la herramienta para destacar las colaboraciones y modificar la tensión de los enlaces.</p>
		</div>
	</div>
    <div id="main" class="row">
      <div class="btn-toolbar" role="toolbar">
        <div class="btn-group">
          <button type="button" class="btn btn-default year" id="2000">2000</button>
          <button type="button" class="btn btn-default year" id="2001">2001</button>
          <button type="button" class="btn btn-default year" id="2002">2002</button>
          <button type="button" class="btn btn-default year" id="2003">2003</button>
          <button type="button" class="btn btn-default year" id="2004">2004</button>
          <button type="button" class="btn btn-default year" id="2005">2005</button>
          <button type="button" class="btn btn-default year" id="2006">2006</button>
          <button type="button" class="btn btn-default year" id="2007">2007</button>
          <button type="button" class="btn btn-default year" id="2008">2008</button>
          <button type="button" class="btn btn-default year" id="2009">2009</button>
          <button type="button" class="btn btn-default year" id="2010">2010</button>
          <button type="button" class="btn btn-default year" id="2011">2011</button>
          <button type="button" class="btn btn-default year" id="2012">2012</button>
          <button type="button" class="btn btn-default year" id="2013">2013</button>
          <button type="button" class="btn btn-default year" id="2014">2014</button>
          <button type="button" class="btn btn-default year" id="acum">ACUMULADO</button>
        </div>
      
      
      <div class="col-md-3" id="tension">
		  <br>
        <p><b>Curvatura de los enlaces</b></p>
        <input type="range" min="0" max="100" value="85">
        <p><b>Año de búsqueda</b></p>
        <p><div id="anob"><b>2014</b></div></p>
        <p><b>Departamentos</b></p>
        <div id="contenedorDescr">
          <div id="var1p" class="var"><div id="var1" class="DescrVar BIOTECNOLOGIA"></div> <span id="var1t"> Biotecnología</span></div>
          <div id="var2p" class="var"><div id="var2" class="DescrVar ELECTRICIDAD"></div> <span id="var2t"> Electricidad</span></div>
          <div id="var3p" class="var"><div id="var3" class="DescrVar ELECTRONICA"></div> <span id="var3t"> Electrónica</span></div>
          <div id="var4p" class="var"><div id="var4" class="DescrVar FISICA"></div> <span id="var4t"> Física</span></div>
          <div id="var5p" class="var"><div id="var5" class="DescrVar IMM"></div> <span id="var5t"> IMM</span></div>
          <div id="var6p" class="var"><div id="var6" class="DescrVar INDUSTRIAS"></div> <span id="var6t"> Industrias</span></div>
          <div id="var7p" class="var"><div id="var7" class="DescrVar INFORMATICA"></div> <span id="var7t"> Informática</span></div>
          <div id="var8p" class="var"><div id="var8" class="DescrVar ING"></div> <span id="var8t"> Ing. Química</span></div>
          <div id="var9p" class="var"><div id="var9" class="DescrVar MATEMATICA"></div> <span id="var9t"> Matemática</span></div>
          <div id="var10p" class="var"><div id="var10" class="DescrVar MECANICA"></div> <span id="var10t"> Mecánica</span></div>
          <div id="var11p" class="var"><div id="var11" class="DescrVar O"></div> <span id="var11t"> O. civiles</span></div>
          <div id="var12p" class="var"><div id="var12" class="DescrVar QUIMICA"></div> <span id="var12t"> Química</span></div>
          <div id="var13p" class="var"><div id="var13" class="DescrVar ARQUITECTURA"></div> <span id="var13t"> Arquitectura</span></div>
          <div id="var14p" class="var"><div id="var14" class="DescrVar CETAM"></div> <span id="var14t"> CETAM</span></div><br>
          <!-- <div id="var15p" class="var"><div id="var15" class="DescrVar SIN DESCRIPCION"></div> <span id="var15t"> Sin definir</span></div> -->
        </div>
        <label>Índices de Colaboración</label>
        <div id="indice"></div>

        <!-- <div class="tooltip-test" data-toggle="tooltip" title="Tooltip on left">Default Tooltip</divs> -->
        <!-- <text dx="8" dy=".31em" text-anchor="start" class="tooltip-test" data-toggle="tooltip" title="FISICA">Gorazd Cvetic </text> -->

      </div>
      <div class="col-md-9 container" id="grafo_circular"></div>
      
    </div>
    <span id="network_extra">
<span id="tosvg" title="Descarga la red en formato SVG"><a href="javascript:(function () { var e = document.createElement('script'); if (window.location.protocol === 'https:') { e.setAttribute('src', 'https://rawgit.com/NYTimes/svg-crowbar/gh-pages/svg-crowbar.js'); } else { e.setAttribute('src', 'http://nytimes.github.com/svg-crowbar/svg-crowbar.js'); } e.setAttribute('class', 'svg-crowbar'); document.body.appendChild(e); })();"><img src="css/icons/download.png"></a></span>
		</span>
    </div>

	<script src="js/jquery.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/d3.js"></script>
    <script type="text/javascript" src="js/d3.layout.js"></script>
    <script type="text/javascript" src="js/packages.js"></script>
    <script src="../js/svg.js"></script>
    <script type="text/javascript">

		$(document).ready(function(){

      // $(".node a").tooltip({
      //     placement : 'right'
      // });
			
			$('#tosvg a').click(function(){
				//~ $('#tosvg a').attr("download",$("#key").text()+".svg")
							//~ .attr("href",getSVG());
			});
	  })	
      var normalize = (function() {
        var from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç", 
            to   = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc",
            mapping = {};
       
        for(var i = 0, j = from.length; i < j; i++ )
            mapping[ from.charAt( i ) ] = to.charAt( i );
       
        return function( str ) {
            var ret = [];
            for( var i = 0, j = str.length; i < j; i++ ) {
                var c = str.charAt( i );
                if( mapping.hasOwnProperty( str.charAt( i ) ) )
                    ret.push( mapping[ c ] );
                else
                    ret.push( c );
            }      
            return ret.join( '' );
        }
      })();

      var colors = {
        "Q1": "#FF0000",
        "Q2": "#238E23",
        "Q3": "#0000FF",
        "Q4": "#FF00FF",
        "BIOTECNOLOGIA": "#00FFFF",
        "ELECTRICIDAD": "#FFFF00",
        "ELECTRONICA": "#5C3317",
        "FISICA": "#A68064",
        "IMM": "#00009C",
        "INDUSTRIAS": "#EBC79E",
        "INFORMATICA": "#CFB53B",
        "ING": "#FF7F00",
        "MATEMATICA": "#FF2400",
        "MECANICA": "#DB70DB",
        "O": "#00FF7F",
        "QUIMICA": "#236B8E",
        "ARQUITECTURA": "#38B0DE",
        "CETAM": "#DB9370",
        "SIN DEFINIR": "#D8D8BF"
      };

      var w = 850,
          h = 600,
          rx = w / 2,
          ry = h / 2,
          m0,
          rotate = 0;

      var splines = [];

      var cluster = d3.layout.cluster()
          .size([360, ry - 120])
          .sort(function(a, b) { return d3.ascending(a.key, b.key); });

      var bundle = d3.layout.bundle();

      var line = d3.svg.line.radial()
          .interpolate("bundle")
          .tension(.85)
          .radius(function(d) { return d.y; })
          .angle(function(d) { return d.x / 180 * Math.PI; });

      // Chrome 15 bug: <http://code.google.com/p/chromium/issues/detail?id=98951>
      var svg;
      load_graph(2014);
      var div;
      function load_graph(year){
        d3.select("#grafo_circular div").remove();

        div = d3.select("#grafo_circular").insert("div", "h2")
            .style("width", w + "px")
            .style("height", h + "px")
            .style("-webkit-backface-visibility", "hidden");

        svg = div.append("svg:svg")
            .attr("width", w)
            .attr("height", h)
          .append("svg:g")
            .attr("transform", "translate(" + rx + "," + ry + ")");

        svg.append("svg:path")
            .attr("class", "arc")
            .attr("d", d3.svg.arc().outerRadius(ry - 120).innerRadius(0).startAngle(0).endAngle(2 * Math.PI))
            .on("mousedown", mousedown);

        d3.json("/usm2/data/circular"+year+".json", function(classes) {

          var nodes = cluster.nodes(packages.root(classes)),
              links = packages.imports(nodes),
              splines = bundle(links);
          
          // INDICE DE COLABORACION
          var interd = 0;
          var intrad = 0;
          var indice = 0;
          var enlaces = 0;
          var coef = 0;
          for(i=0; i<nodes.length; i++){
            if(nodes[i].imports){
              enlaces += nodes[i].imports.length;
              if(nodes[i].imports.length>0){
                for(j=0; j<nodes[i].imports.length; j++){
                  if(nodes[i].imports[j].split('.')[0]==nodes[i].name.split('.')[0]) intrad++;
                  else interd++;
                }
              }
            }
          }
          enlaces /= 2;
          interd /= 2;
          intrad /= 2;
          var posible = nodes.length*(nodes.length-1)/2
          if(posible != 0){
            coef = enlaces/posible*100;
            coef = coef.toFixed(2);
          }
          if(interd+intrad != 0){
            indice = (interd / (intrad+interd))*100;
            indice = indice.toFixed(2);
          }
          $("#indice").html("En este grafo, la colaboración real corresponde a un <b>" + coef + "%</b> respecto a colaboración potencial. Un <b>" + indice + "%</b> de la colaboración real se da entre departamentos.");
          // ----------------------------------------

          var path = svg.selectAll("path.link")
              .data(links)
            .enter().append("svg:path")
              .attr("class", function(d) { return "link source-" + d.source.key.replace(/ /g, '') + " target-" + d.target.key.replace(/ /g, ''); })
              .attr("d", function(d, i) { return line(splines[i]); });

          svg.selectAll("g.node")
              .data(nodes.filter(function(n) { return !n.children; }))
            .enter().append("svg:g")
              .attr("class", function(d){ return "node " + normalize(d.name.split(".")[0]) })
              .attr("id", function(d) { return "node-" + d.key.replace(/ /g, ''); })
              .attr("transform", function(d) { return "rotate(" + (d.x - 90) + ")translate(" + d.y + ")"; })
            .append("svg:text") 
              .attr("dx", function(d) { return d.x < 180 ? 8 : -8; })
              .attr("dy", ".31em")
              .attr("text-anchor", function(d) { return d.x < 180 ? "start" : "end"; })
              .attr("transform", function(d) { return d.x < 180 ? null : "rotate(180)"; })
              .text(function(d) { return d.key; })
              .on("mouseover", mouseover)
              .on("mouseout", mouseout)
            .append("svg:title")
              .text(function(d) { if(d.name.split('.')[0]=='O') return "O. CIVILES"; else if(d.name.split('.')[0]=='ING') return "ING. QUÍMICA"; else return d.name.split('.')[0]});

          d3.select("input[type=range]").on("change", function() {
            line.tension(this.value / 100);
            path.attr("d", function(d, i) { return line(splines[i]); });
          });
        });

        d3.select(window)
            .on("mousemove", mousemove)
            .on("mouseup", mouseup);

      }

      $(".year").click(function(){
        load_graph($(this).attr("id"));
        $("#anob").html('<b>' + $(this).attr("id") + '</b>')
      });

      function mouse(e) {
        return [e.pageX - rx, e.pageY - ry];
      }

      function mousedown() {
        m0 = mouse(d3.event);
        d3.event.preventDefault();
      }

      function mousemove() {
        if (m0) {
          var m1 = mouse(d3.event),
              dm = Math.atan2(cross(m0, m1), dot(m0, m1)) * 180 / Math.PI;
          div.style("-webkit-transform", "translateY(" + (ry - rx) + "px)rotateZ(" + dm + "deg)translateY(" + (rx - ry) + "px)");
        }
      }

      function mouseup() {
        if (m0) {
          var m1 = mouse(d3.event),
              dm = Math.atan2(cross(m0, m1), dot(m0, m1)) * 180 / Math.PI;

          rotate += dm;
          if (rotate > 360) rotate -= 360;
          else if (rotate < 0) rotate += 360;
          m0 = null;

          div.style("-webkit-transform", null);

          svg
              .attr("transform", "translate(" + rx + "," + ry + ")rotate(" + rotate + ")")
            .selectAll("g.node text")
              .attr("dx", function(d) { return (d.x + rotate) % 360 < 180 ? 8 : -8; })
              .attr("text-anchor", function(d) { return (d.x + rotate) % 360 < 180 ? "start" : "end"; })
              .attr("transform", function(d) { return (d.x + rotate) % 360 < 180 ? null : "rotate(180)"; });
        }
      }

      function mouseover(d) {
        svg.selectAll("path.link.target-" + d.key.replace(/ /g, ''))
            .classed("target", true)
            .each(updateNodes("source", true));

        svg.selectAll("path.link.source-" + d.key.replace(/ /g, ''))
            .classed("source", true)
            .each(updateNodes("target", true));
      }

      function mouseout(d) {
        svg.selectAll("path.link.source-" + d.key.replace(/ /g, ''))
            .classed("source", false)
            .each(updateNodes("target", false));

        svg.selectAll("path.link.target-" + d.key.replace(/ /g, ''))
            .classed("target", false)
            .each(updateNodes("source", false));
      }

      function updateNodes(name, value) {
        return function(d) {
          if (value) this.parentNode.appendChild(this);
          svg.select("#node-" + d[name].key.replace(/ /g, '')).classed(name, value);
        };
      }

      function cross(a, b) {
        return a[0] * b[1] - a[1] * b[0];
      }

      function dot(a, b) {
        return a[0] * b[0] + a[1] * b[1];
      }

    </script>
  </body>
</html>
