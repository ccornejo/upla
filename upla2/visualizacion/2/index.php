<?php
//Start session
session_start();
 
//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['sess_user_id']) || (trim($_SESSION['sess_user_id']) == '')) {
	header("location: http://fractalnet-works.ddns.net/proyecto_upla/usm2/login.html");
	exit();
}
?>
<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<title>Calidad de Publicaciones</title>
	<link rel="shortcut icon" href="http://usm.cl/assets/img/icon/favicon.ico">
	<link rel="stylesheet" href="css/jquery-ui.css" />
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,600">
	<link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/tooltip.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../css/style.css"/>
	<link rel="stylesheet" type="text/css" href="css/style.css"/>
	<style type="text/css">
		#explanation, #explanation2{
			z-index: 11;
		}
	</style>
</head>
<body>
	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<a id="img-usm" class="navbar-brand" href="#">
					<img src="/usm2/img/header-usm.png" alt="UTFSM" class="img-thumbnail" width="300">
				</a>
			</div>
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="/usm2"><span class="glyphicon glyphicon-home"></span></a></li>
					<li><a href="../1"><img alt="Navegador de Publicaciones" class="svg" src="../img/1.png" width="20px"></a></li>
					<li class="active"><a href="../2"><img alt="Calidad de Publicaciones" class="svg" src="../img/2b.png" width="20px"></a></li>
					<li><a href="../3"><img alt="Redes de Colaboración" class="svg" src="../img/3.png" width="20px"></a></li>
					<li><a href="../4"><img alt="Grafo de Colaboración" class="svg" src="../img/4.png" width="20px"></a></li>
					<li><a href="../5"><img alt="Productividad en el Tiempo" class="svg" src="../img/5.png" width="20px"></a></li>
				</ul>
				<div id="logout"><a href="http://fractalnet-works.ddns.net/proyecto_upla/usm2/logout.php">Cerrar Sesion</a></div>
			</div>
		</div>
	</div>
	<div id="title">
			<div>
				<h1>Calidad de Publicaciones</h1>
				<p class="lead">Esta visualización permite ver el aporte de cada uno de los departamentos (primera capa desde el centro) al total de publicaciones registradas en el sistema.<br>Las capas siguientes (desde el centro) corresponden a la proporción de los cuartiles (desde 2007), según factor de impacto de las revistas en las cuales cada departamento publica, y la siguiente, al aporte de cada investigador asociado a ese departamento. <br>Interactúa con la herramienta para destacar cada uno de los campos y obtener información adicional. Para congelar la imagen pincha en el campo que desees y para descongelar, pincha en cualquier otro sitio.
				</p>
			</div>
		</div>	
  <div id="main">	
		<div id="journal">
			<div class="btn-toolbar" role="toolbar">
			  <div class="btn-group">
				<button type="button" class="btn btn-default year" id="2000">2000</button>
				<button type="button" class="btn btn-default year" id="2001">2001</button>
				<button type="button" class="btn btn-default year" id="2002">2002</button>
				<button type="button" class="btn btn-default year" id="2003">2003</button>
				<button type="button" class="btn btn-default year" id="2004">2004</button>
				<button type="button" class="btn btn-default year" id="2005">2005</button>
				<button type="button" class="btn btn-default year" id="2006">2006</button>
				<button type="button" class="btn btn-default year" id="2007">2007</button>
				<button type="button" class="btn btn-default year" id="2008">2008</button>
				<button type="button" class="btn btn-default year" id="2009">2009</button>
				<button type="button" class="btn btn-default year" id="2010">2010</button>
				<button type="button" class="btn btn-default year" id="2011">2011</button>
				<button type="button" class="btn btn-default year" id="2012">2012</button>
				<button type="button" class="btn btn-default year" id="2013">2013</button>
				<button type="button" class="btn btn-default year" id="2014">2014</button>
				<button type="button" class="btn btn-default year" id="acum">ACUMULADO</button>
			  </div>
			</div>
			<div id="sequence"></div>
			<div id="chartContainer">
				<div id="row">
					<div id="sidebar-legend">
						<div id="legend">
							<div class="cell" id="piecontcell">
								<div id="contenedorDescr">
									<div id="legtitle"><p><b>Año de búsqueda</b></p></div>
									<div id="ano"></div>
									<div id="legtitle" class="var"><p><b>Departamentos</b></p></div>
									<div id="var1p" class="var"><div id="var1" class="DescrVar"></div> <span id="var1t">Biotecnología</span></div>
									<div id="var2p" class="var"><div id="var2" class="DescrVar"></div> <span id="var2t">Electricidad</span></div>
									<div id="var3p" class="var"><div id="var3" class="DescrVar"></div> <span id="var3t">Electrónica</span></div>
									<div id="var4p" class="var"><div id="var4" class="DescrVar"></div> <span id="var4t">Física</span></div>
									<div id="var5p" class="var"><div id="var5" class="DescrVar"></div> <span id="var5t">IMM</span></div>
									<div id="var6p" class="var"><div id="var6" class="DescrVar"></div> <span id="var6t">Industrias</span></div>
									<div id="var7p" class="var"><div id="var7" class="DescrVar"></div> <span id="var7t">Informática</span></div>
									<div id="var8p" class="var"><div id="var8" class="DescrVar"></div> <span id="var8t">Ing. Química</span></div>
									<div id="var9p" class="var"><div id="var9" class="DescrVar"></div> <span id="var9t">Matemática</span></div>
									<div id="var10p" class="var"><div id="var10" class="DescrVar"></div> <span id="var10t">Mecánica</span></div>
									<div id="var11p" class="var"><div id="var11" class="DescrVar"></div> <span id="var11t">O. civiles</span></div>
									<div id="var12p" class="var"><div id="var12" class="DescrVar"></div> <span id="var12t">Química</span></div>
									<div id="var13p" class="var"><div id="var13" class="DescrVar"></div> <span id="var13t">Arquitectura</span></div>
									<div id="var14p" class="var"><div id="var14" class="DescrVar"></div> <span id="var14t">CETAM</span></div>
									<!-- <div id="var15p" class="var"><div id="var15" class="DescrVar"></div> <span id="var15t">Sin definir</span></div> -->
									<div id="legtitle" class="var"><p><b>Cuartiles</b></p></div>
									<div id="var16p" class="var"><div id="var16" class="DescrVar"></div> <span id="var16t">Q1</span></div>
									<div id="var17p" class="var"><div id="var17" class="DescrVar"></div> <span id="var17t">Q2</span></div>
									<div id="var18p" class="var"><div id="var18" class="DescrVar"></div> <span id="var18t">Q3</span></div>
									<div id="var19p" class="var"><div id="var19" class="DescrVar"></div> <span id="var19t">Q4</span></div>
								</div>								
							</div>							
						</div>
					</div>
					<div id="chart">
						<div id="explanation" style="visibility: hidden;">
						<span id="percentage"></span><br/>
						de su categoría
						</div>
						<div id="explanation2" style="visibility: hidden;">
							<span id="percentage2"></span><br/>
							del total de publicaciones
						</div>
					</div>
					<div id="sidebar-rank">
						<div id="list"></div>											
					</div>
				</div>
			</div>
		</div>	
	
		<span id="network_extra">
			<span id="tosvg" title="Descarga la red en formato SVG"><a href="javascript:(function () { var e = document.createElement('script'); if (window.location.protocol === 'http:') { e.setAttribute('src', 'http://fractalnet-works.ddns.net/proyecto_upla/usm2/visualizacion/2/js/svg-crowbar.js'); } e.setAttribute('class', 'svg-crowbar'); document.body.appendChild(e); })();"><img src="css/icons/download.png"></a></span>
		</span>
	
  </div>

  <script src="js/jquery.min.js"></script>
  <script src="../js/svg.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script src="js/sequences.js" type="text/javascript"></script>
	<script src="js/d3.v3.min.js"></script>
	<script src="js/functions.js"></script>
	<script>

	$(document).ready(function(){
			

	})	
	</script>
	
</body>
</html>
