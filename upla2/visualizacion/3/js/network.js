function uniqueItems(){
	var unique = [];
	$("g .nodo text").each(function(i,e){
		var thisNodo = $.trim($(e).text());
		if(unique.indexOf(thisNodo) == -1)	unique.push(thisNodo);
		else 	$(e).remove();
	});
	//~ $("g .nodo title").each(function(i,e){
		//~ var thisNodo = $.trim($(e).text());
		//~ if(unique.indexOf(thisNodo) == -1)	unique.push(thisNodo);
		//~ else 	$(e).remove();
	//~ });
	$("g .nodo path").each(function(i,e){
		var thisNodo = $.trim($(e).text());
		if(unique.indexOf(thisNodo) == -1)	unique.push(thisNodo);
		else 	$(e).remove();
	});
}
var linkedByIndex = {};
var exp_flag = 0
function neighboring(a, b) {
	if (linkedByIndex[a.id + "," + b.id]) return linkedByIndex[a.id + "," + b.id];
	if (linkedByIndex[b.id + "," + a.id]) return linkedByIndex[b.id + "," + a.id]; 
}

var width = 1000,
	height = $(window).height()-360,
    dr = 4,      // default point radius
    off = 15,    // cluster hull offset
    expand = {}, // expanded clusters
    data, net, force, hullg, hull, linkg, link, nodeg, node;

var curve = d3.svg.line()
    .interpolate("cardinal-closed")
    .tension(.85);

var fill = d3.scale.category20();

function noop() { return false; }

function nodeid(n) {
  return n.size ? "_g_"+n.group : n.short_name;
}

function linkid(l) {
  var u = nodeid(l.source),
      v = nodeid(l.target);
  return u<v ? u+"|"+v : v+"|"+u;
}

function getGroup(n) { return n.group; }

// constructs the network to visualize
function network(data, prev, index, expand) {
	expand = expand || {};
	var gm = {},    // group map
		nm = {},    // node map
		lm = {},    // link map
		gn = {},    // previous group nodes
		gc = {},    // previous group centroids
		nodes = [], // output nodes
		links = []; // output links

  // process previous nodes for reuse or centroid calculation
	if (prev) {
		prev.nodes.forEach(function(n) {
			var i = index(n), o;
			if (n.size > 0) {
				gn[i] = n;
				n.size = 0;
			}else {
				o = gc[i] || (gc[i] = {x:0,y:0,count:0});
				o.x += n.x;
				o.y += n.y;
				o.count += 1;
			}
		});
	}

  // determine nodes
	for (var k=0; k<data.nodes.length; ++k) {
		var n = data.nodes[k],
			i = index(n),
			l = gm[i] || (gm[i]=gn[i]) || (gm[i]={group:i, size:0, nodes:[]});

    if (expand[i]) {
		// the node should be directly visible
		nm[n.short_name] = nodes.length;
		nodes.push(n);
		if (gn[i]) {
			// place new nodes at cluster location (plus jitter)
			n.x = gn[i].x + Math.random();
			n.y = gn[i].y + Math.random();
		}
	}else{
		// the node is part of a collapsed cluster
		if (l.size == 0) {
			// if new cluster, add to set and position at centroid of leaf nodes
			nm[i] = nodes.length;
			nodes.push(l);
			if (gc[i]) {
				l.x = gc[i].x / gc[i].count;
				l.y = gc[i].y / gc[i].count;
			}
		}
      l.nodes.push(n);
    }
		// always count group size as we also use it to tweak the force graph strengths/distances
		l.size += 1;
		n.group_data = l;
	}

	for (i in gm) { gm[i].link_count = 0; }

	// determine links
	for (k=0; k<data.links.length; ++k) {
		var e = data.links[k],
			u = index(e.source),
			v = index(e.target);
			z = e.edge;
			linkedByIndex[e.source.id + "," + e.target.id] = 1;
		if (u != v) {
			gm[u].link_count++;
			gm[v].link_count++;
		}
		u = expand[u] ? nm[e.source.short_name] : nm[u];
		v = expand[v] ? nm[e.target.short_name] : nm[v];
		var i = (u<v ? u+"|"+v : v+"|"+u),
			l = lm[i] || (lm[i] = {source:u, target:v, size:0, relation:z});
		l.size += 1;
	}
	for (i in lm) { links.push(lm[i]); }

	return {nodes: nodes, links: links};
}

function convexHulls(nodes, index, offset) {
	var hulls = {};

	// create point sets
	for (var k=0; k<nodes.length; ++k) {
		var n = nodes[k];
		if (n.size) continue;
		var i = index(n),
			l = hulls[i] || (hulls[i] = []);
		l.push([n.x-offset, n.y-offset]);
		l.push([n.x-offset, n.y+offset]);
		l.push([n.x+offset, n.y-offset]);
		l.push([n.x+offset, n.y+offset]);
	}

	// create convex hulls
	var hullset = [];
	for (i in hulls) {
		hullset.push({group: i, path: d3.geom.hull(hulls[i])});
	}

	return hullset;
}

function drawCluster(d) {
	return curve(d.path); // 0.8
}
function redraw() {
	vis.attr("transform","translate(" + d3.event.translate + ")" + " scale(" + d3.event.scale + ")");
}


// --------------------------------------------------------
var group;
var entity;
var zoom = d3.behavior.zoom();
var body = d3.select("#vis");
var vis = body.append("svg:svg")
   .attr("width", width)
   .attr("height", height)
   .attr("pointer-events", "all")
   .call(zoom.scaleExtent([0.1, 5]).scale(1).on("zoom", redraw)).on("dblclick.zoom", null)
   .append('svg:g')
   .append('svg:g');
vis.append('svg:rect')
    .attr('width', 1000000)
    .attr('height', 1000000)
    .attr('x', -10000)
    .attr('y', -10000)
    .attr('fill', '#FFF');
d3.json("data/data.json", function(json) {
	data = json;
	for (var i=0; i<data.links.length; ++i) {
		o = data.links[i];
		var auxsource=null;
		var auxtarget=null;
		for (var j=0; j<data.nodes.length; ++j) {
			group = data.nodes[j].affiliation
			if (data.nodes[j].id==o.source)
				o.source = data.nodes[j];
    
			if (data.nodes[j].id==o.target)
				o.target = data.nodes[j];
		}
	}	
	hullg = vis.append("g");
	linkg = vis.append("g");
	nodeg = vis.append("g");
	init();

	vis.attr("opacity", 1e-6)
		.transition()
		.duration(1000)
		.attr("opacity", 1);
	expand[group] = true;
	init();
	uniqueItems();
});
var shape;
function init() {
	if (force) force.stop();

	net = network(data, net, getGroup, expand);

	force = d3.layout.force()
		.nodes(net.nodes)
		.links(net.links)
		.size([width, height])
		.linkDistance(200)
		.linkStrength(function(l, i) {
			return 1;
		})
		.gravity(0.1)   // gravity+charge tweaked to ensure good 'grouped' view (e.g. green group not smack between blue&orange, ...
		.charge(-1000)    // ... charge is important to turn single-linked groups to the outside
		.friction(0.5)   // friction adjusted to get dampened display: less bouncy bouncy ball [Swedish Chef, anyone?]
		.start();

	hullg.selectAll("path.hull").remove();
	hull = hullg.selectAll("path.hull")
		.data(convexHulls(net.nodes, getGroup, off))
		.enter().append("path")
		.attr("class", "hull")
		.attr("d", drawCluster)
		.style("fill", function(d) {return fill(d.group); })
		.on("dblclick", function(d) {			
			expand[d.group] = false; init();
		});

	link = linkg.selectAll("line.link").data(net.links, linkid);
	link.exit().remove();
	link.enter().append("line")
		.style("stroke",function(l){return "#333"})
		.on("mouseover", function(){console.log("sss")})
		.on("mouseout", function() { d3.select(this).style("stroke","orange");})
		.attr("class", "link")
		.attr("relation", function(d){
			if (d.source.comu_name != undefined)
				return d.relation;
		})
		.attr("leaf", function(d){
			if (d.source.comu_name != undefined && d.target.comu_name != undefined)
				return "true";
			if (d.target.comu_name != undefined)
				return "true";
			if (d.source.comu_name == undefined && d.target.comu_name == undefined)
				return "false";
		})
		
		.attr("source",function(d){
			return d.source.short_name
		})
		.attr("target",function(d){
			return d.target.short_name
		})
		.attr("t_target",function(d){
			return d.target.type
		})
		.attr("t_source",function(d){
			return d.source.type
		})
		.attr("x1", function(d) { return d.source.x; })
		.attr("y1", function(d) { return d.source.y; })
		.attr("x2", function(d) { return d.target.x; })
		.attr("y2", function(d) { return d.target.y; })
		.style("stroke-width", function(d) {aux = d.size/15; if (aux > 15) aux = 15; return aux || 1; });
	
	node = nodeg.selectAll("g.nodo").data(net.nodes, nodeid);
	node.exit().remove();
  
  
    
	node.enter().append("g")
		.attr("class","nodo")
		.attr("group",function(d){return d.group})
		.attr("id",function(d){console.log(d);return d.id})
		.attr("name",function(d){
			if (d.short_name) return (d.short_name).toLowerCase();
			else return "grouped";
			});
	
	node.append("path")
		.attr("class", function(d) {return "node" + (d.size?"":" leaf"); })
		.attr("id",function(d){return d.id})
		.attr("name",function(d){
			if (d.short_name)	
				return (d.short_name).toLowerCase()
			if (d.nodes[0].comu_name){
				return d.nodes[0].comu_name	
				}
		})
		.attr("type",function(d){
			if(d.type != undefined) 
				return d.type;
			if(d.type == undefined)
				return "3";
		})
		.attr("d", function(d) {
			r = d.size ? (d.size/8)+5 : (Math.log(d.degree)*10)+5
			if (d.group == 1000)
				r = 10
			if (d.short_name != undefined){
				if (r == -Infinity)
					r = 5
				return "m -"+r+",0 a "+r+","+r+" 0 1, 0 "+r*2+",0 a "+r+","+r+" 0 1, 0 -"+r*2+",0" ;
				}
			if (d.short_name == undefined)
				return "M"+r+",0 L"+r+",0 0,"+r+" -"+r+",0 0,-"+r+" Z";
			
		})
		.style("fill", function(d) {
			  return fill(d.group);
		})
		.style("stroke-width", function(d) {
			if(d.type == undefined)
				return 1;
		})
		.on("mouseover",function(d){
			link.style('stroke', function(l) {
				if (d === l.source || d === l.target)	return "red";
				else 	return l.color;
			});
			node.select("text.leaf").style("fill",function(o){
				return neighboring(d, o) ? "blue" : "black";
			})
			node.select("path.leaf").style("fill",function(o){
				return neighboring(d, o) ? "red" : fill(o.group);;
			})
			d3.select(this).style("fill", "red")
			if (d.short_name){
				$("text[name='"+d.short_name+"']").css("fill","blue")
			}
			else
				$("text[name='"+d.nodes[0].comu_name+"']").css("fill","blue")
			
			
		})
		.on("mouseout",function(d){
			link.style('stroke',function(l){return l.color});
			node.select("path.leaf").style("fill",function(o){
				
				return fill(o.group)
			})
			node.select("text.leaf").style("fill",function(o){
				return "black"
			})
			d3.select(this).style("fill", fill(d.group))
			if (d.short_name)
				$("text[name='"+d.short_name+"']").css("fill","black")
			else
				$("text[name='"+d.nodes[0].comu_name+"']").css("fill","black")

		})		
		.on("dblclick", function(d){
			expand[d.group] = !expand[d.group];
			init();
			hide()
		})
		.text(function(d){
			if (d.short_name != undefined)
				return 'n'+d.id;
			else
				return 'g'+d.group;
		});
	node.append("title")
	
		.text(function(d){try {comu = d.group_data.nodes[0].comu_name}catch(e){comu = ""};return "DEPARTAMENTO: "+comu});
		
	node.append("text")
		.attr("class", function(d) {return "node" + (d.size?"":" leaf"); })
		.attr("name",function(d){
			if (d.short_name)
				return d.short_name.toLowerCase()
			if (d.nodes[0].comu_name)
				return d.nodes[0].comu_name		
		})
		.attr("type",function(d){
			if(d.type != undefined) 
				return d.type;
			if(d.type == undefined)
				return "3";
		})
		.on("mouseover",function(d){
			link.style('stroke', function(l) {
				if (d === l.source || d === l.target)	return "red";
				else	return l.color;
			});
			node.select("path.leaf").style("fill",function(o){
				return neighboring(d, o) ? "red" : fill(o.group);;
			})
			node.select("text.leaf").style("fill",function(o){
				return neighboring(d, o) ? "blue" : "black";
			})
			d3.select(this).style("fill", "blue")
			if (d.short_name)
				$("path[name='"+d.short_name+"']").css("fill","red")
			else
				$("path[name='"+d.nodes[0].comu_name+"']").css("fill","red")
		})
		.on("mouseout",function(d){
			link.style('stroke',function(l){return l.color});
			node.select("text.leaf").style("fill",function(o){
				return "black"
			})
			node.select("path.leaf").style("fill",function(o){
				return fill(o.group)
			})
			d3.select(this).style("fill", "black")
			if (d.short_name)
				$("path[name='"+d.short_name+"']").css("fill",fill(d.group))
			else
				$("path[name='"+d.nodes[0].comu_name+"']").css("fill",fill(d.group))
		})	
		.on("dblclick", function(d){
			expand[d.group] = !expand[d.group];
			init();
			hide()
		})
		.attr("text-anchor", "middle") 
		.attr("fill","#000")
		.style("pointer-events", "none")
		.attr("style", "font-size: 10px")
		.attr("font-weight", "100")
		.text( function(d) {
			if (d.short_name)	return d.short_name		
			if (d.nodes[0].comu_name)	return d.nodes[0].comu_name			
		});
		
	
		
	force.on("tick", function() {
		if (!hull.empty()) {
		hull.data(convexHulls(net.nodes, getGroup, off))
			.attr("d", drawCluster);
		}

		link.attr("x1", function(d) { return d.source.x; })
			.attr("y1", function(d) { return d.source.y; })
			.attr("x2", function(d) { return d.target.x; })
			.attr("y2", function(d) { return d.target.y; });

		node.attr("cx", function(d) { return d.x; })
			.attr("cy", function(d) { return d.y; })
			.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")";});
	});
	
	uniqueItems();
	$("path[class=hull]").hide()
	if (exp_flag == 0){
		$("g.nodo[name='grouped']").each(function(d){
			expand[$(this).attr("group")] = true
		})
		exp_flag = 1
	}
	//~ hide()
			
	//~ color()

}
