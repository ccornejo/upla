<?php
header('Content-type:text/html; charset=utf-8');
include("db.php");
mysql_query("TRUNCATE network;");
$sql = "SELECT GROUP_CONCAT(person_id) as ids,publication_id as pid FROM person_has_publication GROUP BY publication_id";
$resultado =mysql_query($sql);
while ($obj = mysql_fetch_assoc($resultado)){
	try{
		$aux = split(",",$obj[ids]);
		for($i=0;$i<count($aux);$i++){
			$j = 1;
			while(1){
				if ($j >= count($aux))
					break;
				$sql2 = "INSERT INTO network VALUES('".$aux[$i]."','".$aux[$j]."','".$obj[pid]."')";
				mysql_query($sql2);
				$j++;
			}
		}
		
	}catch(exception $e){
		
	}	
}
$sql = "SELECT n.source, n.target, count(*) as weight, GROUP_CONCAT(n.edge SEPARATOR ',') as edge FROM  network as n group by n.source, n.target";
$resultado = mysql_query($sql);
$arr2 = array();
$return = exec("python script.py '".$_mysql."'");
//echo "python script.py '".$type."' '".$dis."' '".$dis_flag."'' ".$oecd."' '".$oecd_flag."' '".$inv."' '".$inv_flag."' '".$afil."' '".$afil_flag."' '".$city."' '".$city_flag."' '".$year."' '".$year_flag."' '".$region."' '".$region_flag."' '".$_mysql."'";

$return = explode('?',$return);
$betarray = get_measure($return[0]);
$degarray = get_measure($return[1]);
$prarray = get_measure($return[2]);


$persons = array();
$cont_proj = 0;
while ($obj = mysql_fetch_assoc($resultado)){
	if ((int)$obj[source] > 3000) $obj[source] = 1000000;
	if ((int)$obj[target] > 3000) $obj[target] = 1000000;
	$arr2[] = array('source' => ($obj[source]),
					'target' => ($obj[target]),
					'weight' => (int)($obj[weight]),
					'edge' => $obj[edge],
					 );
	$cont_proj += (int)($obj[cont]);
}

$sql = "SELECT p.id,p.name,p.last_name,p.m_name,p.short_name,p.run,p.type,p.is_usm,d.name as comu_name,d.id as 'group' FROM person as p, department as d, person_has_department as phd,person_has_publication as php WHERE phd.department_id = d.id AND php.person_id = p.id AND phd.person_id = p.id GROUP BY p.id";

$resultado = mysql_query($sql);
$arr = array();

while ($obj = mysql_fetch_assoc($resultado)){
	if ((int)$obj[id] < 3000){
		$arr[] = array('id' => ($obj[id]),
					   'name' => utf8_encode($obj[name]),
					   'last_name' => utf8_encode($obj[last_name]),
					   'm_name' => utf8_encode($obj[m_name]),
					   'short_name' => utf8_encode($obj[short_name]),
					   'run' => utf8_encode($obj[run]),
					   'type' => utf8_encode($obj[type]),
					   'comu_name' => utf8_encode($obj[comu_name]),
					   'group' => utf8_encode($obj[group]),
					   'degree' => (int)($degarray[($obj[id])]),
					   'betweenness' => (float)($betarray[($obj[id])]),
					   'pagerank' => (float)($prarray[($obj[id])]),
						 );
	}
}
$arr[] = array(	'id' => 1000000,
				'name' => '',
				'last_name' => '',
				'm_name' => '',
				'short_name' => 'RED EXTERNA USM',
				'run' => '',
				'type' => '',
				'comu_name' => "RED EXTERNA USM",
				'group' => 10000,
				'degree' => 0,
				'betweenness' => 0,
				'pagerank' => 0,
				);
$salida = '{"nodes":' . json_encode($arr) . ',"links":' . json_encode($arr2) . '}';

$archivo = fopen("../data/data.json","w");
fputs($archivo,$salida);


?>
<?

//include("redir.php");

function cleaner($s){
	if (strlen($s) != 0){
		$s = '"'.$s.'"';
		$s = utf8_decode($s);
		$s = str_replace(',','","',$s);
	}else{
		$s = "";
	}
	return $s;
}
function is_empty($s,$id){	
	if (strlen($s) != 0){
		if($id == 1){
			$s_flag = "project_has_area as pha,";
		}
		if($id == 3){
			$s_flag = "affiliation as a,";
		}		
		if($id == 4){
			$s_flag = "person as p,";
		}
		if($id == 5){
			$s_flag = "region as r,";
		}
		if($id == 7){
			$s_flag = "project_has_OECD_classification as phocde,";
		}
	}else{
		$s_flag = '';
	}
	return $s_flag;
}

function complete_sql($s,$id){
	if (strlen($s) != 0){
		if($id == 1){
			$s = "pha.project_id = pr.id AND pha.area_id IN (".$s.") AND ";
		}

		if($id == 2){
			$s = "(pn.source IN (".$s.") OR pn.target IN (".$s.") ) AND ";
		}
		if($id == 3){
			$s = "(pn.source = a.person_id or pn.target = a.person_id) AND  a.institution_id IN (".$s.") AND ";
		}
		if($id == 4){
			$s = "(pn.source = p.id or pn.target = p.id) AND p.city IN (".$s.") AND ";
		}
		if($id == 5){
			$s = "r.city = p.city AND r.region IN (".$s.") AND ";
		}
		if($id == 6){
			$s = "pr.year IN (".$s.") AND ";
		}
		if($id == 7){
			$s = "phocde.project_id = pr.id AND phocde.OECD_classification_id IN (".$s.") AND ";
		}
	}
	return $s;
}

function add_statistics($s,$t,$k){
	if (strlen($s) != 0){
		$q = explode(',',$s);	
		for ($i=0;$i < count($q); $i++){
			$sql = "INSERT INTO statistics(key_id,code,type,date) VALUES ('".$k."','".utf8_decode($q[$i])."',".$t.",now());";
			mysql_query($sql);
		}
	}
}

function is_flag($k){
	$q = "SELECT * FROM statistics WHERE key_id = '".$k."'";
	$s = mysql_query($q);
	if (mysql_fetch_array($s)){
		$f = 1;
	}
	else{
		$f = 0;
	}
	return $f;
}

function get_data($s,$d){
	$r = mysql_query($s);
	$b= array();
	while ($o = mysql_fetch_array($r)){
		$b[$o[id]] = $o[name].$d.$b[$o[id]];
	}
	return $b;
}

function get_measure($v){
	$v = explode(',',$v);
	for($i=0;$i<count($v);$i=$i+2){
		$vr[$v[$i]] = $v[$i+1];
	}
	return $vr;
}

?>
 <head>

 </head>
 <body>
 <p>GENERANDO RED</p>
 </body>
