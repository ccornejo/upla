<?
//~ header('Content-Type: text/html; charset=UTF-8');
$opt = $_GET["opt"];

$_mysql = "localhost;root;complexity*-*;usm";
$mysql_input = explode(";",$_mysql);
$con = mysqli_connect($mysql_input[0],$mysql_input[1],$mysql_input[2],$mysql_input[3]);
if (!mysqli_set_charset($con, "utf8")) {
    $messaje = array("status" => -1, "error" => "Failed to load utf8: " . mysqli_error($con));
}
$query = "SELECT d.name FROM department as d, publication as p, person_has_publication as php, person_has_department as phd WHERE p.id = php.publication_id AND php.person_id = phd.person_id AND phd.department_id = d.id GROUP by d.name";
$res = mysqli_query($con,$query);
$PUB = array();

while($row = mysqli_fetch_array($res)){
	for ($i = 2000; $i <= date("Y"); $i++){
		$PUB[$row["name"]][$i] = 0;
	}
}

$query = "SELECT d.name, p.year FROM department as d, publication as p, person_has_publication as php, person_has_department as phd, person as per WHERE p.id = php.publication_id AND php.person_id = phd.person_id AND phd.department_id = d.id AND per.id = php.person_id AND per.is_usm = 1 GROUP by p.id";
$res = mysqli_query($con,$query);
while($row = mysqli_fetch_array($res)){
	$PUB[$row["name"]][$row["year"]]++;

}

$query = "SELECT  d.name FROM department as d, patent as p, person_has_patent as php, person_has_department as phd WHERE p.id = php.patent_id AND php.person_id = phd.person_id AND phd.department_id = d.id GROUP by d.name";
$res = mysqli_query($con,$query);
$PAT = array();

while($row = mysqli_fetch_array($res)){
	for ($i = 2000; $i <= date("Y"); $i++){
		$PAT[$row["name"]][$i] = 0;
	}
}

$query = "SELECT  d.name, p.year FROM department as d, patent as p, person_has_patent as php, person_has_department as phd, person as per WHERE p.id = php.patent_id AND php.person_id = phd.person_id AND phd.department_id = d.id AND per.id = php.person_id AND per.is_usm = 1 GROUP by p.id";
$res = mysqli_query($con,$query);
while($row = mysqli_fetch_array($res)){
	$PAT[$row["name"]][$row["year"]]++;

}

$query = "SELECT d.name FROM department as d, congress as c, person_has_congress as phc, person_has_department as phd WHERE c.id = phc.congress_id AND phc.person_id = phd.person_id AND phd.department_id = d.id GROUP by d.name";
$res = mysqli_query($con,$query);
$CON = array();

while($row = mysqli_fetch_array($res)){
	for ($i = 2000; $i <= date("Y"); $i++){
		$CON[$row["name"]][$i] = 0;
	}
}

$query = "SELECT d.name, c.year FROM department as d, congress as c, person_has_congress as phc, person_has_department as phd, person as per WHERE c.id = phc.congress_id AND phc.person_id = phd.person_id AND phd.department_id = d.id AND per.id = phd.person_id AND per.is_usm = 1 GROUP by c.id";
$res = mysqli_query($con,$query);
while($row = mysqli_fetch_array($res)){
	$CON[$row["name"]][$row["year"]]++;

}

if ($opt == 1)
	json_gen($PUB);
if ($opt == 2)
	json_gen($CON);
if ($opt == 3)
	json_gen($PAT);


function json_gen($var){
	
	$json = "[";
	foreach($var as $key => $value){
		$json = $json.'{"key":"'.$key.'","values":[';
		foreach($var[$key] as $year => $cont){
			$json = $json.'['.$year.','.$cont.'],';
		}
		$json = $json.']},';
	}
	$json = $json.']';
	$json = str_replace("],]","]]",$json);
	$json = str_replace("]},]","]}]",$json);
	echo $json;
}


?>
