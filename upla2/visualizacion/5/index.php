<?php
//Start session
session_start();
 
//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['sess_user_id']) || (trim($_SESSION['sess_user_id']) == '')) {
	header("location: http://190.162.227.177/usm2/login.html");
	exit();
}
?>
<!DOCTYPE html>
<head>
	<meta charset="utf-8">

	<link href="nv/src/nv.d3.css" rel="stylesheet" type="text/css">
	<title>Productividad en el Tiempo</title>
	<link rel="shortcut icon" href="http://usm.cl/assets/img/icon/favicon.ico">
	<link rel="stylesheet" type="text/css" href="css/style.css"/>
	<link rel="stylesheet" type="text/css" href="../css/style.css"/>
	<link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body class='with-3d-shadow with-transitions'>
	 <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<a id="img-usm" class="navbar-brand" href="#">
					<img src="/usm2/img/header-usm.png" alt="UTFSM" class="img-thumbnail" width="300">
				</a>
			</div>
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="/usm2"><span class="glyphicon glyphicon-home"></span></a></li>
					<li><a href="../1"><img alt="Navegador de Publicaciones" class="svg" src="../img/1.png" width="20px"></a></li>
					<li><a href="../2"><img alt="Calidad de Publicaciones" class="svg" src="../img/2.png" width="20px"></a></li>
					<li><a href="../3"><img alt="Redes de Colaboración" class="svg" src="../img/3.png" width="20px"></a></li>
					<li><a href="../4"><img alt="Grafo de Colaboración" class="svg" src="../img/4.png" width="20px"></a></li>
					<li class="active"><a href="../5"><img alt="Productividad en el Tiempo" class="svg" src="../img/5b.png" width="20px"></a></li>
				</ul>
				<div id="logout"><a href="http://190.162.227.177/usm2/logout.php">Cerrar Sesion</a></div>
			</div>
		</div>
	</div>
	<div id="title">
		<div>
			<h1>Productividad en el Tiempo</h1>
			<p class="lead">Esta visualización muestra la productividad académica (artículos, congresos y patentes) registrada en el sistema en el tiempo y para cada departamento.
Interactúa con la herramienta para visualizar la productividad en cada año y filtrar uno o varios departamentos.</p>
		</div>
	</div>
	<div id="main">
		
	  <div id="opt">
			<input class="nv-legend-text" type="radio" name="opt" value="1" checked>Publicaciones 
			<input class="nv-legend-text" type="radio" name="opt" value="2">Congresos 
			<input class="nv-legend-text" type="radio" name="opt" value="3">Patentes  
	  </div>
	  <label for="opt">Tipo de Datos: &nbsp;</label><br>
	  <div id="graph">
		<svg id="chart1"></svg>
	  </div>
	  <span id="network_extra">
<span id="tosvg" title="Descarga la red en formato SVG"><a href="javascript:(function () { var e = document.createElement('script'); if (window.location.protocol === 'https:') { e.setAttribute('src', 'https://rawgit.com/NYTimes/svg-crowbar/gh-pages/svg-crowbar.js'); } else { e.setAttribute('src', 'http://nytimes.github.com/svg-crowbar/svg-crowbar.js'); } e.setAttribute('class', 'svg-crowbar'); document.body.appendChild(e); })();"><img src="css/icons/download.png"></a></span>
		</span>
  </div>
  
<script src="js/jquery.min.js"></script>
<script src="nv/lib/d3.v3.js"></script>
<script src="nv/nv.d3.js"></script>
<script src="nv/src/utils.js"></script>
<script src="nv/src/models/axis.js"></script>
<script src="nv/src/tooltip.js"></script>
<script src="nv/src/interactiveLayer.js"></script>
<script src="nv/src/models/legend.js"></script>
<script src="nv/src/models/axis.js"></script>
<script src="nv/src/models/scatter.js"></script>
<script src="nv/src/models/stackedArea.js"></script>
<script src="nv/src/models/stackedAreaChart.js"></script>
<script src="../js/svg.js"></script>
<script>


$(document).ready(function(){
			
			$('#tosvg a').click(function(){
				//~ $('#tosvg a').attr("download",$("#key").text()+".svg")
							//~ .attr("href",getSVG());
			});
	})	
function foo(opt) {
    var jqXHR = $.ajax({
        url : "webservice.php?opt="+opt,
        async: false
    });
    //~ console.log(jqXHR.responseText)
	
    return jqXHR.responseText;
}

function stacked(opt){
	
	var histcatexplong = JSON.parse(foo(opt));
	console.log(histcatexplong)
	var colors = d3.scale.category20();
	keyColor = function(d, i) {return colors(d.key)};

	var chart;
	nv.addGraph(function() {
	  chart = nv.models.stackedAreaChart()
				   // .width(600).height(500)
					.useInteractiveGuideline(true)
					.x(function(d) { return d[0] })
					.y(function(d) { return d[1] })
					.color(keyColor)
					.transitionDuration(300);
					//.clipEdge(true);
	//~ console.log(nv)
	// chart.stacked.scatter.clipVoronoi(false);

	  chart.xAxis
		  .tickFormat(function(d) { return d3.time.format("%Y")(new Date(d,0)) });

	  chart.yAxis
		  .tickFormat(d3.format(',.1f'));

	  d3.select('#chart1')
		.datum(histcatexplong)
		.transition().duration(1000)
		.call(chart)
		// .transition().duration(0)
		.each('start', function() {
			setTimeout(function() {
				d3.selectAll('#chart1 *').each(function() {
				  //~ console.log('start',this.__transition__, this)
				  // while(this.__transition__)
				  if(this.__transition__)
					this.__transition__.duration = 1;
				})
			  }, 0)
		  })
		// .each('end', function() {
		//         d3.selectAll('#chart1 *').each(function() {
		//           console.log('end', this.__transition__, this)
		//           // while(this.__transition__)
		//           if(this.__transition__)
		//             this.__transition__.duration = 1;
		//         })});

	  nv.utils.windowResize(chart.update);

	  // chart.dispatch.on('stateChange', function(e) { nv.log('New State:', JSON.stringify(e)); });

	  return chart;
	});
}


$(document).ready(function(){

	stacked(1)
	
	
	$("#opt input[type=radio]").click(function(){
		if($('input:radio[type=radio]:checked').val() == "1"){
			$("#chart1").remove()
			$("#graph").append('<svg id="chart1"></svg>')
			stacked(1)
		}
		if($('input:radio[type=radio]:checked').val() == "2"){
			$("#chart1").remove()
			$("#graph").append('<svg id="chart1"></svg>')
			stacked(2)
		}
		if($('input:radio[type=radio]:checked').val() == "3"){
			$("#chart1").remove()
			$("#graph").append('<svg id="chart1"></svg>')
			stacked(3)
		}
	});
})



</script>
</body>
</html>
