<?php
//Start session
session_start();
 
//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['sess_user_id']) || (trim($_SESSION['sess_user_id']) == '')) {
	header("location: http://190.162.227.177/usm2/login.html");
	exit();
}
?>
<!DOCTYPE html>
<head>
	
	<title>Navegador de Publicaciones</title>
	<link rel="shortcut icon" href="http://usm.cl/assets/img/icon/favicon.ico">
	<link rel="stylesheet" href="css/jquery-ui.css" />
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/tooltip.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../css/style.css"/>
	<link rel="stylesheet" type="text/css" href="css/style.css"/>
</head>
<body>
	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<a id="img-usm" class="navbar-brand" href="#">
					<img src="/usm2/img/header-usm.png" alt="UTFSM" class="img-thumbnail" width="300">
				</a>
			</div>
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="/usm2"><span class="glyphicon glyphicon-home"></span></a></li>
					<li class="active"><a href="../1"><img alt="Navegador de Publicaciones" class="svg" src="../img/1b.png" width="20px"></a></li>
					<li><a href="../2"><img alt="Calidad de Publicaciones" class="svg" src="../img/2.png" width="20px"></a></li>
					<li><a href="../3"><img alt="Redes de Colaboraci" class="svg" src="../img/3.png" width="20px"></a></li>
					<li><a href="../4"><img alt="Grafo de Colaboraci" class="svg" src="../img/4.png" width="20px"></a></li>
					<li><a href="../5"><img alt="Productividad en el Tiempo" class="svg" src="../img/5.png" width="20px"></a></li>
				</ul>
				<div id="logout"><a href="http://190.162.227.177/usm2/logout.php">Cerrar Sesion</a></div>
			</div>
		</div>
	</div>
	<div id="title">
		<div>
			<h1>Navegador de Publicaciones</h1>
			<p class="lead">Esta visualizaci㮠ordena las publicaciones de investigadores de la UTFSM, registradas en el sistema, segꮠaᯬ departamento y cuartil de la revista (desde 2007).
Interactꡠcon la visualizaci㮠para desplegar o cerrar cada uno de los campos.
Pinchando en la publicaci㮠podr᳠acceder a la b곱ueda de 鳴a en google scholar.</p>
		</div>
	</div>
		
		<div id="main">
		  <div id="container">
			<div class="btn-toolbar" role="toolbar">
			  <div class="btn-group">
				<button type="button" class="btn btn-default year" id="2000">2000</button>
				<button type="button" class="btn btn-default year" id="2001">2001</button>
				<button type="button" class="btn btn-default year" id="2002">2002</button>
				<button type="button" class="btn btn-default year" id="2003">2003</button>
				<button type="button" class="btn btn-default year" id="2004">2004</button>
				<button type="button" class="btn btn-default year" id="2005">2005</button>
				<button type="button" class="btn btn-default year" id="2006">2006</button>
				<button type="button" class="btn btn-default year" id="2007">2007</button>
				<button type="button" class="btn btn-default year" id="2008">2008</button>
				<button type="button" class="btn btn-default year" id="2009">2009</button>
				<button type="button" class="btn btn-default year" id="2010">2010</button>
				<button type="button" class="btn btn-default year" id="2011">2011</button>
				<button type="button" class="btn btn-default year" id="2012">2012</button>
				<button type="button" class="btn btn-default year" id="2013">2013</button>
				<button type="button" class="btn btn-default year" id="2014">2014</button>
			  </div>
			</div>
		  </div>
		  <span id="network_extra">
			<span id="tosvg" title="Descarga la red en formato SVG"><a href="javascript:(function () { var e = document.createElement('script'); if (window.location.protocol === 'https:') { e.setAttribute('src', 'https://rawgit.com/NYTimes/svg-crowbar/gh-pages/svg-crowbar.js'); } else { e.setAttribute('src', 'http://nytimes.github.com/svg-crowbar/svg-crowbar.js'); } e.setAttribute('class', 'svg-crowbar'); document.body.appendChild(e); })();"><img src="css/icons/download.png"></a></span>
		</span>
	  </div>
	  
	<script src="js/jquery.min.js"></script>
	<script src="../js/svg.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script src="js/d3.v3.min.js"></script>
	
  <script>

  var DATA;
  var margin = {top: 30, right: 20, bottom: 30, left: 20},
	  width = 960 - margin.left - margin.right,
	  barHeight = 20,
	  barWidth = width * .8;

  var i = 0,
	  duration = 400,
	  root;

  var tree = d3.layout.tree()
	  .nodeSize([0, 20]);

  var diagonal = d3.svg.diagonal()
	  .projection(function(d) { return [d.y, d.x]; });

  var svg = d3.select("#container").append("svg")
	  .attr("width", width + margin.left + margin.right)
	.append("g")
	  .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  load_year("2014");

  $(".year").click(function(){
	load_year($(this).attr("id"));
  });

  function load_year(year){
	d3.json("/usm2/data/" + year + ".json", function(error, flare) {
	  flare.x0 = 0;
	  flare.y0 = 0;
	  DATA = update(root = flare);
	  DATA = DATA.children;
	  DATA.forEach(function(d){click(d)})
	});
  }

  function update(source) {

	// Compute the flattened node list. TODO use d3.layout.hierarchy.
	var nodes = tree.nodes(root);

	var height = Math.max(500, nodes.length * barHeight + margin.top + margin.bottom);

	d3.select("svg").transition()
		.duration(duration)
		.attr("height", height);

	d3.select(self.frameElement).transition()
		.duration(duration)
		.style("height", height + "px");

	// Compute the "layout".
	nodes.forEach(function(n, i) {
	  n.x = i * barHeight;
	});

	// Update the nodesɊ	var node = svg.selectAll("g.node")
		.data(nodes, function(d) { return d.id || (d.id = ++i); });

	var nodeEnter = node.enter().append("g")
		.attr("class", "node")
		.attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
		.style("opacity", 1e-6);

	// Enter any new nodes at the parent's previous position.
	nodeEnter.append("rect")
		.attr("y", -barHeight / 2)
		.attr("class",function(d){return d.name})
		.attr("height", barHeight)
		.attr("width", barWidth)
		.style("fill", color)
		.on("click", click);

	nodeEnter.append("text")
		.attr("dy", 3.5)
		.attr("dx", 5.5)
		.text(function(d) { return d.name; });

	// Transition nodes to their new position.
	nodeEnter.transition()
		.duration(duration)
		.attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; })
		.style("opacity", 1);

	node.transition()
		.duration(duration)
		.attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; })
		.style("opacity", 1)
	  .select("rect")
		.style("fill", color);

	// Transition exiting nodes to the parent's new position.
	node.exit().transition()
		.duration(duration)
		.attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
		.style("opacity", 1e-6)
		.remove();

	// Update the links
	var link = svg.selectAll("path.link")
		.data(tree.links(nodes), function(d) { return d.target.id; });

	// Enter any new links at the parent's previous position.
	link.enter().insert("path", "g")
		.attr("class", "link")
		.attr("d", function(d) {
		  var o = {x: source.x0, y: source.y0};
		  return diagonal({source: o, target: o});
		})
	  .transition()
		.duration(duration)
		.attr("d", diagonal);

	// Transition links to their new position.
	link.transition()
		.duration(duration)
		.attr("d", diagonal);

	// Transition exiting nodes to the parent's new position.
	link.exit().transition()
		.duration(duration)
		.attr("d", function(d) {
		  var o = {x: source.x, y: source.y};
		  return diagonal({source: o, target: o});
		})
		.remove();

	// Stash the old positions for transition.
	nodes.forEach(function(d) {
	  d.x0 = d.x;
	  d.y0 = d.y;
	});
	
	return source
  }

  // Toggle children on click.
  function click(d) {

	if(d.depth == 4){
	  window.open("http://scholar.google.es/scholar?hl=es&q=" + d.name);
	}else{
	  if (d.children) {
		d._children = d.children;
		d.children = null;
	  } else {
		d.children = d._children;
		d._children = null;
	  }
	  update(d);
	}
  }
  
  function color(d) {
	Q = ["Q1","Q2","Q3","Q4","CUARTIL NO DEFINIDO"]
	if (Q.indexOf(d.name) <= -1){
		try{
			if (d.parent.name == "Q1")
				return "blue"
			if (d.parent.name == "Q2")
				return "green"
			if (d.parent.name == "Q3")
				return "orange"
			if (d.parent.name == "Q4")
				return "red"
			if (d.parent.name == "CUARTIL NO DEFINIDO")
				return "purple"
		}catch(e){
			var a = 1
		}
	}
	if (Q.indexOf(d.name) > -1){
		if (d.name == "Q1")
			return "blue"
		if (d.name == "Q2")
			return "green"
		if (d.name == "Q3")
			return "orange"
		if (d.name == "Q4")
			return "red"
		if (d.name == "CUARTIL NO DEFINIDO")
			return "purple"
	}
	if(d.depth == 4)
		return "#FFF";
	if(d.depth == 1)
		return "#BBB";
	if(d.depth == 0)
		return "#666";
	  
  }
  

  
$(document).ready(function(){

	})		


  </script>
</body>
</html>
