<?php
//Start session
session_start();
 
//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['sess_user_id']) || (trim($_SESSION['sess_user_id']) == '')) {
	header("location: http://190.162.227.177/usm2/login.html");
	exit();
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Ingreso de Datos de Académicos, Investigadores y otros</title>
	<link rel="shortcut icon" href="http://usm.cl/assets/img/icon/favicon.ico">
	<link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/datepicker.css" rel="stylesheet">
    <link href="css/token-input.css" type="text/css" rel="stylesheet">
    <link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap-tokenfield.min.css" type="text/css" rel="stylesheet">
    <link href="css/style.css" type="text/css" rel="stylesheet">
    <script src="js/ie-emulation-modes-warning.js"></script>
    <script src="js/ie10-viewport-bug-workaround.js"></script>
	<style type="text/css">
		.grado, #grado1, #tip, #ing, #ret, #dep, #importar, #eliminar{display: none;}
	</style>
		
		
		
</head>
<body>
	<div class="content">
		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<a id="img-usm" class="navbar-brand" href="#">
						<img src="/usm2/img/header-usm.png" alt="UTFSM" class="img-thumbnail" width="300">
					</a>
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a href="/usm2"><span class="glyphicon glyphicon-home"></span></a></li>
						<li class="active"><a href="persona.php">Persona</a></li>
						<li><a href="proyecto.php">Proyecto</a></li>
						<li><a href="publicacion.php">Publicación</a></li>
						<li><a href="congreso.php">Congreso</a></li>
						<li><a href="patente.php">Patente</a></li>
						<li><a href="revista.php">Revista</a></li>
					</ul>
					<div id="logout"><a href="http://190.162.227.177/usm2/logout.php">Cerrar Sesion</a></div>
				</div>
			</div>
		</div>
		<div id="title">
			<div>
				<h1>Personas</h1>
				<p class="lead">Módulo para el ingreso y edición de datos de académicos, investigadores y otros</p>
			</div>
		</div>
		<div class="field">
			<!-- DATOS PERSONALES -->
			<div class="row">
				<h2>Datos Personales</h2>
				
				<div class="person">
					<div>
						<font>Para cargar datos ya registrados o editar estos, ingresa su <b>NOMBRE o RUN</b></font>
						<input type="text" class="form-control" id="run_editar" placeholder="Ej.: 12587162-k">
						*Si la persona no está en la lista, ingresa sus datos manualmente.
					</div>
				</div>

				<div class="col-lg-3">
					<label>RUN</label>
					<input type="text" class="form-control" id="run" placeholder="Ej.: 12587162-k">
				</div>
				<div class="col-lg-3">
					<label>Nombres</label>
					<input type="text" class="form-control" id="nombre" placeholder="Ej.: Juan Carlos">
				</div>
				<div class="col-lg-3">
					<label>Apellido Paterno</label>
					<input type="text" class="form-control" id="apellidop" placeholder="Ej.: Salazar">
				</div>
				<div class="col-lg-3">
					<label>Apellido Materno</label>
					<input type="text" class="form-control" id="apellidom" placeholder="Ej.: Rodríguez">
				</div>
			</div>
			<!-- FORMACIÓN ACADÉMICA -->
			<div class="row">
				<h2>Formación Académica</h2>
				<table class="person table">
					<thead>
						<tr>
							<th>Tipo</th>
							<th>Titulo/Grado</th>
							<th>Máx.</th>
							<th>Especialidad / Mención</th>
							<th>Universidad</th>
							<th>País</th>
							<th>Año de Egreso</th>
							<th>
								<button type="button" class="btn btn-default" id="agrega_grado">
									<span class="glyphicon glyphicon-plus"></span>
								</button>
							</th>
						</tr>
					</thead>
					<tbody id="tabla_grado">
						<tr>
							<td>
								<div class="radio tipo">
									<label>
										<input type="radio" class="es_titulo" name="opciones" id="es_titulo-1" value="0" checked="true">
										Título
									</label>
									<label>
										<input type="radio" class="es_grado" name="opciones" id="es_grado-1" value="1">
										Grado
									</label> 
								</div>
							</td>
							<td>
								<input type="text" class="form-control titulo" id="titulo-1" placeholder="Ej.: Ingeniero Civil / Dr. PhD. Magíster">
								<select class="form-control grado" id="grado-1" placeholder="Selecciona título">
									<option value="magister">Magister</option>
									<option value="doctorado">Doctorado</option>
								</select>
							</td>
							<td><input type='radio' name='max' class="max" id="max-1" checked="true"></td>
							<td><input type="text" class="form-control especialidad" id="especialidad-1" placeholder="Ej.: Modelación Matemática"></td>
							<td><input type="text" class="tokenfield universidad" id="universidad-1" placeholder="Ej.: Yale University"></td>
							<td><input type="text" class="form-control pais" id="pais_egreso-1" placeholder="Ej. Alemania"></td>
							<td><input type="text" class="form-control ano year_picker" id="egreso-1" placeholder="YYYY"></td>
							<td>
								<button type="button" class="btn btn-default quita" id="quita_grado-1" onclick='borralinea($(this))'>
									<span class="glyphicon glyphicon-remove"></span>
								</button>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- ANTECEDENTES ACADÉMICOS -->
			<div class="row">
				<h2>Antecedentes Académicos</h2>
				<div class="col-lg-12">
					<label class="nontitle">
						<input type="checkbox" id="usm"> ¿Afiliado a la UTFSM? (Marque si la persona actualmente está afiliada a la UTFSM)
					</label>
				</div>
				<div class="col-lg-12" id="ret">
					<label class="nontitle">
						<input type="checkbox" id="retirado"> Retirado? (si la persona está retirada de la institución MARQUE)
					</label>
				</div>
				<div class="col-lg-3" id="ins">
					<label>Institución</label>
					<input type="text" class="tokenfield universidad" id="institucion">
				</div>
				<div class="col-lg-2" id="tip">
					<label>Tipo de Académico</label>
					<select class="form-control" id="tipo" placeholder="Selecciona tipo">
						<option value="Académico Titular">Académico Titular</option>
						<option value="Académico Adjunto">Académico Adjunto</option>
						<option value="Académico Auxiliar">Académico Auxiliar</option>
						<option value="Académico Instructor">Académico Instructor</option>
						<option value="Académico Joven Mecesup (AJ)">Académico Joven Mecesup (AJ)</option>
						<option value="Acad / Docente jornada parcial (PT)">Acad / Docente jornada parcial (PT)</option>
						<option value="Apoyo Académico (AA)">Apoyo Académico (AA)</option>
						<option value="Investigador Jóven (IJ - PIIJ)">Investigador Jóven (IJ - PIIJ)</option>
						<option value="Investigador Asociado (IA)">Investigador Asociado (IA)</option>
						<option value="Asistente Científico (AC - PAC)">Asistente Científico (AC - PAC)</option>
						<option value="Investigador Inserto a la Academia Conicyt (IIA)">Investigador Inserto a la Academia Conicyt (IIA)</option>
						<option value="Postdoctorado (PD)">Postdoctorado (PD)</option>
						<option value="Investigador retornado Conicyt (IRC)">Investigador retornado Conicyt (IRC)</option>
						<option value="Investigador Inserto a la Empresa (IIE)">Investigador Inserto a la Empresa (IIE)</option>
						<option value="Otro Investigador (INV)">Otro Investigador (INV)</option>
						<option value="Inserción a la Investigación (alumnos) (PIC - PIIC)">Inserción a la Investigación (alumnos) (PIC - PIIC)</option>
					</select>
				</div>
				<div class="col-lg-2" id="ing">
					<label>Fecha de Ingreso</label>
					<input type="text" class="form-control date_picker" id="ingreso" placeholder="Ej.: día/mes/año (1/3/74)" class="datepicker">
				</div>

				<div class="col-lg-2" id="dep">
					<label>Departamento(s)</label>
					<input type="text" class="form-control" id="departamento" placeholder="Ej.: BIOTECNOLOGÍA" class="tokenfield">
				</div>

				<div class="col-lg-3">
					<label>Área de Trabajo/Investigación</label>
					<input type="text" class="form-control" id="area" placeholder="Ej.: Optimización" class="tokenfield">
				</div>
				
				<!-- PROGRAMAS -->
				<div class="col-lg-12" id="pro">
					<div class="row">
						<h2>Programa de Magíster/Doctorado</h2>
						<table class="person table">
							<thead>
								<tr>
									<th>Programa</th>
									<th>Año de Ingreso</th>
									<th>Tipo de Participación</th>
									<th>
										<button type="button" class="btn btn-default agrega_programa" id="agrega_programa">
											<span class="glyphicon glyphicon-plus"></span>
											<!-- <font color="blue"> Agregar Programa</font> -->
										</button>
									</th>
								</tr>
							</thead>
							<tbody id="tabla_programa">
								<tr>
									<td>
										<select class="form-control programa" id="programa" placeholder="Selecciona programa_participa">
											<option value="1">Doctorado en Biotecnología</option>
											<option value="2">Doctorado en Ciencias, mención Física</option>
											<option value="3">Doctorado en Ciencias, mención Química</option>
											<option value="4">Doctorado en Matemática</option>
											<option value="5">Doctorado en Ingeniería Electrónica</option>
											<option value="6">Doctorado en Ingeniería Informática</option>
											<option value="7">Doctorado en Ingeniería Química</option>
											<option value="8">Magíster en Ciencias de la Ingeniería Civil</option>
											<option value="9">Magíster en Ciencias de la Ingeniería Eléctrica</option>
											<option value="10">Magíster en Ciencias de la Ingeniería	Electrónica</option>
											<option value="11">Magíster en Ciencias de la Ingeniería Informática</option>
											<option value="12">Magíster en Ciencias de la Ingeniería Mecánica</option>
											<option value="13">Magíster en Ciencias de la Ingeniería Química</option>
											<option value="14">Magíster en Ciencias de la Ingeniería Industrial</option>
											<option value="15">Magíster en Ciencias de la Ingeniería Telemática</option>
											<option value="16">Magíster en Ciencias de la Ingeniería Metalúrgica</option>
											<option value="17">Magíster en Ciencias, mención Física</option>
											<option value="18">Magíster en Ciencias, mención Matemática</option>
											<option value="19">Magíster en Ciencias, mención Química</option>
											<option value="20">Magíster en Redes y Telecomunicaciones</option>
											<option value="21">Magíster en Economía Energética</option>
											<option value="22">MBA-Magíster en Gestión Empresarial </option>
											<option value="23">Magíster en Ingeniería Aeronáutica</option>
											<option value="24">Magíster en Innovación Tecnológica y Emprendimiento</option>
											<option value="25">Magíster en Gestión de Activos y Mantenimiento</option>
											<option value="26">Magíster en Tecnologías de la Información</option>
											<option value="27">Magíster en Gestión y Tecnología Agronómica</option>
										</select> 
									</td>
									<td>
										<input type="text" class="form-control ano year_picker" id="ano_programa" placeholder="Ej.: 2002">
									</td>
									<td>
										<div class="radio-inline">
											<input type="radio" name="participacion" value="claustro" class="claustro" checked>Claustro
										</div>
										<div class="radio-inline">
											<input type="radio" name="participacion" value="colaborador" class="colaborador">Colaborador
										</div>
										<div class="radio-inline">
											<input type="radio" name="participacion" value="visita" class="visitante">Visitante
										</div>
									</td>
									<td class="quita_programa_td">
										<button type="button" class="btn btn-default quita" onclick='borralinea($(this))'>
											<span class="glyphicon glyphicon-remove"></span>
											<!-- <font color="blue"> Quitar Programa</font> -->
										</button>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<!-- TESIS -->
				<div class="col-lg-12">
					<div class="row">
						<h2>Tesis Guiadas</h2>
						<table class="person table">
							<thead>
								<tr>
									<th>Tipo</th>
									<th>Programa</th>
									<th>Tipo de participación</th>
									<th>Universidad</th>
									<th>Título</th>
									<th>Año</th>
									<th>Estudiante</th>
									<th>
										<button type="button" class="btn btn-default agrega_tesis" id="agrega_tesis">
											<span class="glyphicon glyphicon-plus"></span>
											<!-- <font color="blue"> Agregar Tesis</font> -->
										</button>
									</th>
								</tr>
							</thead>
							<tbody id="tabla_tesis">
								<tr>
									<td>
										<select class="form-control tipo_tesis" id="tipo_tesis-1" placeholder="Selecciona tipo">
											<option value="pregrado">Pregrado</option>
											<option value="magister">Magíster</option>
											<option value="doctorado">Doctorado</option>
										</select>
									</td>
									<td>
										<select class="form-control programa" id="programa_tesis-1" placeholder="Selecciona">
											<option value="1">Doctorado en Biotecnología</option>
											<option value="2">Doctorado en Ciencias, mención Física</option>
											<option value="3">Doctorado en Ciencias, mención Química</option>
											<option value="4">Doctorado en Matemática</option>
											<option value="5">Doctorado en Ingeniería Electrónica</option>
											<option value="6">Doctorado en Ingeniería Informática</option>
											<option value="7">Doctorado en Ingeniería Química</option>
											<option value="8">Magíster en Ciencias de la Ingeniería Civil</option>
											<option value="9">Magíster en Ciencias de la Ingeniería Eléctrica</option>
											<option value="10">Magíster en Ciencias de la Ingeniería	Electrónica</option>
											<option value="11">Magíster en Ciencias de la Ingeniería Informática</option>
											<option value="12">Magíster en Ciencias de la Ingeniería Mecánica</option>
											<option value="13">Magíster en Ciencias de la Ingeniería Química</option>
											<option value="14">Magíster en Ciencias de la Ingeniería Industrial</option>
											<option value="15">Magíster en Ciencias de la Ingeniería Telemática</option>
											<option value="16">Magíster en Ciencias de la Ingeniería Metalúrgica</option>
											<option value="17">Magíster en Ciencias, mención Física</option>
											<option value="18">Magíster en Ciencias, mención Matemática</option>
											<option value="19">Magíster en Ciencias, mención Química</option>
											<option value="20">Magíster en Redes y Telecomunicaciones</option>
											<option value="21">Magíster en Economía Energética</option>
											<option value="22">MBA-Magíster en Gestión Empresarial </option>
											<option value="23">Magíster en Ingeniería Aeronáutica</option>
											<option value="24">Magíster en Innovación Tecnológica y Emprendimiento</option>
											<option value="25">Magíster en Gestión de Activos y Mantenimiento</option>
											<option value="26">Magíster en Tecnologías de la Información</option>
											<option value="27">Magíster en Gestión y Tecnología Agronómica</option>
											<option value="28">Programas de Otras Universidades</option>
										</select> 
									</td>
									<td>
										<select class="form-control participacion_tesis" id="participacion_tesis-1" placeholder="Selecciona tipo">
											<option value="Director">Director</option>
											<option value="Co-Director">Co-Director</option>
										</select>
									</td>
									<td><input type="text" class="tokenfield universidad" id="u_tesis-1" placeholder="Ej.: UTFSM"></td>
									<td><input type="text" class="form-control titulo" id="titulo_tesis-1" placeholder="Ej.: Estudio de la radiación..."></td>
									<td><input type="text" class="form-control ano year_picker" id="ano_tesis-1" placeholder="Ej.: 2002"></td>
									<td><input type="text" class="tokenfield estudiante" id="estudiante-1" placeholder="Ej.: Juan Pablo Soto"></td>
									
									<td class="quita_tesis_td">
										<button type="button" class="btn btn-default quita" onclick='borralinea($(this))'>
											<span class="glyphicon glyphicon-remove"></span>
										</button>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="button">
				<button type="button" class="btn btn-default" id="guardar">GUARDAR</button>
				<button type="button" class="btn btn-default" id="eliminar">ELIMINAR</button>
				<button type="button" class="btn btn-default" id="importar">IMPORTAR</button>
			</div>
			<br><br><br>
		</div>
	</div>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.tokeninput.js"></script>
	<script src="js/jquery-ui.min.js"  type="text/javascript"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-tokenfield.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/functions.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script type="text/javascript">
      var idpersona = '';
      var idgrado = new Array();
      var iddepartamento = new Array();
      var idprograma = new Array();
      var idtesis = new Array();
      var idpatente = new Array();
      var jsondeptos = [{value:"1", label:"ARQUITECTURA"},
                { value:"2", label:"BIOTECNOLOGÍA"},
                { value:"3", label:"CETAM"},
                { value:"4", label:"ELECTRICIDAD"},
                { value:"5", label:"ELECTRÓNICA"},
                { value:"6", label:"FISICA"},
                { value:"7", label:"IMM"},
                { value:"8", label:"INDUSTRIAS"},
                { value:"9", label:"INFORMÁTICA"},
                { value:"10", label:"ING. DISEÑO PRODUCTOS"},
                { value:"11", label:"ING. MECÁNICA"},
                { value:"12", label:"ING. QUÍMICA"},
                { value:"13", label:"MATEMÁTICA"},
                { value:"14", label:"MECÁNICA"},
                { value:"15", label:"O.CIVILES"},
                { value:"16", label:"QUÍMICA"},
                { value:"17", label:"SEDE VIÑA DEL MAR (JMC)"},
                { value:"18", label:"SEPARI"}];
      var jsonprog = [{value:"1", nombre:"Doctorado en Biotecnología"},
                {value:"2", nombre:"Doctorado en Ciencias, mención Física"},
                {value:"3", nombre:"Doctorado en Ciencias, mención Química"},
                {value:"4", nombre:"Doctorado en Matemática"},
                {value:"5", nombre:"Doctorado en Ingeniería Electrónica"},
                {value:"6", nombre:"Doctorado en Ingeniería Informática"},
                {value:"7", nombre:"Doctorado en Ingeniería Química"},
                {value:"8", nombre:"Magíster en Ciencias de la Ingeniería Civil"},
                {value:"9", nombre:"Magíster en Ciencias de la Ingeniería Eléctrica"},
                {value:"10", nombre:"Magíster en Ciencias de la Ingeniería  Electrónica"},
                {value:"11", nombre:"Magíster en Ciencias de la Ingeniería Informática"},
                {value:"12", nombre:"Magíster en Ciencias de la Ingeniería Mecánica"},
                {value:"13", nombre:"Magíster en Ciencias de la Ingeniería Química"},
                {value:"14", nombre:"Magíster en Ciencias de la Ingeniería Industrial"},
                {value:"15", nombre:"Magíster en Ciencias de la Ingeniería Telemática"},
                {value:"16", nombre:"Magíster en Ciencias de la Ingeniería Metalúrgica"},
                {value:"17", nombre:"Magíster en Ciencias, mención Física"},
                {value:"18", nombre:"Magíster en Ciencias, mención Matemática"},
                {value:"19", nombre:"Magíster en Ciencias, mención Química"},
                {value:"20", nombre:"Magíster en Redes y Telecomunicaciones"},
                {value:"21", nombre:"Magíster en Economía Energética"},
                {value:"22", nombre:"MBA-Magíster en Gestión Empresarial "},
                {value:"23", nombre:"Magíster en Ingeniería Aeronáutica"},
                {value:"24", nombre:"Magíster en Innovación Tecnológica y Emprendimiento"},
                {value:"25", nombre:"Magíster en Gestión de Activos y Mantenimiento"},
                {value:"26", nombre:"Magíster en Tecnologías de la Información"},
                {value:"27", nombre:"Magíster en Gestión y Tecnología Agronómica"},
                {value:"28", nombre:"Programas de Otras Universidades"}];
      var jsonu = new Array();
      $(document).ready(function(){
        $('html, body').animate({ scrollTop: 0 }, 'fast');
        var urlws = 'webservice/webservice.php';
       	
        $("#ingreso").datepicker({
          format: "dd-mm-yyyy",
          weekStart: 1,
          autoclose: true
        });
        $("#run_editar").tokenInput("/usm2/data/persons.json", {
          resultsLimit:10,
          tokenLimit: 1,
          noResultsText: "No hay resultados para la búsqueda.", 
          onAdd: function(data){
            $("#run").attr("disabled", true);
            idpersona = data.id;
            desplegardatos(data.id);
          },
          onDelete: function(data){
            idpersona = '';
            idgrado = [];
            iddepartamento = [];
            idprograma = [];
            idtesis = [];
            idpatente = [];

            $("#run").attr("disabled", false);
          }
        });
       
        $('#departamento').tokenfield({
          limit: 10,
          createTokensOnBlur: true,
          autocomplete: {
            source: jsondeptos,
            delay: 100,
            minLength: 1
          },
          showAutocompleteOnFocus: true
        }); 
        $('#departamento').on('tokenfield:createtoken', function (data) {
          if (data.attrs.value == data.attrs.label){
            return false;
          } 
        });
        $('#area').tokenfield({
          limit: 10,
          createTokensOnBlur: true,
          autocomplete: {
            delay: 100,
            source: [],
            minLength: 1
          },
          showAutocompleteOnFocus: true
        }); 
        tipo_grado();
        ano_datepicker();
        universidad();
        estudiante();
      });

      $("#guardar").click(function(){
        var jsondatos = new Object();
        jsondatos.run = $("#run").val();
        jsondatos.nombre = $("#nombre").val();
        jsondatos.apellidop = $("#apellidop").val();
        jsondatos.apellidom = $("#apellidom").val();
        jsondatos.nombrecorto = $("#nombre").val().charAt(0) + '. ' + $("#apellidop").val();
        jsondatos.usm = booltoint($("#usm").prop("checked"));
        jsondatos.institucion = $("#institucion").val();
        jsondatos.tipo = $("#tipo").val();
        if ($("#ingreso").val() != ""){
          var fechasplit = $("#ingreso").val().split('-');  
          jsondatos.ingreso = fechasplit.pop() + '-' + fechasplit.pop() + '-' + fechasplit.pop();
        }
        jsondatos.retirado = booltoint($("#retirado").prop("checked"));

        ;
        jsondatos.departamento = $("#departamento").tokenfield("getTokensList");
        jsondatos.grado = tr_to_json("tabla_grado", ["tipo","titulo_grado","max","especialidad","universidad","pais","ano"])
        jsondatos.area =  $("#area").tokenfield("getTokensList");
        jsondatos.programa =  tr_to_json("tabla_programa",["programa","ano","participacion"]);
        jsondatos.tesis =  tr_to_json("tabla_tesis",["tipo_tesis","programa","participacion_tesis","universidad","titulo","ano","estudiante"]);

        console.log(jsondatos);

        var mensaje = '';
        if(jsondatos.run == '') mensaje += '\n-RUN';
        if(jsondatos.nombre == '') mensaje += '\n-Nombre';
        if(jsondatos.apellidop == '') mensaje += '\n-Apellido paterno';
        if(jsondatos.apellidom == '') mensaje += '\n-Apellido materno';
        if(jsondatos.institucion == '') mensaje += '\n-Institución';

        if(mensaje != '') 
          alert('Debe llenar los siguientes campos:' + mensaje);
        else{
          var metodo = idpersona == '' ? 'add_data_to_table' : 'update_table';
          var tabla = 'person';
          var arg1 = ['name', 'last_name','m_name','short_name','run','type','date','is_retired','is_usm', 'area','institution_id'];
          var arg2 = [jsondatos.nombre, jsondatos.apellidop, jsondatos.apellidom, jsondatos.nombrecorto, jsondatos.run, jsondatos.tipo, jsondatos.ingreso, jsondatos.retirado, jsondatos.usm, jsondatos.area, jsondatos.institucion];
          var arg = idpersona == '' ? [tabla, arg1, arg2] : [tabla, idpersona, arg1, arg2];
          var salida = new Object();
          salida["stataus"] = 1;
          salida["message"] = "Persona guardada correctamente"
          // GUARDANDO / EDITANDO DATOS PERSONA
          invocar_webservice(metodo, arg, function(data){
            if(data.stataus == -1){
              salida["status"] = -1;
              salida["message"] = "Error al ingresar persona";
              salida["result"] = data;
            }
            console.log(data);
            if(data.id) idpersona = data.id;
            var tablas = ['person_has_degree','person_has_department','person_has_program','person_has_thesis','person_has_patent'];
            // ELIMINANDO LAS RELACIONES DE LA PERSONA
            eliminarrelaciones(idpersona, tablas, function(data){
              console.log(data);
              // ELIMINANDO GRADOS, TESIS Y PATENTE
              eliminartablas(['degree','thesis'], [idgrado, idtesis], function(data){
                console.log(data);
                // GUARDANDO LOS GRADOS
                var columns = ['is_degree','name','is_superiorgrade','speciality','institution_id','country','year'];
                var values = jsontomatrix(jsondatos.grado);
                guardatabla(columns, values, 'degree', new Array(), function(data){
                  if(data.stataus == -1){
                    salida["status"] = -1;
                    salida["message"] = "Error al ingresar persona";
                    salida["result"] = data;
                  }
                  idgrado = data.id;
                  // ASOCIANDO LOS GRADOS
                  var matrizgrado = arraytomatrix(idgrado);
                  var columns = ['degree_id','person_id'];
                  guardasubtabla(idpersona, columns, matrizgrado, 'person_has_degree', function(data){
                    if(data.stataus == -1){
                      salida["status"] = -1;
                      salida["message"] = "Error al ingresar persona";
                      salida["result"] = data;
                    }
                    console.log("grado->persona",data);
                    // ASOCIANDO DEPARTAMENTOS
                    var columns = ['department_id','person_id'];
                    var depmat = arraytomatrix(jsondatos.departamento.split(","));
                    guardasubtabla(idpersona, columns, depmat, 'person_has_department', function(data){
                      if(data.stataus == -1){
                        salida["status"] = -1;
                        salida["message"] = "Error al ingresar persona";
                        salida["result"] = data;
                      }
                      console.log("departamento->persona",data);
                      // ASOCIANDO PROGRAMAS
                      var columns = ['program_id','year','participation','person_id'];
                      var progmat = jsontomatrix(jsondatos.programa);
                      guardasubtabla(idpersona, columns, progmat, 'person_has_program', function(data){
                        if(data.stataus == -1){
                          salida["status"] = -1;
                          salida["message"] = "Error al ingresar persona";
                          salida["result"] = data;
                        }
                        console.log("programa->persona",data);
                        // GUARDANDO TESIS
                        var columns = ['type','program_id','participation','institution_id','title','year','student_name'];
                        var values = jsontomatrix(jsondatos.tesis);
                        guardatabla(columns, values, 'thesis', new Array(), function(data){
                          if(data.stataus == -1){
                            salida["status"] = -1;
                            salida["message"] = "Error al ingresar persona";
                            salida["result"] = data;
                          }
                          idtesis = data.id;
                          // ASOCIANDO TESIS
                          var matriztesis = arraytomatrix(idtesis);
                          var columns = ['thesis_id','person_id'];
                          guardasubtabla(idpersona, columns, matriztesis, 'person_has_thesis', function(){
                            if(data.stataus == -1){
                              salida["status"] = -1;
                              salida["message"] = "Error al ingresar persona";
                              salida["result"] = data;
                            }
                            console.log("tesis->persona", data);
                            var metodo = 'generate_json_person';
                            var arg = [];
                            invocar_webservice(metodo, arg, function(data){
                              console.log(data);
                            });
                            var metodo = 'generate_json_person_val_label';
                            var arg = [];
                            invocar_webservice(metodo, arg, function(data){
                              console.log(data);
                            });
                            alert(salida.message);
                            location.reload(true);
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        }
      });
      $("#eliminar").click(function(){
        alert("Eliminar");
      });
      $("#importar").click(function(){
        alert("Importar");
      });

      $("#agrega_grado").click(function(){
        var clases = ["tipo","titulo_grado","max","especialidad","universidad","pais","ano","quita"];
        var linea = construye_tr(clases, new Array(8));
        // AGREGA LA LINEA NUEVA
        $(this).parent().parent().parent().parent().children("tbody").append(linea);
        ano_datepicker();
        universidad();
        tipo_grado();
      });

      $("#agrega_programa").click(function(){
        var clases = ["programa","ano","participacion","quita"];
        var linea = construye_tr(clases, new Array(8));
        // AGREGA LA LINEA NUEVA
        $(this).parent().parent().parent().parent().children("tbody").append(linea);
        ano_datepicker();
      });
    
      $("#agrega_tesis").click(function(){
        var clases = ["tipo_tesis","programa","participacion_tesis","universidad","titulo","ano","estudiante","quita"];
        var linea = construye_tr(clases, new Array(8));
        // AGREGA LA LINEA NUEVA
        $(this).parent().parent().parent().parent().children("tbody").append(linea);
        ano_datepicker();
        universidad();
        estudiante()
      });

      // $("#agrega_patente").click(function(){
      //   var clases = ["titulo","numero","ano","estado","quita"];
      //   var linea = construye_tr(clases, new Array(8));
      //   // AGREGA LA LINEA NUEVA
      //   $(this).parent().parent().parent().parent().children("tbody").append(linea);
      //   ano_datepicker();
      // });

      
      $("#usm").click(function(){
        cambiousm();
      });
      // FUNCIONES
      function cambiousm(){
        if($("#usm").prop("checked")){
          $("#tip").css("display", "block");
          $("#ing").css("display", "block");
          $("#ret").css("display", "block");
          $("#dep").css("display", "block");
          $("#institucion").tokenfield("setTokens", [{"value":25,"label":"UNIVERSIDAD TÉCNICA FEDERICO SANTA MARÍA"}]);
        }
        else{
          $("#tip").css("display", "none");
          $("#ing").css("display", "none");
          $("#ret").css("display", "none");
          $("#dep").css("display", "none");
          $("#institucion").tokenfield("setTokens",[]);
        }
      }
      function desplegardatos(id){
        var metodo = 'read_table';
        var tabla = 'person';
        var arg = [tabla, id];
        // console.clear();
        invocar_webservice(metodo, arg, function(data){
          console.log(data);
          var pers = data.result;
          if(pers.run) $("#run").val(pers.run);
          if(pers.name) $("#nombre").val(pers.name);
          if(pers.last_name) $("#apellidop").val(pers.last_name);
          if(pers.m_name) $("#apellidom").val(pers.m_name);
          if(pers.is_usm) $("#usm").prop("checked", pers.is_usm == 1 ? true : false, cambiousm());
          cambiousm();
          if(pers.type) $("#tipo").val(pers.type);
          if(pers.date){
            var f = pers.date;
            var fs = f.split('-');
            var fecha = fs[2] + '-' + fs[1] + '-' + fs[0];
            $("#ingreso").val(fecha);
          } 
          if(pers.is_retired) $("#retirado").prop("checked", pers.is_retired == 1 ? true : false);
          if(pers.area) $("#area").tokenfield("setTokens", pers.area.split(","));
          if(pers.institution_id) {
            var inst = busca_u(jsonu, pers.institution_id);
            $("#institucion").tokenfield("setTokens", [inst]);
          }
          // GRADO
          metodo = 'read_subtable';
          arg = ['person_has_degree', id, 'person_id'];
          invocar_webservice(metodo, arg, function(data){
            var arrid = new Array();
            if(data.result){
              for(i=0; i<data.result.length; i++){
                arrid.push(data.result[i].degree_id);
                idgrado.push(data.result[i].degree_id);
              }
            }
            // console.log('grado -> ',idgrado);  
            leertabla(arrid, 'degree', new Array(), function(data){
              // console.log(data);
              $("#tabla_grado").children("tr").remove();
              for(j=0; j<data.result.length; j++){
                var valores = data.result[j];
                // console.log(valores);
                var clases = ["tipo","titulo_grado","max","especialidad","universidad","pais","ano","quita"];
                var linea = construye_tr(clases, new Array(8));
                // universidad();
                // AGREGA LA LINEA NUEVA
                $("#tabla_grado").append(linea);
                clases.pop();
                json_to_tr("tabla_grado", clases, [valores.is_degree, valores.name, valores.is_superiorgrade, valores.speciality, valores.institution_id, valores.country, valores.year]);
              }
              ano_datepicker();
              universidad();
              tipo_grado();
              // DEPARTAMENTO
              metodo = 'read_subtable';
              arg = ['person_has_department', id, 'person_id'];
              invocar_webservice(metodo, arg, function(data){
                // console.log("deptos -> ", data);
                var jsontf = new Array();
                if(data.result){
                  for(i=0; i<data.result.length; i++){
                    iddepartamento.push(data.result[i].department_id);
                    var dep = new Object();
                    dep.value = data.result[i].department_id;
                    if(jsondeptos[data.result[i].department_id-1]) dep.label = jsondeptos[data.result[i].department_id-1].label;
                    jsontf.push(dep);
                  }
                }
                // console.log(jsontf);
                $("#departamento").tokenfield("setTokens", jsontf);
                // console.log('departamento -> ',iddepartamento);
                // PROGRAMA
                metodo = 'read_subtable';
                arg = ['person_has_program', id, 'person_id'];
                invocar_webservice(metodo, arg, function(data){
                  var arrid = new Array();
                  if(data.result){
                    for(i=0; i<data.result.length; i++){
                      arrid.push(data.result[i].program_id);
                      idprograma.push(data.result[i].program_id);
                    }
                  }
                  // console.log(data);
                  $("#tabla_programa").children("tr").remove();
                  for(j=0; j<data.result.length; j++){
                    var valores = data.result[j];
                    // console.log(valores);
                    var clases = ["programa","ano","participacion","quita"];
                    var linea = construye_tr(clases, new Array(8));
                    // AGREGA LA LINEA NUEVA
                    $("#tabla_programa").append(linea);
                    clases.pop();
                    json_to_tr("tabla_programa", clases, [valores.program_id, valores.year, valores.participation])
                  }
                  ano_datepicker();
          
                  // console.log('programa -> ',idprograma);
                  // TESIS
                  metodo = 'read_subtable';
                  arg = ['person_has_thesis', id, 'person_id'];
                  invocar_webservice(metodo, arg, function(data){
                    var arrid = new Array();
                    if(data.result){
                      for(i=0; i<data.result.length; i++){
                        arrid.push(data.result[i].thesis_id);
                        idtesis.push(data.result[i].thesis_id);
                      }
                    }
                    // console.log('tesis -> ',idtesis);
                    leertabla(arrid, 'thesis', new Array(), function(data){
                      // console.log(data.result.length, data);
                      $("#tabla_tesis").children("tr").remove();
                      for(j=0; j<data.result.length; j++){
                        var valores = data.result[j];
                        // console.log(valores);
                        var clases = ["tipo_tesis","programa","participacion_tesis","universidad","titulo","ano","estudiante","quita"];
                        var linea = construye_tr(clases, new Array(8));
                        // AGREGA LA LINEA NUEVA
                        $("#tabla_tesis").append(linea);
                        clases.pop();
                        json_to_tr("tabla_tesis", clases, [valores.type, valores.program_id, valores.participation, valores.institution_id, valores.title, valores.year, valores.student_name])
                      }
                      ano_datepicker();
                      universidad();
                    });
                  });
                });
              });
            });
          });
        });
      }

    </script>
</body>
</html>
