<?php
//Start session
session_start();
 
//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['sess_user_id']) || (trim($_SESSION['sess_user_id']) == '')) {
	header("location: http://190.162.227.177/usm2/login.html");
	exit();
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Ingreso de Datos de Patentes</title>
  <link rel="shortcut icon" href="http://usm.cl/assets/img/icon/favicon.ico">
	<link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/datepicker.css" rel="stylesheet">
    <link href="css/token-input.css" type="text/css" rel="stylesheet">
    <link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap-tokenfield.min.css" type="text/css" rel="stylesheet">
    <link href="css/style.css" type="text/css" rel="stylesheet">
    <script src="js/ie-emulation-modes-warning.js"></script>
    <script src="js/ie10-viewport-bug-workaround.js"></script>
	<style type="text/css">
		.table, #isb, #edi, #eds, #importar, #eliminar{display: none;}
	</style>	
		
		
		
</head>
<body>
	<div class="content">
		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<a id="img-usm" class="navbar-brand" href="#">
						<img src="/usm2/img/header-usm.png" alt="UTFSM" class="img-thumbnail" width="300">
					</a>
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a href="/usm2"><span class="glyphicon glyphicon-home"></span></a></li>
						<li><a href="persona.php">Persona</a></li>
						<li><a href="proyecto.php">Proyecto</a></li>
						<li><a href="publicacion.php">Publicación</a></li>
						<li><a href="congreso.php">Congreso</a></li>
						<li class="active"><a href="patente.php">Patente</a></li>
						<li><a href="revista.php">Revista</a></li>
					</ul>
					<div id="logout"><a href="http://190.162.227.177/usm2/logout.php">Cerrar Sesion</a></div>
				</div>
			</div>
		</div>
		<div id="title">
			<div>
				<h1>Patentes</h1>
				<p class="lead">Módulo para el ingreso de datos sobre Patentes</p> 
			</div>
		</div>
		<div class="field">
			
			<form role="form">
				<div class="row">
					<h2>Datos Patente</h2>
					<!-- <div class="person">
						<div>
							<font>Para cargar datos ya registrados de una revista y editarlos, ingresa su <b>NOMBRE</b></font>
							<input type="text" class="form-control" id="id" placeholder="Ej.: NatGeo">
							*Si la revista no está en la lista, ingresa sus datos manualmente.
						</div>
					</div> -->

          <div class="form-group group medium">
            <label for="titulo">Título</label>
            <input type="text" class="form-control titulo" id="titulo" placeholder="Ej.: Método y sistema constructivo ...">
          </div>
          <div class="form-group group medium">
            <label for="numero">Número</label>
            <input type="text" class="form-control numero" id="numero" placeholder="Ej.: 49023">
          </div>
          <div class="form-group group medium">
            <label for="ano">Año</label>
            <input type="text" class="form-control ano year_picker" id="ano" placeholder="Ej.: 2002">
          </div>
          <div class="form-group group medium">
            <label for="estado">Estado</label>
            <select class="form-control estado" id="estado" placeholder="Selecciona tipo">
              <option value="Solicitada">Solicitada</option>
              <option value="Registrada">Registrada</option>
            </select> 
          </div>
					<div class="form-group group medium">
						<label for="investigador">Investigador(es) Asociados</label>
						<input type="text" class="tokenfield medium" id="investigador">
            <font color="red">*Es recomendable que TODO participante perteneciente a la USM sea escogido de la lista desplegable. Si la presona no se encuentra, es necesario registrarla en el ambiente persona.</font>
					</div>
					<div></div>
					<div class="button">
						<button type="button" class="btn btn-default" id="guardar_c">GUARDAR</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.tokeninput.js"></script>
	<script src="js/jquery-ui.min.js"  type="text/javascript"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-tokenfield.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/functions.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script type="text/javascript">
      var id = '';
      // var urlws = 'webservice/webservice.php';
      $(document).ready(function(){
        $('html, body').animate({ scrollTop: 0 }, 'fast');
		    $.getJSON( "/usm2/data/persons_val_label.json", function( data ) {
          jsonpers = data;
          $('#investigador').tokenfield({
            limit: 0,
            createTokensOnBlur: true,
            autocomplete: {
              source: jsonpers,
              minLength: 1
            },
            showAutocompleteOnFocus: true
          });
        });
		    $("#ano").css("cursor","pointer");
        $("#ano").prop("readonly", true);
        $("#ano").datepicker({
          format: " yyyy",
          viewMode: "years", 
          minViewMode: "years",
          autoclose: true
        });
        
      });

      $("#guardar_c").click(function(){
        var jsondatos = new Object();
        jsondatos.titulo = $("#titulo").val();
        jsondatos.numero = $("#numero").val();
        jsondatos.ano = $("#ano").val().trim();
        jsondatos.estado = $("#estado").val();
        jsondatos.investigador = arraytomatrix($("#investigador").tokenfield('getTokensList').split(", "));
        
        // console.log(jsondatos);
        
        // var metodo = id == '' ? 'add_data_to_table' : 'update_table';
        var metodo = 'add_data_to_table';
        var tabla = 'patent';
        var arg1 = ['name', 'number','year','status'];
        var arg2 = [jsondatos.titulo, jsondatos.numero, jsondatos.ano, jsondatos.estado];
		    var arg = [tabla, arg1, arg2];
      //   var arg = id == '' ? [tabla, arg1, arg2] : [tabla, idpersona, arg1, arg2];
        var salida = new Object();
        salida["stataus"] = 1;
        salida["message"] = "Patente guardada correctamente"
        // GUARDANDO
        invocar_webservice(metodo, arg, function(data){
          // console.log(data);
          if(data.stataus == -1){
            salida["status"] = -1;
            salida["message"] = "Error al ingresar patente";
            salida["result"] = data;
          }
          if(data.id) id = data.id;
          var tabla = 'person_has_patent';
          var columns = ["person_id", "patent_id"];
          guardasubtabla(id, columns, jsondatos.investigador, tabla, function(data){
            if(data.status == -1){
              salida["status"] = -1;
              salida["message"] = "Error al ingresar patente";
              salida["result"] = data;
            }
            alert(salida.message);
            location.reload(true);
          });  
        });
      });
      
    </script>
</body>
</html>
