<?php    
header('Content-type: application/json; charset=UTF-8');
// include("connection.php");

// Getting the json data from the request
$json_data = json_decode(file_get_contents("php://input"));
$response = '';

// Checking if the data is null..
if(is_null($json_data)) 
{
    $response = json_encode(array("status" => -1, "message" => "Insufficient parmaters!"));
}
else
{      
    // Get method name...           
    $method = $json_data->{"method"};
    $arguments = $json_data->{"arguments"};
    // Call the method...
    $response = $method($arguments); 
}

echo json_encode($response);

/****************************
 * Methods defined for use. *
 ****************************/
// DATOS CONEXION

//////////////////
function add_data_to_table($jsondata){
    $_mysql = "localhost;root;complexity*-*;usm";
    $mysql_input = explode(";",$_mysql);
    $con = mysqli_connect($mysql_input[0],$mysql_input[1],$mysql_input[2],$mysql_input[3]);
    if (!mysqli_set_charset($con, "utf8")) {
        $messaje = array("status" => -1, "error" => "Failed to load utf8: " . mysqli_error($con));
    }

    $table = $jsondata[0];
    $stringcolumns = implode(",",$jsondata[1]);
    $stringvalues = implode("','",$jsondata[2]);

    if (mysqli_connect_errno()) {
        $messaje = array("status" => 0, "error" => "Failed to connect to MySQL: " . mysqli_connect_error());
    }
    else{
        $query = "INSERT INTO " . $table . " (" . $stringcolumns . ") VALUES ('" . $stringvalues . "')";
        if(!mysqli_query($con,$query)){
            $messaje = array("status" => -1, "error" => mysqli_error($con), "query" => $query);
        }
        else{
            $messaje = array("status" => 1, "query" => $query, "id" => mysqli_insert_id($con));
        }
    }
    mysqli_close($con);
    return $messaje;
}

function prueba($jsondata){
    $in = $jsondata[0];
    return $in;
}

function update_table($jsondata){
    $_mysql = "localhost;root;complexity*-*;usm";
    $mysql_input = explode(";",$_mysql);
    $con = mysqli_connect($mysql_input[0],$mysql_input[1],$mysql_input[2],$mysql_input[3]);
    if (!mysqli_set_charset($con, "utf8")) {
        $messaje = array("status" => -1, "error" => "Failed to load utf8: " . mysqli_error($con));
    }
    $table = $jsondata[0];
    $stringquery = "";
    for($i=0; $i<count($jsondata[2]); $i++){
        $stringquery = $stringquery . $jsondata[2][$i] . "='" . $jsondata[3][$i] . "'";
        if($i<count($jsondata[2])-1){
            $stringquery = $stringquery . ",";
        }
    }
    $query = "UPDATE " . $table . " SET " . $stringquery . " WHERE id = '" . $jsondata[1] . "'";
    if (mysqli_connect_errno()) {
        $messaje = array("status" => 0, "error" => "Failed to connect to MySQL: " . mysqli_connect_error());
    }
    else{
        if(!mysqli_query($con,$query)){
            $messaje = array("status" => -1, "error" => mysqli_error($con), "query" => $query);
        }
        else{
            $messaje = array("status" => 1, "query" => $query);
        }
    }
    mysqli_close($con);
    return $messaje;
}

function delete_table($jsondata){
    $_mysql = "localhost;root;complexity*-*;usm";
    $mysql_input = explode(";",$_mysql);
    $con = mysqli_connect($mysql_input[0],$mysql_input[1],$mysql_input[2],$mysql_input[3]);
    if (!mysqli_set_charset($con, "utf8")) {
        $messaje = array("status" => -1, "error" => "Failed to load utf8: " . mysqli_error($con));
    }
    $table = $jsondata[0];
    $columns = $jsondata[1];
    $values = $jsondata[2];
    $condition = "";
    for($i=0; $i<count($columns); $i++){
        $condition = $condition . $columns[$i] . "='" . $values[$i] . "'";
        if(count($columns)>$i+1) {
            $condition = $condition . " AND ";
        }
    }
    $query = "DELETE FROM " . $table . " WHERE " . $condition;
    if (mysqli_connect_errno()) {
        $messaje = array("status" => 0, "error" => "Failed to connect to MySQL: " . mysqli_connect_error());
    }
    else{
        if(!mysqli_query($con,$query)){
            $messaje = array("status" => -1, "error" => mysqli_error($con), "query" => $query);
        }
        else{
            $messaje = array("status" => 1, "query" => $query);
        }
    }
    mysqli_close($con);
    return $messaje;
}

function read_table($jsondata){
    $_mysql = "localhost;root;complexity*-*;usm";
    $mysql_input = explode(";",$_mysql);
    $con = mysqli_connect($mysql_input[0],$mysql_input[1],$mysql_input[2],$mysql_input[3]);
    if (!mysqli_set_charset($con, "utf8")) {
        $messaje = array("status" => -1, "error" => "Failed to load utf8: " . mysqli_error($con));
    }
    $table = $jsondata[0];
    $idtable = $jsondata[1];
    $query = "SELECT * FROM " . $table . " WHERE id = '" . $idtable . "'";
    // return $query;
    if (mysqli_connect_errno()) {
        $messaje = array("status" => 0, "error" => "Failed to connect to MySQL: " . mysqli_connect_error());
    }
    else{
        if(!$res = mysqli_query($con,$query)){
            $messaje = array("status" => -1, "error" => mysqli_error($con), "query" => $query);
        }
        else{
            // if($table == "person"){
            $str = mysqli_fetch_array($res);
            // }
            // else{
            //     $str = mysqli_fetch_row($res);
            // }
            $messaje = array("status" => 1, "query" => $query, "result" => $str, "result2" => $str2);
        }
    }
    mysqli_close($con);
    return $messaje;
}
function read_subtable($jsondata){ // jsondata -> (tabla, valor, columna);
    $_mysql = "localhost;root;complexity*-*;usm";
    $mysql_input = explode(";",$_mysql);
    $con = mysqli_connect($mysql_input[0],$mysql_input[1],$mysql_input[2],$mysql_input[3]);
    if (!mysqli_set_charset($con, "utf8")) {
        $messaje = array("status" => -1, "error" => "Failed to load utf8: " . mysqli_error($con));
    }
    $table = $jsondata[0];
    $idtable = $jsondata[1];
    $idcolumn = $jsondata[2];
    $query = "SELECT * FROM " . $table . " WHERE " . $idcolumn . " = '" . $idtable . "'";
    // return $query;
    if (mysqli_connect_errno()) {
        $messaje = array("status" => 0, "error" => "Failed to connect to MySQL: " . mysqli_connect_error());
    }
    else{
        if(!$res = mysqli_query($con,$query)){
            $messaje = array("status" => -1, "error" => mysqli_error($con), "query" => $query);
        }
        else{
            $str = array();
            while($row = mysqli_fetch_array($res)){
                array_push($str,$row);
            }
            $messaje = array("status" => 1, "query" => $query, "result" => $str);
        }
    }
    mysqli_close($con);
    return $messaje;
}
function generate_json_person(){
    $_mysql = "localhost;root;complexity*-*;usm";
    $mysql_input = explode(";",$_mysql);
    $con = mysqli_connect($mysql_input[0],$mysql_input[1],$mysql_input[2],$mysql_input[3]);
    if (!mysqli_set_charset($con, "utf8")) {
        $messaje = array("status" => -1, "error" => "Failed to load utf8: " . mysqli_error($con));
    }
    $query = "SELECT id, name, last_name, m_name, run FROM usm.person WHERE run IS NOT NULL";
    if (mysqli_connect_errno()) {
        $messaje = array("status" => 0, "error" => "Failed to connect to MySQL: " . mysqli_connect_error());
    }
    else{
        if(!$res = mysqli_query($con,$query)){
            $messaje = array("status" => -1, "error" => mysqli_error($con), "query" => $query);
        }
        else{
            $str = array();
            $jsonperson = array();
            while($row = mysqli_fetch_array($res)){
                array_push($str,$row);
                $lineperson = array("id" => $row["id"], "name" => $row["name"] . ' ' . $row["last_name"] . ' ' . $row["m_name"] . " (" . $row["run"] . ")");
                array_push($jsonperson,$lineperson);
            }
            $filename = "../../data/persons.json";
            $data  = utf8_encode((string)json_encode($jsonperson));
            $fpc = file_put_contents ($filename , $data);
            $messaje = array("status" => 1, "query" => $query, "result" => $str, "json" => $jsonperson, "guardado" => $fpc);
        }
    }
    mysqli_close($con);
    return $messaje;
}
function generate_json_person_val_label(){
    $_mysql = "localhost;root;complexity*-*;usm";
    $mysql_input = explode(";",$_mysql);
    $con = mysqli_connect($mysql_input[0],$mysql_input[1],$mysql_input[2],$mysql_input[3]);
    if (!mysqli_set_charset($con, "utf8")) {
        $messaje = array("status" => -1, "error" => "Failed to load utf8: " . mysqli_error($con));
    }
    $query = "SELECT id, name, last_name, m_name, run FROM usm.person WHERE run IS NOT NULL";
    if (mysqli_connect_errno()) {
        $messaje = array("status" => 0, "error" => "Failed to connect to MySQL: " . mysqli_connect_error());
    }
    else{
        if(!$res = mysqli_query($con,$query)){
            $messaje = array("status" => -1, "error" => mysqli_error($con), "query" => $query);
        }
        else{
            $str = array();
            $jsonperson = array();
            while($row = mysqli_fetch_array($res)){
                array_push($str,$row);
                $lineperson = array("value" => $row["id"], "label" => $row["name"] . ' ' . $row["last_name"] . ' ' . $row["m_name"] . " (" . $row["run"] . ")");
                array_push($jsonperson,$lineperson);
            }
            $filename = "../../data/persons_val_label.json";
            $data  = utf8_encode((string)json_encode($jsonperson));
            $fpc = file_put_contents ($filename , $data);
            $messaje = array("status" => 1, "query" => $query, "result" => $str, "json" => $jsonperson, "guardado" => $fpc);
        }
    }
    mysqli_close($con);
    return $messaje;
}
function generate_json_journal(){
    $_mysql = "localhost;root;complexity*-*;usm";
    $mysql_input = explode(";",$_mysql);
    $con = mysqli_connect($mysql_input[0],$mysql_input[1],$mysql_input[2],$mysql_input[3]);
    if (!mysqli_set_charset($con, "utf8")) {
        $messaje = array("status" => -1, "error" => "Failed to load utf8: " . mysqli_error($con));
    }
    $query = "SELECT id, name FROM usm.journal WHERE name IS NOT NULL";
    if (mysqli_connect_errno()) {
        $messaje = array("status" => 0, "error" => "Failed to connect to MySQL: " . mysqli_connect_error());
    }
    else{
        if(!$res = mysqli_query($con,$query)){
            $messaje = array("status" => -1, "error" => mysqli_error($con), "query" => $query);
        }
        else{
            $str = array();
            $jsonjournal = array();
            while($row = mysqli_fetch_array($res)){
                array_push($str,$row);
                $line = array("id" => $row["id"], "name" => $row["name"]);
                array_push($jsonjournal,$line);
            }
            $filename = "../../data/journal.json";
            $data  = utf8_encode((string)json_encode($jsonjournal));
            $fpc = file_put_contents ($filename , $data);
            $messaje = array("status" => 1, "file" => $filename, "query" => $query, "result" => $str, "json" => $jsonjournal, "guardado" => $fpc);
        }
    }
    mysqli_close($con);
    return $messaje;
}
function generate_json_journal_val_label(){
    $_mysql = "localhost;root;complexity*-*;usm";
    $mysql_input = explode(";",$_mysql);
    $con = mysqli_connect($mysql_input[0],$mysql_input[1],$mysql_input[2],$mysql_input[3]);
    if (!mysqli_set_charset($con, "utf8")) {
        $messaje = array("status" => -1, "error" => "Failed to load utf8: " . mysqli_error($con));
    }
    $query = "SELECT id, name FROM usm.journal WHERE name IS NOT NULL";
    if (mysqli_connect_errno()) {
        $messaje = array("status" => 0, "error" => "Failed to connect to MySQL: " . mysqli_connect_error());
    }
    else{
        if(!$res = mysqli_query($con,$query)){
            $messaje = array("status" => -1, "error" => mysqli_error($con), "query" => $query);
        }
        else{
            $str = array();
            $jsonjournal = array();
            while($row = mysqli_fetch_array($res)){
                array_push($str,$row);
                $line = array("value" => $row["id"], "label" => $row["name"]);
                array_push($jsonjournal,$line);
            }
            $filename = "../../data/journal_val_label.json";
            $data  = utf8_encode((string)json_encode($jsonjournal));
            $fpc = file_put_contents ($filename , $data);
            $messaje = array("status" => 1, "file" => $filename, "query" => $query, "result" => $str, "json" => $jsonjournal, "guardado" => $fpc);
        }
    }
    mysqli_close($con);
    return $messaje;
}
function generate_json_institution(){
    $_mysql = "localhost;root;complexity*-*;usm";
    $mysql_input = explode(";",$_mysql);
    $con = mysqli_connect($mysql_input[0],$mysql_input[1],$mysql_input[2],$mysql_input[3]);
    if (!mysqli_set_charset($con, "utf8")) {
        $messaje = array("status" => -1, "error" => "Failed to load utf8: " . mysqli_error($con));
    }
    $query = "SELECT id, name FROM usm.institution WHERE name IS NOT NULL";
    if (mysqli_connect_errno()) {
        $messaje = array("status" => 0, "error" => "Failed to connect to MySQL: " . mysqli_connect_error());
    }
    else{
        if(!$res = mysqli_query($con,$query)){
            $messaje = array("status" => -1, "error" => mysqli_error($con), "query" => $query);
        }
        else{
            $str = array();
            $jsonu = array();
            while($row = mysqli_fetch_array($res)){
                array_push($str,$row);
                $line = array("id" => $row["id"], "name" => $row["name"]);
                array_push($jsonu,$line);
            }
            $filename = "../../data/ues.json";
            $data  = utf8_encode((string)json_encode($jsonu));
            $fpc = file_put_contents ($filename , $data);
            $messaje = array("status" => 1, "file" => $filename, "query" => $query, "result" => $str, "json" => $jsonu, "guardado" => $fpc);
        }
    }
    mysqli_close($con);
    return $messaje;
}
function generate_json_institution_val_label(){
    $_mysql = "localhost;root;complexity*-*;usm";
    $mysql_input = explode(";",$_mysql);
    $con = mysqli_connect($mysql_input[0],$mysql_input[1],$mysql_input[2],$mysql_input[3]);
    if (!mysqli_set_charset($con, "utf8")) {
        $messaje = array("status" => -1, "error" => "Failed to load utf8: " . mysqli_error($con));
    }
    $query = "SELECT id, name FROM usm.institution WHERE name IS NOT NULL";
    if (mysqli_connect_errno()) {
        $messaje = array("status" => 0, "error" => "Failed to connect to MySQL: " . mysqli_connect_error());
    }
    else{
        if(!$res = mysqli_query($con,$query)){
            $messaje = array("status" => -1, "error" => mysqli_error($con), "query" => $query);
        }
        else{
            $str = array();
            $jsonu = array();
            while($row = mysqli_fetch_array($res)){
                array_push($str,$row);
                $line = array("value" => $row["id"], "label" => $row["name"]);
                array_push($jsonu,$line);
            }
            $filename = "../../data/ues_val_label.json";
            $data  = utf8_encode((string)json_encode($jsonu));
            $fpc = file_put_contents ($filename , $data);
            $messaje = array("status" => 1, "file" => $filename, "query" => $query, "result" => $str, "json" => $jsonu, "guardado" => $fpc);
        }
    }
    mysqli_close($con);
    return $messaje;
}
function exist_in_db($jsondata){
    $_mysql = "localhost;root;complexity*-*;usm";
    $mysql_input = explode(";",$_mysql);
    $con = mysqli_connect($mysql_input[0],$mysql_input[1],$mysql_input[2],$mysql_input[3]);
    if (!mysqli_set_charset($con, "utf8")) {
        $messaje = -1;
    }
    $table = $jsondata[0];
    $idtable = $jsondata[1];
    $query = "SELECT * FROM " . $table . " WHERE id = '" . $idtable . "'";
    if (mysqli_connect_errno()) {
        $messaje = -1;
    }
    else{
        if(!$res = mysqli_query($con,$query)){
            $messaje = -1;
        }
        else{
            $str = mysqli_fetch_array($res);
            $message = count($str);
            if($str == null) {
                $message == 0;
            }
            else {
                $messaje = 1;
            }
        }
    }
    mysqli_close($con);
    return $messaje;
}
function pub_exist_in_db($jsondata){
    $_mysql = "localhost;root;complexity*-*;usm";
    $mysql_input = explode(";",$_mysql);
    $con = mysqli_connect($mysql_input[0],$mysql_input[1],$mysql_input[2],$mysql_input[3]);
    $message = 0;
    if (!mysqli_set_charset($con, "utf8")) {
        $message = -1;
    }
    $table = $jsondata[0];
    $title = $jsondata[1];
    $year = $jsondata[2];
    $query = "SELECT * FROM " . $table . " WHERE title = '" . $title . "' AND year = '" . $year . "'";
    if (mysqli_connect_errno()) {
        $message = -2;
    }
    else{
        if(!$res = mysqli_query($con,$query)){
            $message = array("query" => $query, "result" => mysqli_error($con));
        }
        else{
            $str = mysqli_fetch_array($res);
            if($str) {
                $message = 1;
            }
        }
    }
    mysqli_close($con);
    return $message;
}

// GENERA JSON PARA LISTA
function generate_json_list_year_range($jsondata){
    $_mysql = "localhost;root;complexity*-*;usm";
    $mysql_input = explode(";",$_mysql);
    $con = mysqli_connect($mysql_input[0],$mysql_input[1],$mysql_input[2],$mysql_input[3]);
    if (!mysqli_set_charset($con, "utf8")) {
        $messaje = array("status" => -1, "error" => "Failed to load utf8: " . mysqli_error($con));
    }
    else if (mysqli_connect_errno()) {
        $messaje = array("status" => 0, "error" => "Failed to connect to MySQL: " . mysqli_connect_error());
    }
    else{
        $a = $jsondata[0];
        $b = $jsondata[1];
        for($i=$a; $i<$b+1; $i++){
            $query = "CALL usm.departments_year(" . $i . ")";
            if(!$res = mysqli_query($con,$query)){
                $messaje = array("status" => -1, "error" => mysqli_error($con), "query" => $query);
            }
            else{
                $jsondepts = array();
                while($row = mysqli_fetch_array($res)){
                    $jsonq = quartile_department($i,$row["name"]);
                    $line = array("name" => $row["name"], "size" => $row["size"], "children" => $jsonq);
                    array_push($jsondepts,$line);
                }
                $jsonoutput = array("name" => $i, "children" => $jsondepts);
                $filename = "../../data/".$i.".json";
                $data  = utf8_encode((string)json_encode($jsonoutput));
                $fpc = file_put_contents ($filename , $data);
                $messaje = array("status" => 1, "guardado" => $fpc, "json" => $jsonoutput, "query" => $query);
            }
            mysqli_free_result($res);
        }
    }
    mysqli_close($con);
    return $messaje;
}
function generate_json_list_year($jsondata){
    $_mysql = "localhost;root;complexity*-*;usm";
    $mysql_input = explode(";",$_mysql);
    $con = mysqli_connect($mysql_input[0],$mysql_input[1],$mysql_input[2],$mysql_input[3]);
    if (!mysqli_set_charset($con, "utf8")) {
        $messaje = array("status" => -1, "error" => "Failed to load utf8: " . mysqli_error($con));
    }
    else if (mysqli_connect_errno()) {
        $messaje = array("status" => 0, "error" => "Failed to connect to MySQL: " . mysqli_connect_error());
    }
    else{
        $a = $jsondata[0];
        $query = "CALL usm.departments_year(" . $a . ")";
        if(!$res = mysqli_query($con,$query)){
            $messaje = array("status" => -1, "error" => mysqli_error($con), "query" => $query);
        }
        else{
            $jsondepts = array();
            while($row = mysqli_fetch_array($res)){
                $jsonqu = quartile_department($a,$row["name"]);
                $line = array("name" => $row["name"], "size" => $row["size"], "children" => $jsonqu);
                array_push($jsondepts,$line);
            }
            $jsonoutput = array("name" => $a, "children" => $jsondepts);
            $filename = "../../data/".$a.".json";
            $data  = utf8_encode((string)json_encode($jsonoutput));
            $fpc = file_put_contents ($filename , $data);
            $messaje = array("status" => 1, "guardado" => $fpc, "json" => $jsonoutput, "query" => $query);
        }
        mysqli_free_result($res);
    }
    mysqli_close($con);
    return $messaje;
}
function generate_json_list($jsondata){
    $year = $jsondata[0];
    $_mysql = "localhost;root;complexity*-*;usm";
    $mysql_input = explode(";",$_mysql);
    $con = mysqli_connect($mysql_input[0],$mysql_input[1],$mysql_input[2],$mysql_input[3]);
    if (!mysqli_set_charset($con, "utf8")) {
        $messaje = array("status" => -1, "error" => "Failed to load utf8: " . mysqli_error($con));
    }
    else if (mysqli_connect_errno()) {
        $messaje = array("status" => 0, "error" => "Failed to connect to MySQL: " . mysqli_connect_error());
    }
    else{
        $query = "CALL usm.departments_year(" . $year . ")";
        if(!$res = mysqli_query($con,$query)){
            $messaje = array("status" => -1, "error" => mysqli_error($con), "query" => $query);
        }
        else{
            $jsondepts = array();
            while($row = mysqli_fetch_array($res)){
                $jsonq = quartile_department($year,$row["name"]);
                $line = array("name" => $row["name"], "size" => $row["size"], "children" => $jsonq);
                array_push($jsondepts,$line);
            }
            $jsonoutput = array("name" => $year, "children" => $jsondepts);
            $filename = "../../data/".$year.".json";
            $data  = utf8_encode((string)json_encode($jsonoutput));
            $fpc = file_put_contents ($filename , $data);
            $messaje = array("status" => 1, "guardado" => $fpc, "json" => $jsonoutput, "query" => $query);
        }
    }
    mysqli_close($con);
    return $messaje;
}
function departments_year($year){
    $_mysql = "localhost;root;complexity*-*;usm";
    $mysql_input = explode(";",$_mysql);
    $con = mysqli_connect($mysql_input[0],$mysql_input[1],$mysql_input[2],$mysql_input[3]);
    if (!mysqli_set_charset($con, "utf8")) {
        $messaje = array("status" => -1, "error" => "Failed to load utf8: " . mysqli_error($con));
    }
    else if (mysqli_connect_errno()) {
        $messaje = array("status" => 0, "error" => "Failed to connect to MySQL: " . mysqli_connect_error());
    }
    else{
        $query = "CALL usm.departments_year(" . $year . ")";
        if(!$res = mysqli_query($con,$query)){
            $messaje = array("status" => -1, "error" => mysqli_error($con), "query" => $query);
        }
        else{
            $jsondepts = array();
            while($row = mysqli_fetch_array($res)){
                $jsonq = quartile_department($year,$row["name"]);
                $line = array("name" => $row["name"], "size" => $row["size"], "children" => $jsonq);
                array_push($jsondepts,$line);
            }
            return $jsondepts;
        }
    }
}
function quartile_department($year, $dept){
    $prueba = array();
    $_mysql = "localhost;root;complexity*-*;usm";
    $mysql_input = explode(";",$_mysql);
    $con = mysqli_connect($mysql_input[0],$mysql_input[1],$mysql_input[2],$mysql_input[3]);
    if (!mysqli_set_charset($con, "utf8")) {
        $messaje = array("status" => -1, "error" => "Failed to load utf8: " . mysqli_error($con));
    }
    else if (mysqli_connect_errno()) {
        $messaje = array("status" => 0, "error" => "Failed to connect to MySQL: " . mysqli_connect_error());
    }
    else{
        $query = "CALL usm.quartile_department('" . $year . "','" . $dept . "')";
        if(!$res = mysqli_query($con,$query)){
            $messaje = array("status" => -1, "error" => mysqli_error($con), "query" => $query);
        }
        else{
            $jsonq = array();
            while($row = mysqli_fetch_array($res)){
                $jsonpe = person_quartile($row["name"], $dept, $year);
                $line = array("name" => $row["name"], "size" => $row["size"], "children" => $jsonpe);
                array_push($jsonq,$line);
            }
            return $jsonq;
        }
    }
}
function person_quartile($quart, $dept, $year){
    $prueba = array();
    $_mysql = "localhost;root;complexity*-*;usm";
    $mysql_input = explode(";",$_mysql);
    $con = mysqli_connect($mysql_input[0],$mysql_input[1],$mysql_input[2],$mysql_input[3]);
    if (!mysqli_set_charset($con, "utf8")) {
        $messaje = array("status" => -1, "error" => "Failed to load utf8: " . mysqli_error($con));
    }
    else if (mysqli_connect_errno()) {
        $messaje = array("status" => 0, "error" => "Failed to connect to MySQL: " . mysqli_connect_error());
    }
    else{
        $query = "CALL usm.person_quartile('" . $quart . "','" . $dept . "','" . $year . "')";
        if(!$res = mysqli_query($con,$query)){
            $messaje = array("status" => -1, "error" => mysqli_error($con), "query" => $query);
        }
        else{
            $jsonp = array();
            while($row = mysqli_fetch_array($res)){
                $jsonpu = publication_person($row["idp"], $quart, $dept, $year);
                $line = array("name" => $row["name"], "size" => $row["size"], "children" => $jsonpu);
                array_push($jsonp,$line);
            }
            return $jsonp;
        }
    }
}
function publication_person($pers, $quart, $dept, $year){
    $prueba = array();
    $_mysql = "localhost;root;complexity*-*;usm";
    $mysql_input = explode(";",$_mysql);
    $con = mysqli_connect($mysql_input[0],$mysql_input[1],$mysql_input[2],$mysql_input[3]);
    if (!mysqli_set_charset($con, "utf8")) {
        $messaje = array("status" => -1, "error" => "Failed to load utf8: " . mysqli_error($con));
    }
    else if (mysqli_connect_errno()) {
        $messaje = array("status" => 0, "error" => "Failed to connect to MySQL: " . mysqli_connect_error());
    }
    else{
        $query = "CALL usm.publication_person('" . $pers . "','" . $quart . "','" . $dept . "','" . $year . "')";
        if(!$res = mysqli_query($con,$query)){
            $messaje = array("status" => -1, "error" => mysqli_error($con), "query" => $query);
        }
        else{
            $jsonpub = array();
            while($row = mysqli_fetch_array($res)){
                $line = array("name" => $row["name"], "size" => $row["size"]);
                array_push($jsonpub,$line);
            }
            return $jsonpub;
        }
    }
}
// FIN JSON SUNBURST

// GENERA CSV PARA SUNBURST

function generate_csv_sunburst($jsondata){
    $_mysql = "localhost;root;complexity*-*;usm";
    $mysql_input = explode(";",$_mysql);
    $con = mysqli_connect($mysql_input[0],$mysql_input[1],$mysql_input[2],$mysql_input[3]);
    if (!mysqli_set_charset($con, "utf8")) {
        $messaje = array("status" => -1, "error" => "Failed to load utf8: " . mysqli_error($con));
    }
    $year = $jsondata[0];
    // PUBLICACIONES POR AÑO
    $query = "call usm.pub_per_qua_dep('".$year."')";
    if (mysqli_connect_errno()) {
        $messaje = array("status" => 0, "error" => "Failed to connect to MySQL: " . mysqli_connect_error());
    }
    else{
        if(!$res = mysqli_query($con,$query)){
            $messaje = array("status" => -1, "error" => mysqli_error($con), "query" => $query);
        }
        else{
            $csv = "";
            while($row = mysqli_fetch_array($res)){
                $t = str_replace(",", " ", $row["title"]);
                $tit = str_replace("-", " ", $t); 
                $line = $row["department"]."-".$row["quartile"]."-".str_replace(" ", "", $row["short_name"])."-".$tit.",1\n";
                $csv = $csv.$line;
            }                
            $filename = "../../data/sun".$year.".csv";
            $data  = quitar_tildes($csv);
            $fpc = file_put_contents ($filename , $data);
            $messaje = array("status" => 1, "query" => $query, "guardado" => $fpc);
        }
    }
    mysqli_close($con);
    return $messaje;
}
function generate_csv_sunburst_year_range($jsondata){
    $_mysql = "localhost;root;complexity*-*;usm";
    $mysql_input = explode(";",$_mysql);
    $con = mysqli_connect($mysql_input[0],$mysql_input[1],$mysql_input[2],$mysql_input[3]);
    if (!mysqli_set_charset($con, "utf8")) {
        $messaje = array("status" => -1, "error" => "Failed to load utf8: " . mysqli_error($con));
    }
    // PUBLICACIONES POR AÑO
    
    if (mysqli_connect_errno()) {
        $messaje = array("status" => 0, "error" => "Failed to connect to MySQL: " . mysqli_connect_error());
    }
    else{
        $a = $jsondata[0];
        $b = $jsondata[1];
        for($i=$a; $i<$b+1; $i++){
            $query = "call usm.pub_per_qua_dep('".$i."')";
            if(!$res = mysqli_query($con,$query)){
                $messaje = array("status" => -1, "error" => mysqli_error($con), "query" => $query);
            }
            else{
                $csv = "";
                while($row = mysqli_fetch_array($res)){
                    $t = str_replace(",", " ", $row["title"]);
                    $tit = str_replace("-", " ", $t); 
                    $line = $row["department"]."-".$row["quartile"]."-".str_replace(" ", "", $row["short_name"])."-".$tit.",1\n";
                    $csv = $csv.$line;
                }                
                $filename = "../../data/sun".$i.".csv";
                $data  = quitar_tildes($csv);
                $fpc = file_put_contents ($filename , $data);
                $messaje = array("status" => 1, "query" => $query, "guardado" => $fpc);
            }
        }
    }
    mysqli_close($con);
    return $messaje;
}

function generate_csv_sunburst_range($jsondata){
    $_mysql = "localhost;root;complexity*-*;usm";
    $mysql_input = explode(";",$_mysql);
    $con = mysqli_connect($mysql_input[0],$mysql_input[1],$mysql_input[2],$mysql_input[3]);
    if (!mysqli_set_charset($con, "utf8")) {
        $messaje = array("status" => -1, "error" => "Failed to load utf8: " . mysqli_error($con));
    }
    $year1 = $jsondata[0];
    $year2 = $jsondata[1];
    // PUBLICACIONES POR AÑO
    $query = "call usm.sun_range('".$year1."','".$year2."')";
    if (mysqli_connect_errno()) {
        $messaje = array("status" => 0, "error" => "Failed to connect to MySQL: " . mysqli_connect_error());
    }
    else{
        if(!$res = mysqli_query($con,$query)){
            $messaje = array("status" => -1, "error" => mysqli_error($con), "query" => $query);
        }
        else{
            $csv = "";
            while($row = mysqli_fetch_array($res)){
                $t = str_replace(",", " ", $row["title"]);
                $tit = str_replace("-", " ", $t); 
                $line = $row["department"]."-".$row["quartile"].",".$row["size"]."\n";
                $csv = $csv.$line;
            }                
            $filename = "../../data/sunacum.csv";
            $data  = quitar_tildes($csv);
            $fpc = file_put_contents ($filename , $data);
            $messaje = array("status" => 1, "query" => $query, "guardado" => $fpc);
        }
    }
    mysqli_close($con);
    return $messaje;
}

// GENERA JSON PARA GRAFO (CIRCULAR) DE COLABORACION 
function person_year_range($jsondata){
    $_mysql = "localhost;root;complexity*-*;usm";
    $mysql_input = explode(";",$_mysql);
    $con = mysqli_connect($mysql_input[0],$mysql_input[1],$mysql_input[2],$mysql_input[3]);
    if (!mysqli_set_charset($con, "utf8")) {
        $messaje = array("status" => -1, "error" => "Failed to load utf8: " . mysqli_error($con));
    }
    else if (mysqli_connect_errno()) {
        $messaje = array("status" => 0, "error" => "Failed to connect to MySQL: " . mysqli_connect_error());
    }
    else{
        $a = $jsondata[0];
        $b = $jsondata[1];
        for($i=$a; $i<$b+1; $i++){
            $query = "CALL usm.person_year('" . $i . "')";
            if(!$res = mysqli_query($con,$query)){
                $messaje = array("status" => -1, "error" => mysqli_error($con), "query" => $query);
            }
            else{
                $jsonnode = array();
                while($row = mysqli_fetch_array($res)){
                    $pp = person_person($row["id"], $i);
                    $linen = array("name" => $row["dept"] . '.' . $row["name"], "size" => 0.5, "imports" => $pp);
                    array_push($jsonnode,$linen);
                }
                $filename = "../../data/circular".$i.".json";
                $data  = utf8_encode((string)json_encode($jsonnode));
                $fpc = file_put_contents ($filename , $data);
                $messaje = array("status" => 1, "query" => $query, "json" => $jsonnode, "guardado" => $fpc, "diccionario" => $dictionary);
            }
        }
    }
    return $messaje;
}
function person_year($jsondata){
    $_mysql = "localhost;root;complexity*-*;usm";
    $mysql_input = explode(";",$_mysql);
    $con = mysqli_connect($mysql_input[0],$mysql_input[1],$mysql_input[2],$mysql_input[3]);
    if (!mysqli_set_charset($con, "utf8")) {
        $messaje = array("status" => -1, "error" => "Failed to load utf8: " . mysqli_error($con));
    }
    else if (mysqli_connect_errno()) {
        $messaje = array("status" => 0, "error" => "Failed to connect to MySQL: " . mysqli_connect_error());
    }
    else{
        $year = $jsondata[0];
        $query = "CALL usm.person_year('" . $year . "')";
        if(!$res = mysqli_query($con,$query)){
            $messaje = array("status" => -1, "error" => mysqli_error($con), "query" => $query);
        }
        else{
            $jsonnode = array();
            while($row = mysqli_fetch_array($res)){
                $pp = person_person($row["id"], $year);
                $linen = array("name" => $row["dept"] . '.' . $row["name"], "size" => 0.5, "imports" => $pp);
                array_push($jsonnode,$linen);
            }
            $filename = "../../data/circular".$year.".json";
            $data  = utf8_encode((string)json_encode($jsonnode));
            $fpc = file_put_contents ($filename , $data);
            $messaje = array("status" => 1, "query" => $query, "json" => $jsonnode, "guardado" => $fpc, "diccionario" => $dictionary);
            
        }
    }
    return $messaje;
}
function person_range($jsondata){
    $_mysql = "localhost;root;complexity*-*;usm";
    $mysql_input = explode(";",$_mysql);
    $con = mysqli_connect($mysql_input[0],$mysql_input[1],$mysql_input[2],$mysql_input[3]);
    if (!mysqli_set_charset($con, "utf8")) {
        $messaje = array("status" => -1, "error" => "Failed to load utf8: " . mysqli_error($con));
    }
    else if (mysqli_connect_errno()) {
        $messaje = array("status" => 0, "error" => "Failed to connect to MySQL: " . mysqli_connect_error());
    }
    else{
        $year1 = $jsondata[0];
        $year2 = $jsondata[1];
        $query = "CALL usm.person_range('" . $year1 . "','".$year2."')";
        if(!$res = mysqli_query($con,$query)){
            $messaje = array("status" => -1, "error" => mysqli_error($con), "query" => $query);
        }
        else{
            $jsonnode = array();
            while($row = mysqli_fetch_array($res)){
                $pp = person_person_range($row["id"], $year1, $year2);
                $linen = array("name" => $row["dept"] . '.' . $row["name"], "size" => 0.5, "imports" => $pp);
                array_push($jsonnode,$linen);
            }
            $filename = "../../data/circularacum.json";
            $data  = utf8_encode((string)json_encode($jsonnode));
            $fpc = file_put_contents ($filename , $data);
            $messaje = array("status" => 1, "query" => $query, "json" => $jsonnode, "guardado" => $fpc, "diccionario" => $dictionary);
            
        }
    }
    return $messaje;
}
function person_person($pers, $year){
    $_mysql = "localhost;root;complexity*-*;usm";
    $mysql_input = explode(";",$_mysql);
    $con = mysqli_connect($mysql_input[0],$mysql_input[1],$mysql_input[2],$mysql_input[3]);
    if (!mysqli_set_charset($con, "utf8")) {
        $messaje = array("status" => -1, "error" => "Failed to load utf8: " . mysqli_error($con));
    }
    else if (mysqli_connect_errno()) {
        $messaje = array("status" => 0, "error" => "Failed to connect to MySQL: " . mysqli_connect_error());
    }
    else{
        $query = "CALL usm.person_person('" . $pers . "','" . $year . "')";
        if(!$res = mysqli_query($con,$query)){
            $messaje = array("status" => -1, "error" => mysqli_error($con), "query" => $query);
        }
        else{
            $jsonp = array();
            while($row = mysqli_fetch_array($res)){
                $line = $row["dept"] . '.' . $row["name"];
                array_push($jsonp,$line);
            }
            return $jsonp;
        }
    }
}
function person_person_range($pers, $year1, $year2){
    // $jsonp = array();
    $_mysql = "localhost;root;complexity*-*;usm";
    $mysql_input = explode(";",$_mysql);
    $con = mysqli_connect($mysql_input[0],$mysql_input[1],$mysql_input[2],$mysql_input[3]);
    if (!mysqli_set_charset($con, "utf8")) {
        $jsonp = array("status" => -1, "error" => "Failed to load utf8: " . mysqli_error($con));
        return $jsonp;
    }
    else if (mysqli_connect_errno()) {
        $jsonp = array("status" => 0, "error" => "Failed to connect to MySQL: " . mysqli_connect_error());
        return $jsonp;
    }
    else{
        $query = "CALL usm.person_person_range('" . $pers . "','" . $year1 . "','".$year2."')";
        if(!$res = mysqli_query($con,$query)){
            $jsonp = array("status" => -1, "error" => mysqli_error($con), "query" => $query);
            return $jsonp;
        }
        else{
            $jsonp = array();
            while($row = mysqli_fetch_array($res)){
                $line = $row["dept"] . '.' . $row["name"];
                array_push($jsonp,$line);
            }
            return $jsonp;
        }
    }
}
// FIN GRAFO (CIRCULAR) DE COLABORACION 

function quitar_tildes($cadena) {
    $no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
    $permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E");
    $texto = str_replace($no_permitidas, $permitidas ,$cadena);
    return $texto;
}

function shortname($jsondata){
    $_mysql = "localhost;root;complexity*-*;usm";
    $mysql_input = explode(";",$_mysql);
    $con = mysqli_connect($mysql_input[0],$mysql_input[1],$mysql_input[2],$mysql_input[3]);
    if (!mysqli_set_charset($con, "utf8")) {
        $messaje = array("status" => -1, "error" => "Failed to load utf8: " . mysqli_error($con));
    }
    $id = $jsondata[0];
    $query = "SELECT IFNULL(short_name, 'Desconocido') as name FROM person WHERE id = ".$id;
    if (mysqli_connect_errno()) {
        $messaje = array("status" => 0, "error" => "Failed to connect to MySQL: " . mysqli_connect_error());
    }
    else{
        if(!$res = mysqli_query($con,$query)){
            $messaje = array("status" => -1, "error" => mysqli_error($con), "query" => $query);
        }
        else{
            $str = mysqli_fetch_array($res);
            $messaje = array("status" => 1, "query" => $query, "result" => $str);
        }
    }
    mysqli_close($con);
    return $messaje;
}
?>