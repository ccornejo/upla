<?php
//Start session
session_start();
 
//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['sess_user_id']) || (trim($_SESSION['sess_user_id']) == '')) {
	header("location: http://190.162.227.177/usm2/login.html");
	exit();
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Ingreso de Datos de Publicaciones para Personas Asociadas</title>
  <link rel="shortcut icon" href="http://usm.cl/assets/img/icon/favicon.ico">
	<link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/datepicker.css" rel="stylesheet">
    <link href="css/token-input.css" type="text/css" rel="stylesheet">
    <link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap-tokenfield.min.css" type="text/css" rel="stylesheet">
    <link href="css/style.css" type="text/css" rel="stylesheet">
    <script src="js/ie-emulation-modes-warning.js"></script>
    <script src="js/ie10-viewport-bug-workaround.js"></script>
	<style type="text/css">
		.table, #isb, #edi, #eds, #importar, #eliminar{display: none;}
	</style>
		
</head>
<body>
	<div class="content">
		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<a id="img-usm" class="navbar-brand" href="#">
						<img src="/usm2/img/header-usm.png" alt="UTFSM" class="img-thumbnail" width="300">
					</a>
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a href="/usm2"><span class="glyphicon glyphicon-home"></span></a></li>
						<li><a href="persona.php">Persona</a></li>
						<li><a href="proyecto.php">Proyecto</a></li>
						<li class="active"><a href="publicacion.php">Publicación</a></li>
						<li><a href="congreso.php">Congreso</a></li>
						<li><a href="patente.php">Patente</a></li>
						<li><a href="revista.php">Revista</a></li>
					</ul>
					<div id="logout"><a href="http://190.162.227.177/usm2/logout.php">Cerrar Sesion</a></div>
				</div>
			</div>
		</div>
		<div id="title">
			<div>
				<h1>Publicaciones</h1>
				<p class="lead">Módulo para el ingreso y edición de datos de publicaciones asociadas a investigadores</p>
			</div>
		</div>
		<div class="field">
			<form role="form">
				<div class="row" id="row-participante">
					<h2>Publicaciones registradas en el sistema</h2>
					<font> Conoce las publicaciones registradas de un autor, ingresando su nombre o RUN.</font>
					<div class="col-xs-12">
						<div class="form-group" id="divautor">
							<label for="autor">Autor Registrado (RUT o Nombre)</label>
							<input class="form-control" id="autor" placeholder="Ej.: Patricio Vargas">
							<button type="button" class="btn btn-default" id="agrega_autor">
								<span class="glyphicon glyphicon-eye-open"></span> <font> Ver publicaciones ya registradas</font>
							</button>
						</div>
						<div class="form-group">
							<table class="table" id="tabla_autor_pub">
								<thead>
									<tr>
										<td value="run">RUN</td>
										<td value="ref">Otras publicaciones ya registradas</td>
										<td></td>
									</tr>
								</thead>
								<tbody id="display">
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="row" id="formulario">
					<h2>Carga nuevas Publicaciones</h2>
					<div id="internal">
						<div class="form-group medium">
							<label for="autores">Autor(es)</label>
							<input class="tokenfield" id="autores" placeholder="Ej.: Patricio Vargas, Eduardo Soto">
              <font color="red">*Ingrese la lista de autores en orden de importancia, el primero es el AUTOR PRINCIPAL.<br>*Es recomendable que TODO participante perteneciente a la USM sea escogido de la lista desplegable. Si la presona no se encuentra, es necesario registrarla en el ambiente persona.</font>
						</div>
					 
						<div class="form-group group">			  
							<label for="tipo_publicacion">Tipo de publicación</label>
								<select class="form-control short" id="tipo_publicacion" placeholder="Selecciona tipo">
									<option value="Articulo">Artículo</option>
									<option value="Libro">Libro</option>
									<option value="Capitulo">Capítulo de libro</option>
								</select>
						</div>
						
						<div class="form-group group">
							<label for="titulo">Título</label>
							<input type="text" class="form-control medium" id="titulo" placeholder="Ej.: Investigación Aplicada">
						</div>
						
						<div class="form-group group">
							<label for="ano">Año publicación</label>
							<input type="text" class="form-control short" id="ano" placeholder="Ej.: 2010">
						</div>

						<div class="form-group group short-medium" id="rev_prueba">
							<label id="nombre_label" for="revista">Nombre Revista</label>
							<input type="text" class="tokenfield" id="revista" placeholder="Ej.: International Journal of...">
						</div>

						<div class="form-group group" id="vol"> 
							<label for="volumen">Volumen(Número)</label>
							<input type="text" class="form-control tiny" id="volumen" placeholder="Ej.: 2(10)">
						</div>
						<div class="form-group group">
							<label for="paginas">Páginas</label>
							<input type="text" class="form-control tiny" id="paginas" placeholder="Ej.: 47-56">
						</div>
						<div class="form-group group" id="do">
							<label for="doi">DOI</label>
							<input type="text" class="form-control short" id="doi" placeholder="Ej.:">
						</div>
						<div class="form-group medium" id="kw">
							<label for="keyword">Palabras Clave</label>  (opcional)
							<textarea class="form-control" rows="3" id="keyword" placeholder="Ej.: model, networks"></textarea>
						</div>
						<div class="form-group" id="isb">
							<label for="isbn">ISBN</label>
							<input type="text" class="form-control" id="isbn" placeholder="Ej.:">
						</div>
						<div class="form-group" id="edi">
							<label for="editorial">Editorial</label>
							<input type="text" class="form-control" id="editorial" placeholder="Ej.: ">
						</div>
						<div class="form-group" id="eds">
							<label for="editores">Editores</label>
							<input type="text" class="form-control" id="editores" placeholder="Ej.:">
						</div>
						<div class="button">
							<button type="button" class="btn btn-default" id="guardar">GUARDAR</button>
							<button type="button" class="btn btn-default" id="eliminar">ELIMINAR</button>
							<button type="button" class="btn btn-default" id="importar">IMPORTAR</button>
						</div>
					</div>
				</div>          
			</form>	
		</div>	
	</div>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.tokeninput.js"></script>
	<script src="js/jquery-ui.min.js"  type="text/javascript"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-tokenfield.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/functions.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script type="text/javascript">
      var jsonrev = new Array();
      var jsonpers = new Array();
      var id = '';
      $(document).ready(function(){
        
        $('html, body').animate({ scrollTop: 0 }, 'fast');
		    
		    $("#autor").tokenInput("/usm2/data/persons.json", {
			    resultsLimit:10,
          tokenLimit: 1         
        });
        $("#ano").css("cursor","pointer");
        $("#ano").prop("readonly", true);
        $("#ano").datepicker({
          format: " yyyy",
          viewMode: "years", 
          minViewMode: "years",
          autoclose: true
        });

        $.getJSON( "/usm2/data/journal_val_label.json", function( data ) {
          jsonrev = data;
          $('#revista').tokenfield({
            limit: 1,
            createTokensOnBlur: true,
            autocomplete: {
              source: jsonrev,
              delay: 100,
              minLength: 1
            },
            showAutocompleteOnFocus: true
          });
        });

        $.getJSON( "/usm2/data/persons_val_label.json", function( data ) {
          jsonpers = data;
          $('#autores').tokenfield({
            limit: 0,
            createTokensOnBlur: true,
            autocomplete: {
              source: jsonpers,
              minLength: 1
            },
            showAutocompleteOnFocus: true
          });
        });

      });

      $("#titulo, #ano").change(function(){
        if($("#titulo").val() && $("#ano").val()){
          var metodo = 'pub_exist_in_db';
          var arg = ['publication', $("#titulo").val(), $("#ano").val()];
          invocar_webservice(metodo, arg, function(data){
            if(data==1){
              alert("Ya existe una publicación con el mismo TÍTULO y AÑO");
              $("#guardar").prop("disabled", true);
            }
            else{
              $("#guardar").prop("disabled", false);
            }
          });
        }
      });
      $("#ano").change(function(){
        var metodo = 'exist_in_db';
        var arg = ['project',$(this).val()];
        invocar_webservice(metodo, arg, function(data){
          if(data==1){
            alert("Ya existe un proyecto asociado a este ID");
            $("#guardar").prop("disabled", true);
          }
          else{
            $("#guardar").prop("disabled", false);
          }
        });
      });
    
      $("#guardar").click(function(){
        var mensaje = '';
        var idpublicacion;
        var jsondatos = new Object();
        id_revista("revista", function(data){ // COMPRUEBA SI EXISTE REVISTA, SI NO AGREGA Y RETORNA ID
          jsondatos.revista = data;  
          console.log($("#autores").tokenfield('getTokens'));
          id_person($("#autores").tokenfield('getTokens'), new Array(), function(data){ // COMPRUEBA PERSONAS
            jsondatos.autores = data;
            console.log(jsondatos.autores);
            jsondatos.tipo = $("#tipo_publicacion").val();
            jsondatos.titulo = $("#titulo").val();
            jsondatos.ano = $("#ano").val().trim();
            jsondatos.volumen = $("#volumen").val();
            jsondatos.paginas = $("#paginas").val();
            jsondatos.doi = $("#doi").val();
            jsondatos.keyword = $("#keyword").val();
            jsondatos.isbn = $("#isbn").val();
            jsondatos.editorial = $("#editorial").val();
            jsondatos.editores = $("#editores").val();
                       
            // console.log(jsondatos);
            if($("#autores").tokenfield('getTokens').length == 0) mensaje += '\n-Autores';
            if(jsondatos.titulo == '') mensaje += '\n-Título';
            if(jsondatos.ano == '') mensaje += '\n-Año de publicación';
            if(jsondatos.revista == '' || jsondatos.revista == -1) mensaje += '\n-Nombre de la revista';
            if(jsondatos.tipo == 'Articulo' && jsondatos.volumen == '') mensaje += '\n-Volumen';
            if(jsondatos.paginas == '') mensaje += '\n-Páginas';
            if(jsondatos.tipo == 'Articulo' && jsondatos.doi == '') mensaje += '\n-DOI';
            if(jsondatos.tipo != 'Articulo' && jsondatos.isbn == '') mensaje += '\n-ISBN';
            if(jsondatos.tipo != 'Articulo' && jsondatos.editorial == '') mensaje += '\n-Editorial';
            if(jsondatos.tipo != 'Articulo' && jsondatos.editores == '') mensaje += '\n-Editores';

            if (mensaje != ''){
              alert('Debe llenar los siguientes campos:' + mensaje);
            }
            else{
              var metodo = id == '' ? 'add_data_to_table' : 'update_table';
              var tabla = 'publication';
              var arg1 = ['title', 'type','year','volume','pages','DOI','journal_id','editor','editorial','isbn','keyword'];
              var arg2 = [jsondatos.titulo, jsondatos.tipo, jsondatos.ano, jsondatos.volumen, jsondatos.paginas, jsondatos.doi, jsondatos.revista, jsondatos.editores, jsondatos.editorial, jsondatos.isbn, jsondatos.keyword];
              var arg = id == '' ? [tabla, arg1, arg2] : [tabla, id, arg1, arg2];
              var salida = new Object();
              salida["stataus"] = 1;
              salida["message"] = "Publicación guardada correctamente"
              //INGRESAR LOS DATOS A LA TABLA PUBLICATION
              invocar_webservice(metodo, arg, function(data){
                // console.log("GUARDANDO");
                // console.log(data);
                if(data.stataus == -1){
                  salida["status"] = -1;
                  salida["message"] = "Error al ingresar publicación";
                  salida["result"] = data;
                }
                idpublicacion = data.id; // RESULTADO
                if(data.id) id = data.id;
                var tabla = 'person_has_publication';
                // ELIMINANDO RELACIONES AUTORES
                eliminar_relacion_publicacion(id, tabla, function(da){
                  // console.log(da);
                  // console.log(jsondatos.autores);
                  var columns = ['person_id', 'is_ca', 'publication_id'];
                  // var au = arraytomatrix(jsondatos.autores);
                  // ASOCIANDO LOS AUTORES
                  guardasubtabla(id, columns, jsondatos.autores, 'person_has_publication', function(data){
                    // console.log("AUTORES");
                    // console.log(data);
                    if(data.status == -1){
                      salida["status"] = -1;
                      salida["message"] = "Error al ingresar publicación";
                      salida["result"] = data;
                    }
                    for(i=2000; i<2015; i++){
                      var arg = [i];
                            var metodo = 'generate_json_list_year';
                            invocar_webservice(metodo, arg, function(data){
                              console.log(data);
                            });
                    }
                    var metodo = 'generate_csv_sunburst_year_range';
                    invocar_webservice(metodo, arg, function(data){
                      console.log(data);
                    });
                    var metodo = 'person_year_range';
                    invocar_webservice(metodo, arg, function(data){
                      console.log(data);
                    });
                    var metodo = 'generate_csv_sunburst_range';
                    invocar_webservice(metodo, arg, function(data){
                      console.log(data);
                    });
                    var metodo = 'person_range';
                    invocar_webservice(metodo, arg, function(data){
                      console.log(data);
                    });
                    // console.log("autores->publicación",data);
                    alert(salida.message);
                    location.reload(true);
                  });  
                });
              });
            }
          });
        });
      });
      $("#eliminar").click(function(){
        alert("Eliminar");
      });
      $("#importar").click(function(){
        alert("Importar");
      });
      $("#agrega_autor").click(function(){
        llenatablatokeninputcheck(['autor'], 'autor_pub');
        $("#formulario").css("display","block");
        $("#autor").tokenInput("clear");
      });
      $("#tipo_publicacion").on("change", function(){
        if($("#tipo_publicacion").val() != "Artículo"){
          $("#isb").css("display", "block");
          $("#edi").css("display", "block");
          $("#eds").css("display", "block");
          $("#ca").css("display", "none");
          $("#co_a").html("");
          $("#ind").css("display", "none");
          $("#rev").css("display", "none");
          $("#revista").html("");
          $("#vol").css("display", "none");
          $("#volumen").html("");
          $("#do").css("display", "none");
          $("#doi").html("");
          $("#kw").css("display", "none");
          $("#keyword").html("");
          $("#nombre_label").html("Nombre Libro")
        }
        else{
          $("#isb").css("display", "none");
          $("#isbn").html("");
          $("#edi").css("display", "none");
          $("#editorial").html("");
          $("#eds").css("display", "none");
          $("#editores").html("");
          $("#ca").css("display", "block");
          $("#ind").css("display", "block");
          $("#rev").css("display", "block");
          $("#vol").css("display", "block");
          $("#do").css("display", "block");
          $("#kw").css("display", "block");
          $("#nombre_label").html("Nombre Revista")
        }
      })

      function desplegar_publicacion(id_pub){
        // alert(id_pub);
        id = id_pub;
        var metodo = 'read_table';
        var tabla = 'publication';
        var arg = [tabla, id];
        // console.clear();
        invocar_webservice(metodo, arg, function(data){
          var rev = data.result;

          if(rev.type)$("#tipo_publicacion").val(rev.type);
          if(rev.title){
            $("#titulo").val(rev.title);
            $("#titulo").prop("disabled",true);
          }
          if(rev.year)$("#ano").val(rev.year);
          if(rev.volume)$("#volumen").val(rev.volume);
          if(rev.pages)$("#paginas").val(rev.pages);
          if(rev.DOI)$("#doi").val(rev.DOI);
          if(rev.isbn)$("#isbn").val(rev.isbn);
          if(rev.editorial)$("#editorial").val(rev.editorial);
          if(rev.editor)$("#editores").val(rev.editor);
          if(rev.keyword)$("#keyword").val(rev.keyword);
          if(rev.journal_id){
            var revis = busca_u(jsonrev, rev.journal_id);
            $("#revista").tokenfield("setTokens", [revis]);
          }
          $("#autores").tokenfield("setTokens", []);
          metodo = 'read_subtable';
          arg = ['person_has_publication', id, 'publication_id'];
          invocar_webservice(metodo, arg, function(data){
            if(data.result){
              for(i=0; i<data.result.length; i++){
                var idp = data.result[i].person_id;
                var pers = busca_u(jsonpers,idp);
                if(pers){
                  $("#autores").tokenfield("createToken", pers);
                }
                else{
                  invocar_webservice('shortname',[idp],function(a){
                    var perss = new Object();
                    perss["label"] = a.result.name;
                    perss["value"] = idp;
                    $("#autores").tokenfield("createToken", perss);
                  });
                }
              }
            }
          });
        });
      }

      function eliminar_relacion_publicacion(ide, tabla, callback){
        var retorno = new Object();
        var metodo = 'delete_table';
        var columns = ['publication_id'];
        var values = [ide];
        var arg = [tabla, columns, values];
        invocar_webservice(metodo, arg, function(data){
          if(data.status != 1){
            retorno["status"] = -1;
            retorno["result"] = data;
            callback(retorno);
          }
          else{
            retorno["status"] = 1;
            retorno["result"] = "Eliminado con éxito";
            retorno["query"] = data.query;
            callback(retorno);
          }
        });
      }

  
    </script>
</body>
</html>
