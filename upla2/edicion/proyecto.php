<?php
//Start session
session_start();
 
//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['sess_user_id']) || (trim($_SESSION['sess_user_id']) == '')) {
	header("location: http://190.162.227.177/usm2/login.html");
	exit();
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Ingreso de Datos de Proyectos para Personas Asociadas</title>
  <link rel="shortcut icon" href="http://usm.cl/assets/img/icon/favicon.ico">
	<link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/datepicker.css" rel="stylesheet">
    <link href="css/token-input.css" type="text/css" rel="stylesheet">
    <link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap-tokenfield.min.css" type="text/css" rel="stylesheet">
    <link href="css/style.css" type="text/css" rel="stylesheet">
    <script src="js/ie-emulation-modes-warning.js"></script>
    <script src="js/ie10-viewport-bug-workaround.js"></script>
	<style type="text/css">
		.table, #importar, #eliminar{display: none;}
	</style>	
		
		
		
</head>
<body>
	<div class="content">
		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<a id="img-usm" class="navbar-brand" href="#">
						<img src="/usm2/img/header-usm.png" alt="UTFSM" class="img-thumbnail" width="300">
					</a>
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a href="/usm2"><span class="glyphicon glyphicon-home"></span></a></li>
						<li><a href="persona.php">Persona</a></li>
						<li class="active"><a href="proyecto.php">Proyecto</a></li>
						<li><a href="publicacion.php">Publicación</a></li>
						<li><a href="congreso.php">Congreso</a></li>
						<li><a href="patente.php">Patente</a></li>
						<li><a href="revista.php">Revista</a></li>
					</ul>
					<div id="logout"><a href="http://190.162.227.177/usm2/logout.php">Cerrar Sesion</a></div>
				</div>
			</div>
		</div>
		<div id="title">
			<div>
				<h1>Proyectos</h1>
				<p class="lead">Módulo para el ingreso y edición de datos de proyectos asociados a investigadores</p>
			</div>
		</div>
		<div class="field">
			<form role="form">
				<div class="row" id="row-participante">
					<h2>Proyectos registrados en el sistema</h2>
					<font> Conoce los proyectos en los cuales participa un académico, ingresando su nombre o RUN.</font>
					<div class="col-xs-12">
						<div class="form-group">
							<label for="participante">Participante (RUT o Nombre)</label>
								<input class="form-control" id="participante" placeholder="Ej.: 12675974-k">
								<button type="button" class="btn btn-default" id="agrega_participante">
									<span class="glyphicon glyphicon-eye-open"></span><font> Ver proyectos ya registrados</font>
								</button>
						</div>
						<div class="form-group">
							<table class="table" id="tabla_participante">
								<thead>
									<tr>
										<td value="run">RUN</td>
										<td></td>
										<td></td>
									</tr>
								</thead>
								<tbody id="display">
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="row" id="formulario">
					<h2>Ingresa un Nuevo Proyecto</h2>
					<div id="internal">
						<div class="form-group group">
							<label for="id">ID/Número</label>
							<input type="text" class="form-control short" id="id" placeholder="Ej.: 11121292">
						</div>
						
						<div class="form-group group">
							<label for="titulo">Título</label>
							<input type="text" class="form-control medium" id="titulo" placeholder="Ej.: Automatización de Procesos">
						</div>
						
						<div class="form-group medium">
							<label for="participantes">Participante(s)</label>
							<input class="tokenfield medium" id="participantes" placeholder="Ej.: Patricio Vargas, Eduardo Soto">
              <font color="red">*Es recomendable que TODO participante perteneciente a la USM sea escogido de la lista desplegable. Si la presona no se encuentra, es necesario registrarla en el ambiente persona.</font>
						</div>
						
						<div class="form-group group">
							<label for="tipo">Tipo Proyecto</label>
							<select class="form-control short" id="tipo" placeholder="Selecciona tipo">
								<option value="Fondecyt">Fondecyt</option>
								<option value="Fondef">Fondef</option>
								<option value="Otro">Otros Proyectos</option>
							</select>
						</div>
						<div class="form-group group">
							<label for="subtipo">Subtipo</label>
							<select class="form-control short" id="subtipo" placeholder="Selecciona subtipo">
								<option value="regular">Regular</option>
								<option value="iniciación">Iniciación</option>
								<option value="postdoctorado">Postdoctorado</option>
							</select>
						</div>
					  
						<div class="form-group group">
							<label for="participacion">Participación</label>
							<select class="form-control medium" id="participacion" placeholder="Selecciona tipo">
								<option value="responsable">Investigador Principal (responsable)</option>
								<option value="co-investigador">Co-investigador</option>
							</select>
						</div>
							<div class="form-group group">
								<label for="ano">Año de Adjudicación</label>
								<input type="text" class="form-control short" id="ano" placeholder="Ej.: 2000">
							</div>
						<div><label>Período</label></div>
						<div class="form-group group">
							Desde:
							<input type="text" class="form-control short" id="desde" placeholder="Ej.: día/mes/año (1/3/74)" class="datepicker">
						</div>
						<div class="form-group group">
							Hasta:
							<input type="text" class="form-control short" id="hasta" placeholder="Ej.: día/mes/año (1/3/74)" class="datepicker">
						</div>
							<div class="form-group medium">
								<label for="descripcion">Descripción</label> (No obligatorio)
								<textarea class="form-control" rows="3" id="descripcion" placeholder="Este proyecto ..."></textarea>  
							</div>
						<div class="button">
							<button type="button" class="btn btn-default" id="guardar">GUARDAR</button>
							<button type="button" class="btn btn-default" id="eliminar">ELIMINAR</button>
							<button type="button" class="btn btn-default" id="importar">IMPORTAR</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.tokeninput.js"></script>
	<script src="js/jquery-ui.min.js"  type="text/javascript"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-tokenfield.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/functions.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script type="text/javascript">
      // var urlws = 'webservice/webservice.php';
      var jsonpers = new Array();
      $(document).ready(function(){
        $('html, body').animate({ scrollTop: 0 }, 'fast');
	      $("#participante").tokenInput("/usm2/data/persons.json", {
			    resultsLimit:10,
          tokenLimit: 1         
        });
        $("#desde, #hasta, #ano").prop("readonly", true);
        $("#desde, #hasta, #ano").css("cursor", "pointer");
        $("#desde, #hasta").datepicker({
          format: "dd-mm-yyyy",
          weekStart: 1,
          autoclose: "true",
          minViewMode: 2
        });
        $("#ano").datepicker({
          format: " yyyy",
          viewMode: "years", 
          minViewMode: "years",
          autoclose: true
        });

        $.getJSON( "/usm2/data/persons_val_label.json", function( data ) {
          jsonpers = data;
          $('#participantes').tokenfield({
            limit: 0,
            createTokensOnBlur: true,
            autocomplete: {
              source: jsonpers,
              minLength: 1
            },
            showAutocompleteOnFocus: true
          });
        });
      });
      $("#id").change(function(){
        var metodo = 'exist_in_db';
        var arg = ['project',$(this).val()];
        invocar_webservice(metodo, arg, function(data){
          if(data==1){
            alert("Ya existe un proyecto asociado a este ID");
            $("#guardar").prop("disabled", true);
          }
          else{
            $("#guardar").prop("disabled", false);
          }
        });
      });
      $("#guardar").click(function(){
        var mensaje = '';
        var idproyecto;
        var jsondatos = new Object();
        jsondatos.participante = $("#participantes").tokenfield("getTokensList").split(", ");
        jsondatos.titulo = $("#titulo").val();
        jsondatos.numero = $("#id").val();
        jsondatos.tipo = $("#tipo").val();
        jsondatos.subtipo = $("#subtipo").val();
        jsondatos.participacion = $("#participacion").val();
        jsondatos.ano = $("#ano").val().trim();
        if($("#desde").val()){
          d = $("#desde").val().split('-');
          jsondatos.desde = d[2] + '-' + d[1] + '-' + d[0] ;
        }
        if($("#hasta").val()){
          h = $("#hasta").val().split('-');
          jsondatos.hasta = h[2] + '-' + h[1] + '-' + h[0] ;
        } 
        jsondatos.descripcion = $("#descripcion").val();

        console.log(jsondatos);

        if(jsondatos.titulo == '') mensaje += '\n-Título';
        if(jsondatos.numero == '') mensaje += '\n-ID';
        if(jsondatos.ano == '') mensaje += '\n-Año de adjudicación';
        if($("#desde").val() == '') mensaje += '\n-Inicio período';
        if($("#hasta").val() == '') mensaje += '\n-Fin período';

        if(mensaje != '') 
          alert('Debe llenar los siguientes campos:' + mensaje);
        else{
          var metodo = 'add_data_to_table';
          var tabla = 'project';
          var arg1 = ['title', 'type','subtype','participation','number','start','end','year','description'];
          var arg2 = [jsondatos.titulo, jsondatos.tipo, jsondatos.subtipo, jsondatos.participacion, jsondatos.numero, jsondatos.desde, jsondatos.hasta, jsondatos.ano, jsondatos.descripcion];
          var arg = [tabla, arg1, arg2];
          var salida = new Object();
          salida["stataus"] = 1;
          salida["message"] = "Proyecto guardado correctamente"
          //INGRESAR LOS DATOS A LA TABLA PROJECT
          invocar_webservice(metodo, arg, function(data){
            if(data.stataus == -1){
              salida["status"] = -1;
              salida["message"] = "Error al ingresar publicación";
              salida["result"] = data;
            }
            idproyecto = data.id; // RESULTADO
            var columns = ['person_id', 'project_id'];
            var partmat = arraytomatrix(jsondatos.participante);
            // ASOCIANDO AUTOR PRINCIPAL
            guardasubtabla(idproyecto, columns, partmat, 'person_has_project', function(data){
              if(data.stataus == -1){
                salida["status"] = -1;
                salida["message"] = "Error al ingresar proyecto";
                salida["result"] = data;
              }
              console.log("participante->proyecyo",data);
              alert(salida.message);
              location.reload(true);
            });
          });
        }
      });
      $("#eliminar").click(function(){
        alert("Eliminar");
      });
      $("#importar").click(function(){
        alert("Importar");
      });
      $("#agrega_participante").click(function(){
        llenatablatokeninput(['participante'], 'participante');
        $("#formulario").css("display","block");
        $("#participante").tokenInput("clear");
      });
      $("#tipo").on('change', function(){
        if($("#tipo").prop("selectedIndex") == 0){
          $("#subtipo").html('<option value="regular">Regular</option><option value="iniciación">Iniciación</option><option value="postdoctorado">Postdoctorado</option>');
          $("#participacion").html('<option value="responsable">Investigador Principal (responsable)</option><option value="co-investigador">Co-investigador</option>');
          $("#subtipo").prop("disabled", false);
        }
        else if($("#tipo").prop("selectedIndex") == 1){
          $("#subtipo").html('');
          $("#subtipo").prop("disabled", true);
          $("#participacion").html('<option value="responsable">Investigador Principal (responsable)</option><option value="co-investigador">Co-investigador</option>');
        }
        else if($("#tipo").prop("selectedIndex") == 2){
          $("#subtipo").html('<option value="interno">interno</option><option value="industria">Con financiamiento de la industria</option><option value="regional">Proyectos con financiamiento del gobierno regional</option><option value="corfo">CORFO</option><option value="anillo">Anillo</option><option value="otro">Otro</option>');
          $("#participacion").html('<option value="responsable">Investigador Principal (responsable)</option><option value="co-investigador">Co-investigador</option>');
          $("#subtipo").prop("disabled", false);
        }
      });
      
      $("#subtipo").on('change', function(){
        if($("#subtipo").val() == "postdoctorado"){
          $("#participacion").html('<option value="postdoctorando">Postdoctorando</option><option value="patrocinante">Patrocinante</option>');
        }
        else {
          $("#participacion").html('<option value="responsable">Investigador Principal (responsable)</option><option value="co-investigador">Co-investigador</option>');
        }
      });
      
    </script>
</body>
</html>
