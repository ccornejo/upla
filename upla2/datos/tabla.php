<?php
//Start session
session_start();
 
//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['sess_user_id']) || (trim($_SESSION['sess_user_id']) == '')) {
	header("location: http://190.162.227.177/usm2/login.html");
	exit();
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Tabla Resumen</title>
	<link rel="shortcut icon" href="http://usm.cl/assets/img/icon/favicon.ico">
	<link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/datepicker.css" rel="stylesheet">
    <link href="css/token-input.css" type="text/css" rel="stylesheet">
    <link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap-tokenfield.min.css" type="text/css" rel="stylesheet">
    <link href="css/style.css" type="text/css" rel="stylesheet">
    <script src="js/ie-emulation-modes-warning.js"></script>
    <script src="js/ie10-viewport-bug-workaround.js"></script>
	<style type="text/css">
		#tabla,#exportar{display: none;}
	</style>
</head>
<body>
	<div class="content">
		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<a id="img-usm" class="navbar-brand" href="#">
						<img src="/usm2/img/header-usm.png" alt="UTFSM" class="img-thumbnail" width="300">
					</a>
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a href="/usm2"><span class="glyphicon glyphicon-home"></span></a></li>
						<li><a href="ficha.php">Ficha Docente</a></li>
						<li class="active"><a href="tabla.php">Tabla Resumen</a></li>
					</ul>
					<div id="logout"><a href="http://190.162.227.177/usm2/logout.php">Cerrar Sesion</a></div>
				</div>
			</div>
		</div>
		<div id="title" class="f">
			<div>
				<h1>Tabla Resumen UTFSM</h1>
				<p class="lead">En este módulo puedes generar tablas resumen de publicaciones y proyectos de académicos.</p>
			</div>
		</div>
		<div class="field">
			<div class="form-group">
				<label for="author">NOMBRE O RUT DEL INVESTIGADOR</label>
				<div class="row">
					<div class="col-lg-5">
						<input type="text" class="form-control" id="author" placeholder="Ej.: 12587162-k">
					</div>
					<div class="col-lg-5">
						<select class="form-control" id="programa" placeholder="Selecciona">
							<option value="1">Doctorado en Biotecnología</option>
							<option value="2">Doctorado en Ciencias, mención Física</option>
							<option value="3">Doctorado en Ciencias, mención Química</option>
							<option value="4">Doctorado en Matemática</option>
							<option value="5">Doctorado en Ingeniería Electrónica</option>
							<option value="6">Doctorado en Ingeniería Informática</option>
							<option value="7">Doctorado en Ingeniería Química</option>
							<option value="8">Magíster en Ciencias de la Ingeniería Civil</option>
							<option value="9">Magíster en Ciencias de la Ingeniería Eléctrica</option>
							<option value="10">Magíster en Ciencias de la Ingeniería  Electrónica</option>
							<option value="11">Magíster en Ciencias de la Ingeniería Informática</option>
							<option value="12">Magíster en Ciencias de la Ingeniería Mecánica</option>
							<option value="13">Magíster en Ciencias de la Ingeniería Química</option>
							<option value="14">Magíster en Ciencias de la Ingeniería Industrial</option>
							<option value="15">Magíster en Ciencias de la Ingeniería Telemática</option>
							<option value="16">Magíster en Ciencias de la Ingeniería Metalúrgica</option>
							<option value="17">Magíster en Ciencias, mención Física</option>
							<option value="18">Magíster en Ciencias, mención Matemática</option>
							<option value="19">Magíster en Ciencias, mención Química</option>
							<option value="20">Magíster en Redes y Telecomunicaciones</option>
							<option value="21">Magíster en Economía Energética</option>
							<option value="22">MBA-Magíster en Gestión Empresarial </option>
							<option value="23">Magíster en Ingeniería Aeronáutica</option>
							<option value="24">Magíster en Innovación Tecnológica y Emprendimiento</option>
							<option value="25">Magíster en Gestión de Activos y Mantenimiento</option>
							<option value="26">Magíster en Tecnologías de la Información</option>
							<option value="27">Magíster en Gestión y Tecnología Agronómica</option>
							<option value="28">Programas de Otras Universidades</option>
						  </select> 
          </div>
				</div>
				
				*Puede ingresar mas de uno (para generar tabla resumen con todos los investigadores asociados al programa).
			</div>
			<div class="form-group">
				<button type="button" class="btn btn-default" id="generar" >
					<span >GENERAR</span>
				</button>
			</div>
		<div id="exp">
			<table class="table" id="tabla">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Año de Ingreso</th>
						<th>Publicaciones ISI</th>
						<th>Otras Publicaciones</th>
						<th>Proyectos FONDECYT</th>
						<th>Proyectos FONDECYT como investigador responsable</th>
						<th>Proyectos FONDEF</th>
						<th>Otros Proyectos</th>
					</tr> 
				</thead>
				<tbody>

				</tbody>
			</table>
		</div>
		<div class="form-group">
			<button type="button" class="btn btn-default word-export" id="exportar">
				<span >EXPORTAR</span>
			</button>
			<a id="dlink"  style="display:none;"></a>
		</div>
	</div>
	<script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.tokeninput.js" type="text/javascript"></script>
    <script src="js/FileSaver.js"></script>
    <script src="js/jquery.wordexport.js"></script>
	<script type="text/javascript">
      $(document).ready(function(){
          $("select").change(function(){

			  $(".token-input-list").remove();
			  $("#author").tokenInput('webservice/person.php?id='+$("select").val(), {
				resultsLimit:10,
				preventDuplicates: true,
				noResultsText: "No hay resultados para la búsqueda.",
			  });
			  
			  })
		  $("#author").tokenInput("webservice/person.php?id=1", {
			resultsLimit:10,
			preventDuplicates: true,
			noResultsText: "No hay resultados para la búsqueda.",
		  });
		  
		  $("#generar").click(function(){
			  id = $(".token-input-list").siblings("input[type=text]").val()
			  prog = $("#programa").val();
			  // console.log(prog);
			  $.ajax({
				url : "webservice/tabla_resumen.php?id="+id+"&p="+prog,
				success : function(ret){
          // console.log(ret.split("'"));
          // console.log(JSON.parse(ret));
          var res = JSON.parse(ret);
          // console.log(res.length);
          var res_html = "";
          cont = 0;
          for(r in res){
			if (cont % 2 == 0)
				res_html += "<tr class='par'>";
			else
				res_html += "<tr>";
            res_html += "<td>" + res[r].name + "</td>";
            res_html += "<td>" + res[r].date + "</td>";
            res_html += "<td>" + res[r].isi + "</td>";
            res_html += "<td>" + res[r].nisi + "</td>";
            res_html += "<td>" + res[r].fondecyt + "</td>";
            res_html += "<td>" + res[r].fondecyt_r + "</td>";
            res_html += "<td>" + res[r].fondef + "</td>";
            res_html += "<td>" + res[r].other + "</td>";
            res_html += "</tr>";
            cont++;
          }
          // console.log(res_html);
          $("tbody").empty();
					$("tbody").append(res_html);
          $("#tabla, #exportar").css("display", "block");
				}
			})
		  });
		  
		  $("#exportar").click(function(event) {
				tableToExcel('tabla', 'tabla resumen', 'tabla_resumen.xls')
		  }); 
      
       });

			var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    	})()

    	function utf8_to_b64( str ) {
			    return window.btoa(encodeURIComponent( escape( str )));
			}

			function b64_to_utf8( str ) {
			    return unescape(decodeURIComponent(window.atob( str )));
			}

			function encode_utf8(s) {
			  return unescape(encodeURIComponent(s));
			}

			function decode_utf8(s) {
			  return decodeURIComponent(escape(s));
			}
    </script>
</body>
</html>
