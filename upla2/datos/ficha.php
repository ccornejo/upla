<?php
//Start session
session_start();
 
//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['sess_user_id']) || (trim($_SESSION['sess_user_id']) == '')) {
	header("location: http://190.162.227.177/usm2/login.html");
	exit();
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Fichas Docentes</title>
	<link rel="shortcut icon" href="http://usm.cl/assets/img/icon/favicon.ico">
	<link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/datepicker.css" rel="stylesheet">
  <link href="css/token-input.css" type="text/css" rel="stylesheet">
  <link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css">
  <link href="css/bootstrap-tokenfield.min.css" type="text/css" rel="stylesheet">
  <link href="css/style.css" type="text/css" rel="stylesheet">
  <script src="js/ie-emulation-modes-warning.js"></script>
  <script src="js/ie10-viewport-bug-workaround.js"></script>
	<style type="text/css">
		#tabla,#exportar{display: none;}
	</style>
</head>
<body>
	<div class="content">
		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<a id="img-usm" class="navbar-brand" href="#">
						<img src="/usm2/img/header-usm.png" alt="UTFSM" class="img-thumbnail" width="300">
					</a>
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a href="/usm2"><span class="glyphicon glyphicon-home"></span></a></li>
						<li class="active"><a href="ficha.php">Ficha Docente</a></li>
						<li><a href="tabla.php">Tabla Resumen</a></li>
					</ul>
					<div id="logout"><a href="http://190.162.227.177/usm2/logout.php">Cerrar Sesion</a></div>
				</div>
			</div>
		</div>
		<div id="title" class="f">
			<div>
				<h1>Fichas Docentes UTFSM</h1>
				<p class="lead">En este módulo puedes generar la ficha docente de un académico.</p>
			</div>
		</div>
		<div class="field">
			<div class="form-group">
				<label for="author">NOMBRE O RUT DEL INVESTIGADOR</label>
				<input type="text" class="form-control" id="author" placeholder="Ej.: 12587162-k">  
			</div>
			
			<div id="exp">
				<h2 id="nombre"></h2>
				<table class="table" id="tabla">
					<tbody>
						<tr>
							<td class="header">Carácter del vínculo (claustro, colaborador o visitante)</td>
							<td id="participacion"></td>
						</tr>
						<tr class="par">
							<td class="header">Título,  institución, país</td>
							<td id="titulo"></td>
						</tr>
						<tr>
							<td class="header">Grado máximo (especificar área disciplinar), institución, año de graduación y país</td>
							<td id="grado"></td>
						</tr>
						<tr class="par">
							<td class="header">Línea(s) de investigación</td>
							<td id="linea"></td>
						</tr>
						<tr>
							<td class="header">Número de tesis de magíster dirigidas en los últimos 10 años (finalizadas)</td>
							<td id="magister"></td>
						</tr>
						<tr class="par">
							<td class="header">Número de tesis de doctorado dirigidas en los últimos 10 años (finalizadas)</td>
							<td id="doctorado"></td>
						</tr>
						<tr>
							<td class="header">Número de tesis dirigidas en el programa, en los últimos 10 años (finalizadas)</td>
							<td id="programa"></td>
						</tr>
						<tr class="par">
							<td class="header">Listado de publicaciones en los últimos 10 años. En caso de publicaciones con más de un autor, indicar en negrita el autor principal.</td>
							<td id="publicaciones"></td>
						</tr>
						<tr>
							<td class="header">Listado de proyectos de investigación en los últimos 10 años</td>
							<td id="proyectos"></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="form-group">
				<button type="button" class="btn btn-default word-export" id="exportar">
					<span >EXPORTAR</span>
				</button>

				<a id="dlink"  style="display:none;"></a>

				<!-- <input type="button" onclick="tableToExcel('tabla', 'ficha_docente', 'myfile.xls')" value="Export to Excel"> -->
			</div>
		</div>
	</div>
	<script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.tokeninput.js" type="text/javascript"></script>
    <script src="js/FileSaver.js"></script>
    <script src="js/jquery.wordexport.js"></script>
    <script src="js/docx.js"></script>
	<script type="text/javascript">
      $(document).ready(function(){
        $("#exp").css("display","none")
      });

      $("#author").tokenInput("/usm2/data/persons.json", {
        resultsLimit:10,
        tokenLimit: 1,
        noResultsText: "No hay resultados para la búsqueda.",
        onAdd: function(data){
          $.ajax({
            url : "webservice/json_docente.php",
            type : 'GET',
            data : {"id":data.id},
            contentType : 'application/json utf-8',
            dataType : 'json',
            success : function(ret){
				$("#exp").css("display","block")
              $("#nombre").html(ret.nombre);
              $("#participacion").html(ret.participacion.join(", "));
              $("#titlo").html(ret.titulo);
              $("#grado").html(ret.grado.join(", "));
              $("#linea").html(ret.area);
              var mag = '', doc = '', pro = '';
              if(ret.magister.length > 0) mag += "<ol><li>" + ret.magister.join("</li><li>") + "</li></ol>";
              $("#magister").html(mag);
              if(ret.doctorado.length > 0) doc += "<ol><li>" + ret.doctorado.join("</li><li>") + "</li></ol>";
              $("#doctorado").html(doc);
              if(ret.programa.length > 0) pro += "<ol><li>" + ret.programa.join("</li><li>") + "</li></ol>";
              $("#programa").html(pro);
              var pub = "";
              if(ret.isi.length > 0) pub += "Publicaciones ISI<ol><li>" + ret.isi.join("</li><li>") + "</li></ol>";
              if(ret.scielo.length > 0) pub += "Publicaciones SCIELO<ol><li>" + ret.scielo.join("</li><li>") + "</li></ol>";
              if(ret.otras.length > 0) pub += "Publicaciones no incluidas en ISI/SCIELO<ol><li>" + ret.otras.join("</li><li>") + "</li></ol>";
              $("#publicaciones").html(pub);
              $("#proyectos").html("<ol><li>" + ret.proyecto.join("</li><li>")) + "</li></ol>";
              $("#tabla, #exportar").css("display", "block");
            },
            error: function(e){
              console.log(e);

            }
          });
        }
      });

			

      $("#exportar").click(function(event) {
      	tableToExcel('tabla', 'ficha docente', 'ficha_docente.xls')
      }); 

      var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    	})()

    	function utf8_to_b64( str ) {
			    return window.btoa(encodeURIComponent( escape( str )));
			}

			function b64_to_utf8( str ) {
			    return unescape(decodeURIComponent(window.atob( str )));
			}

			function encode_utf8(s) {
			  return unescape(encodeURIComponent(s));
			}

			function decode_utf8(s) {
			  return decodeURIComponent(escape(s));
			}

    </script>
</body>
</html>
