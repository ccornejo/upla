﻿<!--﻿<?php
/*
//Start session
session_start();
 
//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['sess_user_id']) || (trim($_SESSION['sess_user_id']) == '')) {
	header("location: http://190.162.227.177/usm2/login.html");
	exit();
}*/
?>-->
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Interfaz de Consultas</title>
	<link rel="shortcut icon" href="http://www.upla.cl/inclusion/wp-content/uploads/2013/05/favicon.ico" type="image/x-icon">
	<link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/datepicker.css" rel="stylesheet">
    <link href="css/token-input.css" type="text/css" rel="stylesheet">
    <link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap-tokenfield.min.css" type="text/css" rel="stylesheet">
    <link href="css/style.css" type="text/css" rel="stylesheet">
    <link href="../../css/catalogo.css" rel="stylesheet">
    <script src="js/ie-emulation-modes-warning.js"></script>
    <script src="js/ie10-viewport-bug-workaround.js"></script>
	<style type="text/css">
		#exportar, #quart, #exp{display: none;}
	</style>
</head>

<body>
		<!-- titulo -->
		<?php include '../../header.php';?>
		
		<!-- Content -->
		<div class="container-fluid">
		<div class="row">
		
			<div class="col-md-2">
				<?php include '../../navbar.php';?>			
			</div> <!-- /col-md-2 -->
		
			<!-- Contenido --> 
			<div class="col-md-10">
						<!-- barra ubicacion -->
						<?php include '../../breadcrumb.php'; ?>			

						<!-- Modal -->
						<button type="button" class="btn btn-primary btn-lg glyphicon glyphicon-info-sign" data-toggle="modal" data-target="#myModal"></button>
						<div id="myModal" class="modal fade" role="dialog">
						  <div class="modal-dialog">
						    <!-- Modal content-->
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal">&times;</button>
						     		<h4 class="modal-title">
						     			<!--Título--> 
						     			Consulta
						     			<!--Título--> 
						     		</h4>
						      </div>
						      <div class="modal-body">
						        <p>
						        <!--Texto--> 
						        	La herramienta permite obtener una tabla exportable de Publicaciones, Proyectos o Congresos, por Facultad o Centro por año.
						        <!--Texto--> 
						        </p>
						      </div>
						      <div class="modal-footer">
						        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						      </div>
						    </div>
						  </div>
						</div>
						<!-- /Modal -->

						<br>
						<div class="field">
							<div class="form-group short">
								<label for="tabla">Tabla de Consulta</label>
								<select class="form-control" id="tabla" placeholder="Selecciona">
									<option value="congress">Congreso</option>
									<option value="project">Proyecto</option>
									<option value="publication" selected>Publicación</option>
								</select>					
							</div>
							<div id="filter"><h3>FILTRAR POR:</h3></div>
							<div class="form-group short-medium group">
								<label for="departamento">Departamento</label>
								<select class="form-control" id="departamento">
									<option value="0">Todos</option>
									<option value="Educación">Educación</option>
									<option value="Ed. Física">Ed. Física</option>
									<option value="Cs. Salud">Cs. Salud</option>
									<option value="CEA">CEA</option>
									<option value="Cs. Actividad Física">Cs. Actividad Física</option>
									<option value="San Felipe">San Felipe</option>
									<option value="Contraloría Interna">Contraloría Interna</option>
									<option value="Arte">Arte</option>
									<option value="Ciencias">Ciencias</option>
									<option value="Humanidades">Humanidades</option>
									<option value="Ingeniería">Ingeniería</option>
									<option value="Cs. Sociales">Cs. Sociales</option>
								</select>				
							</div>
							<div class="form-group short group">
								<div class="form-group short group"><label for="ano">Año </label><input type="text" class="form-control" id="ano" value="Todos"></div>
							</div>
							<div class="form-group short group">
								<div class="form-group short group" id="quart">
									<label for="cuartil">Cuartil</label>
									<select class="form-control" id="cuartil">
										<option value="Q0">Todos</option>
										<option value="Q1">Q1</option>
										<option value="Q2">Q2</option>
										<option value="Q3">Q3</option>
										<option value="Q4">Q4</option>
									</select>
								</div>				
							</div>
				
							<div class="form-group">
								<button type="button" class="btn btn-default word-export" id="buscar">
									<span >BUSCAR</span>
								</button>
								<a id="dlink"  style="display:none;"></a>
							</div>
							
							
							<div id="exp">
								<table class="table" id="tabla_exp">
									<thead id="head">
										<tr>
											<th>Publicación</th>
											<th>Tipo</th>
											<th>Año</th>
											<th>Revista</th>
											<th>Indexación</th>
											<th>Autor(es) UPLA</th>
											<th>Otros Autores</th>
										</tr>
									</thead>
									<tbody id="body">
				
									</tbody>
								</table>
							</div>
							<div class="form-group">
								<button type="button" class="btn btn-default word-export" id="exportar">
									<span >EXPORTAR</span>
								</button>
								<a id="dlink"  style="display:none;"></a>
							</div>
					</div>
			</div><!-- /col-md-10 -->
						
	
		</div><!-- /row -->
		</div><!-- /conteriner -->	
			
		<!-- footer -->
		<br/>
		<br/>
		<?php include '../../footer.php'; ?>


	<script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.tokeninput.js" type="text/javascript"></script>
    <script src="js/FileSaver.js"></script>
    <script src="js/jquery.wordexport.js"></script>
	<script src="js/jquery-ui.min.js"  type="text/javascript"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){

			$('#tri-inicio').css('border-left-color','#004462');
		 	$('#barra-inicio').css('background-color','#004462');
		 	$('#barra-inicio').css('color','#fff');
		 	$('#tri-inicio').css('background-color','#848484');
		 	$('#tri1').css('border-left-color','#848484');
		 	$('#barra1').html('Consulta')
		 	$('#barra1').css('background-color','#848484');
		 	$('#barra1').css('color','#fff');
		 	$('#tri1').css('background-color','#fff');

		 	$('.tri#tri2').css('display','none');
		 	$('#barra2').css('display','none')
		 	$('.tri#tri3').css('display','none');
		 	$('#barra3').css('display','none')
		 	

			//~ $("#title").css("background-image","url('./img/query.png')")
			$("#ano").css("cursor","pointer");
			$("#ano").prop("readonly", true);
			$("#ano").datepicker({
			  format: " yyyy",
			  viewMode: "years", 
			  minViewMode: "years",
			  autoclose: true
			});

			$("#buscar").click(function(){
				tabla = $("#tabla").val();
				departamento = $("#departamento").val();
				ano = $("#ano").val().trim();
				if (ano=="Todos"){
					ano="";
				}
				cuartil = $("#cuartil").val();
				$.ajax({	
					url : "webservice/consulta.php?t="+tabla+"&d="+departamento+"&a="+ano+"&q="+cuartil,
					success : function(ret){
						var a = eval(ret);
						var b = '';
						for(i=0; i<a.length; i++){
							b += '<tr>';
							for(j=0; j<a[i].length; j++){
								b += '<td>' + a[i][j].replace(/,/g,", ") + '</td>'
							}
							b += '</tr>';
						}
						$("#body").html(b);
						$("#exp").css("display","block");
						$("#exportar").css("display","block");
					}
				});
			});
		});
		
		$("#tabla").on("change", function(v){
			if($(this).val() == "publication"){
				$("#quart").css("display","block");
				$("#head").html("<tr><th>Publicación</th><th>Tipo</th><th>Año</th><th>Revista</th><th>Indexación</th><th>Autor Principal</th><th>Otros Autores</th></tr>");
				$("#body").empty();
				$("#exp").css("display","none");
				$("#exportar").css("display","none");
			}
			else if($(this).val() == "congress"){
				$("#quart").css("display","none");
				$("#head").html("<tr><th>Título</th><th>Lugar</th><th>Año</th><th>Participantes UPLA</th></tr>");
				$("#body").empty();
				$("#exp").css("display","none");
				$("#exportar").css("display","none");
			} 
			else if($(this).val() == "project"){
				$("#quart").css("display","none");
				$("#head").html("<tr><th>Título</th><th>Tipo</th><th>Subtipo</th><th>Año</th><th>Participantes UPLA</th></tr>");
				$("#body").empty();
				$("#exp").css("display","none");
				$("#exportar").css("display","none");
			} 
		});
	   
		$("#exportar").click(function(event) {
			tableToExcel('tabla_exp', 'tabla resumen', 'tabla_consulta.xls')
		}); 

		var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name, filename) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();

        }
    	})()
    </script>
</body>
</html>
