<?
$json_out = array();

header('Content-Type: text/html; charset=UTF-8');
$_mysql = "localhost;root;complexity*-*;usm";
	$mysql_input = explode(";",$_mysql);
	$con = mysqli_connect($mysql_input[0],$mysql_input[1],$mysql_input[2],$mysql_input[3]);
	mysqli_set_charset($con, "utf8");
	$id = $_GET["id"];

	//~ NOMBRE
	$query = "SELECT name,last_name,m_name FROM person WHERE id = ".$id."";
	$res = mysqli_query($con,$query);
	while($row = mysqli_fetch_array($res)){
		// $nombre = array("nombre" => $row["name"] . " " . $row["last_name"] . " " . $row["m_name"])
		$json_out["nombre"] = $row["name"]." ".$row["last_name"]." ".$row["m_name"];
	}

	//~ PARTICIPACION
	$query = "SELECT participation FROM person_has_program WHERE person_id = ".$id." ";
	$res = mysqli_query($con,$query);
	$participacion = array();
	while($row = mysqli_fetch_array($res)){
		array_push($participacion, $row["participation"]);
	}	
	$json_out["participacion"] = $participacion;

	//~ AREA
	$query = "SELECT area FROM person WHERE id= ".$id." ";
	$res = mysqli_query($con,$query);
	while($row = mysqli_fetch_array($res)){
		$json_out["area"] = $row["area"];
	}

	//~ Title
	$query = "SELECT d.name,ins.name as university,d.country FROM person_has_degree phd LEFT JOIN degree d ON phd.degree_id = d.id LEFT JOIN institution ins on d.institution_id = ins.id WHERE d.is_degree = 0 and phd.person_id = ".$id; 
	$res = mysqli_query($con,$query);
	$titulo = array();
	while($row = mysqli_fetch_array($res)){
		array_push($titulo, $row["name"].", ".$row["university"].", ".$row["country"]);
	}
	$json_out["titulo"] = $titulo;

	//~ Grado
	$query = "SELECT d.name,ins.name as university,d.country FROM person_has_degree phd LEFT JOIN degree d ON phd.degree_id = d.id LEFT JOIN institution ins on d.institution_id = ins.id WHERE d.is_degree = 1 and phd.person_id = ".$id;
	$res = mysqli_query($con,$query);
	$grado = array();
	while($row = mysqli_fetch_array($res)){
		array_push($grado, $row["name"].", ".$row["university"].", ".$row["country"]);
	}
	$json_out["grado"] = $grado;
	$ano_actual = date(Y);
	$ano_pasado = $ano_actual-1;
	//~ PUBLICACIONES ISI
	$query = "SELECT IFNULL(person_by_publication(p.id),convert(concat(p.corresponding_author, ', ', p.other_author) using utf8)) as author,p.title,p.year,p.volume,p.pages,j.NF,j.name,j.issn,j.country,j.publisher_name,i.impact FROM person_has_publication php JOIN publication p on php.publication_id = p.id LEFT JOIN journal j on j.id = p.journal_id LEFT JOIN impact_factor i on i.journal_id = j.id and i.year = ".$ano_pasado." and i.impact > 0 and i.impact is not null WHERE php.person_id = ".$id." AND (j.type = 1 OR j.type = 3)";
	$res = mysqli_query($con,$query);
	$isi = array();
  while($row = mysqli_fetch_array($res)){
  	array_push($isi, $row["author"].", ".$row["title"].", ".$row["name"]." (ISSN: ".$row["issn"].", País: ".$row["country"].", Editorial: ".$row["publisher_name"]."), ".$row["volume"]." (".$row["year"].") ".$row["pages"].". (I.I.: ".$row["impact"]."  NF: ".$row["NF"].")");	
	}
	$json_out["isi"] = $isi;
	
	//~ PUBLICACIONES SCIELO
	$query = "SELECT IFNULL(person_by_publication(p.id),convert(concat(p.corresponding_author, ', ', p.other_author) using utf8)) as author,p.title,p.year,p.volume,p.pages,j.NF,j.name,j.issn,j.country,j.publisher_name,i.impact FROM person_has_publication php JOIN publication p on php.publication_id = p.id LEFT JOIN journal j on j.id = p.journal_id LEFT JOIN impact_factor i on i.journal_id = j.id and i.year = ".$ano_pasado." and i.impact > 0 and i.impact is not null WHERE php.person_id = ".$id." AND (j.type = 2 OR j.type = 3)";
	$res = mysqli_query($con,$query);
	$scielo = array();
  while($row = mysqli_fetch_array($res)){
  	array_push($scielo, $row["author"].", ".$row["title"].", ".$row["name"]." (ISSN: ".$row["issn"].", País: ".$row["country"].", Editorial: ".$row["publisher_name"]."), ".$row["volume"]." (".$row["year"].") ".$row["pages"].". (I.I.: ".$row["impact"]."  NF: ".$row["NF"].")");	
	}
	$json_out["scielo"] = $scielo;
	
	//~ PUBLICACIONES OTRAS
	$query = "SELECT IFNULL(person_by_publication(p.id),convert(concat(p.corresponding_author, ', ', p.other_author) using utf8)) as author,p.title,p.year,p.volume,p.pages,j.NF,j.name,j.issn,j.country,j.publisher_name,i.impact FROM person_has_publication php JOIN publication p on php.publication_id = p.id LEFT JOIN journal j on j.id = p.journal_id LEFT JOIN impact_factor i on i.journal_id = j.id and i.year = ".$ano_pasado." and i.impact > 0 and i.impact is not null WHERE php.person_id = ".$id." AND (j.type = 0 OR j.type IS NULL)";
	$res = mysqli_query($con,$query);
	$otras = array();
  while($row = mysqli_fetch_array($res)){
  	array_push($otras, $row["author"].", ".$row["title"].", ".$row["name"]." (ISSN: ".$row["issn"].", País: ".$row["country"].", Editorial: ".$row["publisher_name"]."), ".$row["volume"]." (".$row["year"].") ".$row["pages"].". (I.I.: ".$row["impact"]."  NF: ".$row["NF"].")");	
	}
	$json_out["otras"] = $otras;
		
	//~ TESIS MAGISTER
	$query = "SELECT t.title, t.student_name,t.year,t.institution_id FROM person_has_thesis as pht JOIN thesis as t ON pht.thesis_id = t.id WHERE t.type = 'magister' and pht.person_id = ".$id;
	$res = mysqli_query($con,$query);
	$magister = array();
  while($row = mysqli_fetch_array($res)){
  	array_push($magister, $row["title"].", ".$row["student_name"].", ".$row["year"].", ".$row["institution"]);
	}
	$json_out["magister"] = $magister;
	
	//~ TESIS DOCTORADO
	$query = "SELECT t.title, t.student_name,t.year,t.institution_id FROM person_has_thesis as pht JOIN thesis as t ON pht.thesis_id = t.id WHERE t.type = 'doctorado' and pht.person_id = ".$id;
	$res = mysqli_query($con,$query);
	$doctorado = array();
  while($row = mysqli_fetch_array($res)){
  	array_push($doctorado, $row["title"].", ".$row["student_name"].", ".$row["year"].", ".$row["institution"]);  	
	}
	$json_out["doctorado"] = $doctorado;
	
	//~ TESIS PROGRAMA
	$query = "SELECT t.title, t.student_name,t.year,t.institution_id FROM person_has_thesis as pht JOIN thesis as t ON pht.thesis_id = t.id WHERE t.program_id = 4 and t.type = 'doctorado' and pht.person_id = ".$id;
	$res = mysqli_query($con,$query);
	$programa = array();
  while($row = mysqli_fetch_array($res)){
  	array_push($programa, $row["title"].", ".$row["student_name"].", ".$row["year"].", ".$row["institution"]);
	}
	$json_out["programa"] = $programa;

	//~ PROYECTOS
	$query = "SELECT  p.number, p.type, p.subtype, p.participation, p.title, p.year, p.start, p.end FROM person_has_project as php, project as p WHERE php.person_id = ".$id." and p.id = php.project_id";
	$res = mysqli_query($con,$query);
	$proyecto = array();
  while($row = mysqli_fetch_array($res)){
  	array_push($proyecto, $row["type"]." N° ".$row["number"].". ".$row["year"].". ".$row["subtype"].", ".$row["title"].", ".$row["start"]." - ".$row["end"].". ".$row["participation"]);
	}
	$json_out["proyecto"] = $proyecto;

	$retorno = utf8_encode((string)json_encode($json_out));


	echo $retorno;
	
?>
