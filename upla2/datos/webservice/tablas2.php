<?
header('Content-Type: text/html; charset=UTF-8');
$_mysql = "localhost;root;complexity*-*;usm";
	$mysql_input = explode(";",$_mysql);
	$con = mysqli_connect($mysql_input[0],$mysql_input[1],$mysql_input[2],$mysql_input[3]);
	$id = $_GET["id"];
	//~ NOMBRE
	$query = "SELECT name,last_name,m_name FROM person WHERE id = ".$id."";
	$res = mysqli_query($con,$query);
    echo "<div style='display:table-row'><div style='display:table-cell'>Nombre</div><div style='display:table-row'>";
	while($row = mysqli_fetch_array($res)){
		echo $row["name"]." ".$row["last_name"]." ".$row["m_name"]."<br>";
	}
	echo '</div></div>';
	//~ PARTICIPACION
	$query = "SELECT participation FROM person_has_program WHERE person_id = ".$id." ";
	$res = mysqli_query($con,$query);
    echo "<div style='display:table-row'><div style='display:table-cell'>Participacion</div><div style='display:table-row'>";
	while($row = mysqli_fetch_array($res)){
		echo $row["participation"]."<br>";
	}
	echo '</div></div>';
	
	//~ AREA
	$query = "SELECT area FROM person WHERE id= ".$id." ";
	$res = mysqli_query($con,$query);
    echo "<div style='display:table-row'><div style='display:table-cell'>Area</div><div style='display:table-row'>";
	while($row = mysqli_fetch_array($res)){
		echo $row["area"]."<br>";
	}
	echo '</div></div>';
	
	//~ Title
	$query = "SELECT d.name,d.university,d.country FROM degree as d, person_has_degree as phd WHERE phd.person_id = ".$id." and phd.degree_id = d.id and is_degree = 0";
	$res = mysqli_query($con,$query);
    echo "<div style='display:table-row'><div style='display:table-cell'>Titulo</div><div style='display:table-row'>";
	while($row = mysqli_fetch_array($res)){
		echo $row["name"]." ".$row["university"]." ".$row["country"]."<br>";
	}
	echo '</div></div>';
	
	//~ Grado
	$query = "SELECT d.name,d.university,d.country FROM degree as d, person_has_degree as phd WHERE phd.person_id = ".$id." and phd.degree_id = d.id and is_degree = 1";
	$res = mysqli_query($con,$query);
    echo "<div style='display:table-row'><div style='display:table-cell'>Grado</div><div style='display:table-row'>";
	while($row = mysqli_fetch_array($res)){
		echo $row["name"]." ".$row["university"]." ".$row["country"]."<br>";
	}
	echo '</div></div>';
	
	//~ PROYECTOS
	$query = "SELECT  p.number, p.type, p.subtype, p.participation, p.title, p.year, p.start, p.end FROM person_has_project as php, project as p WHERE php.person_id = ".$id." and p.id = php.project_id";
	$res = mysqli_query($con,$query);
    echo "<div style='display:table-row'><div style='display:table-cell'>Proyectos</div><div style='display:table-row'>";
    while($row = mysqli_fetch_array($res)){
		echo $row["number"]." ".$row["type"]." ".$row["subtype"]." ".$row["participation"]." ".$row["title"]." ".$row["year"]." ".$row["start"]." ".$row["end"]."<br>";
	}
	echo '</div></div>';
	
	//~ PUBLICACIONES
	$query = "SELECT p.corresponding_author,p.other_author,p.title,p.year,p.volume,j.NF,j.name,j.issn,j.country,j.publisher_name,fi.impact FROM person_has_publication as php, publication as p, journal as j, impact_factor as fi WHERE php.person_id = ".$id." and p.id = php.publication_id and j.id = p.journal_id and fi.journal_id = j.id and fi.year = p.year and p.indexation IN ('ISI','ISI/SciELO') ";
	$res = mysqli_query($con,$query);
    echo "<div style='display:table-row'><div style='display:table-cell'>Articulos ISI</div><div style='display:table-row'>";
    while($row = mysqli_fetch_array($res)){
		echo $row["corresponding_author"]." ".$row["other_author"]." ".$row["title"]." ".$row["year"]." ".$row["volume"]." ".$row["NF"]." ".$row["name"]." ".$row["issn"]." ".$row["country"]." ".$row["publisher_name"]." ".$row["impact"]."<br>";
	}
	echo '</div></div>';
	
	//~ PUBLICACIONES SCIELO
	$query = "SELECT p.corresponding_author,p.other_author,p.title,p.year,p.volume,j.NF,j.name,j.issn,j.country,j.publisher_name,fi.impact FROM person_has_publication as php, publication as p, journal as j, impact_factor as fi WHERE php.person_id = ".$id." and p.id = php.publication_id and j.id = p.journal_id and fi.journal_id = j.id and fi.year = p.year and p.indexation IN ('SciELO','ISI/SciELO')";
	$res = mysqli_query($con,$query);
    echo "<div style='display:table-row'><div style='display:table-cell'>Articulos SciELO</div><div style='display:table-row'>";
    while($row = mysqli_fetch_array($res)){
		echo $row["corresponding_author"]." ".$row["other_author"]." ".$row["title"]." ".$row["year"]." ".$row["volume"]." ".$row["NF"]." ".$row["name"]." ".$row["issn"]." ".$row["country"]." ".$row["publisher_name"]." ".$row["impact"]."<br>";
	}
	echo '</div></div>';
	
	//~ PUBLICACIONES OTRAS
	$query = "SELECT p.corresponding_author,p.other_author,p.title,p.year,p.volume,j.NF,j.name,j.issn,j.country,j.publisher_name,fi.impact FROM person_has_publication as php, publication as p, journal as j, impact_factor as fi WHERE php.person_id = ".$id." and p.id = php.publication_id and j.id = p.journal_id and fi.journal_id = j.id and fi.year = p.year and p.type in ('Libro','Capítulo de libro')";
	$res = mysqli_query($con,$query);
    echo "<div style='display:table-row'><div style='display:table-cell'>Libros/Capitulos</div><div style='display:table-row'>";
    while($row = mysqli_fetch_array($res)){
		echo $row["corresponding_author"]." ".$row["other_author"]." ".$row["title"]." ".$row["year"]." ".$row["volume"]." ".$row["NF"]." ".$row["name"]." ".$row["issn"]." ".$row["country"]." ".$row["publisher_name"]." ".$row["impact"]."<br>";
	}
	echo '</div></div>';
	
	//~ PROYECTOS
	$query = "SELECT  p.number, p.type, p.subtype, p.participation, p.title, p.year, p.start, p.end FROM person_has_project as php, project as p WHERE php.person_id = ".$id." and p.id = php.project_id";
	$res = mysqli_query($con,$query);
    echo "<div style='display:table-row'><div style='display:table-cell'>Proyectos</div><div style='display:table-row'>";
    while($row = mysqli_fetch_array($res)){
		echo $row["number"]." ".$row["type"]." ".$row["subtype"]." ".$row["participation"]." ".$row["title"]." ".$row["year"]." ".$row["start"]." ".$row["end"]."<br>";
	}
	echo '</div></div>';
	
	//~ TESIS MAGISTER
	$query = "SELECT t.title, t.student_name,t.year,t.institution_id FROM person_has_thesis as pht, thesis as t WHERE pht.thesis_id = t.id and t.type = 'Magister' and pht.person_id = ".$id."";
	$res = mysqli_query($con,$query);
    echo "<div style='display:table-row'><div style='display:table-cell'>Tesis Magister</div><div style='display:table-row'>";
    while($row = mysqli_fetch_array($res)){
		echo $row["title"]." ".$row["student_name"]." ".$row["year"]." ".$row["institution"]."<br>";
	}
	echo '</div></div>';
	
	//~ TESIS DOCTORADO
	$query = "SELECT t.title, t.student_name,t.year,t.institution_id FROM person_has_thesis as pht, thesis as t WHERE pht.thesis_id = t.id and t.type = 'Magister' and pht.person_id = ".$id."";
	$res = mysqli_query($con,$query);
    echo "<div style='display:table-row'><div style='display:table-cell'>Tesis Doctorado</div><div style='display:table-row'>";
    while($row = mysqli_fetch_array($res)){
		echo $row["title"]." ".$row["student_name"]." ".$row["year"]." ".$row["institution"]."<br>";
	}
	echo '</div></div>';
	
	//~ TESIS PROGRAMA
	$query = "SELECT t.title, t.student_name,t.year,t.institution_id FROM person_has_thesis as pht, thesis as t WHERE pht.thesis_id = t.id and  t.program_id = 4  and pht.person_id = ".$id."";
	$res = mysqli_query($con,$query);
    echo "<div style='display:table-row'><div style='display:table-cell'>Tesis Programa</div><div style='display:table-row'>";
    while($row = mysqli_fetch_array($res)){
		echo $row["title"]." ".$row["student_name"]." ".$row["year"]." ".$row["institution"]."<br>";
	}
	echo '</div></div>';
	
?>
