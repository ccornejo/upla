<?php
//Start session
session_start();
 
//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['sess_user_id']) || (trim($_SESSION['sess_user_id']) == '')) {
	header("location: login.html");
	exit();
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>USM - Sistema para la gestión y visualización de la productividad académica</title>
		<link rel="shortcut icon" href="http://usm.cl/assets/img/icon/favicon.ico">
		<link href="css/style.css" rel="stylesheet">
	</head>
	<body>
		<div class="page-header">
			<div class="cabecera">
				<a href="http://usm.cl/"><img src="img/header-usm.png" class="usm" alt=""></a>
				
				<div id="logout"><a href="http://190.162.227.177/usm2/logout.php">Cerrar Sesion</a></div>
				<div id="usermanual"><a target="_blank" href="archivos/manual.pdf">Manual de Usuario</a> | </div>
			</div>
			
		</div>
		<div class="main">
			<div class="img">
				<img src="img/menu.png" width="1000px"/>
				<div id="data2"><div></div></div>
				<div id="data"><div></div></div>
				<div id="graph"><div></div></div>
				<div id="record"><div></div></div>
				<div id="query"><div></div></div>
				<div id="usm"><div></div></div>
			</div>
			<div style="left:0;right:0;margin:auto;width:1000px;font-size:11px;color:#666;text-align:right;">Desarrollado por <a target="_blank" style="text-decoration:none;color:#1775A9;" href="http://www.net-works.cl">Networks</a></div>
		</div>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script>
		$(document).ready(function(){
			$("#logout a").css("text-decoration","none")
			$("#logout a").css("font-weight","normal")
			$("#logout a").mouseover(function(){return $("#logout a").css("text-decoration","underline !important")})
			$("#usm").click(function(){
				window.location.href = "http://www.dgip.usm.cl/"
			})
			$("#data").click(function(){
				window.location.href = "edicion/persona.php"
			})
			$("#graph").click(function(){
				window.location.href = "visualizacion/1"
			})
			$("#record").click(function(){
				window.location.href = "datos/ficha.php"
			})
			$("#query").click(function(){
				window.location.href = "datos/consulta.php"
			})
			$("#usm").mouseover(function(){
				$("#data2 div").text("Datos")
				$("#data div").text("Ingresar Datos")
				$("#record div").text("Fichas Docentes")
				$("#graph div").text("Visualizar Datos")
				$("#query div").text("Consultar Datos")
			})
			$("#data").mouseover(function(){
				$("#data2 div").text("Datos")
				$("#data div").text("Ingresar Datos")
			})
			
			$("#query").mouseover(function(){
				$("#data2 div").text("Datos")
				$("#query div").text("Consultar Datos")
			})
			
			$("#data2").mouseover(function(){
				$("#data2 div").text("Datos")
				$("#data div").text("Ingresar Datos")
				$("#record div").text("Fichas Docentes")
				$("#query div").text("Consultar Datos")
			})
			$("#graph").mouseover(function(){
				$("#graph div").text("Visualizar Datos")
			})
			$("#record").mouseover(function(){
				$("#data2 div").text("Datos")
				$("#record div").text("Fichas Docentes")
			})
			
			$("#usm").mouseout(function(){
				$("#data2 div").text("")
				$("#data div").text("")
				$("#record div").text("")
				$("#graph div").text("")
				$("#query div").text("")
			})
			
			$("#data").mouseout(function(){
				$("#data2 div").text("")
				$("#data div").text("")
			})
			
			$("#query").mouseout(function(){
				$("#data2 div").text("")
				$("#query div").text("")
			})
			
			$("#graph").mouseout(function(){
				$("#graph div").text("")
			})
			$("#record").mouseout(function(){
				$("#data2 div").text("")
				$("#record div").text("")
			})
			$("#data2").mouseout(function(){
				$("#data2 div").text("")
				$("#data div").text("")
				$("#record div").text("")
				$("#query div").text("")
			})
		})
	</script>
	</body>
</html>
